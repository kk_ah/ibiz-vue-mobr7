<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>
    /**
     * 视图显示状态
     *
     * @type {boolean}
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public isShow = true;

    /**
     * 浏览器缓存
     *
     * @type {any}
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public  startview :any = {};

    /**
     * 是否不再显示
     *
     * @type {boolean}
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public isNaverShow :boolean = false;

    /**
     * 倒计时
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public countDown(){
        this.seconds--;
    }

    /**
     * 初始化seconds
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public seconds: any = 0;

    /**
     * 点击跳过事件
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public close(){
        this.isShow = false;
        this.$emit("close");
    }

    /**
     * 处理时间变化
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    @Watch('seconds')
    function(newVal:any,oldVal:any){
      if(newVal==0){
        this.close();
      }
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public mounted() {
        let startview = localStorage.getItem("startview");
        if (startview != null) {
            this.startview = JSON.parse(startview);
            let lasttime =  this.startview.time ;
            let now = new Date().getTime();
            if(!this.startview.isNaverShow && now - lasttime > 300000 ){
                this.startshow();
                this.setlocalStorage();
            }else{
                this.close()
            }  
        }else{
            this.startshow();
            this.setlocalStorage();
        }
    }

    /**
     * 开始计时
     *
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public startshow(){
        this.seconds=5;
        setInterval(this.countDown,1000);
    }
        
    /**
     * 设置浏览器缓存
     *
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public setlocalStorage(){
        let show =  {time:new Date().getTime(),isNaverShow:this.isNaverShow};
        localStorage.setItem("startview",JSON.stringify(show));
    }

    /**
     * 不再显示
     *
     * @memberof @memberof ${srfclassname('${view.name}')}
     */
    public neverShow() {
        this.isNaverShow = true;
        this.setlocalStorage();
        this.close();
    }

    
<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>



