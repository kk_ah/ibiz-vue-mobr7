## ibiz启动视图相关文档（移动端）

### 一、支持功能

#### 	1、计时（五秒钟自动退出）

##### 	实现思路：通过计时器（默认5秒）来改变启动视图的显示

##### 	相关代码：

​			![jishi_code](./imgs/jishi_code.png)

#### 	2、跳过（直接退出）

#####	实现思路：通过按钮触发close事件

##### 	相关代码：

​			![tg_code](./imgs/tg_code.png)				

#### 	3、不在显示（以后都不会显示启动页 ps：不清除缓存的情况）

##### 			实现思路：通过按钮触发neverShow 事件 将参数保存进浏览器

##### 					相关代码：

​			![bzxs_code](./imgs/bzxs_code.png)	

### 二、界面UI

​			![startview](./imgs/startview.png)	