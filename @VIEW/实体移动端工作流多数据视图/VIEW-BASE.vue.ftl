<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}
     */
    public isSingleSelect: boolean = false;

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>