<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>
    /**
     * 视图选中数据
     *
     * @type {any[]}
     * @memberof ${srfclassname('${view.name}')}
     */
    public viewSelections: any[] = [];

    /**
     * 是否显示按钮
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}
     */
    @Prop({default: true}) public isShowButtons!: boolean;

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}
     */
    public isSingleSelect: boolean = false;

    /**
     * 确定
     *
     * @memberof ${srfclassname('${view.name}')}
     */
    public onClickOk(): void {
        this.viewDatasChange(this.viewSelections);
        this.$emit('close', this.viewSelections);
    }

    /**
     * 取消
     *
     * @memberof ${srfclassname('${view.name}')}
     */
    public onClickCancel(): void {
        this.$emit('close', null);
    }

    /**
     * 快速搜索值
     *
     * @memberof ${srfclassname('${view.name}')}
     */
    public quickValue = "";

    /**
     * 快速搜索
     *
     * @memberof ${srfclassname('${view.name}')}
     */
    public async quickValueChange(event: any) {
        const pickupviewpanel: any = this.$refs.pickupviewpanel;
        if (pickupviewpanel) {
            let response = await pickupviewpanel.quickSearch(this.quickValue);
            if (response) {
                
            }
        }
    }

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>