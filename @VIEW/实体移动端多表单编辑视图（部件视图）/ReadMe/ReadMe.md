### 多表单编辑视图PC端

**应用场景**：表单嵌入关系界面时，视图类型为多表单编辑视图。

**视图结构：**

主编辑视图（editview）

=》主表单（form）

=》关系界面组件（app-form-druipart）

=》多表单编辑视图（meditview9）

=》多表单编辑视图面板（multieditviewpanel）

=>编辑视图部件视图editview9（从实体  编辑视图部件视图）

=>从实体表单

##### 视图、部件各层之间关联信息：

主编辑视图 (editview)

=》viewState=》主表单(form)   

=》formState=》关系组件（app-form-druipart）

=》formDruipart =》对应视图（此处是多表单编辑视图部件视图meditview9）

=》viewState=》多编辑面板（multieditviewpanel）

=》panelState=》编辑视图部件视图（editview9）	

=》viewState =》子表单(form)

##### 视图逻辑：

加载（load）：  

- 主表单(form)   load （GetTempMajor）  = >    部件服务   get   =>   基类服务   GetTempMajor接口   =》 实体服务 get 方法

![1584356116905](images/多表单编辑视图_PC/1584356116905.png)

此时关系界面的数据已经存储到localstorage中；

- 多表单面板加载：

  掉部件服务get(action:FetchTempDefault) =>  实体服务FetchTempDefault方法 ==》根据srfsessionid等信息取localstorage中关系界面数据

  ![1584357318896](images/多表单编辑视图_PC/1584357318896.png)

- 子表单加载

  调用GetTemp接口

  ![1584357875568](images/多表单编辑视图_PC/1584357875568.png)

![1584357946633](images/多表单编辑视图_PC/1584357946633.png)

1.主表单加载数据传递**父页面参数**给关系界面组件（app-form-druipart），传递的数据主要包含**主表单主键信息，关系信息**，将关系参数整合在viewdata中传递给多表单编辑视图部件视图。

2.多表单编辑视图部件视图通过**viewState**下发通知多编辑面板加载数据，多编辑面板通过父参数加载对应关系视图数据集合。

3.多编辑面板加载成功的数据集合对视图进行遍历，通过viewdata传递给对应的编辑视图部件视图。

4.编辑视图部件视图初始化加载引擎，整合父传递的viewdata到视图参数context中，通知子表单加载对应数据。

保存（save）：

![](images/多表单编辑视图_PC/save.png)

1.主表单传递**formState**对象给关系界面组件，主表单保存前formState会下发beforesave通知关系界面组件保存，关系界面组件通过formDruipart通知多表单编辑视图部件视图保存，多表单视图通过viewState通知给面板部件保存，面板部件通过传递panelState对象通知编辑视图部件视图保存，编辑视图部件视图再通过viewState通知子表单保存。

2.子表单保存成功后，抛出save事件给编辑视图部件视图，通过引擎继续向上抛出viewdataschange事件给多表单面板，等待所有子表单全部保存成功后再向上抛出drdatasaved事件，在多编辑视图中继续向上抛出drdatasaved事件，一直到主表单捕捉到drdatasaved事件，再调用主表单保存事件保存主表单。

- 子表单保存：save方法调用UpdateTemp实体服务接口，更新localstorage中对应条数据

  ![1584358357102](images/多表单编辑视图_PC/1584358357102.png)

- 主表单保存：save调用UpdateTempMajor接口，调用实体服务Update接口，从localstorage中取出关系视图的数据

  ![1584358788091](images/多表单编辑视图_PC/1584358788091.png)

删除（remove）：

![](images/多表单编辑视图_PC/remove.png)

编辑视图部件视图通过viewState 通知子表单删除，子表单删除成功后向上抛出remove事件，编辑视图部件视图向上抛出viewdataschange事件，多表单面板将此条数据删除。







