<#assign self_content>
    /**
    * 界面关系通讯对象
    *
    * @type {Subject<ViewState>}
    * @memberof IBZSAM02MobMEditView9
    */
    @Prop() public formDruipart!: Subject<ViewState>;

    <#if  view.hasPSControl('lefttoolbar')>
        <#assign lefttoolbar = view.getPSControl('lefttoolbar')>
        <@ibizindent blank=8>
            ${P.getCtrlCode(lefttoolbar,'CONTROL.ts').code}
        </@ibizindent>
    </#if>

    <#if  view.hasPSControl('righttoolbar')>
        <#assign righttoolbar = view.getPSControl('righttoolbar')>
        <@ibizindent blank=8>
            ${P.getCtrlCode(righttoolbar,'CONTROL.ts').code}
        </@ibizindent>
    </#if>
</#assign>
<#assign mounted_block>
if(this.formDruipart){
            this.formDruipart.subscribe((res) =>{
                if(Object.is(res.action,'save')){
                    let opt ={data:res.data};
                    Object.assign(opt,this.context);
                    this.viewState.next({ tag:'meditviewpanel', action: 'save', data: opt });
                }
                if(Object.is(res.action,'remove')){
                    let opt ={data:res.data};
                    Object.assign(opt,this.context);
                    this.viewState.next({ tag:'meditviewpanel', action: 'remove', data: opt });
                }
            });
        }           
</#assign>
<#assign viewdata_block>
if(!Object.is(newVal, oldVal) ){
    _this.parseViewParam();
    this.viewState.next({ tag: 'meditviewpanel', action: 'load', data: this.context });            
}
</#assign>
<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>


<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 刷新数据参数
     *
     * @type {number}
     * @memberof ${srfclassname('${view.name}')}
     */
    @Prop() public saveRefView?: number;

    /**
     * 关系数据变化
     *
     * @param {*} $event
     * @memberof ${srfclassname('${view.name}')}
     */
    public onViewDataDirty($event: any) {
        this.$emit('drdatachange', $event);
    }

    /**
     * 关系数据保存执行完成
     *
     * @param {*} $event
     * @memberof ${srfclassname('${view.name}')}
     */
    public onDRDataSaved($event: any) {
        this.$emit('drdatasaved', $event);
    }


<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>