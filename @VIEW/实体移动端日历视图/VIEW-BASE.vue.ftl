<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 多选状态改变事件
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public showCheackChange(value:any){
        this.showCheack = value;
    }

    /**
     * 多选状态
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public showCheack = false;

    /**
     * 取消选择状态
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public cancelSelect() {
        this.showCheackChange(false);
    }

    <#if view.hasPSControl('calendar')>
    <#assign calendar = view.getPSControl('calendar')>
    <#if calendar.getBatchPSDEToolbar?? && calendar.getBatchPSDEToolbar()??>
    <#assign batchToolbar = calendar.getBatchPSDEToolbar()>
    public <#if batchToolbar.getName()??> ${batchToolbar.getName()}_click</#if>(tag:string,event:any) {
       let calendar: any = this.$refs.calendar;
        if(calendar && calendar.<#if batchToolbar.getName()??> ${batchToolbar.getName()}_click</#if> instanceof Function){
            calendar.<#if batchToolbar.getName()??> ${batchToolbar.getName()}_click</#if>(tag,event);
        }
    }
    </#if>
    </#if>

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>