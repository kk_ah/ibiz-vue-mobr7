<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>
<#if view.hasPSControl('tabexppanel')>
<#assign tabexppanel = view.getPSControl('tabexppanel')>

    /**
     * 被激活的分页面板
     *
     * @type {string}
     * @memberof  ${srfclassname('${view.name}')}Base
     */
    protected activiedTabViewPanel: string = '<#list tabexppanel.getPSControls() as tabviewpanel><#if tabviewpanel_index==0>${tabviewpanel.name}</#if></#list>';

    /**
     * 分页导航栏激活
     *
     * @param {*} $event
     * @returns {void}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public tabExpPanelChange($event: any): void {
        let { detail } = $event;
        if (!detail) {
            return;
        }
        let { value } = detail;
        if (!value) {
            return;
        }
        this.viewState.next({ tag: '${tabexppanel.name}', action: 'active', data: { activeItem: value } });
    }
</#if>
<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>