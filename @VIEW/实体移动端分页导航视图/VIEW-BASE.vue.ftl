<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>
<#if view.hasPSControl('tabexppanel')>
<#assign tabexppanel = view.getPSControl('tabexppanel')>
<#assign dename = tabexppanel.getPSAppDataEntity().getName()?lower_case>
<#assign created_block>
this.getLocalStorage();
</#assign>
    /**
     * 被激活的分页面板
     *
     * @type {string}
     * @memberof  ${srfclassname('${view.name}')}Base
     */
    protected activiedTabViewPanel: string = '<#list tabexppanel.getPSControls() as tabviewpanel><#if tabviewpanel_index==0>${tabviewpanel.name}</#if></#list>';

    /**
     * 分页导航栏激活
     *
     * @param {*} $event
     * @returns {void}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public tabExpPanelChange($event: any): void {
        let { detail } = $event;
        if (!detail) {
            return;
        }
        let { value } = detail;
        if (!value) {
            return;
        }
        this.viewState.next({ tag: '${tabexppanel.name}', action: 'active', data: { activeItem: value } });
        this.setLocalStorage(value);     
    }

    /**
     * 子传父修改激活的Panel
     *
     * @param {*} $event
     * @returns {void}
     * @memberof MOBORDERMobTabExpViewBase
     */
    protected changePanel(res:any) : void {
      this.activiedTabViewPanel = res;
    }
    /**
     * localStorage存值
     *
     * @param {*} $event
     * @returns {void}
     * @memberof MOBORDERMobTabExpViewBase
     */    
    public setLocalStorage(value:any) {
        let name:string = '${dename}';
        let id:any = this.context.${dename};
        let obj:any = {"name":name,"id":id,"value":value};
        localStorage.setItem('tabKey',JSON.stringify(obj));    
    }
    /**
     * localStorage取值
     *
     * @param {*} $event
     * @returns {void}
     * @memberof MOBORDERMobTabExpViewBase
     */
    public getLocalStorage() {
        let key:any = localStorage.getItem('tabKey')
        if(key){
        let info:any = JSON.parse(key);
        if (info.name == '${dename}') {
          if (info.id == this.context.${dename}) {
            this.activiedTabViewPanel = info.value;
          }
        } else { 
          this.activiedTabViewPanel = '<#list tabexppanel.getPSControls() as tabviewpanel><#if tabviewpanel_index==0>${tabviewpanel.name}</#if></#list>';
        }
        }
    }

</#if>
<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>