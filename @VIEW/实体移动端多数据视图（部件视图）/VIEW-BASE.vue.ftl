<#--  视图独立定义的内容  -->
<#assign self_content>

    /**
     *  app-form-druipart 组件订阅对象
     *
     * @type {Subject}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop() public formDruipart !: Subject<ViewState>;
</#assign>
<#--  附加生命周期内容  -->
<#assign created_block>
        if (this.formDruipart) {
            this.formDruipart.subscribe(($event: any) => {
                if (Object.is($event.action, 'load')) {
                    let opt = { data: $event.data }
                    Object.assign(opt, this.context);
                    this.viewState.next({ tag: 'mdctrl', action: 'load', data: {} })
                }
            });
        }
</#assign>
<#assign destroyed_block>
        if (this.formDruipart) {
            this.formDruipart.unsubscribe();
        }
</#assign>
<#ibizinclude>
../@MACRO/MOBMDVIEW.vue.ftl
</#ibizinclude>