<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 选中数据
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public selection: any = {};

    /**
     * 表单数据
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public formData:any = {};

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>