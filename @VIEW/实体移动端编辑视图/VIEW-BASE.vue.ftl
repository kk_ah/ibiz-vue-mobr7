<#assign created_block>
        if(this.$route && this.$route.query && this.$route.query.bsinfo){
            const bainfo:any = JSON.parse(this.$route.query.bsinfo as string);
            Object.assign(this.viewparams,bainfo);
        }
</#assign>
<#assign userCloseView>
        let result = await this.cheackChange();
        if(result){
            if (this.viewDefaultUsage === "routerView") {
                this.$store.commit("deletePage", this.$route.fullPath);
                this.$router.go(-1);
            } else {
                this.$emit("close", { status: "success", action: "close", data: args instanceof MouseEvent ? null : args });
            }
        }
</#assign>

<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 保存
     *
     * @protected
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected defSave(): void {
        const _this: any = this;
        <#if view.hasPSControl('form')>
        <#assign form = view.getPSControl('form')> 
        const xData: any = _this.$refs.${form.name};
        </#if>
        if (xData && xData.save instanceof Function) {
            const _data = _this.viewparams;
            xData.save(_data).then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                _this.$emit('viewdataschange', [{ ...response.data }]);
            });
        } else if (_this.save && _this.save instanceof Function) {
            _this.save();
        }
    }

    /**
     * 检查表单是否修改
     *
     * @param {any[]} args
     * @memberof PimEducationMobEditViewBase
     */
    public async cheackChange(): Promise<any>{
        const view = this.$store.getters['viewaction/getAppView'](this.viewtag);
        if (view && view.viewdatachange) {
                const title: any = this.$t('app.tabpage.sureclosetip.title');
                const contant: any = this.$t('app.tabpage.sureclosetip.content');
                const result = await this.$notice.confirm(title, contant);
                if (result) {
                    this.$store.commit('viewaction/setViewDataChange', { viewtag: this.viewtag, viewdatachange: false });
                    return true;
                } else {
                    return false;
                }
        }else{
            return true;
        }
    }
<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>