<#--  视图独立定义的内容  -->
<#assign self_content>

    /**
     * 面板定于对象
     *
     * @type {Subject<ViewState>}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop() public panelState ?:Subject<ViewState>;
</#assign>
<#--  附加生命周期内容  -->
<#assign created_block>
        if (this.panelState) {
            this.panelState.subscribe((res: any) => {
                if (Object.is(res.tag, 'meditviewpanel')) {
                    if (Object.is(res.action, 'save')) {
                        this.viewState.next({ tag: 'form', action: 'save', data: res.data });
                    }
                    if (Object.is(res.action, 'remove')) {
                        this.viewState.next({ tag: 'form', action: 'remove', data: res.data });
                    }
                }
            });
        }
</#assign>
<#assign destroyed_block>
        if (this.panelState) {
            this.panelState.unsubscribe();
        }
</#assign>
<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>