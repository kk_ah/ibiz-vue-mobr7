<#ibizinclude>
./WF_VIEW_CONTENT.vue.ftl
</#ibizinclude>

<#if view.getPSAppViewEngines()??>
    <#list view.getPSAppViewEngines() as engine>
    /**
     * 视图引擎
     *
     * @type {Engine}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected ${engine.getName()?lower_case}: ${engine.getEngineType()}Engine = new ${engine.getEngineType()}Engine();
    </#list>
</#if>

    /**
     * 引擎初始化
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected engineInit(): void {
<#if view.getPSAppViewEngines()??>
    <#list view.getPSAppViewEngines() as engine>
        this.${engine.getName()?lower_case}.init({
            view: this,
        <#if engine.getPSAppViewEngineParams()??>
            <#list engine.getPSAppViewEngineParams() as param>
                <#if param.getParamType() == "LOGIC" && param.getPSAppViewLogic()??>
            ${param.getName()?lower_case}: (args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string) => {
                this.${param.getPSAppViewLogic().getName()}(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
            },
                </#if>
                <#if param.getParamType() == "CTRL" && param.getPSControl()??>
            ${param.getName()?lower_case}: this.$refs.${param.getPSControl().getName()},
                </#if>
                <#if param.getParamType() == "VALUE" && param.getValue()??>
            ${param.getName()?lower_case}: '${param.getValue()?c}',
                </#if>
            </#list>
        </#if>
        <#if de??>
            keyPSDEField: '${appde.getCodeName()?lower_case}',
            <#if appde.getMajorPSAppDEField()??>
            majorPSDEField: '${appde.getMajorPSAppDEField().getCodeName()?lower_case}',
            </#if>
        </#if>
            isLoadDefault: <#if view.isLoadDefault?? && !view.isLoadDefault()>false<#else>true</#if>,
        });
    </#list>
</#if>
    }

    /**
     * Vue声明周期
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected created() {
        this.afterCreated();
    }

    /**
     * Vue声明周期
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public activated() {
        this.afterMounted();
    }

    /**
     * 执行created后的逻辑
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */    
    protected afterCreated(){
        const secondtag = this.$util.createUUID();
        this.$store.commit('viewaction/createdView', { viewtag: this.viewtag, secondtag: secondtag });
        this.viewtag = secondtag;
        this.parseViewParam();
<#if created_block??>${created_block}</#if>
    }

    /**
     * 销毁之前
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected beforeDestroy() {
        this.$store.commit('viewaction/removeView', this.viewtag);
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected mounted() {
        this.afterMounted();
    }

    /**
     * 执行mounted后的逻辑
     * 
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected afterMounted(){
        const _this: any = this;
        <#if view.getPSAppViewEngines()??>
        _this.engineInit();
        </#if>
        if (_this.loadModel && _this.loadModel instanceof Function) {
            _this.loadModel();
        }
<#if mounted_block??>${mounted_block}</#if>
    }

    /**
     * 销毁视图回调
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected destroyed(){
        this.afterDestroyed();
    }

    /**
     * 执行destroyed后的逻辑
     * 
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected afterDestroyed(){
<#if view.isPSDEView()>
    <#if view.getPSAppDataEntity()??>
    <#assign appDataEntity = view.getPSAppDataEntity() />
        <#if appDataEntity.isMajor()>
        if (this.viewDefaultUsage !== "indexView" && Object.keys(localStorage).length > 0) {
            Object.keys(localStorage).forEach((item: string) => {
                if (item.startsWith(this.context.srfsessionid)) {
                    localStorage.removeItem(item);
                }
            });
        }
        </#if>
    </#if>
</#if>
<#if destroyed_block??>${destroyed_block}</#if>
    }
<#--  部件事件  -->
<#if view.getPSControls()??>
    <#list view.getPSControls() as ctrl>
        <#if ctrl.getControlType()??>
            <#if ctrl.getHookEventNames()??>
                <#list ctrl.getHookEventNames() as eventName>

    /**
     * ${ctrl.name} 部件 ${eventName?lower_case} 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected ${ctrl.name}_${eventName?lower_case}($event: any, $event2?: any) {
                    <#if ctrl.getPSControlLogics(eventName)??>
                        <#list ctrl.getPSControlLogics(eventName) as ctrlLogic>
                            <#if ctrlLogic.getLogicType() == "APPVIEWENGINE" && ctrlLogic.getPSAppViewEngine()??>
        this.${ctrlLogic.getPSAppViewEngine().getName()}.onCtrlEvent('${ctrl.name}', '${eventName?lower_case}', $event);
                            <#else>
                                <#if ctrlLogic.getEventArg()?? && ctrlLogic.getEventArg()?length gt 0>
        if (Object.is($event.tag, '${ctrlLogic.getEventArg()}')) {
            this.${ctrlLogic.name}($event, '', $event2);
        }
                                <#else>
        this.${ctrlLogic.name}($event, '', $event2);
                                </#if>
                            </#if>
                        </#list>
                    </#if>
    }
                </#list>
            </#if>
        </#if>
    </#list>
</#if>

<#if view.getPSAppViewLogics()??>
    <#list view.getPSAppViewLogics() as logic>
        <#if logic.getLogicTrigger() == "CUSTOM" || logic.getLogicTrigger() == "CTRLEVENT">

${P.getLogicCode(logic, "LOGIC.vue").code}
        </#if>
    </#list>
</#if>

    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected async closeView(args: any[]): Promise<any> {
        <#if userCloseView??>
        ${userCloseView}
        <#else>
        if (this.viewDefaultUsage === "routerView" ) {
            this.$store.commit("deletePage", this.$route.fullPath);
            this.$router.go(-1);
        } else {
            this.$emit("close", { status: "success", action: "close", data: args instanceof MouseEvent ? null : args });
        }
        </#if>
        
    }

    /**
     * 刷新数据
     *
     * @readonly
     * @type {(number | null)}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get refreshdata(): number | null {
        return this.$store.getters['viewaction/getRefreshData'](this.viewtag);
    }

    /**
     * 监控数据变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @returns
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Watch('refreshdata')
    onRefreshData(newVal: any, oldVal: any) {
        if (newVal === null || newVal === undefined) {
            return;
        }
        if (newVal === 0) {
            return;
        }
        const _this: any = this;
        if (_this.onRefreshView && _this.onRefreshView instanceof Function) {
            _this.onRefreshView();
        }
    }

<#--  视图独立定义的内容start  -->
<#if self_content??>
${self_content}
</#if>
<#--  视图独立定义的内容end  -->