<script lang='ts'>
import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { Subject } from 'rxjs';
import GlobalUiService from '@/global-ui-service/global-ui-service';
<#if appde??>
import ${srfclassname('${appde.getCodeName()}')}Service from '@/app-core/service/${srffilepath2(appde.getCodeName())}/${srffilepath2(appde.getCodeName())}-service';
</#if>

<#if view.getPSAppViewEngines()??>
    <#list view.getPSAppViewEngines() as engine>
import ${engine.getEngineType()}Engine from '@engine/${engine.getEngineCat()?lower_case}/${srffilepath2(engine.getEngineType())}-engine';
    </#list>
</#if>

<#if import_block??>
${import_block}
</#if>

@Component({
    components: {
    },
})
export default class ${srfclassname('${view.name}')}Base extends Vue {

    /**
     * 全局 ui 服务
     *
     * @type {GlobalUiService}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected globaluiservice: GlobalUiService = new GlobalUiService();
<#if appde??>

    /**
     * 实体服务对象
     *
     * @type {${srfclassname('${appde.getCodeName()}')}Service}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected appEntityService: ${srfclassname('${appde.getCodeName()}')}Service = new ${srfclassname('${appde.getCodeName()}')}Service();
</#if>

    /**
     * 数据变化
     *
     * @param {*} val
     * @returns {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Emit() 
    protected viewDatasChange(val: any):any {
        return val;
    }

    /**
     * 视图上下文
     *
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop() protected _context!: string;

    /**
     * 视图参数
     *
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop() protected _viewparams!: string;

    /**
     * 视图默认使用
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop({ default: "routerView" }) protected viewDefaultUsage!: string;

	/**
	 * 视图标识
	 *
	 * @type {string}
	 * @memberof ${srfclassname('${view.name}')}Base
	 */
	protected viewtag: string = '${view.getId()}';

    /**
     * 视图上下文
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected context: any = {};

    /**
     * 视图参数
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected viewparams: any = {};

    /**
     * 视图导航上下文
     *
     * @protected
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected navContext: any = {<#if view.getPSAppViewNavContexts?? && view.getPSAppViewNavContexts()??> <#list view.getPSAppViewNavContexts() as context>'${context.getKey()?lower_case}': '<#if context.isRawValue()>${context.getValue()}<#else>%${context.getValue()}%</#if>'<#if context_has_next>, </#if></#list> </#if>};

    /**
     * 视图导航参数
     *
     * @protected
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected navParam: any = {<#if view.getPSAppViewNavParams?? && view.getPSAppViewNavParams()??> <#list view.getPSAppViewNavParams() as param>'${param.getKey()?lower_case}': '<#if param.isRawValue()>${param.getValue()}<#else>%${param.getValue()}%</#if>'<#if param_has_next>, </#if></#list> </#if>};

    /**
     * 视图模型数据
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected model: any = {
        srfTitle: '<#if view.getTitle()??>${view.getTitle()}</#if>',
        srfCaption: '<#if view.getPSAppDataEntity()??>${view.getPSAppDataEntity().getCodeName()?lower_case}.views.${view.getPSDEViewCodeName()?lower_case}.caption<#else>app.views.${view.getCodeName()?lower_case}.caption</#if>',
        srfSubCaption: '<#if view.getSubCaption()??>${view.getSubCaption()}</#if>',
        dataInfo: '',
    <#if view.getPSSysImage?? && view.getPSSysImage()??>
        iconcls: '${view.getPSSysImage().getImagePath()}',
        icon: '${view.getPSSysImage().getCssClass()}'
    <#else>
        iconcls: '',
        icon: ''
    </#if>
    }

    /**
     * 视图上下文变化
     *
     * @param {string} newVal
     * @param {string} oldVal
     * @memberof  ${srfclassname('${view.name}')}Base
     */
    @Watch('_context')
    on_context(newVal: string, oldVal: string) {
        const _this: any = this;
        this.parseViewParam();
        if (!Object.is(newVal, oldVal) && _this.engine) {
            this.$nextTick(()=>{_this.engine.load();})
        }
        <#if _context_block??>${_context_block}</#if>
    }

    /**
     * 视图参数变化
     *
     * @param {string} newVal
     * @param {string} oldVal
     * @returns
     * @memberof IBizChart
     */
    @Watch('_viewparams')
    on_viewparams(newVal: string, oldVal: string) {
        this.parseViewParam();
    }

    /**
     * 容器模型
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected containerModel: any = {
<#if view.getPSControls()??>
    <#list view.getPSControls() as ctrl>
        view_${ctrl.name}: { name: '${ctrl.name}', type: '${ctrl.getControlType()}' },
    </#list>
</#if>
<#if view.isPickupView() || (view.getViewType?? && (view.getViewType() == 'DEOPTVIEW' || view.getViewType() == 'DEWFSTARTVIEW' || view.getViewType() == 'DEWFACTIONVIEW'))>
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
</#if>
<#if view.getPSWorkflow?? && view.getPSWorkflow()??>
        wflinks: [],
</#if>
    };

    /**
     * 视图状态订阅对象
     *
     * @type {Subject<{action: string, data: any}>}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected viewState: Subject<ViewState> = new Subject();


    /**
     * 是否显示标题
     *
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop({default:true}) protected showTitle?: boolean;
<#--  工具栏模型:BEGIN  -->
<#list view.getAllPSControls() as ctrl>

    <#if ctrl.getControlType() == 'TOOLBAR'>
${P.getCtrlCode(ctrl, 'CONTROL.vue').code}
    </#if>
</#list>
<#--  工具栏模型:END  -->

    /**
     * 解析视图参数
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    protected parseViewParam(): void {
        const { context, param } = this.$viewTool.formatNavigateViewParam(this, ${view.isPSDEView()?c});
        this.context = { ...context };
        this.viewparams = { ...param }
    }
<#--  移动端参数：BEGIN  -->

    /**
     * 是否显示返回按钮
     *
     * @readonly
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get isShowBackButton(): boolean {
        // 存在路由，非路由使用，嵌入
        if (this.viewDefaultUsage === "indexView") {
            return false;
        }
        return true;
    }
<#--  视图是否支持下拉刷新：BEGIN  -->
<#if view.isEnablePullDownRefresh?? && view.isEnablePullDownRefresh()>

    /**
     * 下拉刷新
     *
     * @param {*} $event
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public async pullDownToRefresh($event: any): Promise<any> {
    <#if pullDownToRefreshLogic??>
        ${pullDownToRefreshLogic}<#t/>
    <#else>
        setTimeout(() => {
            $event.srcElement.complete();
        }, 2000);
    </#if>
    }
</#if>
<#--  视图是否支持下拉刷新：END  -->
<#--  移动端参数：END  -->