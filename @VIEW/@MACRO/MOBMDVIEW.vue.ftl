<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>
<#if view.hasPSControl('searchform')>
<#assign searchform = view.getPSControl('searchform')>

    /**
     * 搜索表单状态
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public searchformState: boolean = false;

    /**
     * 是否展开搜索表单
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public isExpandSearchForm: boolean = false;

    /**
     * 执行搜索表单
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public onSearch(): void {
        this.searchformState = false;
        this.isExpandSearchForm = true;
        const form: any = this.$refs.${searchform.name};
        if (form) {
            form.onSearch();
        }
        this.closeSearchform();
    }

    /**
     * 重置搜索表单
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public onReset(): void {
        this.searchformState = false;
        this.isExpandSearchForm = false;
        const form: any = this.$refs.${searchform.name};
        if (form) {
            form.onReset();
        }
        this.closeSearchform();
    }
</#if>
<#--  输出快速搜索：BEGIN  -->
<#if view.isEnableQuickSearch?? && view.isEnableQuickSearch() == true>

    /**
     * 搜索值
     *
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public query: string = '';

    /**
     * 快速搜索值变化
     *
     * @param {*} event
     * @returns
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public async quickValueChange(event: any) {
        let { detail } = event;
        if (!detail) {
            return;
        }
        let { value } = detail;
        this.query = value;

        const mdctrl: any = this.$refs.mdctrl;
        if (mdctrl) {
            let response = await mdctrl.quickSearch(this.query);
            if (response) {
                <#--  console.log(1231231231);  -->
            }
        }
    }
</#if>
<#--  输出快速搜索：END  -->

   /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    @Prop({ default: true }) protected isSingleSelect!: boolean;
<#if view.getPSViewType().getId() == 'DEMOBMDVIEW' && view.hasPSControl('mdctrl')>

<#assign mdctrl = view.getPSControl('mdctrl')>
    <#if mdctrl.getPSAppViewLogics?? && mdctrl.getPSAppViewLogics()??>
    <#list mdctrl.getPSAppViewLogics() as logic>
    <#if logic.getLogicTrigger() == "CUSTOM" || logic.getLogicTrigger() == "CTRLEVENT">

    ${P.getLogicCode(logic, "LOGIC.vue").code}
    </#if>
    </#list>
    </#if>

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */  
    public ActionModel:any ={
    <#if mdctrl.getPSUIActions()??>
    <#list mdctrl.getPSUIActions() as item>
        ${item.getUIActionTag()}: { name: '${item.getUIActionTag()}',disabled: false, visabled: true,noprivdisplaymode:<#if item.getNoPrivDisplayMode(view)??>${item.getNoPrivDisplayMode(view)}</#if>,dataaccaction: '<#if item.getDataAccessAction()??>${item.getDataAccessAction()}</#if>', target: '${item.getActionTarget()}'}<#if item_has_next>,</#if>
    </#list>
    </#if>  
    };

   /**
     * 界面行为模型
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public UIActions = {
        left:[<#if mdctrl.getPSDEUIActionGroup2?? && mdctrl.getPSDEUIActionGroup2()??>
        <#assign left = mdctrl.getPSDEUIActionGroup2()>
            <#if left.getPSUIActionGroupDetails?? && left.getPSUIActionGroupDetails()??>
                <#list left.getPSUIActionGroupDetails() as action>
                    <#assign psuiaction = action.getPSUIAction()>
                    {actionid:'${action.getName()}',title:'<#if action.getPSUIAction?? && action.getPSUIAction()??>${action.getPSUIAction().getName()}</#if>',name: '${psuiaction.getUIActionTag()}',disabled: false, visabled: true,noprivdisplaymode:<#if psuiaction.getNoPrivDisplayMode(view)??>${psuiaction.getNoPrivDisplayMode(view)}</#if>,dataaccaction: '<#if psuiaction.getDataAccessAction()??>${psuiaction.getDataAccessAction()}</#if>', target: '${psuiaction.getActionTarget()}'},
                </#list>
            </#if>
            </#if>],
        right:[<#if mdctrl.getPSDEUIActionGroup?? && mdctrl.getPSDEUIActionGroup()??>
        <#assign right = mdctrl.getPSDEUIActionGroup()>
            <#if right.getPSUIActionGroupDetails?? && right.getPSUIActionGroupDetails()??>
                <#list right.getPSUIActionGroupDetails() as action>
                    <#assign psuiaction = action.getPSUIAction()>
                    {actionid:'${action.getName()}',title:'<#if action.getPSUIAction?? && action.getPSUIAction()??>${action.getPSUIAction().getName()}</#if>',name: '${psuiaction.getUIActionTag()}',disabled: false, visabled: true,noprivdisplaymode:<#if psuiaction.getNoPrivDisplayMode(view)??>${psuiaction.getNoPrivDisplayMode(view)}</#if>,dataaccaction: '<#if psuiaction.getDataAccessAction()??>${psuiaction.getDataAccessAction()}</#if>', target: '${psuiaction.getActionTarget()}'},
                </#list>
            </#if>
            </#if>
        ]
    }





    <#if !mdctrl.isNoSort()>
        <#assign state = false />
        <#list mdctrl.getPSListItems() as item>
            <#if item.isEnableSort()>
            <#assign state = true/>
            <#break>
            </#if>
        </#list>
        <#if state>

    /**
     * 排序对象
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public sort: any = { asc: "", desc: "" };

    /**
     * 排序
     *
     * @param {*} field
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public onSort(field: any) {
        if (this.sort.desc == field) {
            this.sort.desc = "";
            this.sortValue = {};
            this.onViewLoad();
            return
        }
        if (this.sort.asc == field) {
            this.sort.asc = "";
            this.sort.desc = field;
            this.sortValue = { sort: field + ",desc" };
            this.onViewLoad();
        } else {
            this.sort.asc = field;
            this.sort.desc = "";
            this.sortValue = { sort: field + ",asc" };
            this.onViewLoad();
        }
    }
        </#if>
    </#if>
</#if>

    /**
     * 分类值
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public categoryValue :any = {};

    /**
     * 排序值
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public sortValue :any = {};

    /**
     * 刷新视图
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public onRefreshView() {
        let mdctrl: any = this.$refs.mdctrl;
        if (mdctrl) {
            mdctrl.refresh();
        }
    }

    /**
     * 打开搜索表单
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public openSearchform() {
      let search :any = this.$refs.searchform<#if view.getName()??>${view.getName()?lower_case}</#if>;
      if(search){
          search.open();
      }
    }

    /**
     * 关闭搜索表单
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public closeSearchform(){
      let search :any = this.$refs.searchform<#if view.getName()??>${view.getName()?lower_case}</#if>;
      if(search){
          search.close();
      }
    }

    /**
     * 多选状态改变事件
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public showCheackChange(value:any){
        this.showCheack = value;
    }

    /**
     * 多选状态
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public showCheack = false;

    /**
     * 取消选择状态
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public cancelSelect() {
        this.showCheackChange(false);
    }

    /**
     * 视图加载（排序|分类）
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public onViewLoad() {
        let value = Object.assign(this.categoryValue,this.sortValue);
        this.engine.onViewEvent('mdctrl','viewload',value);
    }

    /**
     * 分类搜索
     *
     * @param {*} value
     * @memberof MOBENTITYHDLBBase
     */
    public onCategory(value:any){
        this.categoryValue = value;
        this.onViewLoad();
    }

    <#if view.hasPSControl('mdctrl')>
    <#assign mdctrl = view.getPSControl('mdctrl')>
    <#if mdctrl.getBatchPSDEToolbar?? && mdctrl.getBatchPSDEToolbar()??>
    <#assign batchToolbar = mdctrl.getBatchPSDEToolbar()>
    public <#if batchToolbar.getName()??> ${batchToolbar.getName()}_click</#if>(tag:string,event:any) {
       let mdctrl: any = this.$refs.mdctrl;
        if(mdctrl && mdctrl.<#if batchToolbar.getName()??> ${batchToolbar.getName()}_click</#if> instanceof Function){
            mdctrl.<#if batchToolbar.getName()??> ${batchToolbar.getName()}_click</#if>(tag,event);
        }
    }
    </#if>
    </#if>




<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>