<#--  工作流内容  -->
<#if (view.getPSWorkflow?? && view.getPSWorkflow()??) && (view.isWFIAMode?? && view.isWFIAMode()) && (view.getWFStepValue?? && view.getWFStepValue() == "") && (view.getViewType?? && (view.getViewType() == 'DEWFEDITVIEW3' || view.getViewType() == 'DEWFEDITVIEW'))>
<#assign mounted_block>this.loadWFLinks();</#assign>

    /**
     * 加载权限按钮
     *
     * @memberof ${srfclassname('${view.name}')}
     */
    protected loadWFLinks(): void {
        const url = '${app.getPKGCodeName()?lower_case}/${de.getPSSystemModule().codeName?lower_case}/${de.codeName?lower_case}/wflinks';
        const arg: any = {};
        if (this.engine && this.engine.viewdata && this.engine.viewdata.srfkey) {
            Object.assign(arg, { srfkey: this.engine.viewdata.srfkey });
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            return;
        }
        const get: Promise<any> = this.$http.get(url, arg, true);
        get.then((response: any) => {
            if (!response.status || response.status !== 200) {
                return;
            }
            const { data: _data } = response;
            this.containerModel.wflinks = _data;
        }).catch((response: any) => {
            //console.log(response);
        });
    }

    /**
     * 工作流动态按钮 点击事件
     *
     * @param {*} item
     * @param {*} $event
     * @memberof ${srfclassname('${view.name}')}
     */
    protected wflink_click(item: any, $event: any): void {
        if (Object.is(item.type, 'FRONT')) {
            this.wflink_front(item, $event);
        } else if (Object.is(item.type, 'BACKEND')) {
            this.wflink_backend(item, $event);
        }
    }

    /**
     *  前台界面行为
     *
     * @param {*} item
     * @param {*} $event
     * @memberof ${srfclassname('${view.name}')}
     */
    protected wflink_front(item: any, $event: any): void {
        if (!item.page || Object.is(item.page, '')) {
            return;
        }

        const page: any = {};
        try {
            Object.assign(page, JSON.parse(item.page));
        } catch (error) {

        }
        if (Object.keys(page).length === 0) {
            return;
        }
        const arg: any = {  };
        const _this: any = this;

        if (this.engine && this.engine.viewdata && this.engine.viewdata.srfkey) {
            Object.assign(arg, { srfkey: this.engine.viewdata.srfkey });
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            return;
        }

        const viewname = this.$util.srfFilePath2(page.viewname);
        const view: any = {
            viewname: viewname,
            title: page.title,
            width: page.width,
            height: page.height,
        }
        let container: Subject<any> = this.$appmodal.openModal(view, arg);
        container.subscribe((result: any) => {
            if (!result || !Object.is(result.ret, 'OK')) {
                return;
            }
            const { datas: _datas } = result;
            const [data] = _datas;
            this.wflink_backend(item, $event, data);
        });
    }

    /**
     * 后台界面行为
     *
     * @param {*} item
     * @param {*} $event
     * @param {*} [data]
     * @memberof ${srfclassname('${view.name}')}
     */
    protected wflink_backend(item: any, $event: any, data?: any): void {
        const _this: any = this;
        this.wf_Step(item, $event, data).then((response: any) => {
            if (!response.status || response.status !== 200) {
                if (response.errorMessage) {
                    this.$notify({ type: 'danger', message: response.errorMessage });
                }
                return;
            }
            this.$Notice.success({ title: '', desc: item.name + '操作成功' });

            const { data: _data } = response;
            if (_this.viewdata) {
                _this.$emit('viewdataschange', [{ ..._data }]);
                _this.$emit('close');
            } else if (_this.$tabPageExp) {
                _this.$tabPageExp.onClose(_this.$route.fullPath);
            }
        }).catch((response: any) => {
            if (response && response.status === 401) {
                return;
            }
            this.$notify({ type: 'danger', message: response.errorMessage });
        });
    }

    /**
     * 工作流步骤
     *
     * @private
     * @param {*} item
     * @param {*} $event
     * @param {*} [data]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.name}')}
     */
    private async wf_Step(item: any, $event: any, data?: any): Promise<any> {
        const arg: any = { args: {} };
        if (data) {
            Object.assign(arg, data);
        }
        if (this.engine && this.engine.viewdata && this.engine.viewdata.srfkey) {
            Object.assign(arg, { srfkey: this.engine.viewdata.srfkey });
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            return;
        }
        Object.assign(arg, { link: item.id });
        if (!arg.link || Object.is(arg.link, '')) {
            return;
        }

        const url: string = '${app.getPKGCodeName()?lower_case}/${de.getPSSystemModule().codeName?lower_case}/${de.codeName?lower_case}/wflink';
        const post: Promise<any> = this.$http.post(url, arg, true);
        return post;
    }
</#if>