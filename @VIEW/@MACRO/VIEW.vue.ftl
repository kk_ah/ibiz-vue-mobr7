<script lang='ts'>
import { Component } from 'vue-property-decorator';
import ${srfclassname('${view.name}')}Base from './${srffilepath2(view.name)}-base.vue';

<#if view.getPSControls()??>
<#list view.getPSControls() as ctrl>
<#if ctrl.getControlType?? && ctrl.getControlType() != 'TOOLBAR' && ctrl.getControlType() != 'CONTEXTMENU'>
<#if ctrl.getPSAppDataEntity?? && ctrl.getPSAppDataEntity()??>
import view_${ctrl.getName()} from '@widgets/${srffilepath2(ctrl.getPSAppDataEntity().getCodeName())}/${srffilepath2(ctrl.getCodeName())}-${ctrl.getControlType()?lower_case}/${srffilepath2(ctrl.getCodeName())}-${ctrl.getControlType()?lower_case}.vue';
<#else>
import view_${ctrl.getName()} from '@widgets/app/${srffilepath2(ctrl.getCodeName())}-${ctrl.getControlType()?lower_case}/${srffilepath2(ctrl.getCodeName())}-${ctrl.getControlType()?lower_case}.vue';
</#if>
</#if>
</#list>
</#if>
@Component({
    components: {
    <#if view.getPSControls?? && view.getPSControls()??>
    <#list view.getPSControls() as ctrl>
    <#if ctrl.getControlType() != "TOOLBAR">
        view_${ctrl.getName()}, 
    </#if>
    </#list>
    </#if>
    },
    beforeRouteEnter: (to: any, from: any, next: any) => {
        next((vm: any) => {
            vm.$store.commit('addCurPageViewtag', { fullPath: to.fullPath, viewtag: vm.viewtag });
        });
    },
})
export default class ${srfclassname('${view.name}')} extends ${srfclassname('${view.name}')}Base {

}
</script>