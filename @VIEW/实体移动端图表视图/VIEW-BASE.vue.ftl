<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 搜索值
     *
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}
     */
    public query: string = '';

    /**
     * 是否展开搜索表单
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}
     */
    public isExpandSearchForm: boolean = ${view.isExpandSearchForm()?c};

    /**
     * 快速搜索
     *
     * @param {*} $event
     * @memberof ${srfclassname('${view.name}')}
     */
    public onSearch($event: any): void {
        const refs: any = this.$refs;
        if (refs.chart) {
            refs.chart.refresh({});
        }
    }
<#if view.hasPSControl('searchform')>
<#assign searchform = view.getPSControl('searchform')>
    /**
     * 重置搜索表单
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public onReset(): void {
        const form: any = this.$refs.searchform;
        if (form) {
            form.onReset();
        }
    }
</#if>
<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>