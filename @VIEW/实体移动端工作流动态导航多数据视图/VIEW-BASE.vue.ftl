<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 当前选中tab
     *
     * @type {string}
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public selectTabId: string= "";

    /**
     * tab切换
     *
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public tab_click(item: any) {
        this.curSelectedNode = item;
        Object.assign(this.viewparams,
            {
                'userTaskId': item['userTaskId'],
                'processDefinitionKey': item['processDefinitionKey'],
            }
        );
        (this.$refs.mdctrl as any).load(this.viewparams);
        this.selectTabId = item.userTaskId;
    }

    /**
     * 树导航栏数据
     *
     * @type {any}
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public wfStepModel: Array<any> = [];

    /**
     * 是否展开搜索表单
     *
     * @type {any}
     * @memberof  ${srfclassname('${view.codeName}')}Base
     */
    public isExpandSearchForm: boolean = true;

    /**
     * 是否单选
     *
     * @type {any}
     * @memberof  ${srfclassname('${view.codeName}')}Base
     */
    public isSingleSelect: boolean = true;

    /**
     * 左侧树的默认配置
     *
     * @type {any}
     * @memberof  ${srfclassname('${view.codeName}')}Base
     */
    public defaultProps: any = {
        children: 'children',
        label: 'userTaskName',
    };

    /**
     * 左侧树当前选中节点
     *
     * @type {any}
     * @memberof  ${srfclassname('${view.codeName}')}Base
     */
    public curSelectedNode: any;

    /**
     * 获取树导航栏数据
     *
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public async getWFStepModel(): Promise<any> {
        this.$apploading.show();
        const response: any = await this.appEntityService.WFGetWFStep();
        this.$apploading.hidden();
        if (response && response.status === 200) {
            this.wfStepModel = response.data;
            if (this.wfStepModel && this.wfStepModel.length > 0) {
                this.curSelectedNode = this.wfStepModel[0];
                this.selectTabId = this.wfStepModel[0]['userTaskId'];
            }
            Object.assign(this.viewparams,
                {
                    'userTaskId': this.curSelectedNode['userTaskId'],
                    'processDefinitionKey': this.curSelectedNode['processDefinitionKey'],
                }
            );
        } else {
            const { error: _data } = response;
            this.$notice.error(_data.message);
        }
        return response;
    }

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>