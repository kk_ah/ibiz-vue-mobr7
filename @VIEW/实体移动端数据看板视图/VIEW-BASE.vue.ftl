<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public getDatas(): any[] {
        return [this.context];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public getData(): any {
        return this.context;
    }

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>