import Vue from 'vue';
import Router from 'vue-router';
import { AuthGuard } from '@/utils';
import qs from 'qs';
import store from '@/store';

Vue.use(Router);

const router = new Router({
    routes: [
        <#--  路由内容  -->
        {
            path: '/${view.getCodeName()?lower_case}/:${view.getCodeName()?lower_case}?',
            beforeEnter: (to: any, from: any, next: any) => {
                const routerParamsName = '${view.getCodeName()?lower_case}';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
                }
                const url: string = 'appdata';
                const auth: Promise<any> = AuthGuard.getInstance().authGuard(url, params, store);
                auth.then(() => {
                    next();
                }).catch(() => {
                    next();
                });
            },
            meta: {  
                keepAlive:true,
                caption: '${view.getCaption()}',
                viewType: 'APPINDEX',
                <#if view.getPSSysImage()??>
                imgPath: '${view.getPSSysImage().getImagePath()}',
                iconCls: '${view.getPSSysImage().getCssClass()}',
                </#if>
                parameters: [
                    { pathName: '${view.getCodeName()?lower_case}', parameterName: '${view.getCodeName()?lower_case}' },
                ],
                requireAuth: <#if view.getAccUserMode?? && view.getAccUserMode()?? && (view.getAccUserMode() == 1 || view.getAccUserMode() == 3)>false<#else>true</#if>,
            },
            component: () => import('@pages/${srffilepath2(view.getPSAppModule().getCodeName())}/${srffilepath2(view.getCodeName())}/${srffilepath2(view.getCodeName())}.vue'),
        },
        {
            path: '/viewshell/:viewshell?',
            beforeEnter: (to: any, from: any, next: any) => {
                const routerParamsName = '${view.getCodeName()?lower_case}';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
                }
                const url: string = 'appdata';
                const auth: Promise<any> = AuthGuard.getInstance().authGuard(url, params, store);
                auth.then(() => {
                    next();
                }).catch(() => {
                    next();
                });
            },
            meta: {
                parameters: [
                    { pathName: 'viewshell', parameterName: 'viewshell' },
                ],
            },
            component: () => import('@/components/view-shell/view-shell.vue'),
            children: [
            <#list view.getAllRelatedPSAppViewsEx() as subView>
            <#if !subView.isRedirectView()>
            <#if subView.isPSDEView() && subView.getPSAppDataEntity()??>
                <#assign appDataEntity = subView.getPSAppDataEntity()/>
                <#--  BEGIN：有多重路径  -->
                <#if subView.getPSAppDERSPathCount() gt 0>
                <#list 1..subView.getPSAppDERSPathCount() as count>
                <#assign path = ''/>
                <#--  BEGIN：循环路径下所有关系  -->
                <#list subView.getPSAppDERSPath(count_index) as deRSPath>
                <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
                <#assign path>${path}${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}/:${majorPSAppDataEntity.getCodeName()?lower_case}?/</#assign>
                </#list>
                <#--  END：循环路径下所有关系  -->
                {
                    path: '${path}${srfpluralize(appDataEntity.codeName)?lower_case}/:${appDataEntity.getCodeName()?lower_case}?/${subView.getPSDEViewCodeName()?lower_case}',
                    meta: {
                        caption: '<#if subView.isPSDEView()>${subView.getPSDataEntity().getCodeName()?lower_case}.views.${subView.getPSDEViewCodeName()?lower_case}.caption<#else>app.views.${subView.getCodeName()?lower_case}.caption</#if>',
                        <#if subView.getPSSysImage()??>
                        imgPath: '${subView.getPSSysImage().getImagePath()}',
                        iconCls: '${subView.getPSSysImage().getCssClass()}',
                        </#if>
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            <#list subView.getPSAppDERSPath(count_index) as deRSPath>
                            <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
                            { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' },
                            </#list>
                            { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
                            { pathName: '${subView.getPSDEViewCodeName()?lower_case}', parameterName: '${subView.getPSDEViewCodeName()?lower_case}' },
                        ],
                        requireAuth: <#if subView.getAccUserMode?? && subView.getAccUserMode()?? && (subView.getAccUserMode() == 1 || subView.getAccUserMode() == 3)>false<#else>true</#if>,
                    },
                    component: () => import('@pages/${srffilepath2(subView.getPSAppModule().getCodeName())}/${srffilepath2(subView.getCodeName())}/${srffilepath2(subView.getCodeName())}.vue'),
                },
                </#list>
                </#if>
                <#--  END：有多重路径  -->
                {
                    path: '${srfpluralize(appDataEntity.codeName)?lower_case}/:${appDataEntity.getCodeName()?lower_case}?/${subView.getPSDEViewCodeName()?lower_case}',
                    meta: {
                        caption: '<#if subView.isPSDEView()>${subView.getPSDataEntity().getCodeName()?lower_case}.views.${subView.getPSDEViewCodeName()?lower_case}.caption<#else>app.views.${subView.getCodeName()?lower_case}.caption</#if>',
                        <#if subView.getPSSysImage()??>
                        imgPath: '${subView.getPSSysImage().getImagePath()}',
                        iconCls: '${subView.getPSSysImage().getCssClass()}',
                        </#if>
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
                            { pathName: '${subView.getPSDEViewCodeName()?lower_case}', parameterName: '${subView.getPSDEViewCodeName()?lower_case}' },
                        ],
                        requireAuth: <#if subView.getAccUserMode?? && subView.getAccUserMode()?? && (subView.getAccUserMode() == 1 || subView.getAccUserMode() == 3)>false<#else>true</#if>,
                    },
                    component: () => import('@pages/${srffilepath2(subView.getPSAppModule().getCodeName())}/${srffilepath2(subView.getCodeName())}/${srffilepath2(subView.getCodeName())}.vue'),
                },
            <#else>
                {
                    path: '${subView.getCodeName()?lower_case}',
                    meta: {
                        caption: '<#if subView.isPSDEView()>${subView.getPSDataEntity().getCodeName()?lower_case}.views.${subView.getPSDEViewCodeName()?lower_case}.caption<#else>app.views.${subView.getCodeName()?lower_case}.caption</#if>',
                        <#if subView.getPSSysImage()??>
                        imgPath: '${subView.getPSSysImage().getImagePath()}',
                        iconCls: '${subView.getPSSysImage().getCssClass()}',
                        </#if>
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: '${subView.getCodeName()?lower_case}', parameterName: '${subView.getCodeName()?lower_case}' },
                        ],
                        requireAuth: <#if subView.getAccUserMode?? && subView.getAccUserMode()?? && (subView.getAccUserMode() == 1 || subView.getAccUserMode() == 3)>false<#else>true</#if>,
                    },
                    component: () => import('@pages/${srffilepath2(subView.getPSAppModule().getCodeName())}/${srffilepath2(subView.getCodeName())}/${srffilepath2(subView.getCodeName())}.vue'),
                },
            </#if>
            </#if>
            </#list>
            ]
        },
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                router.app.$store.commit('resetRootStateData');
                next();
            },
            component: () => import('@components/login/login.vue'),
        },
        {
            path: '/404',
            component: () => import('@components/404/404.vue')
        },
        {
            path: '/500',
            component: () => import('@components/500/500.vue')
        },
        {
            path: '*',
            redirect: '${view.getCodeName()?lower_case}'
        },
    ],
});

export default router;
