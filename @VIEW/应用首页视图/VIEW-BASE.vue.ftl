<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#assign mounted_block>
        this.viewState.next({ tag: 'appmenu', action: 'load', data: {} });
        this.$viewTool.setIndexParameters([{ pathName: '${view.getCodeName()?lower_case}', parameterName: '${view.getCodeName()?lower_case}' }]);
        this.$viewTool.setIndexViewParam(this.context);
</#assign>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 菜单位置
     *
     * @private
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    private mode: string ='<#if view.getMainMenuAlign()=="TOP">horizontal</#if><#if view.getMainMenuAlign()=="LEFT" || view.getMainMenuAlign() =="">vertical</#if>';

    /**
     * 当前主题
     *
     * @readonly
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get selectTheme() {
        if (this.$router.app.$store.state.selectTheme) {
            return this.$router.app.$store.state.selectTheme;
        } else if (localStorage.getItem('theme-class')) {
            return localStorage.getItem('theme-class');
        } else {
            return 'app-default-theme';
        }
    }

    /**
     * 当前字体
     *
     * @readonly
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get selectFont() {
        if (this.$router.app.$store.state.selectFont) {
            return this.$router.app.$store.state.selectFont;
        } else if (localStorage.getItem('font-family')) {
            return localStorage.getItem('font-family');
        } else {
            return 'Microsoft YaHei';
        }
    }

    /**
     * 菜单收缩变化
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public collapseChange: boolean = false;

    /**
     * 菜单收缩点击
     *
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public handleClick(): void {
        this.collapseChange = !this.collapseChange;
    }

    /**
     * 默认打开的视图
     *
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public defPSAppView: any = {
        <#if view.getDefPSAppView()??>
        <#assign dataview = view.getDefPSAppView()>
        codename: '${dataview.getCodeName()?lower_case}',
        viewtitle: '${dataview.getTitle()}',
        openmode: '${dataview.getOpenMode()}',
        width: ${dataview.getWidth()?c},
        height: ${dataview.getHeight()?c},
        deResParameters: [],
        <#--  BEGIN：是否应用实体视图  -->
        <#if dataview.isPSDEView()>
        <#assign appDataEntity = dataview.getPSAppDataEntity()/>
        routepath: '/${view.getCodeName()?lower_case}/:${view.getCodeName()?lower_case}?/${srfpluralize(appDataEntity.codeName)?lower_case}/:${appDataEntity.getCodeName()?lower_case}?/${dataview.getPSDEViewCodeName()?lower_case}/:${dataview.getPSDEViewCodeName()?lower_case}?',
        parameters: [
            { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
            { pathName: '${dataview.getPSDEViewCodeName()?lower_case}', parameterName: '${dataview.getPSDEViewCodeName()?lower_case}' },
        ],
        <#else>
        routepath: '/${view.getCodeName()?lower_case}/:${view.getCodeName()?lower_case}?/${dataview.getCodeName()?lower_case}/:${dataview.getCodeName()?lower_case}?',
        parameters: [
            { pathName: '${dataview.getCodeName()?lower_case}', parameterName: '${dataview.getCodeName()?lower_case}' },
        ],
        </#if>
        <#--  END：是否应用实体视图  -->
        </#if>
    };

    /**
     * 应用起始页面
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    public isDefaultPage: boolean = ${view.isDefaultPage()?c};

    /**
     * 获取样式
     *
     * @readonly
     * @type {string[]}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get themeClasses(): string[] {
        return [
            Object.is(this.selectTheme, 'app_theme_blue') ? 'app_theme_blue' : '',
            Object.is(this.selectTheme, 'app-default-theme') ? 'app-default-theme' : '',
            Object.is(this.selectTheme, 'app_theme_darkblue') ? 'app_theme_darkblue' : '',
        ];
    }

    /**
     * 主题字体
     *
     * @readonly
     * @type {*}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get themeStyle(): any {
        return {
            'height': '100vh',
            'font-family': this.selectFont,
        }
    }

    /**
     * 获取路由列表
     *
     * @readonly
     * @type {any[]}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get getRouterList(): any[] {
        return this.$store.state.historyPathList;
    }

    /**
     * 获取路由键值
     *
     * @readonly
     * @type {string}
     * @memberof ${srfclassname('${view.name}')}Base
     */
    get getRouterViewKey(): string {
        return this.$route.fullPath;
    }

<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>