export const PageComponents = {
    install(Vue: any, opt: any) {
        <#--  应用发布所有视图  -->
        <#if view.getAllRelatedPSAppViewsEx?? && view.getAllRelatedPSAppViewsEx()??>
        <#list view.getAllRelatedPSAppViewsEx() as appview>
        Vue.component('${srffilepath2(appview.getCodeName())}', () => import('@pages/${srffilepath2(appview.getPSAppModule().getCodeName())}/${srffilepath2(appview.getCodeName())}/${srffilepath2(appview.getCodeName())}.vue'));
        </#list>
        </#if>
    }
};