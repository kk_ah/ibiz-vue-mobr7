<#assign mounted_block>
        this.viewState.next({ tag: 'dashboard', action: 'load', data: {} });
</#assign>

<#ibizinclude>
../@MACRO/LAYOUTPANEL_VIEW.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_HEADER-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW_CONTENT-BASE.vue.ftl
</#ibizinclude>

    /**
     * 应用起始页面
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')}
     */
    public isDefaultPage: boolean = ${view.isDefaultPage()?c};
    
<#ibizinclude>
../@MACRO/VIEW_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/VIEW-BASE.style.ftl
</#ibizinclude>