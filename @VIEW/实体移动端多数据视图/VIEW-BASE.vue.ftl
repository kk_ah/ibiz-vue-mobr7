<#assign pullDownToRefreshLogic>
<#--  附加刷新操作  -->
        let mdctrl: any = this.$refs.mdctrl;
        if (mdctrl && mdctrl.pullDownToRefresh instanceof Function) {
            const response: any = await mdctrl.pullDownToRefresh();
            if (response) {
                $event.srcElement.complete();
            }
        }
</#assign>
<#ibizinclude>
../@MACRO/MOBMDVIEW.vue.ftl
</#ibizinclude>