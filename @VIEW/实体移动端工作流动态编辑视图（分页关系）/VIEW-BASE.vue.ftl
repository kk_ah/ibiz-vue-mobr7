<#assign self_content>
    /**
     * 工具栏模型数据
     * 
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public linkModel:Array<any> = [];

    /**
     * 获取工具栏按钮
     * 
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public getWFLinkModel():Promise<any>{
        return new Promise((resolve:any, reject:any) =>{
            let datas: any[] = [];
            let xData: any = this.$refs.form;
            if (xData.getDatas && xData.getDatas instanceof Function) {
                datas = [...xData.getDatas()];
            }
            if(Object.keys(this.viewparams).length > 0){
                Object.assign(datas,{'taskDefinitionKey':this.viewparams.userTaskId});
            }
            this.appEntityService.GetWFLink(JSON.parse(JSON.stringify(this.context)),datas,true).then((response:any) =>{
                if (response && response.status === 200) {
                    this.linkModel = response.data;
                    resolve(response.data);
                }
            }).catch((response: any) => {
                if (response && response.status) {
                    this.$notice.error(response.message);
                    return;
                }
                if (!response || !response.status || !response.data) {
                    this.$notice.error('系统异常');
                    return;
                }
            });
        });
    }

    /**
     * 动态工具栏点击
     * 
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public dynamic_toolbar_click(linkItem:any, $event:any){
        let datas: any[] = [];
        let xData: any = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        xData.wfsubmit(datas,linkItem).then((response: any) => {
            if (!response || response.status !== 200) {
                return;
            }
            const { data: _data } = response;

            if (this.viewdata) {
                this.$emit('viewdataschange', [{ ..._data }]);
                this.$emit('close');
            } else if (this.$tabPageExp) {
                this.$tabPageExp.onClose(this.$route.fullPath);
            }
        });
    }

</#assign>
<#ibizinclude>
../@MACRO/VIEW-BASE.vue.ftl
</#ibizinclude>