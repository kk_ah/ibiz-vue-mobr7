<#assign import_block>
import { toastController, alertController } from '@ionic/core';
</#assign>
<#assign self_content>
    /**
     * 工作流审批意见控件绑定值
     *
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public srfwfmemo: string = "";

    /**
     * 工具栏模型数据
     *
     * @type {Array<any>}
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public linkModel: Array<any> = [];

    /**
     * 获取工具栏按钮
     *
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public async getWFLinkModel(): Promise<any> {
        let datas: any[] = [];
        let xData: any = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if (Object.keys(this.viewparams).length > 0) {
            Object.assign(datas, { 'taskDefinitionKey': this.viewparams.userTaskId });
        }
        const response: any = await this.appEntityService.GetWFLink({ ...this.context }, datas);
        if (response && response.status === 200) {
            this.linkModel = response.data;
        } else {
            const { data: _data } = response;
            this.$notice.error(_data.message);
        }
        return response;
    }

    /**
     * 动态工具栏点击
     *
     * @param {*} linkItem
     * @param {*} $event
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public async dynamic_toolbar_click(linkItem: any, $event: any) {
        let datas: any;
        let xData: any = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = xData.getDatas()[0];
        }
        let falg = await this.openSrfwfmemo(linkItem);
        if(!falg){
            return
        }
        let res = await xData.save();
        if (!res || res.status !== 200) {
            return;
        }
        let response = await xData.wfsubmit(this.context,linkItem,datas);
        if (!response || response.status !== 200) {
            return;
        }
        const { data: _data } = response;
        if (this.viewparams) {
            this.$emit('viewdataschange', [{ ..._data }]);
            this.$emit('close');
        } else if (this.$tabPageExp) {
            this.$tabPageExp.onClose(this.$route.fullPath);
        }
    }

    /**
     * 工作流审批意见
     *
     * @protected
     * @param {*} linkItem
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.codeName}')}Base
     */
    public async openSrfwfmemo(linkItem:any) :Promise<any>{
        let falg = false;
        const alert = await alertController.create({
        header: linkItem.sequenceFlowName,
        buttons: [{
                        text: '取消',
                    },
                    {
                        text: '提交',
                        handler: () => {
                            falg = true
                        }
                    },],
        inputs : [{
                    name: 'srfwfmemo',
                    id: 'srfwfmemo',
                    type: 'textarea',
                    placeholder: '请输入'+linkItem.sequenceFlowName+"意见（非必填）",
                    value:this.srfwfmemo
                },]});
        await alert.present();
        let reData =  await alert.onDidDismiss();
        this.srfwfmemo = reData.data.values.srfwfmemo;
        return falg;
    }

</#assign>
<#ibizinclude>
../@MACRO/VIEW-BASE.vue.ftl
</#ibizinclude>