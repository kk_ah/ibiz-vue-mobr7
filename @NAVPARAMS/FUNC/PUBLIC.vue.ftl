<#--  BEGIN：获取导航上下文  -->
<#macro getNavigateContext currentItem>
  { <#t>
  <#if currentItem.getPSNavigateContexts?? && currentItem.getPSNavigateContexts()??>
  <#list currentItem.getPSNavigateContexts() as navContext>
  "${navContext.getKey()}": <#if navContext.isRawValue()?? && navContext.isRawValue() == true>"${navContext.getValue()}"<#else>"%${navContext.getValue()}%"</#if><#if navContext_has_next>,</#if> <#t>
  </#list>
  </#if>
  } <#t>
</#macro>
<#--  END：获取导航上下文  -->

<#--  BEGIN：获取导航参数  -->
<#macro getNavigateParams currentItem>
  { <#t>
  <#if currentItem.getPSNavigateParams?? && currentItem.getPSNavigateParams()??>
  <#list currentItem.getPSNavigateParams() as navParam >
  "${navParam.getKey()}": <#if navParam.isRawValue()?? && navParam.isRawValue() == true>"${navParam.getValue()}"<#else>"%${navParam.getValue()}%"</#if><#if navParam_has_next>,</#if> <#t>
  </#list>
  </#if>
  } <#t>
</#macro>
<#--  END：获取导航参数  -->