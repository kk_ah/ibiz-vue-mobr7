<#--  BEGIN：获取父关系路由参数  -->
<#macro getDeResParameters currentView>
  <#if currentView.isPSDEView()>
    [<#t>
    <#if currentView.getPSAppDERSPathCount() gt 0>
      <#list currentView.getPSAppDERSPath(currentView.getPSAppDERSPathCount() - 1) as deRSPath>
      <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/><#t>
        { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' }, <#t>
      </#list>
    </#if>
    ]<#t>
  <#else>
    []<#t>
  </#if>
</#macro>
<#--  END：获取父关系路由参数  -->

<#--  BEGIN：获取视图本身路由参数  -->
<#macro getParameters currentView>
  [<#t>
  <#if currentView.isPSDEView()>
    <#assign appDataEntity = currentView.getPSAppDataEntity()/>
    { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' }, <#t>
    { pathName: '${currentView.getPSDEViewCodeName()?lower_case}', parameterName: '${currentView.getPSDEViewCodeName()?lower_case}' } <#t>
  <#else>
    { pathName: '${currentView.getCodeName()?lower_case}', parameterName: '${currentView.getCodeName()?lower_case}' } <#t>
  </#if>
  ]<#t>
</#macro>
<#--  END：获取视图本身路由参数  -->

<#--  BEGIN：获取选择界面视图参数  -->
<#macro getPickupView currentItem>
  <#if currentItem.getPickupPSAppView()??>
    <#assign pickupview=currentItem.getPickupPSAppView()>
    "{ <#t>
      viewname: '${srffilepath2(pickupview.codeName)}'<#t>
      , title: '${pickupview.title}'<#t>
      , deResParameters: <@getDeResParameters pickupview /><#t>
      , parameters: <@getParameters pickupview /><#t>
    <#if (pickupview.getWidth() gt 0)>
      , width: ${pickupview.getWidth()?c}<#t>
    </#if>
    <#if (pickupview.getHeight() gt 0)>
      , height: ${pickupview.getHeight()?c}<#t>
    </#if>
    <#if  pickupview.getOpenMode()??>
      , placement:'${pickupview.getOpenMode()}'<#t>
    </#if>
    <#t> }"
  <#else><#t>
    "{}"<#t>
  </#if>
</#macro>
<#--  END：获取选择界面视图参数  -->

<#--  BEGIN：获取数据链接视图参数  -->
<#macro getLinkView currentItem>
  <#if currentItem.getLinkPSAppView()??>
    <#assign linkview=currentItem.getLinkPSAppView()>
    "{ <#t>
    viewname: '${linkview.codeName}'<#t>
    , title: '${linkview.title}'<#t>
    , deResParameters: <@getDeResParameters linkview /><#t>
    , parameters: <@getParameters linkview /><#t>
    , width: ${linkview.getWidth()?c}<#t>
    , height: ${linkview.getHeight()?c}<#t>
    , placement: '${linkview.getOpenMode()}'<#t>
    , isRedirectView: <#t>
    <#if linkview.isRedirectView()>
      true<#t>
      <#assign link_de = linkview.getPSDataEntity()/>
      , url: '/${app.getPKGCodeName()?lower_case}/${link_de.getPSSystemModule().codeName?lower_case}/${link_de.codeName?lower_case}/${linkview.getPSDEViewCodeName()?lower_case}/getmodel'<#t>
    <#else><#t>
      false<#t>
    </#if>
    <#t> }"
  <#else><#t>
    "{}"<#t>
  </#if>
</#macro>
<#--  END：获取数据链接视图参数  -->

<#--  BEGIN：获取自定义AC模板  -->
<#macro getItemRender currentItem>
  <#if currentItem.itemRender??>
    <template v-slot:default="{item}"><#t>
        ${currentItem.itemRender.code}<#t>
    </template><#t>
  </#if>
</#macro>
<#--  END：获取自定义AC模板  -->

<#--  BEGIN：获取AC参数  -->
<#macro getAcParams currentItem>
  "{ <#t>
  <#if currentItem.getPSAppDataEntity?? && currentItem.getPSAppDataEntity()?? && currentItem.getPSAppDEDataSet?? && currentItem.getPSAppDEDataSet()??>
    <#assign _appde = currentItem.getPSAppDataEntity()/>
    <#assign deDataSet = currentItem.getPSAppDEDataSet()/>
    <#if !P.exists("importService3", _appde.getId() + deDataSet.getCodeName(), "")>
      serviceName: '${_appde.getCodeName()?lower_case}'<#t>
      , interfaceName: '${deDataSet.getCodeName()}'<#t>
    </#if>
  </#if>
  }"<#t>
</#macro>
<#--  END：获取AC参数  -->

<#--  BEGIN：获取AC参数  -->
<#macro getItemParams currentItem>
  '{ <#t>
  <#if currentItem.getContextJOString?? && currentItem.getContextJOString()??>
    context:${editor.getContextJOString()},<#t>
  </#if>
  <#if currentItem.getParamJOString?? && currentItem.getParamJOString()??>
    param:${editor.getParamJOString()},<#t>
  </#if>
<#if currentItem.getEditorParam('PARENTDATA','') != ''>
    parentdata:${currentItem.getEditorParam('PARENTDATA','')?replace("'", "\"")}, <#t>
</#if>
  }' <#t>
</#macro>
<#--  END：获取AC参数  -->