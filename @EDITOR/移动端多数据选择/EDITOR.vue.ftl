<#ibizinclude>../@MACRO/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if editor.getPickupPSAppView()??>
<app-mob-mpicker 
    :data="data"
    :navigateContext ='<@getNavigateContext editor />'
    :navigateParam ='<@getNavigateParams editor />'
    :disabled="detailsModel.${editor.name}.disabled"
    :value="data.${editor.name}"
    name="${editor.name}"
    :context="context"
    :viewparams="viewparams"
    :service="service"
<#if editor.getPSAppDataEntity?? && editor.getPSAppDataEntity()??>
    deMajorField='${editor.getPSAppDataEntity().getMajorPSAppDEField().getCodeName()?lower_case}'
    deKeyField='${editor.getPSAppDataEntity().getCodeName()?lower_case}'
</#if>
    :pickupView=<@getPickupView editor />
    @formitemvaluechange="onFormItemValueChange" 
    style="${editor.getEditorCssStyle()}"/>
</#if>