<app-mob-radio-list 
    style="width: 100%;" 
    <#if editor.getPSCodeList()??>
    <#assign codelist=editor.getPSCodeList()>
    type="${codelist.getCodeListType()?lower_case}" 
    tag="${codelist.codeName}"
    </#if>
    :value="data.${editor.name}"  
    :disabled="detailsModel.${editor.name}.disabled" 
    @change="($event)=>this.data.${editor.name} = $event"/>