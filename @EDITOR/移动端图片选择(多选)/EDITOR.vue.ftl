<app-mob-picture 
    name='${editor.name}' 
    style="${editor.getEditorCssStyle()}overflow: auto;"  
    :multiple="true" 
    :formState="formState" 
    :ignorefieldvaluechange="ignorefieldvaluechange" 
    :data="JSON.stringify(this.data)" 
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    :context="context" 
    :viewparams="viewparams" 
    :uploadParam='<#if editor.getEditorParam('uploadParam','') != ''>${editor.getEditorParam('uploadParam','')}<#else>{}</#if>' 
    :exportParam='<#if editor.getEditorParam('exportParam','') != ''>${editor.getEditorParam('exportParam','')}<#else>{}</#if>'
    @formitemvaluechange="onFormItemValueChange" />