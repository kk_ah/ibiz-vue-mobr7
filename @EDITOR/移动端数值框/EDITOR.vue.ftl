<app-mob-input 
    class="app-form-item-number" 
    <#if editor.getPlaceHolder()??>
    placeholder="${editor.getPlaceHolder()}" 
    </#if>
    type="number"  
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    @change="($event)=>this.data.${editor.name} = $event"/>