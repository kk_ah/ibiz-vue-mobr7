<app-mob-input 
    class="app-form-item-input" 
    <#if editor.getPlaceHolder()??>
    placeholder="${editor.getPlaceHolder()}"
    type="text"  
    </#if>
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    @change="($event)=>this.data.${editor.name} = $event" />