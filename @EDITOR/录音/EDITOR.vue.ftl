<app-mob-recorder 
    name='${editor.name}' 
    style="${editor.getEditorCssStyle()}"
    uploadparams='${editor.getEditorParam('uploadparams','')}' 
    exportparams='${editor.getEditorParam('exportparams','')}' 
    :customparams="<#if editor.getEditorParam('customparams','') != ''>${editor.getEditorParam('customparams','')}<#else>{}</#if>" 
    :formState="formState" 
    :ignorefieldvaluechange="ignorefieldvaluechange" 
    :data="JSON.stringify(this.data)" 
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    @formitemvaluechange="onFormItemValueChange" />