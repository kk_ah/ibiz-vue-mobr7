<app-mob-stepper 
    class="app-form-item-stepper"  
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    @change="($event)=>this.data.${editor.name} = $event"/>
