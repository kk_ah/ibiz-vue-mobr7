<app-mob-datetime-picker 
    class="app-form-item-datetime" 
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled"
    @change="($event)=>this.data.${editor.name} = $event"/>