<app-mob-span  
    <#if item.getPSCodeList?? && item.getPSCodeList()??>
    <#assign codelist=item.getPSCodeList()> 
    codeListType="${codelist.getCodeListType()}" 
    tag="${codelist.codeName}"
    :isCache="false" 
    </#if>
    v-if="data.${editor.name}" 
    :context="context" 
    :value="data.${editor.name}" 
    :itemParam="<#if item.getEditorParam('PARENTDATA','') != ''>{parentdata:${item.getEditorParam('PARENTDATA','')}}<#else>{}</#if>"/>