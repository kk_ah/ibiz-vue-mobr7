<#--  编辑器  -->
<#ibizinclude>../@MACRO/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<app-mob-select-drop-down 
    name='${editor.name}' 
    <#if editor.getPSAppDataEntity?? && editor.getPSAppDataEntity()??>
    deMajorField='${editor.getPSAppDataEntity().getMajorPSAppDEField().getCodeName()?lower_case}'
    deKeyField='${editor.getPSAppDataEntity().getCodeName()?lower_case}id'
    </#if>
    valueitem='${item.getValueItemName()}' 
    style="${editor.getEditorCssStyle()}" 
    editortype="dropdown" 
    :formState="formState"
    :data="data"
    :context="context"
    :navigateContext ='<@getNavigateContext editor />'
    :navigateParam ='<@getNavigateParams editor />'
    :viewparams="viewparams"
    :itemParam=<@getItemParams editor />
    :disabled="detailsModel.${editor.name}.disabled"
    :service="service"
    :acParams=<@getAcParams editor />
    :value="data.${editor.name}" 
    @formitemvaluechange="onFormItemValueChange">
    <@getItemRender editor />
</app-mob-select-drop-down>