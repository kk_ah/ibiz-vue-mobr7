 <#--  编辑器  -->
 <#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<app-mob-select 
    <#if editor.getPlaceHolder()??> 
    placeholder="${editor.getPlaceHolder()}"
    </#if>
    <#if editor.getPSCodeList()??>
    <#assign codelist=editor.getPSCodeList()> 
    tag="${codelist.codeName}"
    codeListType="${codelist.getCodeListType()}" 
    :isCache="false" 
    </#if>
    :disabled="detailsModel.${editor.name}.disabled" 
    :data="data" 
    :context="context" 
    :viewparams="viewparams"
    :value="data.${editor.name}"  
    :navigateContext ='<@getNavigateContext editor />'
    :navigateParam ='<@getNavigateParams editor />'
    @change="($event)=>this.data.${editor.name} = $event" />