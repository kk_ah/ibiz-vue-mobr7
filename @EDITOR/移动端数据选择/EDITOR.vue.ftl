<#--  编辑器  -->
<#ibizinclude>../@MACRO/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if editor.getEditorParam("PICKUPVIEW",true)??>
    <#if editor.getPickupPSAppView()??>
<app-mob-picker
    name='${editor.name}'
    <#if editor.getPSAppDataEntity?? && editor.getPSAppDataEntity()??>
    deMajorField='${editor.getPSAppDataEntity().getCodeName()?lower_case}name'
    deKeyField='${editor.getPSAppDataEntity().getCodeName()?lower_case}id'
    </#if>
    valueitem='${editor.getValueItemName()}' 
    editortype="" 
    style="${editor.getEditorCssStyle()}"  
    :formState="formState"
    :data="data"
    :context="context"
    :viewparams="viewparams"
    :navigateContext ='<@getNavigateContext editor />'
    :navigateParam ='<@getNavigateParams editor />'
    :itemParam=<@getItemParams editor />
    :disabled="detailsModel.${editor.name}.disabled"
    :service="service"
    :acParams=<@getAcParams editor />
    :value="data.${editor.name}" 
    :pickupView=<@getPickupView editor />
    @formitemvaluechange="onFormItemValueChange">
    <@getItemRender editor />
</app-mob-picker>
    </#if>
</#if>