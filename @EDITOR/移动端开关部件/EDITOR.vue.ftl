<app-mob-switch 
    class="app-form-item-switch" 
    :value="data.${editor.name}"  
    :disabled="detailsModel.${editor.name}.disabled"
    @change="($event)=>this.data.${editor.name} = $event" />