<app-mob-datetime-picker 
    displayFormat="YYYY-MM-DD"
    class="app-form-item-datetime" 
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled"
    @change="($event)=>this.data.${editor.name} = $event"/>