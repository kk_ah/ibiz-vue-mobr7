<app-mob-textarea  
    class="app-form-item-textarea" 
    <#if editor.getPlaceHolder()??>
    placeholder="${editor.getPlaceHolder()}" 
    </#if>
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    @change="($event)=>this.data.${editor.name} = $event" />