## 导航参数说明

导航参数有两个部分，分别是导航上下文参数（`SRFNAVCTX`）和导航参数（`SRFNAVPARAM`）

### 配置相关

#### 示例：

```typescript
ITEMPARAM={ param: {'n_xx_id':'%field4%'}, context: {'XXXXid': '%majorentityname%'} }
SRFNAVPARAM.test = test
SRFNAVPARAM.test1 = %majorentityname%
SRFNAVCTX.ctx = %srforgid%
SRFNAVCTX.ctx1 = ctx1
```
#### 对象运行时：

```typescript
context:{ CTX:"%srforgid%", CTX1:"ctx1", XXXXID:"%majorentityname%" } 
param:{ test1:"%majorentityname%", test:"test", n_xx_id:"%majorentityname%" } 
```

上下文参数支持变量，变量使用两个百分号定义，如%param% ，上下文参数变量计算源来自上下文参数，如把当前用户标识`srfuserid`放入上下文参数用户`USER`上，可以这么定义 ```{USER:’%srfuserid%’}```

#### 配置方法

##### 默认方法

导航参数的默认配置为 使用**SRFNAVPARAM.** 或者 **SRFNAVCTX.** 前缀

##### 场景技巧

对不同的场景有着不同的配置技巧

- 重新定义（**只支持编辑器**）

​		如默认定义不足，比如想定义更多的上下文参数，可以通过ITEMPARAM参数指定，如下，param 属性定义了附加参数，context属性定义了附加上下文 例如 `ITEMPARAM={param:{'n_xx_id':'%field4%'},context:{'XXXXid':'%majorentityname%'}}`

- 直接赋值  

  test=test  会转为为导航视图参数

#### 导航参数支持场景

1. ##### 编辑器

   支持重新定义

   不支持直接赋值

2. ##### 应用功能

   不支持重新定义

   支持直接赋值

3. ##### 应用视图引用

   不支持重新定义

   不支持直接赋值

   导航参数必须显式使用**SRFNAVPARAM.** 前缀

4. ##### 实体关系界面

   不支持重新定义

   支持直接赋值

5. ##### 实体视图面板

   不支持重新定义

   支持直接赋值

6. 界面行为

   不支持重新定义

   支持直接赋值

### 解构赋值

#### 解构赋值方法

值是`%xxx%`类型，需要动态解构赋值。

其取值赋值顺序为：

- 首先从视图传递给编辑器的**视图上下文**中获取匹配值，如存在，则赋值给相应的属性。
- 其次从视图传递给编辑器的**视图参数**中获取匹配值，如存在，则赋值给相应的属性。（属性若有值会被覆盖）
- 最后从表单数据中获取匹配值，如存在，则赋值给相应的属性。

#### 注意事项

- 如果在赋值过程中该属性存在值会覆盖之前值，请配置参数时，合理配置。
- 如在上下文参数定义了` {ORDER:''}` 或 `{ORDER:null} `意味着将上下文参数中的`【ORDER】`进行移除

### 参数作用

##### 指定视图导航参数增强处理能力

导航参数可以用于表单默认值，也可作为数据查询条件往后台请求，灵活使用导航参数可以降低对后台的要求，如在做odoo的客户及联系人表格视图时，odoo将这两类数据存储在同一个实体中，如直接在导航参数中直接定义`SRFNAVPARAM.n_iscompany_eq=1 `就可以在前台直接分类数据，而无需后台提供查询能力。

