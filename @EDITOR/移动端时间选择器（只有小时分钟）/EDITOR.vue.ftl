<app-mob-datetime-picker 
    displayFormat="hh:mm"
    class="app-form-item-datetime" 
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled"
    @change="($event)=>this.data.${editor.name} = $event"/>