

# 图片上传编辑器(单选)参数说明

文件上传编辑器支持编辑器参数，其编辑器参数分为两类，分别是上传参数和下载参数。上传参数和其他参数不同，上传参数只为自己的上传地址做服务，因此不需要分成 context、param两个对象。

编辑器配置如下：

```typescript
uploadParam={"orderid":"%orderid%","aaaa":"111111","uuu":"hhhhh","bbb":"%testid%"}
exportParam={"orderid":"%orderid%","aaaa":"111111","uuu":"hhhhh","bbb":"%testid%"}
```

其中，上传参数和下载参数都包括两个部分，示例代码如下。

## 上传参数：

```typescript
uploadParam={"orderid":"%orderid%","aaaa":"111111","uuu":"hhhhh","bbb":"%testid%"}
```

## 下载参数

```typescript
exportParam={"orderid":"%orderid%","aaaa":"111111","uuu":"hhhhh","bbb":"%testid%"}
```

## 动态解构

上传参数和下载参数中的  `"orderid":"%orderid%"` 和  ``"bbb":"%testid%"`` 需要动态解构赋值。

其取值赋值顺序为：

- 首先从视图传递给编辑器的**视图上下文**中获取匹配值，如存在，则赋值给相应的属性。
- 其次从视图传递给编辑器的**视图参数**中获取匹配值，如存在，则赋值给相应的属性。
- 最后从表单数据中获取匹配值，如存在，则赋值给相应的属性。

> 如果表单中能获取匹配值，会覆盖视图上下文中获取的配置值，请配置参数时，合理配置。

动态结构后台的参数对象分别为：

```typescript
uploadParam={"orderid":"05676b25da4b479c2100c010a87e6dd5","aaaa":"111111","uuu":"hhhhh","bbb":"yyyy"}
```

```typescript
exportParam={"orderid":"05676b25da4b479c2100c010a87e6dd5","aaaa":"111111","uuu":"hhhhh","bbb":"yyyy"}
```

## 数据处理

通过qs对象将对象序列化成URL的形式，以&进行拼接.最终的参数传递如下

```http
http://localhost:8111/ibizutil/upload?orderid=05676b25da4b479c2100c010a87e6dd5&aaaa=111111&uuu=hhhhh&bbb=yyyy
```


```http
http://localhost:8111/ibizutil/download/fileid?orderid=05676b25da4b479c2100c010a87e6dd5&aaaa=111111&uuu=hhhhh&bbb=yyyy
```





