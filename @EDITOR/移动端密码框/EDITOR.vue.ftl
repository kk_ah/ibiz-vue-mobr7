<app-mob-input 
    class="app-form-item-password" 
    <#if editor.getPlaceHolder()??>
    placeholder="${editor.getPlaceHolder()}" 
    </#if>
    type="password"  
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled"  
    @change="($event)=>this.data.${editor.name} = $event" />