<app-mob-slider 
    class="app-form-item-slider"  
    :value="data.${editor.name}" 
    :disabled="detailsModel.${editor.name}.disabled" 
    @change="($event)=>this.data.${editor.name} = $event"/>
