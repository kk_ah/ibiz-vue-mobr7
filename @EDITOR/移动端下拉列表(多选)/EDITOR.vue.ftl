 <#--  编辑器  -->
 <#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<app-mob-check-list 
    <#if editor.getPlaceHolder()??> 
    placeholder="${editor.getPlaceHolder()}"
    </#if>
<#if editor.getPSCodeList()??>
<#assign codelist=editor.getPSCodeList()>
    <#if codelist??>
        <#if codelist.getOrMode() != ""> 
    orMode="${codelist.getOrMode()?lower_case}"
        </#if>
        <#if codelist.getValueSeparator() != "">
    valueSeparator="${codelist.getValueSeparator()}"
        </#if>
        <#if codelist.getTextSeparator() != ""> 
    textSeparator="${codelist.getTextSeparator()}"
        </#if>
    </#if> 
    type="${codelist.getCodeListType()?lower_case}"  
    tag="${codelist.codeName}"
</#if>
    :disabled="detailsModel.${editor.name}.disabled" 
    :data="data"
    :context="context"
    :viewparams="viewparams"
    :value="data.${editor.name}"   
    :navigateContext ='<@getNavigateContext editor />'
    :navigateParam ='<@getNavigateParams editor />'
    @change="($event)=>this.data.${editor.name} = $event"/>