<#ibizinclude>../../@MACRO/View.ftl</#ibizinclude>
<#ibizinclude>../../@MACRO/RouteParameters.ftl</#ibizinclude>
<#ibizinclude>../../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if item.getPSViewLogic?? && item.getPSViewLogic()??>
<#assign viewlogic = item.getPSViewLogic()/>
    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [contextJO={}]
     * @param {*} [paramJO={}]
     * @param {*} [$event]
     * @param {*} [xData]
     * @param {*} [container]
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.name}')}
     */
    public async ${item.name}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
    <#if viewlogic.getOpenDataPSAppView()??>
        <#assign dataview = viewlogic.getOpenDataPSAppView()/>
        const params: any = { ...paramJO };
        let context = { ...this.context, ...contextJO };
        if (args.length > 0) {
            Object.assign(context, args[0]);
        }
        let response: any = null;
        <#if view.getPSAppViewRefs?? && view.getPSAppViewRefs()??>
        <#list view.getPSAppViewRefs() as refItem>
        <#if refItem.getName() == 'EDITDATA'>
        let panelNavParam = <@getNavigateParams refItem />;
        let panelNavContext = <@getNavigateContext refItem />;
        //导航参数处理
        const { context: _context, param: _params } = this.$viewTool.formatNavigateParam( panelNavContext, panelNavParam, context, params, {});
        </#if>
        </#list>
        </#if>
<#--  BEGIN: 输出视图数据  -->
<#if !(dataview.isRedirectView()) && (dataview.getOpenMode() == 'POPUPMODAL' || dataview.getOpenMode()?index_of('DRAWER') == 0 || dataview.getOpenMode() =='POPOVER')>
<@outPutViewInfo dataview/>
</#if>
<#--  END: 输出视图数据  -->
        <#if dataview.isRedirectView()>
        <#assign dataview_de = dataview.getPSDataEntity()/>
        const url: string = '/${app.getPKGCodeName()?lower_case}/${dataview_de.getPSSystemModule().codeName?lower_case}/${dataview_de.codeName?lower_case}/${dataview.getPSDEViewCodeName()?lower_case}/getmodel';
        response = await this.globaluiservice.openService.openRedirect(url, _context, _params);
        <#elseif dataview.getOpenMode() == 'INDEXVIEWTAB' || dataview.getOpenMode() == ''>
        <#--  打开顶级分页视图 -->
            <#--  BEGIN：准备参数  -->
            <@outPutRouteParameters dataview/>
            <#--  END：准备参数  -->
        const routeParam: any = this.globaluiservice.openService.formatRouteParam(_context, deResParameters, parameters, args, _params);
        response = await this.globaluiservice.openService.openView(routeParam);
        <#elseif dataview.getOpenMode() = 'POPUPMODAL'>
        <#--  打开模态  -->
        response = await this.globaluiservice.openService.openModal(view, _context, _params);
        <#elseif dataview.getOpenMode()?index_of('DRAWER') == 0>
        <#--  打开抽屉  -->
        response = await this.globaluiservice.openService.openDrawer(view, _context, _params);
        <#elseif dataview.getOpenMode() == 'POPOVER'>
        <#--  打开气泡卡片  -->
        response = await this.globaluiservice.openService.openPopOver(view, _context, _params);
        <#else>
        this.$notice.warning('${dataview.title} 不支持该模式打开');
        </#if>
        if (response) {
            if (!response || !Object.is(response.ret, 'OK')) {
                return;
            }
            if (!xData || !(xData.refresh instanceof Function)) {
                return;
            }
            xData.refresh(response.datas);
        }
    <#else>
        this.$notice.warning('未指定关系视图');
    </#if>
    }
</#if>
