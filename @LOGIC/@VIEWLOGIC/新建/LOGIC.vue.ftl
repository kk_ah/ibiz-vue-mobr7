<#ibizinclude>../../@MACRO/View.ftl</#ibizinclude>
<#ibizinclude>../../@MACRO/RouteParameters.ftl</#ibizinclude>
<#ibizinclude>../../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if item.getPSViewLogic?? && item.getPSViewLogic()??>
<#assign viewlogic = item.getPSViewLogic()/>
    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [contextJO={}]
     * @param {*} [paramJO={}]
     * @param {*} [$event]
     * @param {*} [xData]
     * @param {*} [container]
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${view.name}')}
     */
    public async ${item.name}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
    <#if viewlogic.isEnableWizardAdd()>
        <#if viewlogic.getWizardPSAppView?? && viewlogic.getWizardPSAppView()??>
        <#assign wizardAppView = viewlogic.getWizardPSAppView() />
        let wizardAppView:any = {viewname:'${srffilepath2(wizardAppView.getCodeName())}',height: ${wizardAppView.getHeight()?c},width: ${wizardAppView.getWidth()?c},title: '${wizardAppView.title}'};
        let container: Subject<any> = this.globaluiservice.openService.openModal(wizardAppView, JSON.parse(JSON.stringify(this.context)), args[0]);
        container.subscribe((result: any) => {
            if (!result || !Object.is(result.ret, 'OK')) {
                return;
            }
            if(result && result.datas && result.datas.length >0 ){
                let targetkey:string = result.datas[0].srfkey;
                <#if viewlogic.getNewDataPSAppViews?? && viewlogic.getNewDataPSAppViews()??>
                let newDataAppViews:any ={
                <#list viewlogic.getNewDataPSAppViews() as newDataAppView>
                <#compress>${newDataAppView.getRefMode()}:<#if newDataAppView.isPSDEView()><#assign appDataEntity = newDataAppView.getPSAppDataEntity() />[{ pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },{ pathName: '${newDataAppView.getPSDEViewCodeName()?lower_case}', parameterName: '${newDataAppView.getPSDEViewCodeName()?lower_case}' }]<#else>[]</#if><#if newDataAppView_has_next>,</#if></#compress>
                </#list>
                };
                </#if>
                const data: any = {};
                if(args[0].srfsourcekey) data.srfsourcekey = args[0].srfsourcekey;
                let tempContext = JSON.parse(JSON.stringify(this.context));
                const openIndexViewTab = (data: any) => {
                    const _data: any = { w: (new Date().getTime()) };
                    Object.assign(_data, data);
                    const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, [], newDataAppViews[targetkey], args, _data);
                    this.$router.push(routePath);
                }
                openIndexViewTab(data);
            }
        })
        <#else>
        this.$notice.warning('请添加新建数据向导视图');
        </#if>
    <#elseif viewlogic.isEnableBatchAdd()>
        this.$notice.warning('支持批添加未实现');
    <#elseif viewlogic.isBatchAddOnly()>
        this.$notice.warning('只支持批添加未实现');
    <#elseif viewlogic.getNewDataPSAppView()??>
        <#assign dataview = viewlogic.getNewDataPSAppView()/>
        const params: any = { ...paramJO };
        let context = { ...this.context, ...contextJO };
        if (args.length > 0) {
            Object.assign(context, args[0]);
        }
        let response: any = null;
        <#if view.getPSAppViewRefs?? && view.getPSAppViewRefs()??>
        <#list view.getPSAppViewRefs() as refItem>
        <#if refItem.getName() == 'NEWDATA'>
        let panelNavParam = <@getNavigateParams refItem />;
        let panelNavContext = <@getNavigateContext refItem />;
        //导航参数处理
        const { context: _context, param: _params } = this.$viewTool.formatNavigateParam( panelNavContext, panelNavParam, context, params, {});
        </#if>
        </#list>
        </#if>
<#--  BEGIN: 输出视图数据  -->
<#if !(dataview.isRedirectView()) && (dataview.getOpenMode() == 'POPUPMODAL' || dataview.getOpenMode()?index_of('DRAWER') == 0 || dataview.getOpenMode() =='POPOVER')>
<@outPutViewInfo dataview/>
</#if>
<#--  END: 输出视图数据  -->
        <#if dataview.getOpenMode() == 'INDEXVIEWTAB' || dataview.getOpenMode() == ''>
        <#--  打开顶级分页视图  -->
            <#--  BEGIN：准备参数  -->
            <@outPutRouteParameters dataview/>
            <#--  END：准备参数  -->
        const routeParam: any = this.globaluiservice.openService.formatRouteParam(_context, deResParameters, parameters, args, _params);
        response = await this.globaluiservice.openService.openView(routeParam);
        <#elseif dataview.getOpenMode() = 'POPUPMODAL'>
        <#--  打开模态  -->
        response = await this.globaluiservice.openService.openModal(view, _context, _params);
        <#elseif dataview.getOpenMode()?index_of('DRAWER') == 0>
        <#--  打开抽屉  -->
        response = await this.globaluiservice.openService.openDrawer(view, _context, _params);
        <#elseif dataview.getOpenMode() == 'POPOVER'>
        <#--  打开气泡卡片  -->
        response = await this.globaluiservice.openService.openPopOver(view, _context, _params);
        <#else>
        this.$notice.warning('${dataview.title} 不支持该模式打开');
        </#if>
        if (response) {
            if (!response || !Object.is(response.ret, 'OK')) {
                return;
            }
            if (!xData || !(xData.refresh instanceof Function)) {
                return;
            }
            xData.refresh(response.datas);
        }
    <#else>
        this.$notice.warning('未指定关系视图');
    </#if>
    }
    </#if>
