    <#--  逻辑事件  -->
    /**
     * 逻辑事件
     *
     * @protected
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @returns {Promise<any>}
     * @memberof <#if item.getPSControlContainer?? && item.getPSControlContainer()??>${srfclassname('${item.getPSControlContainer().name}')}Base</#if>
     */
    protected async ${item.name}(params: any = {}, tag?: any, $event?: any): Promise<any> {
        <#if item.getEventArg()?? && item.getEventArg()?length gt 0>
        // 参数
        </#if>
        <#if item.getLogicType() == 'APPVIEWUIACTION' && item.getPSAppViewUIAction()??>
        <#assign uiAction = item.getPSAppViewUIAction().getPSUIAction() />

        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let contextJO: any = <#if uiAction.getContextJOString?? && uiAction.getContextJOString()??>${uiAction.getContextJOString()}<#else>{}</#if>;
        let paramJO: any = <#if uiAction.getUIActionParamJO()??>${uiAction.getUIActionParamJO().toString()}<#else>{}</#if>;
        <#if uiAction.getParamJOString?? && uiAction.getParamJOString()??>Object.assign(paramJO, ${uiAction.getParamJOString()});</#if>
        <#if item.getPSAppViewUIAction().getXDataPSControl?? && item.getPSAppViewUIAction().getXDataPSControl()??>
        <#assign xData = item.getPSAppViewUIAction().getXDataPSControl()/>
        <#if xData != item.getPSAppViewUIAction().getPSControlContainer()>
        xData = this.$refs.${xData.name?lower_case};
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        <#else>
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        </#if>
        <#else>
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        </#if>
        // 界面行为
        <#if item.getPSAppViewUIAction().getPSUIAction()??>
        <#assign uiaction = item.getPSAppViewUIAction().getPSUIAction()/>
        <#-- 属于应用实体的界面行为走UI服务start -->
        <#if uiaction.getPSAppDataEntity?? && uiaction.getPSAppDataEntity()??>
        <#assign curAppEntity = uiaction.getPSAppDataEntity() />
        <#if uiaction.getPSAppWFVer?? && uiaction.getPSAppWFVer()??>
        const curUIService: any = await this.globaluiservice.getService('${uiaction.getPSAppWFVer().getCodeName()?lower_case}_wfui_action');
        <#else>
        const curUIService: any = await this.globaluiservice.getService('${curAppEntity.getCodeName()?lower_case}_ui_action');
        </#if>
        if (curUIService) {
            curUIService.${uiaction.getFullCodeName()}(datas, contextJO, paramJO, $event, xData, this);
        }
        <#else>
        this.globaluiservice.${uiaction.getFullCodeName()}(datas, contextJO, paramJO, $event, xData, this);
        </#if>
        </#if>
        </#if>
    }