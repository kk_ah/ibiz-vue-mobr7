/**
 * ${item.getName()}
 *
 * @param {*} [item={}]
 * @memberof ${srfclassname('${ctrl.codeName}')}
 */
protected click${item.codeName}(item: any = {}){
    ${item.getJSCode()}
}