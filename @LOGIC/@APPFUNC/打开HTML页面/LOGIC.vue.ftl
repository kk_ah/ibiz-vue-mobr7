/**
 * ${item.getName()}
 *
 * @param {*} [item={}]
 * @memberof ${srfclassname('${ctrl.codeName}')}
 */
protected click${item.codeName}(item: any = {}){
   const localdata: any = this.$store.getters.getLocalData();
   const url = `${item.getHtmlPageUrl()}`;
   window.open(url, '_blank');
}