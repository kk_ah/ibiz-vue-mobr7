<#ibizinclude>../../@MACRO/View.ftl</#ibizinclude>
<#ibizinclude>../../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if item.getPSAppView()??>

/**
 * ${item.getName()}
 *
 * @param {*} [item={}]
 * @memberof ${srfclassname('${ctrl.codeName}')}
 */
<#assign dataview = item.getPSAppView()/>
protected click${item.codeName}(item: any = {}) {
    <#if dataview.isRedirectView()>
    this.$notify({ type: 'warning', message: '重定向视图暂不支持应用功能打开' });
    <#else>
    let navigateParam: any = <@getNavigateParams item />;
    let navigateContext: any = <@getNavigateContext item />;
    const { param: _param, context: _context } = this.$viewTool.formatNavigateParam(navigateContext, navigateParam, this.context, this.viewparams, {});
    let context = { ..._context };
    let param = { ..._param };
<#if dataview.getOpenMode() == 'POPUPMODAL' || dataview.getOpenMode() == 'POPUPAPP' || dataview.getOpenMode() == 'POPOVER' || dataview.getOpenMode()?index_of('DRAWER') == 0 >
<@outPutViewInfo dataview/>
</#if>
        <#if dataview.getOpenMode() == 'INDEXVIEWTAB' || dataview.getOpenMode() == ''>
    const deResParameters: any[] = [];
            <#--  BEGIN：准备参数  -->
            <#--  BEGIN：是否应用实体视图  -->
            <#if dataview.isPSDEView()>
                <#if dataview.getPSAppDataEntity()??>
                <#assign appDataEntity = dataview.getPSAppDataEntity()/>
    const parameters: any[] = [
        { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
        { pathName: '${dataview.getPSDEViewCodeName()?lower_case}', parameterName: '${dataview.getPSDEViewCodeName()?lower_case}' },
    ];
                <#else>
    const parameters: any[] = [];
    throw new Error('实体 ${dataview.getPSDataEntity().getCodeName()} 未添加到应用中');
                </#if>
            <#else>
    const parameters: any[] = [
        { pathName: '${dataview.getCodeName()?lower_case}', parameterName: '${dataview.getCodeName()?lower_case}' },
    ];
            </#if>
            <#--  END：是否应用实体视图  -->
            <#--  END：准备参数  -->
    const routeParam: any = this.globaluiservice.openService.formatRouteParam(context, deResParameters, parameters, [], param);
    this.globaluiservice.openService.openView(routeParam);
        </#if>
        <#if dataview.getOpenMode() == 'POPUP'>
    console.log('-----POPUP-----非模式弹出，暂时不实现');
        </#if>
        <#if dataview.getOpenMode() == 'POPUPMODAL'>
    this.globaluiservice.openService.openModal(view, context, param);
        </#if>
        <#if dataview.getOpenMode() == 'POPUPAPP'>
    console.log('-----POPUPAPP-----独立程序弹出，暂时不实现');
        </#if>
        <#if dataview.getOpenMode()?index_of('DRAWER') == 0>
    this.globaluiservice.openService.openDrawer(view, context, param);
        </#if>
        <#if dataview.getOpenMode() == 'POPOVER'>
    this.globaluiservice.openService.openPopOver(view, context, param);
        </#if>
        <#if dataview.getOpenMode()?index_of('USER') == 0>
    console.log('-----${dataview.getOpenMode()}-----用户自定义');
        </#if>
    </#if>
}
</#if>