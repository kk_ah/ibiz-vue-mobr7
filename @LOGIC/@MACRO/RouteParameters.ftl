<#macro outPutRouteParameters dataview>
<#if dataview??>
    <#if dataview.isPSDEView()>
    <#--  BEGIN：应用实体视图  -->
        <#-- 存在关系start -->
        <#if dataview.getPSAppDERSPathCount() gt 0>
            <#list 1..dataview.getPSAppDERSPathCount() as count>
            <#assign condition = ''/>
                <#list dataview.getPSAppDERSPath(count_index)  as deRSPath>
                    <#if deRSPath.getMajorPSAppDataEntity?? && deRSPath.getMajorPSAppDataEntity()??>
                        <#assign _dataEntity = deRSPath.getMajorPSAppDataEntity()/>
                        <#assign condition>${condition}context.${_dataEntity.getCodeName()?lower_case} && </#assign>
                    </#if>
                </#list>
            </#list>
        let deResParameters: any[] = [];
            <#-- 如果是主实体需对context判断start -->
            <#if dataview.getPSAppDataEntity().isMajor()>
        if (${condition}true) {
            deResParameters = [
                <#list dataview.getPSAppDERSPath(dataview.getPSAppDERSPathCount() - 1) as deRSPath>
                <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
            { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' },
                </#list>
            ]
        }
            <#else>
        deResParameters = [
                <#list dataview.getPSAppDERSPath(dataview.getPSAppDERSPathCount() - 1) as deRSPath>
                <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
            { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' },
                </#list>
        ];
            </#if>
            <#-- 如果是主实体需对context判断end -->
        <#else>
        const deResParameters: any[] = [];
        </#if>
        <#-- 存在关系end -->
            <#assign appDataEntity = dataview.getPSAppDataEntity()/>
        const parameters: any[] = [
            { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
            { pathName: '${dataview.getPSDEViewCodeName()?lower_case}', parameterName: '${dataview.getPSDEViewCodeName()?lower_case}' },
        ];
    <#--  END：应用实体视图  -->
    <#else>
    <#--  BEGIN：非应用实体视图  -->
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: '${dataview.getCodeName()?lower_case}', parameterName: '${dataview.getCodeName()?lower_case}' },
        ];
    <#--  END：非应用实体视图  -->
    </#if>
<#else>
        const deResParameters: any[] = [];
        const parameters: any[] = [];
</#if>
</#macro>