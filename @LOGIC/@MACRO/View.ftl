<#macro outPutViewInfo dataview>
<#if dataview??>
        const view: any = { 
            viewname: '${srffilepath2(dataview.getCodeName())}', 
            height: ${dataview.getHeight()?c}, 
            width: ${dataview.getWidth()?c},  
            title: '${dataview.title}', 
            placement: '${dataview.getOpenMode()}',
        };
<#else>
        const view: any = {
        };
</#if>
</#macro>