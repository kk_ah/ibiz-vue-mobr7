    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 当前数据
     * @param {any} context 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @returns {Promise<any>}
     * @memberof <#if item.getPSWFVersion?? && item.getPSWFVersion()??>${srfclassname('${item.getPSWFVersion().getCodeName()}')}WFUIService<#else>${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}UIService</#if>
     */
    public async ${item.getFullCodeName()}(args: any[],context: any ={}, params?: any, $event?: any, xData?: any,actionContext?: any){
        <#if item.getFrontPSAppView?? && item.getFrontPSAppView()??>
        <#assign dataview = item.getFrontPSAppView()>
        let data: any = {};
        <#-- 是否先保存目标数据start -->
        <#if item.isSaveTargetFirst()??>
        const result:any = await xData.save(args,false);
        args = [result.data];
        </#if>
         <#-- 是否先保存目标数据end -->
        Object.assign(context,actionContext.context);
        const _this: any = actionContext;
        const _args: any[] = actionContext.$util.deepCopy(args);
        context = actionContext.$uiActionTool.handleActionParams(_args,context,params,data).context;
        data = actionContext.$uiActionTool.handleActionParams(_args,context,params,data).data;
        <#--  BEGIN：是否应用实体视图  -->
        <#if dataview.isPSDEView()>
        const deResParameters: any[] = [
            <#--  BEGIN：存在父关系路径  -->
            <#if dataview.getPSAppDERSPathCount() gt 0>
            <#list dataview.getPSAppDERSPath(dataview.getPSAppDERSPathCount() - 1) as deRSPath>
            <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
            { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' },
            </#list>
            </#if>
            <#--  END：存在父关系路径  -->
        ];
        <#else>
        const deResParameters: any[] = [];
        </#if>
        <#--  END：是否应用实体视图  -->
        <#--  BEGIN：是否应用实体视图  -->
        <#if dataview.isPSDEView()>
        <#assign appDataEntity = dataview.getPSAppDataEntity()/>
        const parameters: any[] = [
            { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
        ];
        <#else>
        const parameters: any[] = [];
        </#if>
        <#--  END：是否应用实体视图  -->
        <#if dataview.getOpenMode() =='INDEXVIEWTAB' || dataview.getOpenMode() == '' >
        const openIndexViewTab = (data: any) => {
            const routePath = actionContext.$viewTool.buildUpRoutePath(actionContext.$route, data, deResParameters, parameters, _args, data);
            actionContext.$router.push(routePath);
            <#--  BEGIN 是否重新加载数据  -->
            <#if item.isReloadData?? && item.isReloadData()>
            if (xData && xData.refresh && xData.refresh instanceof Function) {
                xData.refresh(args);
            } else if (_this.refresh && _this.refresh instanceof Function) {
                _this.refresh(args);
            }
            </#if>
            <#--  END 是否重新加载数据  -->
            <#--  BEGIN 后续界面行为  -->
            <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
            <#assign nextPSUIAction = item.getNextPSUIAction()/>
            if (this.${nextPSUIAction.getFullCodeName()} && this.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                this.${nextPSUIAction.getFullCodeName()}([data],context,params, $event, xData,actionContext);
            }
            </#if>
            <#--  END 后续界面行为  -->
            return null;
        }
        openIndexViewTab(data);
        <#elseif dataview.getOpenMode() = 'POPUPMODAL'>
        <#--  打开模态  -->
        const openPopupModal = (view: any, data: any) => {
            let container: Subject<any> = actionContext.$appmodal.openModal(view, data, deResParameters, parameters, args, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                const _this: any = actionContext;
                 <#--  是否重新加载数据  -->
                <#if item.isReloadData?? && item.isReloadData()>
                if (xData && xData.refresh && xData.refresh instanceof Function) {
                    xData.refresh(args);
                } else if (_this.refresh && _this.refresh instanceof Function) {
                    _this.refresh(args);
                }
                </#if>
                <#--  后续界面行为  -->
                <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                <#assign nextPSUIAction = item.getNextPSUIAction()/>
                if (_this.${nextPSUIAction.getFullCodeName()} && _this.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                    _this.${nextPSUIAction.getFullCodeName()}(result.datas,context,params, $event, xData,actionContext);
                }
                </#if>
                if(result.datas && result.datas.length >0){
                    let resultData:any = result.datas[0];
                    Object.assign(resultData,data);
                    xData.service.wfsubmit('WFSubmit',resultData, true).then((response: any) => {
                        if (!response || response.status !== 200) {
                            actionContext.$Notice.error({ title: '错误', desc: response.message });
                            return;
                        }
        
                        const _this: any = actionContext;
                        return response;
                    }).catch((response: any) => {
                        if (!response || !response.status || !response.data) {
                            actionContext.$Notice.error({ title: '错误', desc: '系统异常！' });
                            return;
                        }
                        if (response.status === 401) {
                            return;
                        }
                        const { data: _data } = response;
                        actionContext.$Notice.error({ title: _data.title, desc: _data.message });
                        return null;
                    });
                }
            });
        }
        const view: any = {
            viewname: '${srffilepath2(dataview.getCodeName())}', 
            height: ${dataview.getHeight()?c}, 
            width: ${dataview.getWidth()?c},  
            title: '${dataview.title}', 
        };
        openPopupModal(view, data);
        <#elseif dataview.getOpenMode()?index_of('DRAWER') == 0>
         const openDrawer = (view: any, data: any) => {
                let container: Subject<any> = await actionContext.$appdrawer.openDrawer(view, data, deResParameters, parameters, _args, data);
                container.subscribe((result: any) => {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    const _this: any = actionContext;
                    <#--  是否重新加载数据  -->
                    <#if item.isReloadData?? && item.isReloadData()>
                    if (xData && xData.refresh && xData.refresh instanceof Function) {
                        xData.refresh(args);
                    } else if (_this.refresh && _this.refresh instanceof Function) {
                        _this.refresh(args);
                    }
                    </#if>
                    <#--  后续界面行为  -->
                    <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                    <#assign nextPSUIAction = item.getNextPSUIAction()/>
                    if (this.${nextPSUIAction.getFullCodeName()} && this.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                        this.${nextPSUIAction.getFullCodeName()}(result.datas,context,params, $event, xData,actionContext);
                    }
                    </#if>
                    if(result.datas && result.datas.length >0){
                    let resultData:any = result.datas[0];
                    Object.assign(resultData,data);
                    <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
                    const curService:${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}Service =  new ${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}Service();
                    curService.WFSubmit(context,resultData, true).then((response: any) => {
                        if (!response || response.status !== 200) {
                            actionContext.$Notice.error({ title: '错误', desc: response.message });
                            return;
                        }
        
                        const _this: any = actionContext;
                        return response;
                    }).catch((response: any) => {
                        if (!response || !response.status || !response.data) {
                            actionContext.$Notice.error({ title: '错误', desc: '系统异常！' });
                            return;
                        }
                        if (response.status === 401) {
                            return;
                        }
                        const { data: _data } = response;
                        actionContext.$Notice.error({ title: _data.title, desc: _data.message });
                        return null;
                    });
                    </#if>
                }
                });
            }
            const view: any = {
                viewname: '${srffilepath2(dataview.getCodeName())}', 
                height: ${dataview.getHeight()?c}, 
                width: ${dataview.getWidth()?c},  
                title: '${dataview.title}', 
                placement: '${dataview.getOpenMode()}',
            };
            openDrawer(view, data);
        <#elseif dataview.getOpenMode() == 'POPOVER'>
            const openPopOver = (view: any, data: any) => {
                let container: Subject<any> = actionContext.$apppopover.openPop($event, view, data, deResParameters, parameters, _args, data);
                container.subscribe((result: any) => {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    const _this: any = actionContext;
                    <#--  是否重新加载数据  -->
                    <#if item.isReloadData?? && item.isReloadData()>
                    if (xData && xData.refresh && xData.refresh instanceof Function) {
                        xData.refresh(args);
                    } else if (_this.refresh && _this.refresh instanceof Function) {
                        _this.refresh(args);
                    }
                    </#if>
                    <#--  后续界面行为  -->
                    <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                    <#assign nextPSUIAction = item.getNextPSUIAction()/>
                    if (this.${nextPSUIAction.getFullCodeName()} && this.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                        this.${nextPSUIAction.getFullCodeName()}(result.datas,context, params, $event, xData,actionContext);
                    }
                    </#if>
                    if(result.datas && result.datas.length >0){
                    let resultData:any = result.datas[0];
                    Object.assign(resultData,data);
                    <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
                    const curService:${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}Service =  new ${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}Service();
                    curService.WFSubmit(context,resultData, true).then((response: any) => {
                        if (!response || response.status !== 200) {
                            actionContext.$Notice.error({ title: '错误', desc: response.message });
                            return;
                        }
        
                        const _this: any = actionContext;
                        return response;
                    }).catch((response: any) => {
                        if (!response || !response.status || !response.data) {
                            actionContext.$Notice.error({ title: '错误', desc: '系统异常！' });
                            return;
                        }
                        if (response.status === 401) {
                            return;
                        }
                        const { data: _data } = response;
                        actionContext.$Notice.error({ title: _data.title, desc: _data.message });
                        return null;
                    });
                    </#if>
                }
                });
            }
            const view: any = {
                viewname: '${srffilepath2(dataview.getCodeName())}', 
                height: ${dataview.getHeight()?c}, 
                width: ${dataview.getWidth()?c},  
                title: '${dataview.title}', 
                placement: '${dataview.getOpenMode()}',
            };
            openPopOver(view, data);
        <#else>
            actionContext.$Notice.warning({ title: '错误', desc: '${dataview.title} 不支持该模式打开' });
        </#if>
        </#if>
    }