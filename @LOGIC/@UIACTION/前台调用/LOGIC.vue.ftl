<#--  前台界面行为  -->
<#ibizinclude>../../@MACRO/View.ftl</#ibizinclude>
<#ibizinclude>../../@MACRO/RouteParameters.ftl</#ibizinclude>
<#ibizinclude>../../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if front_block??>
${front_block}
<#else>
    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName] 
     * @returns {Promise<any>}
     * @memberof <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}UIService</#if>
     */
    public async ${item.getFullCodeName()}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
    <#if item.render??>
        ${item.render.code}
    <#else>
        <#if item.getActionTarget() == 'SINGLEDATA'>
        <#--  BEGIN: 数据目标: 单项数据  -->
        this.notice.error('不支持单项数据');
        <#--  END: 数据目标: 单项数据  -->
        <#elseif item.getActionTarget() == 'MULTIDATA'>
        <#--  BEGIN: 数据目标: 多项数据  -->
        this.notice.error('不支持多项数据');
        <#--  END: 数据目标: 多项数据  -->
        <#else>
        <#--  BEGIN: 数据目标: 单项数据（主键），多项数据（主键），无数据  -->
        <#--  BEGIN: 数据处理逻辑  -->
            <#-- 是否先保存目标数据start -->
            <#if item.isSaveTargetFirst()>
        const result: any = await xData.save(args,false);
        args = [result.data];
            </#if>
            <#-- 是否先保存目标数据end -->
        const _args: any[] = Util.deepCopy(args);
        const actionTarget: string | null = <#if item.getActionTarget()??>'${item.getActionTarget()}'<#else>null</#if>;
            <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
            <#assign appDataEntity = item.getPSAppDataEntity() />
                <#if item.getActionTarget() == 'SINGLEKEY' || item.getActionTarget() == 'MULTIKEY'>
                <#assign valueItem><#if item.getValueItem?? && item.getValueItem() != ''>${item.getValueItem()}<#else>${appDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}</#if></#assign>
                <#assign paramItem><#if item.getParamItem?? && item.getParamItem() != ''>${item.getParamItem()}<#else>${appDataEntity.getCodeName()?lower_case}</#if></#assign>
                <#assign textItem><#if item.getTextItem?? && item.getTextItem() != ''>${item.getTextItem()}<#else>${appDataEntity.getMajorPSAppDEField().getCodeName()?lower_case}</#if></#assign>
        Object.assign(contextJO, { ${appDataEntity.getCodeName()?lower_case}: '%${paramItem}%' });
        Object.assign(paramJO, { ${valueItem}: '%${paramItem}%' });
        Object.assign(paramJO, { ${textItem}: '%${textItem}%' });
                </#if>
            </#if>
            
        let context: any = this.handleContextParam(actionTarget, _args, contextJO);
        let params: any = this.handleActionParam(actionTarget, _args, paramJO);
        context = { ...container.context, ...context };
        <#-- 构建srfparentdename和srfparentkey start -->
        let parentObj: any = {
            srfparentdename: srfParentDeName ? srfParentDeName : null,
            srfparentkey: srfParentDeName ? context[srfParentDeName.toLowerCase()] : null,
        };
        Object.assign(context, parentObj);
        Object.assign(params, parentObj);
        let panelNavParam= <@getNavigateParams item />;
        let panelNavContext= <@getNavigateContext item />;
        const { context: _context, param: _params } = this.viewTool.formatNavigateParam( panelNavContext, panelNavParam, context, params, {});
            <#--  BEGIN：界面行为结果  -->
            <#if item.getFrontProcessType() != 'OTHER'>
        let response: any = null;
            </#if>
            <#--  END：界面行为结果  -->
        <#-- 构建srfparentdename和srfparentkey end -->
        <#--  END: 数据处理逻辑  -->
            <#if item.getFrontProcessType() == 'OPENHTMLPAGE'>
            <#--  BEGIN: 前台处理模式：打开HTML  -->
                <#-- 关闭编辑视图 -->
                <#if item.isCloseEditView()>
        container.closeView(null);
                </#if>
        response = await this.openService.openPopupApp(`${item.getHtmlPageUrl()}`);
            <#--  END: 前台处理模式：打开HTML  -->
            <#elseif (item.getFrontProcessType() == 'TOP' || item.getFrontProcessType() == 'WIZARD') && (item.getFrontPSAppView?? && item.getFrontPSAppView()??)>
            <#--  BEGIN: 前台处理模式：打开顶级视图，打开顶级视图或向导（模态）  -->
            <#assign dataview = item.getFrontPSAppView()>
                <#-- 关闭编辑视图 -->
                <#if item.isCloseEditView()>
        container.closeView(null);
                </#if>
<#--  BEGIN: 输出视图数据  -->
<#if !(dataview.isRedirectView()) && (dataview.getOpenMode() == 'POPUPMODAL' || dataview.getOpenMode()?index_of('DRAWER') == 0 || dataview.getOpenMode() =='POPOVER')>
<@outPutViewInfo dataview/>
</#if>
<#--  END: 输出视图数据  -->
                <#--  BEGIN: 打开重定向视图  -->
                <#if dataview.isRedirectView()>
                <#assign dataview_de = dataview.getPSDataEntity()/>
        const url: string = '/${app.getPKGCodeName()?lower_case}/${dataview_de.getPSSystemModule().codeName?lower_case}/${dataview_de.codeName?lower_case}/${dataview.getPSDEViewCodeName()?lower_case}/getmodel';
        response = await this.openService.openRedirect(url, _context, _params);
                <#--  END: 打开重定向视图  -->
                <#elseif dataview.getOpenMode() =='INDEXVIEWTAB' || dataview.getOpenMode() == ''>
                <#--  BEGIN: 打开顶级分页视图  -->
                <#--  BEGIN：准备参数  -->
                <@outPutRouteParameters dataview/>    
                <#--  END：准备参数  -->
        const routeParam: any = this.openService.formatRouteParam(_context, deResParameters, parameters, _args, _params);
        response = await this.openService.openView(routeParam);
                <#--  END: 打开顶级分页视图  -->
                <#elseif dataview.getOpenMode() = 'POPUPMODAL'>
                <#-- BEGIN: 打开模态  -->
        response = await this.openService.openModal(view, _context, _params);
                <#-- END: 打开模态  -->
                <#elseif dataview.getOpenMode()?index_of('DRAWER') == 0>
                <#--  BEGIN: 打开抽屉  -->
        response = await this.openService.openDrawer(view, _context, _params);
                <#--  END: 打开抽屉  -->
                <#elseif dataview.getOpenMode() == 'POPOVER'>
                <#--  BEGIN: 打开气泡卡片  -->
        response = await this.openService.openPopOver(view, _context, _params);
                <#--  END: 打开气泡卡片  -->
                <#--  END: 前台处理模式：打开顶级视图，打开顶级视图或向导（模态）  -->
                <#else>
                <#--  BEGIN: 其他打开模式  -->
        this.notice.warning('${dataview.title} 不支持该模式打开');
                <#--  END: 其他打开模式  -->
                </#if>
            <#elseif item.getFrontProcessType() == 'OTHER'>
            <#--  BEGIN: 前台处理模式：用户自定义  -->
        // 自定义实体界面行为
        this.notice.warning('${item.getCaption()} 未实现');
            <#--  END: 前台处理模式：用户自定义  -->
            </#if>
            <#--  BEGIN：界面行为结果  -->
            <#if item.getFrontProcessType() != 'OTHER'>
                <#if (item.isReloadData?? && item.isReloadData()) || (item.getNextPSUIAction?? && item.getNextPSUIAction()??)>
        if (response) {
                    <#--  BEGIN：刷新数据  -->
                    <#if item.isReloadData?? && item.isReloadData()>
            if (xData && xData.refresh && xData.refresh instanceof Function) {
                xData.refresh(args);
            }
                    </#if>
                    <#--  END：刷新数据  -->
                    <#--  BEGIN：后续界面行为  -->
                    <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                    <#assign nextPSUIAction = item.getNextPSUIAction()/>
                        <#if !(nextPSUIAction.getPSAppDataEntity?? && nextPSUIAction.getPSAppDataEntity()??)>
            if (this.globaluiservice.${nextPSUIAction.getFullCodeName()} && this.globaluiservice.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                this.globaluiservice.${nextPSUIAction.getFullCodeName()}(response.datas, contextJO, paramJO, $event, xData, container);
            }
                        <#elseif nextPSUIAction.getPSAppDataEntity().getId() != item.getId()>
            const nextPSUIActionUIService: any = await this.globaluiservice.getService('${nextPSUIAction.getPSAppDataEntity().getCodeName()?lower_case}_ui_action');
            if (nextPSUIActionUIService) {
                nextPSUIActionUIService.${nextPSUIAction.getFullCodeName()}(response.datas, contextJO, paramJO, $event, xData, container);
            }
                        <#else>
            this.${nextPSUIAction.getFullCodeName()}(response.datas, contextJO, paramJO, $event, xData, container);
                        </#if>
                    </#if>
                    <#--  END：后续界面行为  -->
        }
                </#if>
        return response;
            </#if>
            <#--  END：界面行为结果  -->
        </#if>
  <#--  END: 数据目标: 单项数据（主键），多项数据（主键），无数据  -->
    </#if>
    }
</#if>
    