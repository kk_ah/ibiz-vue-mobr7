    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 当前数据
     * @param {any} context 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @returns {Promise<any>}
     * @memberof <#if item.getPSWFVersion?? && item.getPSWFVersion()??>${srfclassname('${item.getPSWFVersion().getCodeName()}')}WFUIService<#else>${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}UIService</#if>
     */
    public async ${item.getFullCodeName()}(args: any[],context: any = {}, params?: any, $event?: any, xData?: any,actionContext?: any) {
<#if item.getActionTarget() == 'SINGLEDATA'>
        actionContext.$Notice.error({ title: '错误', desc: '不支持单项数据' });
<#elseif item.getActionTarget() == 'MULTIDATA'>
        actionContext.$Notice.error({ title: '错误', desc: '不支持多项数据' });
<#else>
        let data: any = {};
        <#-- 是否先保存目标数据start -->
        <#if item.isSaveTargetFirst()??>
        const result:any = await xData.save(args,false);
        args = [result.data];
        </#if>
         <#-- 是否先保存目标数据end -->
        Object.assign(context,actionContext.context);
        const _args: any[] = actionContext.$util.deepCopy(args);
        const _this: any = actionContext;
    <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
    <#assign appDataEntity = item.getPSAppDataEntity() />
        <#if item.getActionTarget() == 'SINGLEKEY'>
        <#--  单项数据主键  -->

        // 值项名称 - 单项数据主键，默认为应用实体codeName
        const valueItem: string = '<#if item.getValueItem?? && item.getValueItem() != ''>${item.getValueItem()}<#else>${appDataEntity.getCodeName()?lower_case}</#if>';
        // 数据项名称 - 单项数据主键，默认为应用实体codeName
        const paramItem: string = '<#if item.getParamItem?? && item.getParamItem() != ''>${item.getParamItem()}<#else>${appDataEntity.getCodeName()?lower_case}</#if>';
        // 信息项名称
        const textParam:string = '<#if item.getTextItem?? && item.getTextItem() != ''>${item.getTextItem()}<#else>srfmajortext</#if>';
        if (_args.length > 0 && _args[0].hasOwnProperty(valueItem)) {
            const _params2: any = {};
            Object.assign(_params2, { [paramItem]: _args[0][valueItem] });
            Object.assign(_params2,{[textParam]: _args[0]["srfmajortext"]});
            // 参数层级处理
            if (Object.is(paramItem, '${appDataEntity.getCodeName()?lower_case}')) {
                Object.assign(_args[0], _params2);
            }
            // 非默认值转换 删除原始值
            if (Object.is(valueItem, '${appDataEntity.getCodeName()?lower_case}') && !Object.is(paramItem, '${appDataEntity.getCodeName()?lower_case}')) {
                delete _args[0].${appDataEntity.getCodeName()?lower_case};
            }
            <#--  Object.assign(data, _args[0]);  -->
        }
        </#if>
        <#if item.getActionTarget() == 'MULTIKEY'>
    
        <#--  多项数据主键  -->
        // 值项名称 -  多项数据主键，默认为应用实体codeName
        const valueItem: string = '<#if item.getValueItem?? && item.getValueItem() != ''>${item.getValueItem()}<#else>${appDataEntity.getCodeName()?lower_case}</#if>';
        // 数据项名称 - 多项数据主键，默认为应用实体codeName
        const paramItem: string = '<#if item.getParamItem?? && item.getParamItem() != ''>${item.getParamItem()}<#else>${appDataEntity.getCodeName()?lower_case}</#if>';
        const _params2: any = {};
        let _keys: string[] = [];
        _args.forEach((arg: any) => {
            if (arg.hasOwnProperty(valueItem)) {
                _keys.push(arg[valueItem]);
            }
        });

        Object.assign(_params2, { [paramItem]: _keys.join(';') });
        // 参数层级处理
        if (Object.is(paramItem, '${appDataEntity.getCodeName()?lower_case}')) {
            Object.assign(_args[0], _params2);
        }
         <#--  Object.assign(data, _args[0]);  -->
        </#if>
    </#if>
        context = actionContext.$uiActionTool.handleActionParams(_args,context,params,data).context;
        data = actionContext.$uiActionTool.handleActionParams(_args,context,params,data).data;
        const backend = () => {
        <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
            const curService:${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}Service =  new ${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}Service();
            curService.WFSubmit(context,data, ${item.isShowBusyIndicator()?c}).then((response: any) => {
                if (!response || response.status !== 200) {
                    actionContext.$Notice.error({ title: '错误', desc: response.message });
                    return;
                }
                <#if item.getSuccessMsg?? && item.getSuccessMsg()??>
                actionContext.$Notice.success({ title: '成功', desc: '${item.getSuccessMsg()}' });
                </#if>

                const _this: any = actionContext;
                <#--  是否重新加载数据  -->
                <#if item.isReloadData?? && item.isReloadData()>
                if (xData && xData.refresh && xData.refresh instanceof Function) {
                    xData.refresh(args);
                } else if (_this.refresh && _this.refresh instanceof Function) {
                    _this.refresh(args);
                }
                </#if>
                <#--  后续界面行为  -->
                <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                <#assign nextPSUIAction = item.getNextPSUIAction()/>
                const { data: result } = response;
                let _args: any[] = [];
                if (Object.is(actionContext.$util.typeOf(result), 'array')) {
                    _args = [...result];
                } else if (Object.is(actionContext.$util.typeOf(result), 'object')) {
                    _args = [{...result}];
                } else {
                    _args = [...args];
                }
                //调用后续界面行为
                if (_this.${nextPSUIAction.getFullCodeName()} && _this.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                    _this.${nextPSUIAction.getFullCodeName()}(_args,context,params, $event, xData,context);
                }
                </#if>
                return response;
            }).catch((response: any) => {
                if (!response || !response.status || !response.data) {
                    actionContext.$Notice.error({ title: '错误', desc: '系统异常！' });
                    return;
                }
                if (response.status === 401) {
                    return;
                }
                const { data: _data } = response;
                actionContext.$Notice.error({ title: _data.title, desc: _data.message });
                return null;
            });
        </#if>
        };
        <#if item.getFrontPSAppView?? && item.getFrontPSAppView()??>
        <#assign frontview = item.getFrontPSAppView()>
        <#if frontview.getOpenMode()?index_of('DRAWER') == 0>
        const view: any = {
            viewname: '${srffilepath2(frontview.getCodeName())}',
            title: '${frontview.title}',
            height: ${frontview.getHeight()?c},
            width: ${frontview.getWidth()?c},
            placement: '${frontview.getOpenMode()}'
        };
        const appdrawer = await actionContext.$appdrawer.openDrawer(view, data);
        appdrawer.subscribe((result: any) => {
            if (result && Object.is(result.ret, 'OK')) {
                Object.assign(data, { srfactionparam: result.datas });
                backend();
            }
        });
        <#else>
        const view = { 
            viewname: '${srffilepath2(frontview.getCodeName())}', 
            title: '${frontview.title}', 
            height: ${frontview.getHeight()?c}, 
            width: ${frontview.getWidth()?c}, 
        };
        const appmodal = actionContext.$appmodal.openModal(view, data);
        appmodal.subscribe((result:any) => {
            if (result && Object.is(result.ret, 'OK')) {
                Object.assign(data, { srfactionparam: result.datas });
                backend();
            }
        });
        </#if>
    <#elseif item.getConfirmMsg?? && item.getConfirmMsg()??>
        actionContext.$Modal.confirm({
            title: '警告',
            content: '${item.getConfirmMsg()}',
            onOk: () => {
                backend();
            },
            onCancel: () => { }
        });
    <#else>
        backend();
    </#if>
</#if>
        
    }