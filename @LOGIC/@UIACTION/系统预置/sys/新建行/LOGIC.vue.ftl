    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof <#if item.getPSControlContainer?? && item.getPSControlContainer()??>${srfclassname('${item.getPSControlContainer().name}')}Base</#if>
     */
    public async ${item.getFullCodeName()}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        const _this: any = this;
        const data: any = {};
        if (container.newRow && container.newRow instanceof Function) {
            container.newRow(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
        } else if(xData.newRow && xData.newRow instanceof Function) {
            xData.newRow([{ ...data }], paramJO, $event, xData);
        }else{
            this.notice.error('newdata 视图处理逻辑不存在，请添加!');
        }
    }