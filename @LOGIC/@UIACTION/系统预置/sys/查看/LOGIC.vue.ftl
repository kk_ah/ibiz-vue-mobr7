    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof <#if item.getPSControlContainer?? && item.getPSControlContainer()??>${srfclassname('${item.getPSControlContainer().name}')}Base</#if>
     */
    public async ${item.getFullCodeName()}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        if (args.length === 0) {
            return;
        }
        const _this: any = this;
        if (container.opendata && container.opendata instanceof Function) {
            const data: any = { };
            <#--  if (args.length > 0) {
                Object.assign(data, { ${keyPSDEField}: args[0].${keyPSDEField} })
            }  -->
            container.opendata(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
        } else {
            this.notice.error('opendata 视图处理逻辑不存在，请添加!');
        }
    }