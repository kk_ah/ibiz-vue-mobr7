    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof <#if item.getPSControlContainer?? && item.getPSControlContainer()??>${srfclassname('${item.getPSControlContainer().name}')}Base</#if>
     */
    public async ${item.getFullCodeName()}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        <#--  const _this: any = this;
        if (!xData || !(xData.import instanceof Function)) {
            return ;
        }
        xData.import(args);  -->
        <#--  this.$notify({ type: 'danger', message: '数据导入未支持' });  -->
    }