    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof <#if item.getPSControlContainer?? && item.getPSControlContainer()??>${srfclassname('${item.getPSControlContainer().name}')}Base</#if>
     */
    public async ${item.getFullCodeName()}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        if (args.length === 0) {
            return;
        }
        const _this: any = this;
        if (container.newdata && container.newdata instanceof Function) {
            const data: any = {};
            if (args.length > 0) {
                Object.assign(data, { srfsourcekey: args[0].srfkey })
            }
            container.newdata(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
        } else if (xData && xData.copy instanceof Function) {
            const data2: any = {};
            if (args.length > 0) {
                Object.assign(data2, { srfsourcekey: args[0].srfkey })
            }
            xData.copy(data2);
        } else {
            this.notice.error('opendata 视图处理逻辑不存在，请添加!');
        }
    }