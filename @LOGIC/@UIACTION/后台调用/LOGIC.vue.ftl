<#--  后台界面行为  -->
<#ibizinclude>../../@MACRO/View.ftl</#ibizinclude>
<#ibizinclude>../../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if backend_block??>
${backend_block}
<#else>
    /**
     * ${item.getCaption()}
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName] 
     * @returns {Promise<any>}
     * @memberof <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>${srfclassname('${item.getPSAppDataEntity().getCodeName()}')}UIService</#if>
     */
    public async ${item.getFullCodeName()}(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
    <#if item.render??>
        ${item.render.code}
    <#else>
        <#--  BEGIN: 自定义确认  -->
        <#if item.getConfirmMsg?? && item.getConfirmMsg()??>
        const state: boolean = await Notice.getInstance().confirm('警告', '${item.getConfirmMsg()}');
        if (!state) {
            return Promise.reject();
        }
        </#if>
        <#--  BEGIN: 自定义确认  -->
        <#if item.getActionTarget() == 'SINGLEDATA'>
        this.notice.error('不支持单项数据');
        <#elseif item.getActionTarget() == 'MULTIDATA'>
        this.notice.error('不支持多项数据');
        <#else>
            <#-- 是否先保存目标数据start -->
            <#if item.isSaveTargetFirst()>
        const result:any = await xData.save(args,false);
        args = [result.data];
            </#if>
         <#-- 是否先保存目标数据end -->
        const _args: any[] = Util.deepCopy(args);
        const actionTarget: string | null = <#if item.getActionTarget()??>'${item.getActionTarget()}'<#else>null</#if>;
            <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
            <#assign appDataEntity = item.getPSAppDataEntity() />
                <#if item.getActionTarget() == 'SINGLEKEY' || item.getActionTarget() == 'MULTIKEY'>
                <#assign valueItem><#if item.getValueItem?? && item.getValueItem() != ''>${item.getValueItem()}<#else>${appDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}</#if></#assign>
                <#assign paramItem><#if item.getParamItem?? && item.getParamItem() != ''>${item.getParamItem()}<#else>${appDataEntity.getCodeName()?lower_case}</#if></#assign>
                <#assign textItem><#if item.getTextItem?? && item.getTextItem() != ''>${item.getTextItem()}<#else>${appDataEntity.getMajorPSAppDEField().getCodeName()?lower_case}</#if></#assign>
        Object.assign(contextJO, { ${appDataEntity.getCodeName()?lower_case}: '%${paramItem}%' });
        Object.assign(paramJO, { ${valueItem}: '%${paramItem}%' });
        Object.assign(paramJO, { ${textItem}: '%${textItem}%' });
                </#if>
            </#if>
        let context: any = this.handleContextParam(actionTarget, _args, contextJO);
        let params: any = this.handleActionParam(actionTarget, _args, paramJO);
        context = { ...container.context, ...context };
        <#-- 构建srfparentdename和srfparentkey start -->
        let parentObj: any = {
            srfparentdename: srfParentDeName ? srfParentDeName : null,
            srfparentkey: srfParentDeName ? context[srfParentDeName.toLowerCase()] : null,
        };
        Object.assign(context, parentObj);
        Object.assign(params, parentObj);
        <#-- 构建srfparentdename和srfparentkey end -->
        // 直接调实体服务需要转换的数据
        if (context && context.srfsessionid) {
            context.srfsessionkey = context.srfsessionid;
            delete context.srfsessionid;
        }
        // 导航参数
        let panelNavParam= <@getNavigateParams item />;
        let panelNavContext= <@getNavigateContext item />;
        const { context: _context, param: _params } = this.viewTool.formatNavigateParam( panelNavContext, panelNavParam, context, params, {});
            <#-- 关闭编辑视图 -->
            <#if item.isCloseEditView()>
              container.closeView(null);
            </#if>
        const backend = async () => {
            <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()?? && item.getPSAppDEMethod?? && item.getPSAppDEMethod()??>
            const curUIService: any = await this.globaluiservice.getAppEntityService('${item.getPSAppDataEntity().getCodeName()?lower_case}');
            <#if item.isShowBusyIndicator()>
            </#if>
            const response: any = await curUIService.${item.getPSAppDEMethod().getCodeName()}(_context, _params);
            <#if item.isShowBusyIndicator()>
            </#if>
            if (response && response.status === 200) {
                <#if item.getSuccessMsg?? && item.getSuccessMsg()??>
                this.notice.success('${item.getSuccessMsg()}');
                <#else>
                this.notice.success('${item.getCaption()}成功！');
                </#if>
                <#--  是否重新加载数据  -->
                <#if item.isReloadData?? && item.isReloadData()>
                if (xData && xData.refresh && xData.refresh instanceof Function) {
                    xData.refresh(args);
                }
                </#if>
                <#--  后续界面行为  -->
                <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                <#assign nextPSUIAction = item.getNextPSUIAction()/>
                const { data: result } = response;
                let _args: any[] = [];
                if (Object.is(container.$util.typeOf(result), 'array')) {
                    _args = [...result];
                } else if (Object.is(container.$util.typeOf(result), 'object')) {
                    _args = [{...result}];
                } else {
                    _args = [...args];
                }
                    <#--  BEGIN：后续界面行为  -->
                    <#if item.getNextPSUIAction?? && item.getNextPSUIAction()??>
                    <#assign nextPSUIAction = item.getNextPSUIAction()/>
                        <#if !(nextPSUIAction.getPSAppDataEntity?? && nextPSUIAction.getPSAppDataEntity()??)>
                if (this.globaluiservice.${nextPSUIAction.getFullCodeName()} && this.globaluiservice.${nextPSUIAction.getFullCodeName()} instanceof Function) {
                    this.globaluiservice.${nextPSUIAction.getFullCodeName()}(response.datas, contextJO, paramJO, $event, xData, container);
                }
                        <#elseif nextPSUIAction.getPSAppDataEntity().getId() != item.getId()>
                const nextPSUIActionUIService: any = await this.globaluiservice.getService('${nextPSUIAction.getPSAppDataEntity().getCodeName()?lower_case}_ui_action');
                if (nextPSUIActionUIService) {
                    nextPSUIActionUIService.${nextPSUIAction.getFullCodeName()}(response.datas, contextJO, paramJO, $event, xData, container);
                }
                        <#else>
                this.${nextPSUIAction.getFullCodeName()}(response.datas, contextJO, paramJO, $event, xData, container);
                        </#if>
                    </#if>
                    <#--  END：后续界面行为  -->
                </#if>
            } else {
                this.notice.error('系统异常！');
            }
            return response;
            <#else>
            this.notice.error('模型异常，应用实体方法不存在');
            </#if>
        };
            <#if item.getFrontPSAppView?? && item.getFrontPSAppView()??>
            <@outPutViewInfo dataview/>
            <#assign frontview = item.getFrontPSAppView()>
                <#--  抽屉打开  -->
                <#if frontview.getOpenMode()?index_of('DRAWER') == 0>
        const result: any = await this.openService.openPopOver(view, _context, _params);
                <#--  模态打开  -->
                <#else>
        const result: any = await this.openService.openModal(view, _context, _params);
                </#if>
        if (result && Object.is(result.ret, 'OK')) {
            Object.assign(params, { srfactionparam: result.datas });
            retrun backend();
        }
            <#else>
        return backend();
            </#if>
        </#if>
    </#if>
    }
</#if>