<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
const path = require('path');
const os = require('os');

function resolve(dir) {
    return path.join(__dirname, dir)
}

<#assign indexPageCount = 0/>
module.exports = {
    publicPath: './',
    // 去除 map 文件 1
    productionSourceMap: false,
    outputDir: process.env.VUE_APP_OUTPUTDIR,
    devServer: {
        host: '0.0.0.0',
        port: 8111,
        compress: true,
        disableHostCheck: true,
        // proxy: "http://127.0.0.1:8080/${app.getPKGCodeName()}",
        historyApiFallback: {
            rewrites: [
                <#if app.getAllRefPSAppViews()??>
                <#list app.getAllRefPSAppViews() as appview>
                <#if appview.getViewType() == "APPINDEXVIEW" && !appview.isDefaultPage()>
               // { from: /^\/${appview.getCodeName()?lower_case}$/, to: '/${appview.getCodeName()?lower_case}.html' },
                </#if>
                </#list>
                </#if>
            ]
        }
    },
    pages: {
        <#if app.getAllRefPSAppViews()??>
        <#list app.getAllRefPSAppViews() as appview>
        <#if appview.getViewType() == "APPINDEXVIEW" && !appview.isDefaultPage()>
        <#if indexPageCount == 0>
        // 新增默认首页入口
        index : {
            // page 的入口
            entry: 'src/pages/${srffilepath2(appview.getPSAppModule().getCodeName())}/${srffilepath2(appview.getCodeName())}/main.ts',
            // 模板来源
            template: 'src/template.html',
            // 在 dist/index.html 的输出
            filename: 'index.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: '${appview.getTitle()}',
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            // chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
        </#if>
        <#assign indexPageCount = indexPageCount + 1/>
        ${appview.getCodeName()?lower_case}: {
            // page 的入口
            entry: 'src/pages/${srffilepath2(appview.getPSAppModule().getCodeName())}/${srffilepath2(appview.getCodeName())}/main.ts',
            // 模板来源
            template: 'src/template.html',
            // 在 dist/index.html 的输出
            filename: '${appview.getCodeName()?lower_case}.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: '${appview.getTitle()}',
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            // chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
        </#if>
        </#list>
        </#if>
        <#if app.getDefaultPSAppView()??>
        <#assign defView = app.getDefaultPSAppView()/>
        <#assign indexPageCount = indexPageCount + 1/>
        ${defView.getCodeName()?lower_case}: {
            // page 的入口
            entry: 'src/pages/${srffilepath2(defView.getPSAppModule().getCodeName())}/${srffilepath2(defView.getCodeName())}/main.ts',
            // 模板来源
            template: 'src/template.html',
            // 在 dist/index.html 的输出
            filename: 'index.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: '${defView.getTitle()}',
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            // chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
        </#if>
    },
    // 多核打包
    parallel: os.cpus().length > 1,
    chainWebpack: (config) => {
        <#if app.getDefaultPSAppView()??>
        <#assign defView = app.getDefaultPSAppView()/>
        // 删除自动计算预加载资源
        config.plugins.delete('preload-${defView.getCodeName()?lower_case}')
        // 删除预加载资源
        config.plugins.delete('prefetch-${defView.getCodeName()?lower_case}')
        </#if>
        <#if app.getAllRefPSAppViews()??>
        <#list app.getAllRefPSAppViews() as appview>
        <#if appview.getViewType() == "APPINDEXVIEW" && !appview.isDefaultPage()>
        config.plugins.delete('preload-${appview.getCodeName()?lower_case}')
        config.plugins.delete('prefetch-${appview.getCodeName()?lower_case}')
        </#if>
        </#list>
        </#if>
        config.resolve.alias
            .set('@pages', resolve('src/pages'))
            .set('@components', resolve('src/components'))
            .set('@widgets', resolve('src/widgets'))
            .set('@engine', resolve('src/engine'))
            .set('@interface', resolve('src/interface'))
            .set('@locale', resolve('src/locale'))
            .set('@mock', resolve('src/mock'))
            .set('@codelist', resolve('src/codelist'))
            .set('@app-core', resolve('src/app-core'))
            .set('@ibiz-core', resolve('src/ibiz-core'))
            .set('@ui-service', resolve('src/ui-service'))
            .set('@global-ui-service', resolve('src/global-ui-service'))
    },
    configureWebpack: config => {
        let ForkTsCheckerPlugin; 
        if(config.plugins.length > 0){
            ForkTsCheckerPlugin = config.plugins.find(element =>{
                return  element.workersNumber && element.memoryLimit;
            })
        }
        if (Object.is(config.mode, 'production')) {
            // 最大进程数
            ForkTsCheckerPlugin.workersNumber = os.cpus().length > 4 ? 4 : os.cpus().length; // 会占用额外内存不释放，不建议开发阶段使用
            // 单个进程最大使用内存
            ForkTsCheckerPlugin.memoryLimit = 4096;
        } else {
            // 最大进程数
            // ForkTsCheckerPlugin.workersNumber = os.cpus().length > 4 ? 4 : os.cpus().length; // 会占用额外内存不释放，不建议开发阶段使用
            // 单个进程最大使用内存
            ForkTsCheckerPlugin.memoryLimit = 4096;
        }
    },
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'less',  
            patterns: [path.resolve(__dirname, 'src/styles/var.less')]
        }
    },
}