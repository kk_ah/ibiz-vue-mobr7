<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
 [
    <#list app.getAllPSAppCodeLists() as codelist>
    {
    <#if codelist.getCodeListType() == "STATIC" && codelist.getAllPSCodeItems()??>
        "srfkey": "${codelist.codeName}",
        "emptytext": "${codelist.getEmptyText()}",
        "codelisttype":"static",
        "items": [
            <#if codelist.getAllPSCodeItems()??>
            <#list codelist.getAllPSCodeItems() as codeitem>
            <#if codeitem_index gt 0>,</#if> {
                "id": "${codeitem.getValue()?j_string}",
                "label": "${codeitem.getText()?j_string}",
                "text": "${codeitem.getText()?j_string}",
                <#comment>判断是否为数值代码项</#comment>
                <#if codelist.isCodeItemValueNumber?? && codelist.isCodeItemValueNumber()>
                "value": ${codeitem.getValue()?j_string},
                <#else>
                "value": "${codeitem.getValue()?j_string}",
                </#if>
                <#if codeitem.getPSSysImage()??>
                <#assign sysimage = codeitem.getPSSysImage()/>
                <#if sysimage.getImagePath() ==  "">
                "iconcls":"${sysimage.getCssClass()}",
                <#else>
                "icon":"${sysimage.getImagePath()}",
                </#if>
                </#if>
                <#if codeitem.getColor?? && codeitem.getColor()??>
                "color": "${codeitem.getColor()?j_string}",
                </#if>
                <#if codeitem.getPSSysCss?? && codeitem.getPSSysCss()??>
                "className":"${codeitem.getPSSysCss().getCssName()}",
                </#if>
                "disabled": <#if codeitem.isDisableSelect()>true<#else>false</#if>
            }
            </#list>
            </#if>
        ]
    <#else>
        "srfkey": "${codelist.codeName}",
        "emptytext": "${codelist.getEmptyText()}",
        "codelisttype":"dynamic",
        "appdataentity":"<#if codelist.getPSAppDataEntity?? && codelist.getPSAppDataEntity()??>${codelist.getPSAppDataEntity().codeName}</#if>",
        "appdedataset":"<#if codelist.getPSAppDEDataSet?? && codelist.getPSAppDEDataSet()??>${codelist.getPSAppDEDataSet().codeName}</#if>",
        "items": []
    </#if>
    }<#if codelist_has_next>,</#if> 
    </#list>
]