<#ibiztemplate>
TARGET=PSAPPCODELIST
</#ibiztemplate>
<#if item.getCodeListType() == "DYNAMIC"> 
<#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
import ${srfclassname('${item.getPSAppDataEntity().codeName}')}Service from '../service/${srffilepath2(item.getPSAppDataEntity().codeName)}/${srffilepath2(item.getPSAppDataEntity().codeName)}-service';
</#if>
/**
 * 代码表--${item.getName()}
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}
 */
export default class ${srfclassname('${item.getCodeName()}')} {

   /**
     * 是否启用缓存
     *
     * @type boolean
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public isEnableCache:boolean = ${item.isEnableCache()?c};

    /**
     * 过期时间
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public static expirationTime:any;

    /**
     * 预定义类型
     *
     * @type string
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public predefinedType:string ='<#if item.getPredefinedType?? && item.getPredefinedType()??>${item.getPredefinedType()}</#if>';

    /**
     * 缓存超长时长
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public cacheTimeout:any = ${item.getCacheTimeout()?c};

    /**
     * 代码表模型对象
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public codelistModel:any = {
        codelistid:"${item.getCodeName()}"
    };

    /**
     * 获取过期时间
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public getExpirationTime(){
        return ${srfclassname('${item.getCodeName()}')}.expirationTime;
    }

    /**
     * 设置过期时间
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public setExpirationTime(value:any){
        ${srfclassname('${item.getCodeName()}')}.expirationTime = value; 
    }

    /**
     * 自定义参数集合
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public userParamNames:any ={
        <#if item.getUserParamNames()??>
        <#list item.getUserParamNames() as paramName>
        ${paramName}:"${item.getUserParam(paramName)}"<#if paramName_has_next>,</#if>
        </#list>
        </#if>
    }

    /**
     * 查询参数集合
     *
     * @type any
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public queryParamNames:any ={
        <#if item.getMinorSortDir()?? && item.getMinorSortPSAppDEField()??>
        sort: '${item.getMinorSortPSAppDEField().getCodeName()?lower_case},${item.getMinorSortDir()?lower_case}'
        </#if>
    }

<#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()??>
    /**
     * ${item.getPSAppDataEntity().getLogicName()}应用实体服务对象
     *
     * @type {${srfclassname('${item.getPSAppDataEntity().codeName}')}Service}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public ${item.getPSAppDataEntity().codeName?lower_case}Service: ${srfclassname('${item.getPSAppDataEntity().codeName}')}Service = new ${srfclassname('${item.getPSAppDataEntity().codeName}')}Service();
</#if>

<#-- 不为子系统代码表 -->
<#if item.isSubSysCodeList() == false>
<#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()?? && item.getPSAppDEDataSet?? && item.getPSAppDEDataSet()??> 

    /**
     * 处理数据
     *
     * @public
     * @param {any[]} items
     * @returns {any[]}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public doItems(items: any[]): any[] {
        let _items: any[] = [];
        items.forEach((item: any) => {
            let itemdata:any = {};
            Object.assign(itemdata,{id:item.<#if item.getValuePSAppDEField()??>${item.getValuePSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case}</#if>});
            Object.assign(itemdata,{value:item.<#if item.getValuePSAppDEField()??>${item.getValuePSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case}</#if>});
            Object.assign(itemdata,{text:item.<#if item.getTextPSAppDEField()??>${item.getTextPSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getMajorPSAppDEField().codeName?lower_case}</#if>});
            Object.assign(itemdata,{label:item.<#if item.getTextPSAppDEField()??>${item.getTextPSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getMajorPSAppDEField().codeName?lower_case}</#if>});
            <#if item.getPValuePSAppDEField()??>Object.assign(itemdata,{pvalue:item.${item.getPValuePSAppDEField().getCodeName()?lower_case}});</#if>
            _items.push(itemdata);
        });
        return _items;
    }

    /**
     * 获取数据项
     *
     * @param {*} context
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public getItems(context: any={}, data: any={}, isloading?: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            data = this.handleQueryParam(data);
            const promise: Promise<any> = this.${item.getPSAppDataEntity().codeName?lower_case}Service.${item.getPSAppDEDataSet().codeName}(context, data, isloading);
            promise.then((response: any) => {
                if (response && response.status === 200) {
                    const data =  response.data;
                    resolve(this.doItems(data));
                } else {
                    resolve([]);
                }
            }).catch((response: any) => {
                console.error(response);
                reject(response);
            });
        });
    }
    <#else>

    /**
     * 获取数据项
     *
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public getItems(data: any={}, isloading?: boolean): Promise<any> {
        return Promise.reject([]);
    }
    </#if>
    <#else>
    <#if item.getPSAppDataEntity?? && item.getPSAppDataEntity()?? && item.getPSAppDEDataSet?? && item.getPSAppDEDataSet()??> 

    /**
     * 获取数据项
     *
     * @param {string} context 
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public getItems(context:any = {}, data: any={}, isloading?: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            data = this.handleQueryParam(data);
            const promise: Promise<any> = this.${item.getPSAppDataEntity().codeName?lower_case}Service.${item.getPSAppDEDataSet().codeName}(context, data, isloading);
            promise.then((response: any) => {
                if (response && response.status === 200) {
                    const data =  response.data;
                    resolve(this.doItems(data));
                } else {
                    resolve([]);
                }
            }).catch((response: any) => {
                console.error(response);
                reject(response);
            });
        });
    }

    /**
     * 处理数据
     *
     * @public
     * @param {any[]} items
     * @returns {any[]}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public doItems(items: any[]): any[] {
        let _items: any[] = [];
        items.forEach((item: any) => {
            let itemdata:any = {};
            Object.assign(itemdata,{id:item.<#if item.getValuePSAppDEField()??>${item.getValuePSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case}</#if>});
            Object.assign(itemdata,{value:item.<#if item.getValuePSAppDEField()??>${item.getValuePSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case}</#if>});
            Object.assign(itemdata,{text:item.<#if item.getTextPSAppDEField()??>${item.getTextPSAppDEField().getCodeName()?lower_case}<#else>${item.getPSAppDataEntity().getMajorPSAppDEField().codeName?lower_case}</#if>});
            <#if item.getPValuePSAppDEField()??>Object.assign(itemdata,{pvalue:item.${item.getPValuePSAppDEField().getCodeName()?lower_case}});</#if>
            _items.push(itemdata);
        });
        return _items;
    }
    <#else>

    /**
     * 获取数据项
     *
     * @param {string} context 
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public getItems(context:any = {}, data: any={}, isloading?: boolean): Promise<any> {
        return Promise.reject([]);
    }    
    </#if>    
    </#if>

    /**
     * 处理查询参数
     * @param data 传入data
     * @memberof ${srfclassname('${item.getCodeName()}')}
     */
    public handleQueryParam(data:any){
        let tempData:any = data?JSON.parse(JSON.stringify(data)):{};
        if(this.userParamNames && Object.keys(this.userParamNames).length >0){
            Object.keys(this.userParamNames).forEach((name: string) => {
                if (!name) {
                    return;
                }
                let value: string | null = this.userParamNames[name];
                if (value && value.startsWith('%') && value.endsWith('%')) {
                    const key = value.substring(1, value.length - 1);
                    if (this.codelistModel && this.codelistModel.hasOwnProperty(key)) {
                        value = (this.codelistModel[key] !== null && this.codelistModel[key] !== undefined) ? this.codelistModel[key] : null;
                    } else {
                        value = null;
                    }
                }
                Object.assign(tempData, { [name]: value });
            });
        }
        Object.assign(tempData,{page: 0, size: 1000});
        if(this.queryParamNames && Object.keys(this.queryParamNames).length > 0){
            Object.assign(tempData,this.queryParamNames);
        }
        return tempData;
    }
}
</#if>