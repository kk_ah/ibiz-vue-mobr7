import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';
<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>

/**
 * 代码表服务构造器
 *
 * @export
 * @class CodeListConstructor
 */
export class CodeListServiceConstructor extends ServiceConstructorBase {

    /**
     * 初始化
     *
     * @protected
     * @memberof CodeListConstructor
     */
    protected init(): void {
<#if app.getAllPSAppCodeLists()??>
<#list app.getAllPSAppCodeLists() as codelist>
<#if codelist.getCodeListType() == "DYNAMIC">
        this.allService.set('${codelist.codeName}', () => import('@/app-core/code-list/${srffilepath2(codelist.codeName)}'));
</#if>
</#list>
</#if>
    }


}
export const codeListServiceConstructor: CodeListServiceConstructor = new CodeListServiceConstructor();