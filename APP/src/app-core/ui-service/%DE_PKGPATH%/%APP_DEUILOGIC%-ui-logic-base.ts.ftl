<#ibiztemplate>
TARGET=PSAPPDEUILOGIC
</#ibiztemplate>
import { UILogicBase } from '@/ibiz-core';

/**
 * ${item.name}界面逻辑基类
 *
 * @export
 * @class ${item.codeName}UILogicBase
 * @extends {UILogicBase}
 */
export class ${item.codeName}UILogicBase extends UILogicBase { }