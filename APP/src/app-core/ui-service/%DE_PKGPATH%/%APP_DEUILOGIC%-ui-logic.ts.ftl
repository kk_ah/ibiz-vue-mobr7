<#ibiztemplate>
TARGET=PSAPPDEUILOGIC
</#ibiztemplate>
import { ${item.codeName}UILogicBase } from './${srffilepath2(item.getCodeName())}-ui-logic-base';

/**
 * ${item.name}界面逻辑
 *
 * @export
 * @class ${item.codeName}UILogic
 * @extends {UILogicBase}
 */
export class ${item.codeName}UILogic extends ${item.codeName}UILogicBase { }
// 默认导出
export default ${item.codeName}UILogic;