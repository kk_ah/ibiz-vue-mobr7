<#ibiztemplate>
TARGET=PSAPPWFVER
</#ibiztemplate>
import { WFUIActionBase } from '@/ibiz-core';
import { Subject } from 'rxjs';

/**
 * ${item.getPSWFVersion().name}WFUI服务对象基类
 *
 * @export
 * @class ${srfclassname(item.getPSWFVersion().getCodeName())}WFUIActionBase
 * @extends {WFUIActionBase}
 */
export class ${srfclassname(item.getPSWFVersion().getCodeName())}WFUIActionBase extends WFUIActionBase {
    <#if item.getPSAppWFUIActions()??>
    <#list item.getPSAppWFUIActions() as wfUIAction>
    <#if wfUIAction.getUIActionMode() != "SYS">

    ${P.getLogicCode(wfUIAction, "LOGIC.vue").code}
    </#if>
    </#list>
    </#if>

}