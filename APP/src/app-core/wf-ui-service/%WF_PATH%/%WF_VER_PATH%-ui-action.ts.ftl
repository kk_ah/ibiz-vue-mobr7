<#ibiztemplate>
TARGET=PSAPPWFVER
</#ibiztemplate>
import ${srfclassname(item.getPSWFVersion().getCodeName())}UIServiceBase from './${item.getPSWFVersion().getCodeName()}-ui-service-base';

/**
 * ${item.getName()}UI服务对象
 *
 * @export
 * @class ${srfclassname(item.getPSWFVersion().getCodeName())}WFUIAction
 * @extends {${srfclassname(item.getPSWFVersion().getCodeName())}WFUIActionBase}
 */
export class ${srfclassname(item.getPSWFVersion().getCodeName())}WFUIAction extends ${srfclassname(item.getPSWFVersion().getCodeName())}WFUIActionBase { }
// 默认导出
export default ${srfclassname(item.getPSWFVersion().getCodeName())}WFUIAction;