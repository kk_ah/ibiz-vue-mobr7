<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';
/**
 * 实体权限服务注册中心
 *
 * @export
 * @class AuthServiceConstructor
 */
export class AuthServiceConstructor extends ServiceConstructorBase {

    /**
     * 初始化
     *
     * @protected
     * @memberof AuthServiceConstructor
     */
    protected init(): void {
<#if app.getAllPSAppDataEntities()??>
<#list app.getAllPSAppDataEntities() as curAppEntity>
        this.allService.set('${curAppEntity.getCodeName()?lower_case}', () => import('@/app-core/auth-service/${srffilepath2(curAppEntity.getCodeName())}/${srffilepath2(curAppEntity.getCodeName())}-auth-service'));
</#list>
</#if>
    }


}
export const authServiceConstructor: AuthServiceConstructor = new AuthServiceConstructor();