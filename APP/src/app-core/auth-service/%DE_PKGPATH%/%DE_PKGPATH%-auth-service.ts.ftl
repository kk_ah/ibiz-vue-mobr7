<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
import ${srfclassname('${item.getCodeName()}')}AuthServiceBase from './${srffilepath2(item.getCodeName())}-auth-service-base';


/**
 * ${de.getLogicName()}权限服务对象
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}AuthService
 * @extends {${srfclassname('${item.getCodeName()}')}AuthServiceBase}
 */
export default class ${srfclassname('${item.getCodeName()}')}AuthService extends ${srfclassname('${item.getCodeName()}')}AuthServiceBase {

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}AuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}AuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}