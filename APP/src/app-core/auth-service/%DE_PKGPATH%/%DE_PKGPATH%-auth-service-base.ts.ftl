<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
import { AuthService } from '@ibiz-core/auth-service/auth-service-base';


/**
 * ${de.getLogicName()}权限服务对象基类
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}AuthServiceBase
 * @extends {AuthService}
 */
export default class ${srfclassname('${item.getCodeName()}')}AuthServiceBase extends AuthService {

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}AuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}AuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof ${srfclassname('${item.getCodeName()}')}AuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}