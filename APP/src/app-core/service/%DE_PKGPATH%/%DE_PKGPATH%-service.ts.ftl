<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
import { Http,Util } from '@/ibiz-core/utils';
import {${srfclassname('${item.getCodeName()}')}ServiceBase} from './${srffilepath2(item.getCodeName())}-service-base';


/**
 * ${de.getLogicName()}服务对象
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}Service
 * @extends {${srfclassname('${item.getCodeName()}')}ServiceBase}
 */
export class ${srfclassname('${item.getCodeName()}')}Service extends ${srfclassname('${item.getCodeName()}')}ServiceBase {

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}Service.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}Service
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
// 默认导出
export default ${srfclassname('${item.getCodeName()}')}Service;