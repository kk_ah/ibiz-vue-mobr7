<#ibiztemplate>
TARGET=PSAPPDELOGIC
</#ibiztemplate>
import { Http,Util } from '@/ibiz-core/utils';
import {${srfclassname('${item.getCodeName()}')}LogicBase} from './${srffilepath2(item.getCodeName())}-logic-base';

/**
 * ${item.name}
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}Logic
 */
export class ${srfclassname('${item.getCodeName()}')}Logic extends ${srfclassname('${item.getCodeName()}')}LogicBase{

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}Logic
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}Logic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}

// 默认导出
export default ${srfclassname('${item.getCodeName()}')}Logic;
