<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
import { Http,Util } from '@/ibiz-core/utils';
<#--  import  {EntityService}  from '@/ibiz-core/service/entity-service-base';  -->
import  { EntityService }  from '@/ibiz-core';
<#if item.getAllPSAppDELogics()??>
<#list item.getAllPSAppDELogics() as singleLogic>
<#if !P.exists("importService", singleLogic.getId(), "")>
<#if singleLogic.getLogicHolder() == 2 || singleLogic.getLogicHolder() == 3>
import { ${srfclassname('${singleLogic.getCodeName()}')}Logic } from './${srffilepath2(singleLogic.getCodeName())}-logic';
</#if>
</#if>
</#list>
</#if>

<#-- 设置本地缓存 -->
<#macro setStore appEntity>
        <#if appEntity.getMajorPSAppDERSs()??>
        <#list appEntity.getMajorPSAppDERSs() as deReRs>
        <#-- 主实体 -->
        <#assign majorPSAppDataEntity = deReRs.getMajorPSAppDataEntity() />
        <#-- 从实体 -->
        <#assign minorPSAppDataEntity = deReRs.getMinorPSAppDataEntity() />
        <#-- 从实体支持临时数据模式为true，才设置本地缓存 start -->
        <#-- @author zpc -->
        <#-- @update 2020.7.13 15:00 -->
        <#if minorPSAppDataEntity.isEnableTempData()>
            this.tempStorage.setItem(context.srfsessionkey+'_${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}',JSON.stringify(res.data.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}?res.data.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}:[]));
        </#if>
        <#-- 从实体支持临时数据模式为true，才设置本地缓存 end -->
        </#list>
        </#if>
</#macro>

<#-- 获取本地缓存 -->
<#macro getStore appEntity>
        <#if appEntity.getMajorPSAppDERSs()??>
        <#list appEntity.getMajorPSAppDERSs() as deReRs>
        <#-- 主实体 -->
        <#assign majorPSAppDataEntity = deReRs.getMajorPSAppDataEntity() />
        <#-- 从实体 -->
        <#assign minorPSAppDataEntity = deReRs.getMinorPSAppDataEntity() />
        <#-- 从实体支持临时数据模式为true，才从前端缓存拿取数据，组装数据给后台 start -->
        <#-- @author zpc -->
        <#-- @update 2020.7.13 15:00 -->
        <#if minorPSAppDataEntity.isEnableTempData()>
        let ${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}'),'undefined')){
            ${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}') as any);
            if(${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data && ${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data.length && ${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data.length > 0){
                ${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.${minorPSAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case} = null;
                            <#-- 置空父键 -->
                            if(item.hasOwnProperty('${majorPSAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}') && item.${majorPSAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}) item.${majorPSAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case} = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case} = ${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}Data;
        </#if>
        <#-- 从实体支持临时数据模式为true，才从前端缓存拿取数据，组装数据给后台 end -->
        </#list>
        </#if>
</#macro>

<#-- 本地供数satrt -->
<#macro local_supply item singleAppMethod>
<#if singleAppMethod.getPredefinedType?? && singleAppMethod.getPredefinedType()??>
<#-- 预定义类型为索引实体start -->
<#if singleAppMethod.getPredefinedType() == "INDEXDE">
<#if singleAppMethod.getPSAppCodeList?? && singleAppMethod.getPSAppCodeList()??>
<#assign appCodeList = singleAppMethod.getPSAppCodeList() />
        let codelistModel:any = {tag:'${appCodeList.codeName}',codelistType:'${appCodeList.getCodeListType()}'};
        let res:any = await this.getCodeList(codelistModel.tag,codelistModel.codelistType,context,data);
        if(res && res.length > 0){
            res.forEach((ele:any) => {
                // 仿真返回数据集
                ele.${item.getKeyPSAppDEField().getCodeName()?lower_case} = ele.value;
                ele.${item.getMajorPSAppDEField().getCodeName()?lower_case} = ele.text
            });
        } 
        return {status:200,data:res};
</#if>
<#else>
        <@service_block item=item singleAppMethod=singleAppMethod />
</#if>
<#-- 预定义类型为索引实体end -->
</#if>
</#macro>
<#-- 本地供数end -->

<#-- 检查行为是否可以执行 start -->
<#-- @author zpc -->
<#-- @update 2020.7.13 18:00 -->
<#-- 获取测试行为的修饰符和名称 start -->
<#macro getTestActionModifier appdeAction>
<#compress>
<#if appdeAction.getTestActionMode() == 1>private</#if><#if appdeAction.getTestActionMode() == 3>public</#if>
</#compress>
</#macro>
<#-- 获取测试行为的修饰符和名称 end -->

<#-- 定义service_block start -->
<#macro service_block item singleAppMethod afterActionStr = "">
 <#if item.getPSAppDERSPathCount() gt 0 && singleAppMethod.getPSDEServiceAPIMethod?? &&  singleAppMethod.getPSDEServiceAPIMethod()??>
    <#assign singleServiceApi = singleAppMethod.getPSDEServiceAPIMethod()/>
    <#list 1..item.getPSAppDERSPathCount() as count>
        <#assign path = ''/>
        <#assign condition = ''/>
        <#list item.getPSAppDERSPath(count_index)  as deRSPath>
            <#if deRSPath.getMajorPSAppDataEntity?? && deRSPath.getMajorPSAppDataEntity()??>
                <#assign _dataEntity = deRSPath.getMajorPSAppDataEntity()/>
                <#assign condition>${condition}context.${_dataEntity.getCodeName()?lower_case} && </#assign>
                <#assign path>${path}${srfpluralize(_dataEntity.codeName)?lower_case}/<#noparse>$</#noparse>{context.${_dataEntity.getCodeName()?lower_case}}/</#assign>
            </#if>
        </#list>
        <#-- 方法类型为SELECT -->
        <#if singleAppMethod.getMethodType() == "SELECT">
        if(${condition}context.${item.getCodeName()?lower_case}){
            <#if singleServiceApi.getRequestMethod() == 'PUT' ||  singleServiceApi.getRequestMethod() == 'POST'>
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);
            <#else>
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);
            </#if><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        }
        <#-- 方法类型为FETCH -->
        <#elseif singleAppMethod.getMethodType() == "FETCH">
        if(${condition}true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/${path}${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,tempData,isloading);<#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        }
        <#-- 方法类型为FETCHTEMP -->
        <#elseif singleAppMethod.getMethodType() == "FETCHTEMP">
        if(${condition}true){
            let res:any = Http.getInstance().get(`/${path}${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);<#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        }
        <#else>
        <#-- 方法类型为DEACTION start -->
            <#if singleServiceApi.getRequestParamType() == "NONE">
            <#-- 无参数情况start -->
        if(${condition}true){
            let res:any = await Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);
            res.data.${item.getCodeName()?lower_case} = data.${item.getCodeName()?lower_case};
            <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        }
            <#-- 无参数情况end -->
            <#elseif singleServiceApi.getRequestParamType() == "FIELD">
            <#-- 指定属性情况start -->
        if(${condition}context.${item.getCodeName()?lower_case}){
            <#-- 指定属性情况start -->
            <#-- GET -->
            <#if singleServiceApi.getRequestMethod() == "GET">
            let res:any = await Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);
            <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
            <#elseif singleServiceApi.getRequestMethod() == "POST">
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);<#if afterActionStr?? && afterActionStr !="" >
            ${afterActionStr}</#if>
            return res;
            <#else>
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);<#if afterActionStr?? && afterActionStr !="" >
            ${afterActionStr}</#if>
            return res;
            </#if>
            <#-- 指定属性情况end -->
        }
            <#-- 指定属性情况start -->
            <#elseif singleServiceApi.getRequestParamType() == "ENTITY">
            <#-- 提交对象数据情况start -->
            <#if singleAppMethod.getCodeName() == 'Create'>
        if(${condition}true){
            let masterData:any = {};
            <@getStore appEntity=item />
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}`,data,isloading);
            <#if item.getMajorPSAppDERSs()??>
            <#list item.getMajorPSAppDERSs() as deReRs>
            <#-- 主实体 -->
            <#assign majorPSAppDataEntity = deReRs.getMajorPSAppDataEntity() />
            <#-- 从实体 -->
            <#assign minorPSAppDataEntity = deReRs.getMinorPSAppDataEntity() />
            this.tempStorage.setItem(tempContext.srfsessionkey+'_${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}',JSON.stringify(res.data.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}?res.data.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}:[]));
            </#list>
            </#if><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        }
            <#else>
        if(${condition}context.${item.getCodeName()?lower_case}){
            let masterData:any = {};
            <@getStore appEntity=item />
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${path}${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);
            <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        }
            </#if>
            <#-- 提交对象数据情况end -->
            <#else>
            <#-- URI参数传递情况start -->
        // URI参数传递情况未实现
            <#-- URI参数传递情况start -->
            </#if>
         <#-- 方法类型为DEACTION end -->
        </#if>
    </#list>
    </#if>
    <#if item.isMajor() && singleAppMethod.getPSDEServiceAPIMethod?? &&  singleAppMethod.getPSDEServiceAPIMethod()??>
        <#assign singleServiceApi = singleAppMethod.getPSDEServiceAPIMethod()/>
        <#-- 方法类型为SELECT -->
        <#if singleAppMethod.getMethodType() == "SELECT">
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            <#if singleServiceApi.getRequestMethod() == 'PUT' ||  singleServiceApi.getRequestMethod() == 'POST'>
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);
            <#else>
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);
            </#if><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        <#--  }  -->
        <#-- 方法类型为FETCH -->
        <#elseif singleAppMethod.getMethodType() == "FETCH">
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,tempData,isloading);<#if afterActionStr?? && afterActionStr !="" >
        ${afterActionStr}</#if>
        return res;
        <#-- 方法类型为FETCHTEMP -->
        <#elseif singleAppMethod.getMethodType() == "FETCHTEMP">
        let res:any =  Http.getInstance().get(`/${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);<#if afterActionStr?? && afterActionStr !="" >
        ${afterActionStr}</#if>
        return res;
        <#else>
        <#-- 方法类型为DEACTION start -->
            <#if singleServiceApi.getRequestParamType() == "NONE">
        <#-- 无参数情况start -->
        let res:any = await  Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);
        res.data.${item.getCodeName()?lower_case} = data.${item.getCodeName()?lower_case};
        <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
        return res;
            <#-- 无参数情况end -->
            <#elseif singleServiceApi.getRequestParamType() == "FIELD">
            <#-- 指定属性情况start -->
            <#-- GET -->
            <#if singleServiceApi.getRequestMethod() == "GET">
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            let res:any = await Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);
            <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        <#--  }  -->
            <#elseif singleServiceApi.getRequestMethod() == "POST">
            <#-- REMOVE -->
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);<#if afterActionStr?? && afterActionStr !="" >
            ${afterActionStr}</#if>
            return res;
        <#--  }    -->
            <#else>
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,isloading);<#if afterActionStr?? && afterActionStr !="" >
            ${afterActionStr}</#if>
            return res;
        <#--  }     -->
            </#if>
            <#-- 指定属性情况end -->
            <#elseif singleServiceApi.getRequestParamType() == "ENTITY">
            <#-- 提交对象数据情况start -->
        <#if singleAppMethod.getCodeName() == 'Update'>
        let masterData:any = {};
        <@getStore appEntity=item />
        Object.assign(data,masterData);
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            let res:any = await  Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);
            <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        <#--  }  -->
        <#elseif singleAppMethod.getCodeName() == 'Save'>
        let masterData:any = {};
        <@getStore appEntity=item />
        Object.assign(data,masterData);
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            let res:any = await  Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);
            <@setStore appEntity=item /><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        <#--  }  -->
        <#elseif singleAppMethod.getCodeName() == 'Create'>
        let masterData:any = {};
        <@getStore appEntity=item />
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}`,data,isloading);
        <#if item.getMajorPSAppDERSs()??>
        <#list item.getMajorPSAppDERSs() as deReRs>
        <#-- 主实体 -->
        <#assign majorPSAppDataEntity = deReRs.getMajorPSAppDataEntity() />
        <#-- 从实体 -->
        <#assign minorPSAppDataEntity = deReRs.getMinorPSAppDataEntity() />
        this.tempStorage.setItem(tempContext.srfsessionkey+'_${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}',JSON.stringify(res.data.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}?res.data.${srfpluralize(minorPSAppDataEntity.codeName)?lower_case}:[]));
        </#list>
        </#if><#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
        return res;
        <#else>
        <#--  if(context.${item.getCodeName()?lower_case}){  -->
            let res:any = Http.getInstance().${singleServiceApi.getRequestMethod()?lower_case}(`/${srfpluralize(item.codeName)?lower_case}/<#noparse>$</#noparse>{context.${item.getCodeName()?lower_case}}<#if singleServiceApi.getRequestPath()??>${singleServiceApi.getRequestPath()}</#if>`,data,isloading);<#if afterActionStr?? && afterActionStr !="" >${afterActionStr}</#if>
            return res;
        <#--  }  -->
        </#if>
            <#-- 提交对象数据情况end -->
            <#else>
            <#-- URI参数传递情况start -->
        // URI参数传递情况未实现
            <#-- URI参数传递情况start -->
            </#if>
         <#-- 方法类型为DEACTION end -->
        </#if>
    </#if>
    <#-- 嵌套成员 -->
    <#if item.isMajor() == false && !(singleAppMethod.getPSDEServiceAPIMethod?? &&  singleAppMethod.getPSDEServiceAPIMethod()??)>
     <#if singleAppMethod.getMethodType() == "SELECT">
        // ${singleAppMethod.getCodeName()} ---SELECT
        <#-- 方法类型为FETCH -->
     <#elseif singleAppMethod.getMethodType() == "FETCH">
        // ${singleAppMethod.getCodeName()} ---FETCH
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let result:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            if(result){
                return {"status":200,"data":result};
            }else{
                return {"status":200,"data":[]};
            } 
        }else{
            return {"status":200,"data":[]};
        }
        </#list>
        </#if>
        <#-- 方法类型为FETCH -->
     <#elseif singleAppMethod.getMethodType() == "FETCHTEMP">
        // ${singleAppMethod.getCodeName()} ---FETCHTEMP
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let result:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            if(result){
                return {"status":200,"data":result};
            }else{
                return {"status":200,"data":[]};
            } 
        }else{
            return {"status":200,"data":[]};
        }
        </#list>
        </#if>
     <#else>
        <#if singleAppMethod.getCodeName() == "Save">
        // 从实体Save 行为暂未实现
        <#elseif singleAppMethod.getCodeName() == "Remove">
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let result:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            if(result){
                let tempResult:any = result.filter((item:any) =>{
                    return !Object.is(item.${item.getCodeName()?lower_case},data.${item.getCodeName()?lower_case});
                })
                this.tempStorage.setItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}',JSON.stringify(tempResult));
                 return {"status":200,"data":data};
            }else{
                return {"status":500,"data":null};
            } 
        }
        </#list>
        </#if>
        <#elseif singleAppMethod.getCodeName() == "Update">
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let result:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            let tempData:Array<any> = [];
            if(result && result.length >0){
                result.forEach((item:any) => {
                    let singleData:any = {};
                    if(Object.is(item.${item.getCodeName()?lower_case},data.${item.getCodeName()?lower_case})){
                        Object.keys(item).forEach((field:any) =>{
                            singleData[field] = data[field]?data[field]:item[field];
                        })
                    }
                    if(Object.keys(singleData).length >0){
                        tempData.push(singleData);
                    }else{
                        tempData.push(item);
                    }  
                });
                this.tempStorage.setItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}',JSON.stringify(tempData));
                return {"status":200,"data":data};
            }else{
                return {"status":500,"data":null};
            }
        }
        </#list>
        </#if>
        <#elseif singleAppMethod.getCodeName() == "GetDraft">
        let tempData = {${item.getCodeName()?lower_case}:data.${item.getCodeName()?lower_case},srfsessionkey:context.srfsessionkey};
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        Object.assign(tempData,{${majorEntity.getCodeName()?lower_case}:data.${majorEntity.getCodeName()?lower_case}});
        </#list>
        </#if>
        return {"status":200,"data":tempData}; 
        <#elseif singleAppMethod.getCodeName() == "Create">
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
         if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let ${srfpluralize(item.codeName)?lower_case}:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            ${srfpluralize(item.codeName)?lower_case}.push(data);
            this.tempStorage.setItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}',JSON.stringify(${srfpluralize(item.codeName)?lower_case}));
            return {"status":200,"data":data};
        }else{
            return {"status":200,"data":{}};
        }
        </#list>
        </#if>
        <#elseif singleAppMethod.getCodeName() == "CheckKey">
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let result:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            if(result){
               let flag:boolean = false;
                result.forEach((item:any) => {
                    if(Object.is(item.${item.getCodeName()?lower_case},data.${item.getCodeName()?lower_case})){
                        flag = true;
                    }
                });
                return {"status":200,"data":flag};
            }else{
                return {"status":500,"data":null};
            } 
        }
        </#list>
        </#if>
        <#elseif singleAppMethod.getCodeName() == "Get">
        <#if item.getMinorPSAppDERSs()??>
        <#list item.getMinorPSAppDERSs() as singleDeRs>
        <#assign majorEntity = singleDeRs.getMajorPSAppDataEntity()/>
        if(context.srfsessionkey && !Object.is(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}'),'undefined')){
            let result:any = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_${srfpluralize(item.codeName)?lower_case}') as any);
            if(result){
                let tempResult:any = result.filter((item:any) =>{
                    return Object.is(item.${item.getCodeName()?lower_case},data.${item.getCodeName()?lower_case});
                })
                let tempResultData:any = tempResult.length>0?tempResult[0]:Object.assign({},data);
                return {"status":200,"data":tempResultData};
            }else{
                return {"status":500,"data":null};
            } 
        }
        </#list>
        </#if>
        <#else>
            <#list 1..item.getPSAppDERSPathCount() as count>
                <#assign path = ''/>
                <#assign condition = ''/>
                <#list item.getPSAppDERSPath(count_index)  as deRSPath>
                    <#if deRSPath.getMajorPSAppDataEntity?? && deRSPath.getMajorPSAppDataEntity()??>
                        <#assign _dataEntity = deRSPath.getMajorPSAppDataEntity()/>
                        <#assign condition>${condition}context.${_dataEntity.getCodeName()?lower_case} && </#assign>
                        <#assign path>${path}${srfpluralize(_dataEntity.codeName)?lower_case}/<#noparse>$</#noparse>{context.${_dataEntity.getCodeName()?lower_case}}/</#assign>
                    </#if>
                </#list>
        if(${condition} true){
            return Http.getInstance().post(`${path}${srfpluralize(item.codeName)?lower_case}/${singleAppMethod.getCodeName()?lower_case}`,data,isloading);
        }
            </#list>
        </#if>
     </#if>
    </#if>
</#macro>
<#-- 定义service_block end -->

<#-- 行为执行之前 start -->
<#-- @author zpc -->
<#-- @update 2020.7.14 11:00 -->
<#macro excuteBeforeAction appdeAction>
    <#if appdeAction.getBeforePSDEActionLogics?? && appdeAction.getBeforePSDEActionLogics()??>
        <#list appdeAction.getBeforePSDEActionLogics() as beforelogic>
        <#if beforelogic.getPSDELogic()?? && beforelogic.getPSDELogic().getLogicHolder?? && beforelogic.getPSDELogic().getLogicHolder()?? && (beforelogic.getPSDELogic().getLogicHolder() == 2 || beforelogic.getPSDELogic().getLogicHolder() == 3)>
        <#assign singleLogic = beforelogic.getPSDELogic() />
        let ${singleLogic.getCodeName()?lower_case}:${srfclassname('${singleLogic.getCodeName()}')}Logic = new ${srfclassname('${singleLogic.getCodeName()}')}Logic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        data = await ${singleLogic.getCodeName()?lower_case}.onExecute(context,data,isloading?true:false);
        </#if>
        </#list>
    </#if>
</#macro>
<#-- 行为执行之前 end -->

<#-- 行为执行之后 start -->
<#-- @author zpc -->
<#-- @update 2020.7.14 14:00 -->
<#macro excuteAfterAction appdeAction>
    <#if appdeAction.getAfterPSDEActionLogics?? && appdeAction.getAfterPSDEActionLogics()??>
        <#list appdeAction.getAfterPSDEActionLogics() as afterlogic>
        <#if afterlogic.getPSDELogic()?? && afterlogic.getPSDELogic().getLogicHolder?? && afterlogic.getPSDELogic().getLogicHolder()?? && (afterlogic.getPSDELogic().getLogicHolder() == 2 || afterlogic.getPSDELogic().getLogicHolder() == 3)>
        <#assign singleLogic = afterlogic.getPSDELogic() />
    <@compress single_line=true>let ${singleLogic.getCodeName()?lower_case}:${srfclassname('${singleLogic.getCodeName()}')}Logic = new ${srfclassname('${singleLogic.getCodeName()}')}Logic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(res)).data});</@compress>
            <@compress single_line=true>let ${singleLogic.getCodeName()?lower_case}Data:any = await ${singleLogic.getCodeName()?lower_case}.onExecute(context,res.data,isloading?true:false);</@compress>
            res ={status:200,data:${singleLogic.getCodeName()?lower_case}Data};
        </#if>
        </#list>
    </#if>
</#macro>
<#-- 行为执行之后 end -->


/**
 * ${de.getLogicName()}服务对象基类
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}ServiceBase
 * @extends {EntityServie}
 */
export class ${srfclassname('${item.getCodeName()}')}ServiceBase extends EntityService {

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}ServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}ServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof ${srfclassname('${item.getCodeName()}')}ServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='${item.codeName?lower_case}';
        this.APPDEKEY = '${item.getKeyPSAppDEField().getCodeName()?lower_case}';
        this.APPDENAME = '${srfpluralize(item.codeName)?lower_case}';
        this.APPDETEXT = '<#if item.getMajorPSAppDEField?? && item.getMajorPSAppDEField()??>${item.getMajorPSAppDEField().getCodeName()?lower_case}</#if>';
        this.APPNAME = '${app.getCodeName()?lower_case}';
        this.SYSTEMNAME = '${app.getPSSystem().getCodeName()?lower_case}';
    }

// 实体接口
<#-- 实体接口调用start -->
<#if item.getAllPSAppDEMethods?? && item.getAllPSAppDEMethods()??>
<#list item.getAllPSAppDEMethods() as singleAppMethod>
    <#if singleAppMethod.render??>
        ${singleAppMethod.render.code}
    <#else>
    <#if singleAppMethod.isBuiltinMethod() == false>

    /**
     * ${singleAppMethod.getCodeName()}接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${item.getCodeName()}')}ServiceBase
     */
    public async ${singleAppMethod.getCodeName()}(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
    <#if singleAppMethod.getPSDEAction()??>
    <#assign appdeAction = singleAppMethod.getPSDEAction() /> 
    <#-- 检查行为是否可以执行 start -->
    <#-- @author zpc -->
    <#-- @update 2020.7.13 16:00 -->
    <#if appdeAction.getTestActionMode?? && appdeAction.getTestActionMode()?? && (appdeAction.getTestActionMode() == 1 || appdeAction.getTestActionMode() == 3)>
        let result:any = await this.test${srfclassname('${singleAppMethod.getCodeName()}')}(context,data);
		if(!result) return;
    </#if>
    <#-- 检查行为是否可以执行 end -->

    <#-- 行为执行之前 start -->
    <#-- @author zpc -->
    <#-- @update 2020.7.14 11:00 -->
    <@excuteBeforeAction appdeAction = appdeAction />
    <#-- 行为执行之前 end -->
    <#--是否存在前端处理逻辑start -->
    <#if appdeAction.getPSDELogic?? && appdeAction.getPSDELogic()?? && (appdeAction.getActionHolder() == 2 || appdeAction.getActionHolder() == 3)   >
    <#assign appdelogic = appdeAction.getPSDELogic() />
        let appLogic:${srfclassname('${appdelogic.getCodeName()}')}Logic = new ${srfclassname('${appdelogic.getCodeName()}')}Logic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        <@excuteAfterAction appdeAction = appdeAction />
        return {status:200,data:res};
    <#else>
    <#-- 行为执行之后 start -->
    <#-- @author zpc -->
    <#-- @update 2020.7.14 14:00 -->
    <#assign afterActionStr><@excuteAfterAction appdeAction = appdeAction /></#assign>
    <@service_block item=item singleAppMethod=singleAppMethod afterActionStr = afterActionStr/>
    <#-- 行为执行之后 end -->
    </#if>
    <#--是否存在前端处理逻辑end -->
    <#else>
    <#-- 无实体行为，有接口走接口，无接口走本地供数 start -->
    <#-- @author zpc -->
    <#-- @update 2020.7.22 17:00 -->
    <@local_supply item=item singleAppMethod=singleAppMethod />
    <#-- 无实体行为，有接口走接口，无接口走本地供数 end -->
    </#if>
    }
    <#-- 检查行为是否可以执行 start -->
    <#-- @author zpc -->
    <#-- @update 2020.7.13 17:00 -->
    <#if singleAppMethod.getPSDEAction()??>
    <#assign appdeAction = singleAppMethod.getPSDEAction() />
    <#if appdeAction.getTestActionMode?? && appdeAction.getTestActionMode()?? && (appdeAction.getTestActionMode() == 1 || appdeAction.getTestActionMode() == 3)>
    
    /**
     * test${srfclassname('${singleAppMethod.getCodeName()}')}方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${item.getCodeName()}')}ServiceBase
     */
    <@getTestActionModifier appdeAction=appdeAction/> async test${srfclassname('${singleAppMethod.getCodeName()}')}(context: any = {},data: any = {}): Promise<any> {
        return true;
    }
    </#if>
    </#if>
    <#-- 检查行为是否可以执行 end -->
    </#if>
    </#if>
</#list>
</#if>
<#-- 实体接口调用end -->
}