<#ibiztemplate>
TARGET=PSAPPDELOGIC
</#ibiztemplate>
<#if item.getPSDELogicNodes()??>
    <#list item.getPSDELogicNodes() as delogicNode>
         <#if delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "DEACTION">
            <#if delogicNode.getDstPSAppDataEntity()?? && delogicNode.getDstPSAppDEAction()??>
            <#assign targetEntity = delogicNode.getDstPSAppDataEntity() />
                <#if !P.exists("importService", targetEntity.getId(), "")>
import { ${srfclassname('${targetEntity.getCodeName()}')}Service } from '@app-core/service/${srffilepath2(targetEntity.getCodeName())}/${srffilepath2(targetEntity.getCodeName())}-service';
                </#if>
            </#if>
         </#if>
    </#list>
</#if>
import { Verify } from '@/ibiz-core/utils';

<#-- 设置参数 -->
<#macro setParam logicNode>
<#if logicNode.getPSDELogicNodeParams()??>
<#list logicNode.getPSDELogicNodeParams() as logicparam>
<#if logicparam.getDstPSDELogicParam()?? && logicparam.getDstFieldName()?? && logicparam.getSrcValueType()??>
<#-- 源参数 -->
<#if logicparam.getSrcPSDELogicParam()??><#assign srcParam = logicparam.getSrcPSDELogicParam() /></#if>
<#-- 源参数属性名称 -->
<#if logicparam.getSrcFieldName()??>
<#assign srcFieldParamName = logicparam.getSrcFieldName() />
<#if srcParam?? && srcParam.getParamPSAppDataEntity?? && srcParam.getParamPSAppDataEntity()?? && srcParam.getParamPSAppDataEntity().getPSAppDEField(srcFieldParamName,true)??>
<#assign srcFieldParam=srfcaseformat(srcParam.getParamPSAppDataEntity().getPSAppDEField(srcFieldParamName,true).getCodeName()?lower_case,'l_u2lC') />
<#else>
<#assign srcFieldParam= logicparam.getSrcFieldName()?lower_case />
</#if>
</#if>
<#-- 目标参数 -->
<#assign dstParam = logicparam.getDstPSDELogicParam() />
<#-- 目标参数属性名称 -->
<#assign dstFieldParamName = logicparam.getDstFieldName() />
<#if dstParam?? && dstParam.getParamPSAppDataEntity?? && dstParam.getParamPSAppDataEntity()?? && dstParam.getParamPSAppDataEntity().getPSAppDEField(dstFieldParamName,true)??>
<#assign dstFieldParam=srfcaseformat(dstParam.getParamPSAppDataEntity().getPSAppDEField(dstFieldParamName,true).getCodeName()?lower_case,'l_u2lC') />
<#else>
<#assign dstFieldParam= logicparam.getDstFieldName()?lower_case />
</#if>
<#-- 源逻辑参数 -->
    let tempDstParam${logicparam_index}Context:any = this.paramsMap.get('${dstParam.getCodeName()}').context?this.paramsMap.get('${dstParam.getCodeName()}').context:{};
    let tempDstParam${logicparam_index}Data:any = this.paramsMap.get('${dstParam.getCodeName()}').data?this.paramsMap.get('${dstParam.getCodeName()}').data:{};
<#if logicparam.getSrcValueType() =="SRCDLPARAM">
    let tempSrcParam${logicparam_index}Data:any = this.paramsMap.get('${srcParam.getCodeName()}').data?this.paramsMap.get('${srcParam.getCodeName()}').data:{};
    <#if dstParam.getParamPSAppDataEntity?? &&  dstParam.getParamPSAppDataEntity()?? && dstFieldParam == dstParam.getParamPSAppDataEntity().getKeyPSAppDEField().getCodeName()?lower_case>
    Object.assign(tempDstParam${logicparam_index}Context,{${dstParam.getParamPSAppDataEntity().getCodeName()?lower_case}:tempSrcParam${logicparam_index}Data['${srcFieldParam}']});
    </#if>
    Object.assign(tempDstParam${logicparam_index}Data,{${dstFieldParam}:tempSrcParam${logicparam_index}Data['${srcFieldParam}']});
<#-- 应用上下文 -->
<#elseif logicparam.getSrcValueType() =="APPDATA">
    <#if dstParam.getParamPSAppDataEntity?? &&  dstParam.getParamPSAppDataEntity()?? && dstFieldParam == dstParam.getParamPSAppDataEntity().getKeyPSAppDEField().getCodeName()?lower_case>
    Object.assign(tempDstParam${logicparam_index}Context,{${dstParam.getParamPSAppDataEntity().getCodeName()?lower_case}:context['${srcFieldParam}']});
    </#if>
    Object.assign(tempDstParam${logicparam_index}Data,{${dstFieldParam}:context['${srcFieldParam}']});
<#-- 数据上下文 -->
<#elseif logicparam.getSrcValueType() =="DATACONTEXT">
    <#if dstParam.getParamPSAppDataEntity?? &&  dstParam.getParamPSAppDataEntity()?? && dstFieldParam == dstParam.getParamPSAppDataEntity().getKeyPSAppDEField().getCodeName()?lower_case>
    Object.assign(tempDstParam${logicparam_index}Context,{${dstParam.getParamPSAppDataEntity().getCodeName()?lower_case}:params['${srcFieldParam}']});
    </#if>
    Object.assign(tempDstParam${logicparam_index}Data,{${dstFieldParam}:params['${srcFieldParam}']});
<#-- 直接值 -->
<#elseif logicparam.getSrcValueType() =="SRCVALUE">
    <#if dstParam.getParamPSAppDataEntity?? &&  dstParam.getParamPSAppDataEntity()?? && dstFieldParam == dstParam.getParamPSAppDataEntity().getKeyPSAppDEField().getCodeName()?lower_case>
    Object.assign(tempDstParam${logicparam_index}Context,{${dstParam.getParamPSAppDataEntity().getCodeName()?lower_case}:${logicparam.getSrcValue()}});
    </#if>
    Object.assign(tempDstParam${logicparam_index}Data,{${dstFieldParam}:"${logicparam.getSrcValue()}"});
</#if>
    this.paramsMap.set('${dstParam.getCodeName()}',{data:tempDstParam${logicparam_index}Data,context:tempDstParam${logicparam_index}Context});
</#if>
</#list>
</#if>
</#macro>

<#-- 调用下一节点处理逻辑 -->
<#macro executeNext logicNode>
<#if logicNode.getPSDELogicLinks()??>
<#list logicNode.getPSDELogicLinks() as logiclink>
<#if logiclink.getDstPSDELogicNode()??>
<#assign nextnode = logiclink.getDstPSDELogicNode() />
<#list item.getPSDELogicLinks() as curLink >
<#if curLink.getId() == logiclink.getId()>
<#assign curIndex = curLink_index/>
</#if>
</#list>
        if(this.compute${curIndex}Cond(params)){
            return this.execute${nextnode.codeName}(context,params,isloading);   
        }
</#if>
</#list>
<#else>
        return this.paramsMap.get(this.defaultParamName).data;
</#if>
</#macro>

<#-- 获取判断条件 -->
<#macro getCond item><#if item.getLogicType() == 'GROUP'><#if item.isNotMode()>!(</#if><#if item.getPSDELogicLinkConds()??><#list item.getPSDELogicLinkConds() as subLogic><#if subLogic_index gt 0><#if item.getGroupOP() == 'AND'> && </#if><#if item.getGroupOP() == 'OR'> || </#if></#if><@getCond subLogic /></#list></#if><#if item.isNotMode()>)</#if><#elseif item.getLogicType() == 'SINGLE'>Verify.testCond(params.${item.getDstFieldName()?lower_case}, '${item.getCondOP()}', '${item.getValue()}')</#if></#macro>

/**
 * ${item.name}
 *
 * @export
 * @class ${srfclassname('${item.getCodeName()}')}LogicBase
 */
export class ${srfclassname('${item.getCodeName()}')}LogicBase {

    /**
     * 名称
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}LogicBase
     */
    private name:string ="${item.codeName}";

    /**
     * 唯一标识
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}LogicBase
     */
    private id:string = "${item.id}";

    /**
     * 默认参数名称
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}LogicBase
     */
    private defaultParamName:string = "${item.getDefaultParamName()}";

    /**
     * 参数集合
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}LogicBase
     */
    private paramsMap:Map<string,any> = new Map();

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}LogicBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}LogicBase
     */
    constructor(opts: any = {}) {
        this.initParams(opts);
    }

    /**
     * 初始化参数集合
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}LogicBase
     */
    public initParams(opts:any){
    <#if item.getPSDELogicParams?? && item.getPSDELogicParams()??>
    <#list item.getPSDELogicParams() as logicParam>
        <#if logicParam.isDefault()>
        this.paramsMap.set('${logicParam.getCodeName()}',opts);
        <#else>
        this.paramsMap.set('${logicParam.getCodeName()}',{});
        </#if>
    </#list>
    </#if>
    }

<#if item.getPSDELogicLinks()??>
<#list item.getPSDELogicLinks() as curLink>

    /**
     * 计算${curLink_index}节点结果
     * 
     * @param params 传入参数
     */
    public compute${curLink_index}Cond(params:any):boolean{
    <#if curLink.getPSDELogicLinkGroupCond()??>
        <#assign linkCond = curLink.getPSDELogicLinkGroupCond() />
        if(<@getCond linkCond />){
           return true;
        }else{
            return false;
        }
    <#else>
        return true;
    </#if>  
    }
</#list>
</#if>

    /**
     * 执行逻辑
     * 
     * @param context 应用上下文
     * @param params 传入参数
     */
    public onExecute(context:any,params:any,isloading:boolean){
        return this.execute${item.getStartPSDELogicNode().codeName}(context,params,isloading);
    }

<#if item.getPSDELogicNodes()??>
    <#list item.getPSDELogicNodes() as delogicNode>

    <#-- 开始节点 -->
    <#if delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "BEGIN">
    /**
    * ${delogicNode.getName()}
    * 
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        //开始节点
        <@setParam delogicNode />
        <@executeNext delogicNode />
    }
    <#-- 行为处理节点 -->
    <#elseif delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "DEACTION">
    /**
    * ${delogicNode.getName()}
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        // 行为处理节点
        <@setParam delogicNode  />
        <#if delogicNode.getDstPSAppDataEntity()?? && delogicNode.getDstPSAppDEAction()??>
        <#assign targetEntity = delogicNode.getDstPSAppDataEntity() />
        <#assign deAction = delogicNode.getDstPSAppDEAction() />
        let result: any;
        <#if delogicNode.getDstPSDELogicParam()??>
        let actionParam:any = this.paramsMap.get('${delogicNode.getDstPSDELogicParam().getCodeName()}');
        <#else>
        let actionParam:any = this.paramsMap.get(this.defaultParamName);
        </#if>
        const targetService:${srfclassname('${targetEntity.getCodeName()}')}Service = new ${srfclassname('${targetEntity.getCodeName()}')}Service();
        if (targetService['${deAction.getCodeName()}'] && targetService['${deAction.getCodeName()}'] instanceof Function) {
            result = await targetService['${deAction.getCodeName()}'](actionParam.context,actionParam.data, false);
        }
        if(result && result.status == 200){
            Object.assign(actionParam.data,result.data);
                <@executeNext delogicNode />
        }
        </#if>
    }
    <#-- 准备参数节点 -->
    <#elseif delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "PREPAREPARAM">
    /**
    * ${delogicNode.getName()}
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        // 准备参数节点
            <@setParam delogicNode />
            <@executeNext delogicNode />
    }
    <#-- 开始流程节点 -->
    <#elseif delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "STARTWF">
    /**
    * ${delogicNode.getName()}
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        // 开始流程节点
        return this.paramsMap.get(this.defaultParamName).data;
    }
    <#-- 异常捕获节点  -->
    <#elseif delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "THROWEXCEPTION">
    /**
    * ${delogicNode.getName()}
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        // 异常捕获节点
        return this.paramsMap.get(this.defaultParamName).data;
    }
    <#-- 前端插件  -->
    <#elseif delogicNode.getLogicNodeType()?? && delogicNode.getLogicNodeType() == "PFPLUGIN">
    /**
    * ${delogicNode.getName()}
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        // 前端插件
        return this.paramsMap.get(this.defaultParamName).data;
    }
    <#else>
    /**
    * ${delogicNode.getName()}
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async execute${delogicNode.codeName}(context:any,params:any,isloading:boolean){
        // ${delogicNode.getLogicNodeType()}暂未支持
        console.log("${delogicNode.getLogicNodeType()}暂未支持");
        return this.paramsMap.get(this.defaultParamName).data;
    }
    </#if>
    </#list>
</#if>


}