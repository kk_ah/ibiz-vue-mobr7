<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';

/**
 * 应用实体服务
 *
 * @export
 * @class AppEntityServiceConstructor
 * @extends {ServiceConstructorBase}
 */
export class AppEntityServiceConstructor extends ServiceConstructorBase {
<#if app.getAllPSAppDataEntities?? && app.getAllPSAppDataEntities()??>

    /**
     * 初始化
     *
     * @protected
     * @memberof AppEntityServiceConstructor
     */
    protected init(): void {
<#list app.getAllPSAppDataEntities() as appEntity>
        this.allService.set('${appEntity.getCodeName()?lower_case}', () => import('@/app-core/service/${srffilepath2(appEntity.getCodeName())}/${srffilepath2(appEntity.getCodeName())}-service'));
</#list>
    }
</#if>

}

/**
 * 应用实体服务构造器
 */
export const appEntityServiceConstructor: AppEntityServiceConstructor = new AppEntityServiceConstructor();