import { CounterService } from '@/ibiz-core/counter/counter-service-base';
<#ibiztemplate>
TARGET=PSAPPCOUNTER
</#ibiztemplate>
/**
 * ${item.getName()}计数器服务对象基类
 *
 * @export
 * @class ${item.getCodeName()}CounterServiceBase
 */
export default class ${srfclassname('${item.getCodeName()}')}CounterServiceBase extends CounterService {

    /**
     * 当前计数器数据对象
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}CounterServiceBase
     */
    public counterData:any ={};

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}CounterServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}CounterServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
        this.initCounterData();
        setInterval(() => {
            this.fetchCounterData();
        }, <#if item.getTimer()??>${item.getTimer()?c}<#else>6000</#if>);
    }

    /**
     * 初始化当前计数器数据对象
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}CounterServiceBase
     */
    public initCounterData(){
        this.fetchCounterData();
    }

    /**
     * 查询数据
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}CounterServiceBase
     */
    public async fetchCounterData(){
        this.counterData = {
            item1:parseInt((Math.random()*10)+''),
            item2:parseInt((Math.random()*100)+''),
            item3:parseInt((Math.random()*100)+''),
            item4:parseInt((Math.random()*100)+''),
            item5:parseInt((Math.random()*100)+''),
            item6:parseInt((Math.random()*100)+''),
            item7:parseInt((Math.random()*100)+''),
            item8:parseInt((Math.random()*100)+''),
            item9:parseInt((Math.random()*100)+''),
            item10:parseInt((Math.random()*100)+'')
        }
    }

    /**
     * 刷新数据
     *
     * @memberof ${srfclassname('${item.getCodeName()}')}CounterServiceBase
     */
    public async refreshData(){
        const res = await this.fetchCounterData();
        return res;
    }

}