<#ibiztemplate>
TARGET=PSAPPCOUNTER
</#ibiztemplate>
import ${srfclassname('${item.getCodeName()}')}CounterServiceBase from './${srffilepath2(item.getCodeName())}-counter-base';

/**
 * ${item.getName()}计数器服务对象
 *
 * @export
 * @class ${item.getCodeName()}CounterService
 */
export default class ${srfclassname('${item.getCodeName()}')}CounterService extends ${srfclassname('${item.getCodeName()}')}CounterServiceBase {

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}CounterService.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}CounterService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}