<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';

/**
 * 计数器服务注册中心
 *
 * @export
 * @class CounterServiceConstructor
 * @extends {ServiceConstructorBase}
 */
export class CounterServiceConstructor extends ServiceConstructorBase {

    /**
     * 初始化
     *
     * @protected
     * @memberof CounterServiceConstructor
     */
    protected init(): void {
<#if app.getAllPSAppCounters()??>
<#list app.getAllPSAppCounters() as counter>
        this.allService.set('${counter.codeName?lower_case}', () => import('@/app-core/counter/${srffilepath2(counter.getCodeName())}/${srffilepath2(counter.getCodeName())}-counter'));
</#list>
</#if>
    }

}
/**
 * 计数器服务构造器
 */
export const counterServiceConstructor: CounterServiceConstructor = new CounterServiceConstructor();