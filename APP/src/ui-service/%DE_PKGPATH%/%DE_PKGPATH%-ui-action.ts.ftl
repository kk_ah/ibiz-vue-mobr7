<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
import ${srfclassname(item.getCodeName())}UIActionBase from './${srffilepath2(item.getCodeName())}-ui-action-base';

/**
 * ${item.getLogicName()}UI服务对象
 *
 * @export
 * @class ${srfclassname(item.getCodeName())}UIAction
 * @extends {${srfclassname(item.getCodeName())}UIActionBase}
 */
export class ${srfclassname(item.getCodeName())}UIAction extends ${srfclassname(item.getCodeName())}UIActionBase { }
// 默认导出
export default ${srfclassname(item.getCodeName())}UIAction;
