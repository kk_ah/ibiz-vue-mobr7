<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
<#macro getMainStateOPPrivs mainState >
<#compress>
<#if mainState.getPSDEMainStateOPPrivs?? && mainState.getPSDEMainStateOPPrivs()??>
{<#list mainState.getPSDEMainStateOPPrivs() as stateOPPriv><#if stateOPPriv.getPSDEOPPriv?? && stateOPPriv.getPSDEOPPriv()??>'${stateOPPriv.getName()}':<#if mainState.isOPPrivAllowMode?? && mainState.isOPPrivAllowMode()?? && mainState.isOPPrivAllowMode()>1<#else>0</#if>,</#if></#list>}
<#else>
{}
</#if>
</#compress>
</#macro>
<#macro getDataEntityMainStateOPPrivs mainState dataEntity>
<#compress>
<#if dataEntity.getAllPSDEOPPrivs?? && dataEntity.getAllPSDEOPPrivs()??>
{<#list dataEntity.getAllPSDEOPPrivs() as mainStateOPPriv>'${mainStateOPPriv.getName()}':<#if mainState.isOPPrivAllowMode()>0<#else>1</#if><#if mainStateOPPriv_has_next>,</#if></#list>}
<#else>
{}
</#if>
</#compress>
</#macro>
import ${srfclassname('${item.getCodeName()}')}Service from '@/app-core/service/${srffilepath2(item.getCodeName())}/${srffilepath2(item.getCodeName())}-service';
import ${srfclassname('${item.getCodeName()}')}AuthService from '@/app-core/auth-service/${srffilepath2(item.getCodeName())}/${srffilepath2(item.getCodeName())}-auth-service';
<#if item.getAllPSAppDEUIActions()??>
<#list item.getAllPSAppDEUIActions() as appdeUIAction>
<#if appdeUIAction.getPSAppDataEntity()??>
<#if !P.exists("importService", appdeUIAction.getPSAppDataEntity().getId(), "")>
<#if item.getId() != appdeUIAction.getPSAppDataEntity().getId() >
import ${srfclassname('${appdeUIAction.getPSAppDataEntity().getCodeName()}')}Service from '@/app-core/service/${srffilepath2(appdeUIAction.getPSAppDataEntity().getCodeName())}/${srffilepath2(appdeUIAction.getPSAppDataEntity().getCodeName())}-service';
</#if>
</#if>
</#if>
</#list>
</#if>
<#if item.getPSAppWF()?? && item.getPSAppWF().getPSWorkflow()??>
<#assign workflow = item.getPSAppWF().getPSWorkflow()/>
<#if workflow.getPSWFDEs()??>
<#list workflow.getPSWFDEs() as singleWFDE>
<#if singleWFDE.getWFProxyMode()?? && singleWFDE.getWFProxyMode() == 2 && singleWFDE.getPSDataEntity().getId() == de.getId()>
<#assign curWorkFlow = singleWFDE/>
</#if>
</#list>
</#if>
</#if>
import EntityUIActionBase from '@/utils/ui-service-base/entity-ui-action-base';
import { Util, Loading } from '@/ibiz-core/utils';
import { Notice } from '@/utils';
import { Environment } from '@/environments/environment';
/**
 * ${item.getLogicName()}UI服务对象基类
 *
 * @export
 * @class ${srfclassname(item.getCodeName())}UIActionBase
 * @extends {UIActionBase}
 */
export default class ${srfclassname(item.getCodeName())}UIActionBase extends EntityUIActionBase {

    /**
     * 是否支持工作流
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */
    public isEnableWorkflow:boolean = false;

    /**
     * 当前UI服务对应的数据服务对象
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */
    public dataService:${srfclassname('${item.getCodeName()}')}Service = new ${srfclassname('${item.getCodeName()}')}Service();

    /**
     * 所有关联视图
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */ 
    public allViewMap: Map<string, Object> = new Map();

    /**
     * 状态值
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */ 
    public stateValue: number = <#if curWorkFlow?? && curWorkFlow.getEntityWFState()??>${curWorkFlow.getEntityWFState()}<#else>0</#if>;

    /**
     * 状态属性
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */ 
    public stateField: string = "<#if curWorkFlow?? && curWorkFlow.getWFStatePSDEField()??>${curWorkFlow.getWFStatePSDEField().getCodeName()?lower_case}</#if>";

    /**
     * 主状态属性集合
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */  
    public mainStateFields:Array<any> = [<#if item.getPSDataEntity?? && item.getPSDataEntity()?? && item.getPSDataEntity().getDEMainStateDEFields()??><#list item.getPSDataEntity().getDEMainStateDEFields() as demsField>'${demsField.getCodeName()?lower_case}'<#if demsField_has_next>,</#if></#list></#if>];

    /**
     * 主状态集合Map
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */  
    public allDeMainStateMap:Map<string,string> = new Map();

    /**
     * 主状态操作标识Map
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */ 
    public allDeMainStateOPPrivsMap:Map<string,any> = new Map();

    /**
     * Creates an instance of  ${srfclassname('${item.getCodeName()}')}UIServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */
    constructor(opts: any = {}) {
        super();
        this.authService = new ${srfclassname('${item.getCodeName()}')}AuthService(opts);
        this.initViewMap();
        this.initDeMainStateMap();
        this.initDeMainStateOPPrivsMap();
    }

    /**
     * 初始化视图Map
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */  
    public initViewMap(){
    <#if item.getAllPSAppViews()??>
    <#list item.getAllPSAppViews() as singleView>
        this.allViewMap.set('<#if singleView.getFuncViewMode()??>${singleView.getFuncViewMode()}</#if>:<#if singleView.getFuncViewParam()??>${singleView.getFuncViewParam()}</#if>',{viewname:'${singleView.getPSDEViewCodeName()?lower_case}',srfappde:'${srfpluralize(item.codeName)?lower_case}'});
    </#list>
    </#if>
    }

    /**
     * 初始化主状态集合
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */  
    public initDeMainStateMap(){
        <#if item.getPSDataEntity?? && item.getPSDataEntity()?? && item.getPSDataEntity().getAllPSDEMainStates()??>
        <#list item.getPSDataEntity().getAllPSDEMainStates() as demState>
        <#if demState.getMSTag()??>
        this.allDeMainStateMap.set('${demState.getMSTag()}','${demState.getMSTag()}');
        </#if>
        </#list>
        </#if>
    }

    /**
     * 初始化主状态操作标识
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */  
    public initDeMainStateOPPrivsMap(){
        <#if item.getPSDataEntity?? && item.getPSDataEntity()?? && item.getPSDataEntity().getAllPSDEMainStates()??>
        <#list item.getPSDataEntity().getAllPSDEMainStates() as demState>
        <#if demState.getMSTag()??>
        this.allDeMainStateOPPrivsMap.set('${demState.getMSTag()}',Object.assign(<@getDataEntityMainStateOPPrivs mainState=demState dataEntity=item.getPSDataEntity() />,<@getMainStateOPPrivs mainState=demState />));
        </#if>
        </#list>
        </#if>
    }

    <#if item.getAllPSAppDEUIActions()??>
    <#list item.getAllPSAppDEUIActions() as appdeUIAction>
${P.getLogicCode(appdeUIAction, "LOGIC.vue").code}
    </#list>
    </#if>

    /**
     * 获取指定数据的重定向页面
     * 
     * @param srfkey 数据主键
     * @param isEnableWorkflow  重定向视图是否需要处理流程中的数据
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */
    public async getRDAppView(srfkey:string,isEnableWorkflow:boolean){
        this.isEnableWorkflow = isEnableWorkflow;
        // 进行数据查询
        let result:any = await this.dataService.Get({${item.getCodeName()?lower_case}:srfkey});
        const curData:any = result.data;
        //判断当前数据模式,默认为true，todo
        const iRealDEModel:boolean = true;

        let bDataInWF:boolean = false;
		let bWFMode:any = false;
		// 计算数据模式
		if (this.isEnableWorkflow) {
			bDataInWF = await this.dataService.testDataInWF({stateValue:this.stateValue,stateField:this.stateField},curData);
			if (bDataInWF) {
				bDataInWF = true;
				bWFMode = await this.dataService.testUserExistWorklist(null,curData);
			}
        }
        let strPDTViewParam:string = await this.getDESDDEViewPDTParam(curData, bDataInWF, bWFMode);
        //若不是当前数据模式，处理strPDTViewParam，todo

        //查找视图

        //返回视图
        return this.allViewMap.get(strPDTViewParam);
    }

    /**
	 * 获取实际的数据类型
     * 
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
	 */
	public getRealDEType(entity:any){

    }

    /**
     * 获取实体单数据实体视图预定义参数
     * 
     * @param curData 当前数据
     * @param bDataInWF 是否有数据在工作流中
     * @param bWFMode   是否工作流模式
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */
    public async getDESDDEViewPDTParam(curData:any, bDataInWF:boolean, bWFMode:boolean){
        let strPDTParam:string = '';
		if (bDataInWF) {
			// 判断数据是否在流程中
            <#if app.getAllPSAppWFVers?? && app.getAllPSAppWFVers()??>
            <#list app.getAllPSAppWFVers() as appWfVer>
            <#if appWfVer.getPSAppWF()?? && appWfVer.getPSAppWF().getPSWorkflow()??>
            <#assign workflow = appWfVer.getPSAppWF().getPSWorkflow() />
            <#if workflow.getPSWFDEs()??>
            <#list workflow.getPSWFDEs() as singleWFDE>
            <#if singleWFDE.getWFProxyMode()?? && singleWFDE.getWFProxyMode() == 2 && singleWFDE.getPSDataEntity().getId() == de.getId()>
            let strPDTParam:string = await this.get${appWfVer.getCodeName()}EditViewPDTParam(curData, bWFMode);
			if (strPDTParam) return strPDTParam;
            </#if>
            </#list>
            </#if>
            </#if>
            </#list>
            </#if>
        }
        //多表单，todo
        const multiFormDEField:string|null =<#if de.getFormTypePSDEField()??>"${de.getFormTypePSDEField().getCodeName()?lower_case}"<#else>null</#if>;

        if (multiFormDEField) {
			const objFormValue:string = curData[multiFormDEField];
			if(!Environment.isAppMode){
				return 'MOBEDITVIEW:'+objFormValue;
			}
			return 'EDITVIEW:'+objFormValue;
        }
		if(!Environment.isAppMode){
            if(this.getDEMainStateTag(curData)){
                <#noparse>return `MOBEDITVIEW:MSTAG:${ this.getDEMainStateTag(curData)}`;</#noparse>
            }
			return 'MOBEDITVIEW:';
        }
        if(this.getDEMainStateTag(curData)){
            <#noparse>return `EDITVIEW:MSTAG:${ this.getDEMainStateTag(curData)}`;</#noparse>
        }
		return 'EDITVIEW:';
    }
<#if app.getAllPSAppWFVers?? && app.getAllPSAppWFVers()??>
<#list app.getAllPSAppWFVers() as appWfVer>
<#if appWfVer.getPSAppWF()?? && appWfVer.getPSAppWF().getPSWorkflow()??>
<#assign workflow = appWfVer.getPSAppWF().getPSWorkflow() />
<#if workflow.getPSWFDEs()??>
<#list workflow.getPSWFDEs() as singleWFDE>
<#if singleWFDE.getWFProxyMode()?? && singleWFDE.getWFProxyMode() == 2 && singleWFDE.getPSDataEntity().getId() == de.getId()>
<#assign curWFDE = singleWFDE/>
<#if appWfVer.getPSWFVersion()?? && appWfVer.getPSWFVersion().getWFVersion()??>
<#assign wfversion>${appWfVer.getPSWFVersion().getWFVersion()}</#assign>
</#if>

    /**
     * 获取指定数据的流程数据界面预置参数
     * 
     * @param curData 当前数据
     * @param bWorkMode 是否为工作模式
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */
    public async get${appWfVer.getCodeName()}EditViewPDTParam(curData:any, bWorkMode:boolean){
        // 判断数据是否在流程中
        let bWorkFlow:boolean =await this.dataService.testDataInWF({stateValue:this.stateValue,stateField:this.stateField},curData);
        if (bWorkFlow) {
            // 多表单todo
            let multiFormValue:string|null = null;
            
            //工作流
			let wfStepValue:any = curData['<#if curWFDE.getWFStepPSDEField()?? && curWFDE.getWFStepPSDEField().getCodeName()??>${curWFDE.getWFStepPSDEField().getCodeName()?lower_case}<#else>wfstep</#if>'];
			let nVer:number = ${wfversion};
			if(!Environment.isAppMode){
				if (multiFormValue == null) {
					if (bWorkMode) {
						return  "MOBWFEDITVIEW:${curWFDE.getCodeName()}:" +((nVer == 1) ? "" : nVer) +"W:"+ wfStepValue;
					} else {
						return "MOBWFEDITVIEW:${curWFDE.getCodeName()}:D";
					}
				} else {
					if (bWorkMode) {
						return "MOBWFEDITVIEW:" + multiFormValue + "${curWFDE.getCodeName()}:" + ((nVer == 1) ? "" : nVer) +"W:"+ wfStepValue;
					} else {
						return "MOBWFEDITVIEW:" + multiFormValue + "${curWFDE.getCodeName()}:D";
					}
				}
			}
			else{
				if (multiFormValue == null) {
					if (bWorkMode) {
						return "WFEDITVIEW:${curWFDE.getCodeName()}:" + ((nVer == 1) ? "" : nVer) +"W:"+ wfStepValue;
					} else {
						return "WFEDITVIEW:${curWFDE.getCodeName()}:D";
					}
				} else {
					if (bWorkMode) {
						return "WFEDITVIEW:" + multiFormValue + "${curWFDE.getCodeName()}:" + ((nVer == 1) ? "" : nVer) +"W:"+ wfStepValue;
					} else {
						return "WFEDITVIEW:" + multiFormValue + "${curWFDE.getCodeName()}:D";
					}
				}
			}
        }
        return '';
    }
</#if>
</#list>
</#if>
</#if>
</#list>
</#if>

    /**
     * 获取数据对象的主状态标识
     * 
     * @param curData 当前数据
     * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
     */  
    public getDEMainStateTag(curData:any){
        if(this.mainStateFields.length === 0) return null;

        this.mainStateFields.forEach((singleMainField:any) =>{
            if(!(singleMainField in curData)){
                <#noparse>console.warn(`当前数据对象不包含属性${singleMainField}，可能会发生错误`);</#noparse>
            }
        })
        for (let i = 0; i <= 1; i++) {
            let strTag:string = (curData[this.mainStateFields[0]])?(i == 0) ? curData[this.mainStateFields[0]] : "":"";
            if (this.mainStateFields.length >= 2) {
                for (let j = 0; j <= 1; j++) {
                    <#noparse>let strTag2:string = (curData[this.mainStateFields[1]])?`${strTag}__${(j == 0) ? curData[this.mainStateFields[1]] : ""}`:strTag;</#noparse>
                    if (this.mainStateFields.length >= 3) {
                        for (let k = 0; k <= 1; k++) {
                            <#noparse>let strTag3:string = (curData[this.mainStateFields[2]])?`${strTag2}__${(k == 0) ? curData[this.mainStateFields[2]] : ""}`:strTag2;</#noparse>
                            // 判断是否存在
                            return this.allDeMainStateMap.get(strTag3);
                        }
                    }else{
                        return this.allDeMainStateMap.get(strTag2);
                    }
                }
            }else{
                return this.allDeMainStateMap.get(strTag);
            }
        }
        return null;
    }

    /**
    * 获取数据对象当前操作标识
    * 
    * @param data 当前数据
    * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
    */  
   public getDEMainStateOPPrivs(data:any){
        if(this.getDEMainStateTag(data)){
            return this.allDeMainStateOPPrivsMap.get((this.getDEMainStateTag(data) as string));
        }else{
            return null;
        }
   }

    /**
    * 获取数据对象所有的操作标识
    * 
    * @param data 当前数据
    * @memberof  ${srfclassname('${item.getCodeName()}')}UIServiceBase
    */ 
   public getAllOPPrivs(data:any){
       return this.authService.getOPPrivs(this.getDEMainStateOPPrivs(data));
   }

}