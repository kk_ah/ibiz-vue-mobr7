<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

const datas: Array<any> = [
    <#if app.getAllPSAppCodeLists()??>
    <#list app.getAllPSAppCodeLists() as codelist>
    {
    <#if codelist.getCodeListType() == "STATIC" && codelist.getAllPSCodeItems()??>
        srfkey: '${codelist.codeName}',
        emptytext: '${codelist.getEmptyText()}',
        "codelisttype":"static",
        items: [
            <#if codelist.getAllPSCodeItems()??>
            <#list codelist.getAllPSCodeItems() as codeitem>
            {
                id: '${codeitem.getValue()?j_string}',
                label: '${codeitem.getText()?j_string}',
                text: '${codeitem.getText()?j_string}',
                <#comment>判断是否为数值代码项</#comment>
                <#if codelist.isCodeItemValueNumber?? && codelist.isCodeItemValueNumber()>
                value: ${codeitem.getValue()?j_string},
                <#else>
                value: '${codeitem.getValue()?j_string}',
                </#if>
                disabled: <#if codeitem.isDisableSelect()>true<#else>false</#if>,
            },
            </#list>
            </#if>
        ]
    <#else>
        "srfkey": "${codelist.codeName}",
        "emptytext": "${codelist.getEmptyText()}",
        "codelisttype":"dynamic",
        "appdataentity":"<#if codelist.getPSAppDataEntity?? && codelist.getPSAppDataEntity()??>${codelist.getPSAppDataEntity().codeName}</#if>",
        "appdedataset":"<#if codelist.getPSAppDEDataSet?? && codelist.getPSAppDEDataSet()??>${codelist.getPSAppDEDataSet().codeName}</#if>",
        "items": []
    </#if>
    }<#if codelist_has_next>,</#if>
    </#list>
    </#if>
];

// 获取全部数组
mock.onGet('./assets/json/data-dictionary.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, datas, config.headers, config];
});
