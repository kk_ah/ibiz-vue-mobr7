{
	id: '${item.getId()?j_string}',
	name: '${item.getName()?j_string}',
	text: '${item.getText()?j_string}',
	type: '<#if item.getItemType()??>${item.getItemType()?j_string}<#else></#if>',
	counterid: '<#if item.getCounterId()??>${item.getCounterId()?j_string}<#else></#if>',
	tooltip: '<#if item.getTooltip()??>${item.getTooltip()?j_string}</#if>',
	expanded: <#if item.isExpanded()>true<#else>false</#if>,
	separator: <#if item.isSeperator()>true<#else>false</#if>,
	hidden: <#if item.isHidden()>true<#else>false</#if>,
	hidesidebar: <#if item.isHideSideBar()>true<#else>false</#if>,
	opendefault: <#if item.isOpenDefault()>true<#else>false</#if>,
<#if item.getPSSysImage()??>
	iconcls: '${item.getPSSysImage().getCssClass()}',
	icon: '${item.getPSSysImage().getImagePath()}',
<#else>
	iconcls: '',
	icon: '',
</#if>
<#if item.getPSSysCss()??>
    textcls: '${item.getPSSysCss().getCssName()}',
<#else>
	textcls: '',
</#if>		
	<#if item.getPSAppFunc()??>
	appfunctag: '${item.getPSAppFunc().codeName}',
	<#else>
	appfunctag: '',
	</#if>
	resourcetag: '<#if item.getAccessKey()??>${item.getAccessKey()}</#if>',
<#if item.getPSAppMenuItems()??>
	items: [
		<#list item.getPSAppMenuItems() as child>
		${P.getPartCode(child,"ITEM").code},
		</#list>
	],
</#if>
}