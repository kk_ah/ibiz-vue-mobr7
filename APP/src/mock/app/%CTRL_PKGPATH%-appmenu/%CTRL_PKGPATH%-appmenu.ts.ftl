<#ibiztemplate>
TARGET=PSAPPVIEWCTRL_APPMENU
</#ibiztemplate>
import { MockAdapter } from '@/mock/mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'
const Random = Mock.Random;

// 获取应用数据
mock.onGet('v7/${srffilepath2(ctrl.codeName)}${ctrl.getControlType()?lower_case}').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, {
        name: '${ctrl.getName()?j_string}',
        <#if ctrl.getPSAppMenuItems()??>
        items:  [
            <#list ctrl.getPSAppMenuItems() as child>
            ${P.getPartCode(child,"ITEM").code},
            </#list>
        ],
    </#if>
    }];
});

