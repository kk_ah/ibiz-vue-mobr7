<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
// 预置 api 接口对象
import './appdata/appdata';
import './codelist/codelist';
import './login/login';
import './upload/upload';
<#--  
// 应用级部件接口对象
<#list app.getAllPSAppMenuModels() as menu>
import './app/${srffilepath2(menu.codeName)}-${menu.getControlType()?lower_case}/${srffilepath2(menu.codeName)}-${menu.getControlType()?lower_case}';
</#list>  -->

// 实体级接口对象
<#list app.getAllPSAppDataEntities() as dataEntitey>
import './entity/${srffilepath2(dataEntitey.getCodeName())}s/${srffilepath2(dataEntitey.getCodeName())}s';
</#list>