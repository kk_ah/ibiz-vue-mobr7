<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
import qs from 'qs';
import { MockAdapter } from '@/mock/mock-adapter';
const mock = MockAdapter.getInstance();
<#--  日志方法: BEGIN  -->
<#macro startLog appMethod>
    console.groupCollapsed("实体:${item.codeName?lower_case} 方法: ${appMethod.getCodeName()}");
    console.table({url:config.url, method: config.method, data:config.data});
</#macro>
<#macro endLog appMethod dataName>
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(${dataName});
    console.groupEnd();
    console.groupEnd();
</#macro>
<#--  日志方法: END  -->

<#--  输出排序方法: BEGIN -->
<#--  排序宏  -->
<#macro outputSort priority position>
  <#assign outputSort_nested><#nested></#assign>
  <#if !outputSort_maxPriority?has_content>
    <#assign outputSort_maxPriority = priority>
  <#else>
    <#if priority gt outputSort_maxPriority >
      <#assign outputSort_maxPriority = priority>
    </#if>
  </#if>
  <#assign outputSort_template>${
    "<#if position == 'before'>
      <#assign outputSort_param"+priority+">
        "+r"${outputSort_nested}<#t>"+"
        <#if outputSort_param"+priority+"?has_content>"+r"${outputSort_param"+priority+"}</#if><#t>
      </#assign>
    </#if>
    <#if position== 'after'>
      <#assign outputSort_param"+priority+">
        <#if outputSort_param"+priority+"?has_content>"+r"${outputSort_param"+priority+"}</#if><#t>
        "+r"${outputSort_nested}<#t>"+"
      </#assign>
    </#if>"
  }</#assign>
  <@outputSort_template?interpret />
</#macro>
<#--  输出宏  -->
<#macro outputSort_print>
  <#if outputSort_maxPriority?has_content && outputSort_maxPriority?is_number>
    <#list 0..outputSort_maxPriority as i >
      <#assign outputSort_template>${
        "<#if outputSort_param"+i+"?has_content>"
        +r"${outputSort_param"+i+"}<#t>"
        +"</#if>"
      }</#assign>
      <@outputSort_template?interpret />
    </#list>
  </#if>
</#macro>
<#--  重置变量  -->
<#macro outputSort_clear>
  <#if outputSort_maxPriority?has_content && outputSort_maxPriority?is_number>
    <#list 0..outputSort_maxPriority as i >
      <#assign outputSort_template>${
        "<#if outputSort_param"+i+"?has_content>"
        +r"<#assign outputSort_param"+i+r"=''/>"
        +"</#if>"
      }</#assign>
      <@outputSort_template?interpret />
    </#list>
    <#assign outputSort_maxPriority='' />
  </#if>
</#macro>
<#--  输出排序方法: END -->
<@outputSort_clear />

// 模拟数据
const mockDatas: Array<any> = [
<#if de.getAllPSDESampleDatas?? &&  de.getAllPSDESampleDatas()??>
<#list de.getAllPSDESampleDatas() as data>
    ${data.getDataJO().toString()}<#if data_has_next>,</#if>
</#list>
</#if>
];


//getwflink
mock.onGet(new RegExp(/^\/wfcore\/${app.getPSSystem().getCodeName()?lower_case}-app-${app.getCodeName()?lower_case}\/${srfpluralize(item.codeName)?lower_case}\/[a-zA-Z0-9\-\;]+\/usertasks\/[a-zA-Z0-9\-\;]+\/ways$/)).reply((config: any) => {
    console.groupCollapsed("实体:${item.codeName?lower_case} 方法: getwflink");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status,[
        {"sequenceFlowId":"dfdsfdsfdsfdsfds","sequenceFlowName":"同意",
         "taskId":"aaaaddddccccddddd","processDefinitionKey":"support-workorders-approve-v1",
         "processInstanceId":"ddlfldldfldsfds","refViewKey":""},
        {"sequenceFlowId":"ddssdfdfdfdfsfdf","sequenceFlowName":"不同意",
         "taskId":"aaaaddddccccddddd","processDefinitionKey":"support-workorders-approve-v1",
         "processInstanceId":"ddfdsldlfdlldsf","refViewKey":"workorder_ltform_editview"}
        ]];
});

// getwfstep
mock.onGet(new RegExp(/^\/wfcore\/${app.getPSSystem().getCodeName()?lower_case}-app-${app.getCodeName()?lower_case}\/${srfpluralize(item.codeName)?lower_case}\/process-definitions-nodes$/)).reply((config: any) => {
    console.groupCollapsed("实体:${item.codeName?lower_case} 方法: getwfstep");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, [
        {"userTaskId":"sddfddfd-dfdf-fdfd-fdf-dfdfd",
        "userTaskName":"待审",
        "cnt":0,
        "processDefinitionKey":"support-workorders-approve-v1",
        "processDefinitionName":"工单审批流程v1"
        },
        {"userTaskId":"sddfddfd-dfdf-fdfd-fdf-87927",
        "userTaskName":"待分配",
        "cnt":3,
        "processDefinitionKey":"support-workorders-approve-v1",
        "processDefinitionName":"工单审批流程v1"}
        ]];
});

// createBatch
mock.onPost(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/batch$/)).reply((config: any) => {
    console.groupCollapsed("实体:${item.codeName?lower_case} 方法: createBatch");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, {}];
});

// updateBatch
mock.onPut(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/batch$/)).reply((config: any) => {
    console.groupCollapsed("实体:${item.codeName?lower_case} 方法: updateBatch");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, {}];
});

// removeBatch
mock.onDelete(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/batch$/)).reply((config: any) => {
    console.groupCollapsed("实体:${item.codeName?lower_case} 方法: removeBatch");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, {}];
});

<#-- 实体接口mock数据:BEGIN -->
<#if item.getAllPSAppDEMethods?? && item.getAllPSAppDEMethods()??>
  <#list item.getAllPSAppDEMethods() as singleAppMethod>
    <#if singleAppMethod.getPSDEServiceAPIMethod?? &&  singleAppMethod.getPSDEServiceAPIMethod()??>
      <#assign singleServiceApi = singleAppMethod.getPSDEServiceAPIMethod()/>
    </#if>
    <#-- 应用实体关系路径数大于0 && 拥有实体服务接口方法: BEGIN -->
    <#if item.getPSAppDERSPathCount() gt 0 && singleServiceApi??>
      <#list 1..item.getPSAppDERSPathCount() as count>
        <#assign path = ''/>
        <#assign param = ''/>
        <#list item.getPSAppDERSPath(count_index)  as deRSPath>
            <#if deRSPath.getMajorPSAppDataEntity?? && deRSPath.getMajorPSAppDataEntity()??>
                <#assign _dataEntity = deRSPath.getMajorPSAppDataEntity()/>
                <#assign param>${param}'${_dataEntity.getKeyPSAppDEField().getCodeName()?lower_case}'<#if deRSPath_has_next>,</#if></#assign>
                <#assign path>${path}${srfpluralize(_dataEntity.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})\/</#assign>
            </#if>
        </#list>

        <#-- 方法类型为SELECT: BEGIN -->
        <#if singleAppMethod.getMethodType() == "SELECT">

// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = [${param},'${item.getKeyPSAppDEField().getCodeName()?lower_case}'];
    const matchArray:any = new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.${item.getKeyPSAppDEField().getCodeName()?lower_case}, tempValue.${item.getKeyPSAppDEField().getCodeName()?lower_case}));
<@endLog appMethod=singleAppMethod dataName="_items"/>
    return [status, _items];
});
        <#-- 方法类型为SELECT: END -->
        <#-- 方法类型为FETCH: BEGIN -->
        <#elseif singleAppMethod.getMethodType() == "FETCH">

// ${singleAppMethod.getCodeName()}
mock.onGet(new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = [${param}];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
<@endLog appMethod=singleAppMethod dataName="items"/>
    return [status, items];
});
        <#-- 方法类型为FETCH: END -->
        <#-- 方法类型为DEACTION: BEGIN -->
        <#else>
            <#-- 方法请求不需要参数情况: BEGIN -->
            <#if singleServiceApi.getRequestParamType() == "NONE">

// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    // ${singleAppMethod.getCodeName()}
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
<@endLog appMethod=singleAppMethod dataName="{}"/>
    return [status, {}];
});
            <#-- 方法请求不需要参数情况: END -->
            <#-- 方法请求参数为指定字段属性值: BEGIN -->
            <#elseif singleServiceApi.getRequestParamType() == "FIELD">
<@outputSort 1 'after'>

// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = [${param},'${item.getKeyPSAppDEField().getCodeName()?lower_case}'];
    const matchArray:any = new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.${item.getKeyPSAppDEField().getCodeName()?lower_case}, tempValue.${item.getKeyPSAppDEField().getCodeName()?lower_case}));
<@endLog appMethod=singleAppMethod dataName="_items?_items:{}"/>
    return [status, _items?_items:{}];
});
</@outputSort>
            <#-- 方法请求参数为指定字段属性值: END -->
            <#-- 方法请求参数为实体对象数据: BEGIN -->
            <#elseif singleServiceApi.getRequestParamType() == "ENTITY">
    
// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = [${param},'${item.getKeyPSAppDEField().getCodeName()?lower_case}'];
    const matchArray:any = new RegExp(/^\/${path}${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
<@endLog appMethod=singleAppMethod dataName="{}"/>
    return [status, {}];
});
            <#-- 方法请求参数为实体对象数据: END -->
            <#-- 方法请求参数为URI参数数据: BEGIN -->
            <#else>
// URI参数传递情况未实现
            </#if>
            <#-- 方法请求参数为URI参数数据: END -->
        </#if>
        <#-- 方法类型为DEACTION: END -->
      </#list>
    </#if>
    <#-- 应用实体关系路径数大于0 && 拥有实体服务接口方法: END -->
    <#--  主实体方法: BEGIN  -->
    <#if item.isMajor()>
        <#-- 方法类型为SELECT: BEGIN -->
        <#if singleAppMethod.getMethodType() == "SELECT">

// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['${item.getKeyPSAppDEField().getCodeName()?lower_case}'];
    const matchArray:any = new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.${item.getKeyPSAppDEField().getCodeName()?lower_case}, tempValue.${item.getKeyPSAppDEField().getCodeName()?lower_case}));
<@endLog appMethod=singleAppMethod dataName="_items"/>
    return [status, _items];
});
        <#-- 方法类型为SELECT: EWND -->
        <#-- 方法类型为FETCH: BEGIN -->
        <#elseif singleAppMethod.getMethodType() == "FETCH">
    
// ${singleAppMethod.getCodeName()}
mock.onGet(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
<@endLog appMethod=singleAppMethod dataName="mockDatas"/>
    return [status, mockDatas ? mockDatas : []];
});

// ${singleAppMethod.getCodeName()}
mock.onGet(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
<@startLog singleAppMethod />
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    <#--  if(config.query){
        records = records.filter(item => {
          return item.accountname.includes(config.query);
        })
    }  -->
<@endLog appMethod=singleAppMethod dataName="records ?  records : []"/>
    return [status, records ?  records : []];
});
        <#-- 方法类型为FETCH: END -->
        <#-- 方法类型为DEACTION: BEGIN -->
        <#else>
            <#-- 方法请求不需要参数情况: BEGIN -->
            <#if singleServiceApi.getRequestParamType() == "NONE">

// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    // ${singleAppMethod.getCodeName()}
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
<@endLog appMethod=singleAppMethod dataName="{}"/>
    return [status, {}];
});
            <#-- 方法请求不需要参数情况: END -->
            <#-- 方法请求参数为指定字段属性值: BEGIN -->
            <#elseif singleServiceApi.getRequestParamType() == "FIELD">
<@outputSort 1 'after'>

// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['${item.getKeyPSAppDEField().getCodeName()?lower_case}'];
    const matchArray:any = new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.${item.getKeyPSAppDEField().getCodeName()?lower_case}, tempValue.${item.getKeyPSAppDEField().getCodeName()?lower_case}));
<@endLog appMethod=singleAppMethod dataName="_items?_items:{}"/>
    return [status, _items?_items:{}];
});
</@outputSort>
            <#-- 方法请求参数为指定字段属性值: END -->
            <#-- 方法请求参数为实体对象数据: BEGIN -->
            <#elseif singleServiceApi.getRequestParamType() == "ENTITY">
        
// ${singleAppMethod.getCodeName()}
mock.on${srfclassname('${singleServiceApi.getRequestMethod()?lower_case}')}(new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/?([a-zA-Z0-9\-\;]{0,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/)).reply((config: any) => {
<@startLog singleAppMethod />
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['${item.getKeyPSAppDEField().getCodeName()?lower_case}'];
    const matchArray:any = new RegExp(/^\/${srfpluralize(item.codeName)?lower_case}\/([a-zA-Z0-9\-\;]{1,35})<#if singleServiceApi.getRequestPath()??>\${singleServiceApi.getRequestPath()}</#if>$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
       <#if singleAppMethod.getCodeName() == "Create">
<@endLog appMethod=singleAppMethod dataName="mockDatas[0]"/>
    return [status, mockDatas[0]];
              <#else>
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.${item.getKeyPSAppDEField().getCodeName()?lower_case}, tempValue.${item.getKeyPSAppDEField().getCodeName()?lower_case}));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['${item.getKeyPSAppDEField().getCodeName()?lower_case}'] == tempValue['${item.getKeyPSAppDEField().getCodeName()?lower_case}'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
<@endLog appMethod=singleAppMethod dataName="data"/>
    return [status, data];
              </#if>
});
            <#-- 方法请求参数为实体对象数据: END -->
            <#-- 方法请求参数为URI参数数据: BEGIN -->
            <#else>
// URI参数传递情况未实现
            </#if>
            <#-- 方法请求参数为URI参数数据: END -->
        </#if>
        <#-- 方法类型为DEACTION: END -->
    </#if>
    <#--  主实体方法: END  -->
  </#list>
</#if>
<#-- 实体接口mock数据end -->

<@outputSort_print />