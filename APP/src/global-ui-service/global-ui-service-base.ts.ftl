<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
import { Util, Loading } from '@/ibiz-core/utils';
import UIActionBase from '@/utils/ui-service-base/ui-action-base';

/**
 * 全局界面行为基类
 *
 * @export
 * @class GlobalUiServiceBase
 * @extends {UIActionBase}
 */
export default class GlobalUiServiceBase extends UIActionBase {

    /**
     * Creates an instance of UiServiceBase.
     * 
     * @memberof UiServiceBase
     */
    constructor() {
        super();
    }

<#if app.getAllPSAppDEUIActions?? && app.getAllPSAppDEUIActions()??>
<#list app.getAllPSAppDEUIActions() as uiAction>
<#if !uiAction.getPSAppDataEntity()?? && !uiAction.isGroup()>

${P.getLogicCode(uiAction, "LOGIC.vue").code}
</#if>
</#list>
</#if>
}