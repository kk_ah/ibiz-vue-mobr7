<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';

/**
 * 应用实体服务
 *
 * @export
 * @class GlobalUiServiceConstructor
 * @extends {ServiceConstructorBase}
 */
export default class GlobalUiServiceConstructor extends ServiceConstructorBase {
<#if app.getAllPSAppDataEntities?? && app.getAllPSAppDataEntities()??>

    /**
     * 初始化
     *
     * @protected
     * @memberof GlobalUiServiceConstructor
     */
    protected init(): void {
<#list app.getAllPSAppDataEntities() as appde>
    <#if appde.getAllPSAppDEUIActions?? && appde.getAllPSAppDEUIActions()??>
    <#list appde.getAllPSAppDEUIActions() as uiAction>
        <#assign exist = true>
        <#if exist??>
        <#break>
        </#if>
    </#list>
    </#if>
    <#if exist?? && exist == true>
    <#if !P.exists("importActionService", appde.getId(), "")>
    <#assign exist = false>
        this.allService.set('${appde.getCodeName()?lower_case}_ui_action', () => import('@/ui-service/${srffilepath2(appde.getCodeName())}/${srffilepath2(appde.getCodeName())}-ui-action'));
    </#if>
    </#if>
</#list>
    }
</#if>

}