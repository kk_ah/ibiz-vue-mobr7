<#--  BEGIN：获取标题语言资源  -->
<#macro getTitleLanguage obj langrestype>
  <#if obj.getTitlePSLanguageRes()?? && obj.getTitlePSLanguageRes().getContent(langrestype, false)?? && obj.getTitlePSLanguageRes().getContent(langrestype, false) != ''>
    ${obj.getTitlePSLanguageRes().getContent(langrestype, false)}<#t>
  <#elseif obj.getCaption()??>
    ${obj.getCaption()}<#t>
  </#if>
</#macro>
<#--  END：获取标题语言资源  -->

<#--  BEGIN：获取文本语言资源  -->
<#macro getTextLanguage obj langrestype>
  <#if obj.getTextPSLanguageRes()?? && obj.getTextPSLanguageRes().getContent(langrestype, false)?? && obj.getTextPSLanguageRes().getContent(langrestype, false) != ''>
    ${obj.getTextPSLanguageRes().getContent(langrestype, false)}<#t>
  <#elseif obj.getText()??>
    ${obj.getText()}<#t>
  </#if>
</#macro>
<#--  END：获取文本语言资源  -->

<#--  BEGIN：获取树节点名称语言资源  -->
<#macro getNameLanguage obj langrestype>
  <#if obj.getNamePSLanguageRes()?? && obj.getNamePSLanguageRes().getContent(langrestype, false)?? && obj.getNamePSLanguageRes().getContent(langrestype, false) != ''>
    ${obj.getNamePSLanguageRes().getContent(langrestype, false)}<#t>
  <#elseif obj.getText()??>
    ${obj.getText()?j_string}<#t>
  </#if>
</#macro>
<#--  END：获取树节点名称语言资源  -->

<#--  BEGIN：获取空值语言资源  -->
<#macro getEmptyTextLanguage obj langrestype emptyValue>
  <#if obj.getEmptyTextPSLanguageRes()?? && obj.getEmptyTextPSLanguageRes().getContent(langrestype, false)?? && obj.getEmptyTextPSLanguageRes().getContent(langrestype, false) != ''>
    ${obj.getEmptyTextPSLanguageRes().getContent(langrestype, false)}<#t>
  <#elseif obj.getEmptyText() != '未定义'>
    ${obj.getEmptyText()}<#t>
  <#else>
    ${emptyValue}<#t>
  </#if>
</#macro>
<#--  END：获取空值语言资源  -->