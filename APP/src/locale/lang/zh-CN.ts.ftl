<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
<#list app.getAllPSAppDataEntities() as dataEntitey>
import ${dataEntitey.getCodeName()?lower_case}_zh_CN from '@locale/lanres/${srffilepath2(dataEntitey.getCodeName())}/${srffilepath2(dataEntitey.getCodeName())}_zh_CN';
</#list>
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';
<#assign langrestype = 'ZH_CN'/>

export default {
<#--  本地化语言资源 中文  -->
    app: {
        gridpage: {
            choicecolumns: '选择列',
            refresh: '刷新',
            show: '显示',
            records: '条',
            totle: '共',
        },
        tabpage: {
            sureclosetip: {
                title: '关闭提醒',
                content: '表单数据已经修改，确定要关闭？',
            },
            closeall: '关闭所有',
            closeother: '关闭其他',
        },
        fileUpload: {
            caption: '上传',
        },
        searchButton: {
            search: '搜索',
            reset: '重置',
        },
        // 非实体视图
        views: {
            <#if app.isPubRefViewOnly()><#assign allAppViews = app.getAllRefPSAppViews()/><#else><#assign allAppViews = app.getAllPSAppViews()/></#if>
            <#if allAppViews??>
            <#list allAppViews as obj>
            <#if !obj.isPSDEView()>
            ${obj.getCodeName()?lower_case}: {
                caption: '<#ibizinclude>../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
            },
            </#if>
            </#list>
            </#if>
        },
        menus: {
            <#list app.getAllPSAppMenuModels() as menu>
            ${menu.getCodeName()?lower_case}: {
                <#list menu.getAllPSAppMenuItems() as obj>
                ${obj.getName()?lower_case}: '<#ibizinclude>../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
                </#list>
            },
            </#list>
        },
        components: {
            app_icon_menu: {
                statusValue_open: '展开',
                statusValue_close: '收回',
            }
        },

        button: {
            cancel: '取消',
            confirm: '确认',
            back: '返回',
            loadmore: '加载更多'
        },
        loadding: '加载中',
        fastsearch: '快速搜索',
        pulling_text: '下拉刷新',
        ctrl:{
            form:"表单",
            multieditviewpanel:"多编辑面板",
            searchform:"搜索表单",
        },
        view:"视图",
        notConfig:"参数未配置",
        message:{
            success: "成功",
            fail: "失败",
            savedSuccess: "保存成功",
            deleteSccess: "删除成功",
            warning: "警告",
            confirmToDelete: "确认删除 ",
            unrecoverable: " 删除操作将不可恢复",
            totle: "共",
            data: "条数据"
        },
        statusMessage:{
            200: '服务器成功返回请求的数据。',
            201: '新建或修改数据成功。',
            202: '一个请求已经进入后台排队（异步任务）。',
            204: '删除数据成功。',
            400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
            401: '用户没有权限（令牌、用户名、密码错误）。',
            403: '用户得到授权，但是访问是被禁止的。',
            404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
            405: '方法不被允许',
            406: '请求的格式不可得。',
            410: '请求的资源被永久删除，且不会再得到的。',
            422: '当创建一个对象时，发生一个验证错误。',
            500: '服务器发生错误，请检查服务器。',
            502: '网关错误。',
            503: '服务不可用，服务器暂时过载或维护。',
            504: '网关超时。',
        },
        errorMessage: {
            100: '未知',
            101: '请求发生错误',
            5001: '数据不存在',
            5002: '数据已存在，无法重复创建',
            5003: '新建失败',
            5004: '数据不存在，无法保存',
            5005: '数据删除失败'
        }
    },
    <#list app.getAllPSAppDataEntities() as dataEntitey>
    ${dataEntitey.getCodeName()?lower_case}: ${dataEntitey.getCodeName()?lower_case}_zh_CN,
    </#list>
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN
};