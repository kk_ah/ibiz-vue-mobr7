<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
<#list app.getAllPSAppDataEntities() as dataEntitey>
import ${dataEntitey.getCodeName()?lower_case}_en_US from '@locale/lanres/${srffilepath2(dataEntitey.getCodeName())}/${srffilepath2(dataEntitey.getCodeName())}_en_US';
</#list>
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
<#assign langrestype = 'EN'/>

export default {
<#--  local langue english  -->
    app: {
        gridpage: {
            choicecolumns: 'Choice columns',
            refresh: 'refresh',
            show: 'Show',
            records: 'records',
            totle: 'totle',
        },
        tabpage: {
            sureclosetip: {
                title: 'Close warning',
                content: 'Form data Changed, are sure close?',
            },
            closeall: 'Close all',
            closeother: 'Close other',
        },
        fileUpload: {
            caption: 'Upload',
        },
        searchButton: {
            search: 'Search',
            reset: 'Reset',
        },
        // 非实体视图
        views: {
            <#if app.isPubRefViewOnly()><#assign allAppViews = app.getAllRefPSAppViews()/><#else><#assign allAppViews = app.getAllPSAppViews()/></#if>
            <#if allAppViews??>
            <#list allAppViews as obj>
            <#if !obj.isPSDEView()>
            ${obj.getCodeName()?lower_case}: {
                caption: '<#ibizinclude>../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
            },
            </#if>
            </#list>
            </#if>
        },
        menus: {
            <#list app.getAllPSAppMenuModels() as menu>
            ${menu.getCodeName()?lower_case}: {
                <#list menu.getAllPSAppMenuItems() as obj>
                ${obj.getName()?lower_case}: '<#ibizinclude>../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
                </#list>
            },
            </#list>
        },
        components: {
            app_icon_menu: {
                statusValue_open: 'open',
                statusValue_close: 'close',
            }
        },
        button: {
            cancel: 'Cancel',
            confirm: 'Confirm',
            back: 'Back',
            loadmore: 'Load more'
        },
        loadding: 'Loadding',
        fastsearch: 'Quick search',
        pulling_text: 'Pull down to refresh',
        ctrl:{
            form:"Form",
            multieditviewpanel:"multieditviewpanel",
            searchform:"searchform",
        },
        view:"View",
        notConfig:" Parameter not configured",
        message: {
            success: "successfully",
            fail: "failed",
            savedSuccess: "Saved successfully",
            deleteSccess: "successfully deleted",
            warning: " Warning ",
            confirmToDelete: "Confirm to delete",
            unrecoverable: " Will the delete operation be unrecoverable ",
            totle: " A total of ",
            data: " data"
        },
        statusMessage:{
            200: 'The server successfully returned the requested data',
            201: 'New or modified data is successful.',
            202: 'The request has been queued in the background (asynchronous task).',
            204: 'The data was deleted successfully.',
            400: 'There was an error in the request issued, and the server did not create or modify data.',
            401: 'The user does not have permission (token, user name, wrong password).',
            403: 'The user is authorized, but access is prohibited.',
            404: 'The request issued was for a non-existent record, and the server did not operate.',
            405: 'Method not allowed',
            406: 'The requested format is not available.',
            410: 'The requested resource is permanently deleted and will no longer be available.',
            422: 'When creating an object, a validation error occurred.',
            500: 'An error occurred on the server, please check the server.',
            502: 'Gateway error.',
            503: 'The service is unavailable, the server is temporarily overloaded or maintained.',
            504: 'Gateway timeout.',
        },
        errorMessage: {
            100: 'unknown',
            101: 'Request error',
            5001: 'Data does not exist',
            5002: 'Data already exists and cannot be recreated',
            5003: 'New creation failed',
            5004: 'The data does not exist and cannot be saved',
            5005: 'Data deletion failed'
        }
    },
    <#list app.getAllPSAppDataEntities() as dataEntitey>
    ${dataEntitey.getCodeName()?lower_case}: ${dataEntitey.getCodeName()?lower_case}_en_US,
    </#list>
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};