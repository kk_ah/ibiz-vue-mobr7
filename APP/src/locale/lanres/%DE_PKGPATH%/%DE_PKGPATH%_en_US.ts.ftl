<#ibiztemplate>
TARGET=PSAPPDATAENTITY
</#ibiztemplate>
<#assign langrestype = 'EN'/>

export default {
// test
<#--  实体视图输出  -->
<#if item.getAllPSAppViews()??>
	views: {
		<#list item.getAllPSAppViews() as obj>
		${obj.getPSDEViewCodeName()?lower_case}: {
			caption: '<#ibizinclude>../../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
		},
		</#list>
	},
</#if>
<#--  实体部件输出  -->
<#if item.getPSControls()??>
<#assign ctrltypes = ['FORM', 'GRID', 'SEARCHFORM', 'TOOLBAR']>
<#list ctrltypes as ctrltype>
<#list item.getPSControls() as ctrl>
<#if ctrl.getControlType() == ctrltype>
<#if !P.exists(ctrltype, ctrl.getCodeName(), "_class_")>
	${ctrl.getCodeName()?lower_case}_${ctrl.getControlType()?lower_case}: {
		<#if ctrl.getControlType() == 'FORM' || ctrl.getControlType() == 'SEARCHFORM'>
		details: {
			<#list ctrl.getAllPSDEFormDetails() as obj>
			${obj.name?lower_case}: '<#ibizinclude>../../@MACRO/CAPTIONLANG.tsx</#ibizinclude>', 
			</#list>
		},
		</#if>
		<#if ctrl.getControlType() == 'TREEVIEW'>
		nodes: {
			<#if ctrl.getPSDETreeNodes()??>
			<#list ctrl.getPSDETreeNodes() as obj>
        <#if obj.getTreeNodeType() == 'STATIC'>
			${obj.getNodeType()?lower_case}: "<@getNameLanguage obj langrestype />",
        </#if>
			</#list>
			</#if>
		},
    	</#if>
		<#if ctrl.getControlType() == 'GRID'>
		columns: {
			<#if ctrl.getAllPSDEGridColumns()??>
			<#list ctrl.getAllPSDEGridColumns() as obj>
			${obj.getName()?lower_case}: '<#ibizinclude>../../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
			</#list>
			</#if>
		},
		</#if>
		<#if ctrl.getControlType() == 'TOOLBAR'>
	    <#if ctrl.getAllPSDEToolbarItems()??>
		<#list ctrl.getAllPSDEToolbarItems() as obj>
		${obj.getName()?lower_case}: {
			caption: '<#ibizinclude>../../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
			tip: '${obj.getName()?lower_case}',
		},
		</#list>
		</#if>
		</#if>
		<#--  部件界面行为  -->
		<#if ctrl.getPSUIActions?? && ctrl.getPSUIActions()??>
		uiactions: {
			<#list ctrl.getPSUIActions() as obj>
			${obj.getUIActionTag()?lower_case}: '<#ibizinclude>../../@MACRO/CAPTIONLANG.tsx</#ibizinclude>',
			</#list>
		},
		</#if>
	},
</#if>
</#if>
</#list>
</#list>
</#if>
};