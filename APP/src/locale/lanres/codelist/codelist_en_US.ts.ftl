<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
<#ibizinclude>../../@MACRO/FUNC.ts</#ibizinclude>
<#assign langrestype = 'EN'/>
export default {
<#if app.getAllPSAppCodeLists()??>
  <#list app.getAllPSAppCodeLists() as codelist>
    <#if codelist.getCodeListType() == "STATIC">
    ${codelist.codeName}: {
      <#if codelist.getAllPSCodeItems?? && codelist.getAllPSCodeItems()??>
        <#if codelist.isCodeItemValueNumber()>
          <#list codelist.getAllPSCodeItems() as codeitem>
        ${codeitem.getValue()?j_string}: '<@getTextLanguage codeitem langrestype />',
          </#list>
        <#else>
          <#list codelist.getAllPSCodeItems() as codeitem>
        '${codeitem.getValue()?j_string}': '<@getTextLanguage codeitem langrestype />',
          </#list>
        </#if>
      </#if>
        empty: '<@getEmptyTextLanguage codelist langrestype '' />',
    },
    <#elseif codelist.getCodeListType() == "DYNAMIC">
    ${codelist.codeName}: {
        empty: '<@getEmptyTextLanguage codelist langrestype '' />',
    },
    </#if>
  </#list>
</#if>
};