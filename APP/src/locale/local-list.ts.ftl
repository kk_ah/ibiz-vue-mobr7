<#ibiztemplate>
TARGET=PSSYSAPP
</#ibiztemplate>
export const localList: any[] = [
<#--  语言资源列表  -->
    {
        type: 'ZH-CN',
        name: '中文简体',
    },
    <#if app.getAllPSAppLans()??>
    <#list app.getAllPSAppLans() as lans>
    <#if lans.getLanguage() == 'EN'>
    {
        type: 'EN-US',
        name: 'English',
    },
    </#if>
    </#list>
    </#if>
];