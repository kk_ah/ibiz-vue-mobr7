<template>
    <div class="app-mob-dataviewexpbar <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#if ctrl.render??>
        ${ctrl.render.code}
    <#else>
        <div class="dataviewexpbar-container">
            <div class="dataviewexpbar_list">
            <#if ctrl.getPSDEDataView()??>
            <#assign card = ctrl.getPSDEDataView()/>
            ${P.getCtrlCode(card, 'CONTROL.html').code}
            </#if>
            </div>
            
             <component 
              v-if="selection.view && !Object.is(this.selection.view.viewname, '')" 
              :is="selection.view.viewname"
              class="viewcontainer2"
              :viewDefaultUsage="false"
              :_context="JSON.stringify(selection.context)"
              :_viewparam="JSON.stringify(selection.viewparam)"

              >
            </component>
        </div>
    </#if>
    </div>
</template>