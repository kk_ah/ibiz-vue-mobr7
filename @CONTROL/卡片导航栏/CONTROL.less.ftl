<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
.dataviewexpbar-container{
    display: flex;
    height: calc(100vh - 96px);
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    .dataviewexpbar_list{
        width: 100%;
        height: 100%;
        overflow:scroll;
        overflow-x: hidden;
    }
    .viewcontainer2{
        width: 90%;
        margin: auto;
        overflow:scroll;
        overflow-x: hidden;
    }
}