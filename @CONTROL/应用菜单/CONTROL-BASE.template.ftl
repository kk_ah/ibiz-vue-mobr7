<template>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <!-- 预置菜单样式：图标视图 BEGIN -->
    <app-mob-menu-ionic-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}"  
        counterName="<#if ctrl.getPSSysCounter?? && ctrl.getPSSysCounter()??>${ctrl.getPSSysCounter().getCodeName()?lower_case}</#if>"  
        :items="menus" 
        :menuModels="menuMode.getAppFuncs()" 
        @select="select($event)"  
        v-if="controlStyle == 'ICONVIEW'">
    </app-mob-menu-ionic-view>
    <!-- 预置菜单样式：图标视图 END -->
    <!-- 预置菜单样式：列表视图 BEGIN -->
    <app-mob-menu-list-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}" 
        counterName="<#if ctrl.getPSSysCounter?? && ctrl.getPSSysCounter()??>${ctrl.getPSSysCounter().getCodeName()?lower_case}</#if>" 
        :items="menus" 
        :menuModels="menuMode.getAppFuncs()" 
        @select="select($event)" 
        v-else-if="controlStyle == 'LISTVIEW'" >
    </app-mob-menu-list-view>
    <!-- 预置菜单样式：列表视图 END -->
    <!-- 预置菜单样式：图片滑动视图 BEGIN -->
    <app-mob-menu-swiper-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}" 
        counterName="<#if ctrl.getPSSysCounter?? && ctrl.getPSSysCounter()??>${ctrl.getPSSysCounter().getCodeName()?lower_case}</#if>" 
        :items="menus" 
        :menuModels="menuMode.getAppFuncs()" 
        @select="select($event)" 
        v-else-if="controlStyle == 'SWIPERVIEW'">
    </app-mob-menu-swiper-view>
    <!-- 预置菜单样式：图片滑动视图 END -->
    <!-- 预置菜单样式：默认样式 BEGIN -->
    <app-mob-menu-default-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}" 
        counterName="<#if ctrl.getPSSysCounter?? && ctrl.getPSSysCounter()??>${ctrl.getPSSysCounter().getCodeName()?lower_case}</#if>" 
        :items="menus" 
        :menuModels="menuMode.getAppFuncs()" 
        v-model="defaultActive"
        @select="select($event)" 
        v-else>
    </app-mob-menu-default-view>
    <!-- 预置菜单样式：默认样式 BEGIN -->
</#if>
</template>