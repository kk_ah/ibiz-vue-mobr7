<#assign extendsClass>AppMenuServiceBase</#assign>
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_HEADER.ts.ftl
</#ibizinclude>

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public get(params: any = {}): Promise<any> {
        return this.http.get('v7/${srffilepath2(ctrl.codeName)}${ctrl.getControlType()?lower_case}', params);
    }

<#ibizinclude>
../@MACRO/SERVICE/SERVICE_BOTTOM.ts.ftl
</#ibizinclude>