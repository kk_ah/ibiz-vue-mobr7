# 菜单预置样式

菜单预置样式可以通过在菜单部件中配置预置样式，也可以在菜单被引用的位置配置预置样式，后者产生的模型值，会覆盖前者的模型值。该模式保证一个菜单部件可以在不同的场景中被使用，同时，该部件本身具备预置样式绘制能力，使得菜单部件成为独立的资源内容

## 代码

本部分代码分为两个部分，分别是样式类型和组件文件代码。

预置样式提供多个类型，菜单实现 `ICONVIEW` 、`LISTVIEW`、`SWIPERVIEW`，其他样式采用默认样式输出

```typescript
/**
 * 部件样式
 * 
 * @protected
 * @type {(string | 'ICONVIEW' | 'LISTVIEW' | 'SWIPERVIEW' | 'LISTVIEW2' | 'LISTVIEW3' | 'LISTVIEW4')}   
 *  默认空字符串 | 图标视图 | 列表视图 | 图片滑动视图 | 列表视图（无刷新） | 列表视图（无滑动） | 列表视图（无背景）
 * @memberof AppIndexView
 */
@Prop({ default: '' }) protected controlStyle!: string | 'ICONVIEW' | 'LISTVIEW' | 'SWIPERVIEW' | 'LISTVIEW2' | 'LISTVIEW3' | 'LISTVIEW4';

```

渲染内容代码

```html
<template>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <!-- 预置菜单样式：图标视图 BEGIN -->
    <app-mob-menu-ionic-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}"  
        :items="menus" 
        @select="select($event)"  
        v-if="controlStyle == 'ICONVIEW'">
    </app-mob-menu-ionic-view>
    <!-- 预置菜单样式：图标视图 END -->
    <!-- 预置菜单样式：列表视图 BEGIN -->
    <app-mob-menu-list-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}" 
        :items="menus" 
        @select="select($event)" 
        v-else-if="controlStyle == 'LISTVIEW'" >
    </app-mob-menu-list-view>
    <!-- 预置菜单样式：列表视图 END -->
    <!-- 预置菜单样式：图片滑动视图 BEGIN -->
    <app-mob-menu-swiper-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}" 
        :items="menus" 
        @select="select($event)" 
        v-else-if="controlStyle == 'SWIPERVIEW'">
    </app-mob-menu-swiper-view>
    <!-- 预置菜单样式：图片滑动视图 END -->
    <!-- 预置菜单样式：默认样式 BEGIN -->
    <app-mob-menu-default-view 
        class="<#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>" 
        menuName="${ctrl.codeName?lower_case}" 
        :items="menus" 
        v-model="defaultActive"
        @select="select($event)" 
        v-else>
    </app-mob-menu-default-view>
    <!-- 预置菜单样式：默认样式 BEGIN -->
</#if>
</template>
```



## 图标视图

### 配置

![config](./img/ionic-view/config.png)

### 效果

<img src="./img/ionic-view/ionic-view1.jpg" alt="ionic-view1" style="zoom:25%;" />

## 列表视图

### 配置

![config](./img/list-view/config.png)

### 效果

<img src="./img/list-view/list-view1.jpg" alt="list-view1" style="zoom:25%;" />

## 图片滑动视图

### 配置

![config](./img/swiper-view/config.png)

### 效果

<img src="./img/swiper-view/swiper-view1.jpg" alt="ionic-view1" style="zoom:25%;" />

<img src="./img/swiper-view/swiper-view2.jpg" alt="ionic-view2" style="zoom:25%;" />

<img src="./img/swiper-view/swiper-view3.jpg" alt="ionic-view3" style="zoom:25%;" />

<img src="./img/swiper-view/swiper-view4.jpg" alt="ionic-view4" style="zoom:25%;" />

## 默认样式

### 配置

菜单默认样式，属于用于导航应用功能展示，其一般处于底部的导航区域，不需要额外添加配置。

### 效果

<img src="./img/default-view/default-view1.jpg" alt="default-view1" style="zoom:25%;" />