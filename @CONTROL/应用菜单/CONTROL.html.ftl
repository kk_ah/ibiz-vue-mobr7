<#--  content  -->
<#assign content>
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}" 
    controlStyle="<#if ctrl.getControlStyle?? && ctrl.getControlStyle()??>${ctrl.getControlStyle()}</#if>"   
<#if (view.isDefaultPage?? && view.isDefaultPage())>
    v-model="collapseChange"  
    :mode="mode"  
    :selectTheme="selectTheme"  
    :isDefaultPage="isDefaultPage"  
    :defPSAppView="defPSAppView" 
</#if>
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>