<#ibizinclude>
../@MACRO/MODEL/MODEL_HEADER.ts.ftl
</#ibizinclude>

    /**
     * 菜单项集合
     *
     * @private
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Model
     */
    private items: any[] = [
        <#list ctrl.getPSAppMenuItems() as child>
        ${P.getPartCode(child,"ITEM").code},
        </#list>
    ];

	/**
	 * 应用功能集合
	 *
	 * @private
	 * @type {any[]}
	 * @memberof ${srfclassname('${ctrl.codeName}')}Model
	 */
	private funcs: any[] = [
        <#if view.isDefaultPage?? && view.isDefaultPage()>
        <#list ctrl.getPSAppFuncs() as appFuncs>
        {
            appfunctag: '${appFuncs.getCodeName()}',
            appfuncyype: '${appFuncs.getAppFuncType()}',
            <#if appFuncs.getAppFuncType() == 'APPVIEW'>
            <#assign dataview = appFuncs.getPSAppView()/>
            openmode: '${dataview.getOpenMode()}', 
            componentname: '${srffilepath2(dataview.getCodeName())}', 
            codename: '${dataview.getCodeName()?lower_case}',
            deResParameters: [],
            <#--  BEGIN：是否应用实体视图  -->
            <#if dataview.isPSDEView()>
            <#assign appDataEntity = dataview.getPSAppDataEntity()/>
            routepath: '/${view.getCodeName()?lower_case}/:${view.getCodeName()?lower_case}?/${srfpluralize(appDataEntity.codeName)?lower_case}/:${appDataEntity.getCodeName()?lower_case}?/${dataview.getPSDEViewCodeName()?lower_case}/:${dataview.getPSDEViewCodeName()?lower_case}?',
            parameters: [
                { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
                { pathName: '${dataview.getPSDEViewCodeName()?lower_case}', parameterName: '${dataview.getPSDEViewCodeName()?lower_case}' },
            ],
            <#else>
            routepath: '/${view.getCodeName()?lower_case}/:${view.getCodeName()?lower_case}?/${dataview.getCodeName()?lower_case}/:${dataview.getCodeName()?lower_case}?',
            parameters: [
                { pathName: '${dataview.getCodeName()?lower_case}', parameterName: '${dataview.getCodeName()?lower_case}' },
            ],
            </#if>
            <#--  END：是否应用实体视图  -->
            </#if>
        },
        </#list>
        </#if>
	];

    /**
     * 获取所有菜单项集合
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Model
     */
    public getAppMenuItems(): any[] {
        return this.items;
    }

    /**
     * 获取所有应用功能集合
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Model
     */
    public getAppFuncs(): any[] {
        return this.funcs;
    }
<#ibizinclude>
../@MACRO/MODEL/MODEL_BOTTOM.ts.ftl
</#ibizinclude>