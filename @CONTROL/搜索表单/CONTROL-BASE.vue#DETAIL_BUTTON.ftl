<#if item.render??>
    ${item.render.code}
<#else>
<div class="app-form-item-button" v-show="detailsModel.${item.name}.visible">
    <nut-button 
        v-if="detailsModel.${item.name}.visible" 
        class="app-form-button<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>" 
        style='<#if item.getHeight() gt 0>height: ${item.getHeight()}px;</#if>'>${item.caption}</nut-button>
</div>
</#if>