<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#if item.render??>
    ${item.render.code}
<#else>
<#assign dritem = item.getPSDEDRItem() />
<#assign refView = item.getPSAppView() />
<#assign appDataEntity = refView.getPSAppDataEntity()/>
<app-form-druipart
    class='<#if item.getPSSysCss?? && item.getPSSysCss()??>${item.getPSSysCss().getCssName()}</#if>' 
    parameterName='${appde.getCodeName()?lower_case}' 
    refviewtype='<#if refView.getPSViewType()??>${refView.getPSViewType().getId()}</#if>'  
    refreshitems='<#if item.getRefreshItems()??>${item.getRefreshItems()}</#if>' 
    viewname='${srffilepath2(refView.codeName)}' 
    paramItem='<#if item.getParamItem()??>${item.getParamItem()}<#else>${appde.getCodeName()?lower_case}</#if>' 
    style="<#if item.getPSLayoutPos()?? && item.getPSLayoutPos().getLayout() == "FLEX">height: 100%;</#if><#if item.getContentHeight() == 0><#if refView.getHeight() gt 0>height:${refView.getHeight()?c}px</#if><#else>height:${item.getContentHeight()?c}px;</#if>" 
    :formState="formState" 
    :parentdata='<#if dritem.getParentDataJO?? && dritem.getParentDataJO()??>${dritem.getParentDataJO()}<#else>{}</#if>' 
    :parameters="[
        <#if refView.getPSAppDERSPathCount() gt 0>
        <#list refView.getPSAppDERSPath(refView.getPSAppDERSPathCount() - 1) as deRSPath>
        <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
        { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' },
        </#list>
        </#if>
    ]" 
    :context="context" 
    :viewparams="viewparams" 
    :navigateContext ='<@getNavigateContext item />' 
    :navigateParam ='<@getNavigateParams item />' 
    :ignorefieldvaluechange="ignorefieldvaluechange" 
    :data="JSON.stringify(this.data)"  
    @drdatasaved="drdatasaved($event)"/>
</#if>
