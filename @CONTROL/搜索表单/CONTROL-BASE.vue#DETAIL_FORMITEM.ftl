<#if !item.isHidden()>
<#ibizinclude>
../@MACRO/CONTROL/LANGBASE.vue.ftl
</#ibizinclude>
<#if item.render??>
    ${item.render.code}
<#else>
<app-form-item 
    name='${item.name}' 
    class='<#if item.getPSSysCss?? && item.getPSSysCss()??>${item.getPSSysCss().getCssName()}</#if>' 
    <#if item.getLabelPSSysCss?? && item.getLabelPSSysCss()??>
    labelStyle="${item.getLabelPSSysCss().getCssName()}"  
    </#if> 
    uiStyle="${item.getDetailStyle()}"  
    labelPos="${item.getLabelPos()}" 
    ref="${item.name}_item"  
    :itemValue="this.data.${item.name}" 
    v-show="detailsModel.${item.name}.visible" 
    :itemRules="this.rules.${item.name}" 
    :caption="<#if langbase??>$t('${langbase}.details.${item.name}')<#else>'${item.getCaption()}'</#if>"  
    :labelWidth="${item.getLabelWidth()?c}"  
    :isShowCaption="${item.isShowCaption()?c}"
    :disabled="detailsModel.${item.name}.disabled"  
    :error="detailsModel.${item.name}.error" 
    :isEmptyCaption="${item.isEmptyCaption()?c}">
    <#if item.isCompositeItem()>
    <#assign formitems=item.getPSDEFormItems()>
        <app-range-editor 
            name="${item.name}"  
            editorType="${item.getEditorType()}"  
            format="${item.getEditorParam("TIMEFMT","")}"  
            style="${item.getEditorCssStyle()}" 
            v-model="data.${item.name}" 
            :activeData="data"  
            :refFormItem="[<#list formitems as formitem><#if formitem_index gt 0>,</#if>'${formitem.name}'</#list>]" 
            @formitemvaluechange="onFormItemValueChange"/>
    <#else>
        ${P.getEditorCode(item, "EDITOR.vue").code}
    </#if>
</app-form-item>
</#if>
</#if>