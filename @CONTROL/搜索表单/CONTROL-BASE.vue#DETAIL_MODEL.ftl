<#ibizinclude>
../@MACRO/CONTROL/LANGBASE.vue.ftl
</#ibizinclude>
<#assign detail>caption: '${item.getCaption()}', detailType: '${item.getDetailType()}', name: '${item.getName()}', visible: <#if item.getPSDEFDGroupLogic('PANELVISIBLE')??>false<#else>true</#if>, isShowCaption: ${item.isShowCaption()?c}, form: this</#assign>
<#if item.getDetailType()  == 'BUTTON'>
new FormButtonModel({ ${detail}<#if item.getPSUIAction()??><#assign uiaction=item.getPSUIAction()>, uiaction: { type: '${uiaction.getUIActionType()}', 
 tag: '${uiaction.getUIActionTag()}'<#if (uiaction.getActionTarget()!="")>, actiontarget: '${uiaction.getActionTarget()}'</#if> }</#if> })
<#elseif item.getDetailType()  == 'DRUIPART'>
new FormDRUIPartModel({ ${detail} })
<#elseif item.getDetailType()  == 'FORMITEM'>
new FormItemModel({ ${detail}, disabled: false, enableCond: ${item.getEnableCond()?c} })
<#elseif item.getDetailType()  == 'FORMPAGE'>
new FormPageModel({ ${detail} })
<#elseif item.getDetailType()  == 'FORMPART'>
new FormPartModel({ ${detail} })
<#elseif item.getDetailType()  == 'GROUPPANEL'>
new FormGroupPanelModel({ ${detail}, uiActionGroup: { caption: '<#if item.getPSUIActionGroup()??>${item.getPSUIActionGroup().getName()}</#if>', langbase: '<#if langbase??>${langbase}</#if>', extractMode: '<#if item.getActionGroupExtractMode?? && item.getActionGroupExtractMode()??>${item.getActionGroupExtractMode()}<#else>ITEM</#if>', details: [<#if item.getPSUIActionGroup()??><#list item.getPSUIActionGroup().getPSUIActionGroupDetails() as uadetails><#if uadetails_index gt 0>, </#if>{ name: '${item.getName()}_${uadetails.getName()}', caption: '${uadetails.getPSUIAction().getCaption()}', uiactiontag: '${uadetails.getPSUIAction().getUIActionTag()?lower_case}'<#if uadetails.getPSUIAction().getPSSysImage()??>, <#assign viewimg=uadetails.getPSUIAction().getPSSysImage()/><#if viewimg.getCssClass() != ''>icon: '${viewimg.getCssClass()}'<#else>img: '${viewimg.getImagePath()}'</#if></#if> }</#list></#if>] } })
<#elseif item.getDetailType()  == 'IFRAME'>
new FormIFrameModel({ ${detail} })
<#elseif item.getDetailType()  == 'RAWITEM'>
new FormRowItemModel({ ${detail} })
<#elseif item.getDetailType()  == 'TABPAGE'>
new FormTabPageModel({ ${detail} })
<#elseif item.getDetailType()  == 'TABPANEL'>
new FormTabPanelModel({ ${detail}, tabPages: [<#list item.getPSDEFormTabPages() as formmenber><#if formmenber_index gt 0>, </#if>{ name: '${formmenber.name}', index: ${formmenber_index}, visible: <#if formmenber.getPSDEFDGroupLogic('PANELVISIBLE')??>false<#else>true</#if> }</#list>] })
<#elseif item.getDetailType()  == 'USERCONTROL'>
new FormUserControlModel({ ${detail} })
</#if>