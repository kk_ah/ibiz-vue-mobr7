<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>

.app-form {
    height: 100%;
    >.van-tabs {
        height: 100%;
        >.van-tabs__content {
            height: calc(100% - 44px);
            overflow: auto;
        }
    }
}