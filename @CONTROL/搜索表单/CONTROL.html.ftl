<#--  content  -->
<#assign content>
    <#if ctrl.isEnableAutoSave??>:autosave="${ctrl.isEnableAutoSave()?c}"</#if> 
    :viewtag="viewtag"
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}"
    updateAction="<#if ctrl.getUpdatePSControlAction ?? && ctrl.getUpdatePSControlAction()?? && ctrl.getUpdatePSControlAction().getPSAppDEMethod()??>${ctrl.getUpdatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    removeAction="<#if ctrl.getRemovePSControlAction ?? &&  ctrl.getRemovePSControlAction()?? && ctrl.getRemovePSControlAction().getPSAppDEMethod()??>${ctrl.getRemovePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    loaddraftAction="<#if ctrl.getGetDraftPSControlAction ?? &&  ctrl.getGetDraftPSControlAction()?? && ctrl.getGetDraftPSControlAction().getPSAppDEMethod()??>${ctrl.getGetDraftPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    loadAction="<#if ctrl.getGetPSControlAction ?? &&  ctrl.getGetPSControlAction()?? && ctrl.getGetPSControlAction().getPSAppDEMethod()??>${ctrl.getGetPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    createAction="<#if ctrl.getCreatePSControlAction ?? &&  ctrl.getCreatePSControlAction()?? && ctrl.getCreatePSControlAction().getPSAppDEMethod()??>${ctrl.getCreatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    WFSubmitAction="<#if ctrl.getWFSubmitPSControlAction ?? &&  ctrl.getWFSubmitPSControlAction()?? && ctrl.getWFSubmitPSControlAction().getPSAppDEMethod()??>${ctrl.getWFSubmitPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    WFStartAction="<#if ctrl.getWFStartPSControlAction ?? &&  ctrl.getWFStartPSControlAction()?? && ctrl.getWFStartPSControlAction().getPSAppDEMethod()??>${ctrl.getWFStartPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    style='<#if ctrl.getWidth() gt 0>width: ${ctrl.getWidth()?c}px</#if><#if ctrl.getHeight() gt 0>height: ${ctrl.getHeight()?c}px</#if>' 
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>