<div v-show="detailsModel.${item.name}.visible" :style="{height:${item.getContentHeight()}px;}">
    <iframe src='${item.getIFrameUrl()}'></iframe>
</div>
