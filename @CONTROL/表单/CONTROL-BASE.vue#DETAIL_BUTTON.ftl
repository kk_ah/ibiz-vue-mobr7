<#if item.render??>
    ${item.render.code}
<#else>
<div class="app-form-item-button" v-show="detailsModel.${item.name}.visible">
    <ion-button v-if="detailsModel.${item.name}.visible" class="app-form-button
    <#if item.getPSSysCss()??>${item.getPSSysCss().getCssName()}</#if>"  
    style='<#if item.getHeight() gt 0>height: ${item.getHeight()}px;</#if>'>
    <#if item.getPSSysImage()??>
    <#assign sysimage = item.getPSSysImage()/>
    <#if sysimage.getImagePath() ==  "">
        <ion-icon name="${sysimage.getCssClass()}" style="margin-right: 2px"></ion-icon>
    <#else>
        <img src="${sysimage.getImagePath()}" style="margin-right: 2px"/>
    </#if>
    </#if>${item.caption}</ion-button>
</div>
</#if>