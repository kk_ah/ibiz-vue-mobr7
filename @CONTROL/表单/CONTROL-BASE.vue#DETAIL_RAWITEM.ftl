<#if item.render??>
${item.render.code}
<#else>
<ion-item v-show="detailsModel.${item.name}.visible" class="app-form-item">
    ${item.getRawContent()}
</ion-item>
</#if>