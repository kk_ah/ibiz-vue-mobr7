<#assign content>
<#if item.getPSSysImage()??><#assign img=item.getPSSysImage()><#if img.getCssClass()?? && (img.getCssClass()?length gt 0)><i class="${img.getCssClass()}" style="margin-right: 2px;"></i></#if></#if>
<#list item.getPSDEFormDetails() as formmenber>
    <#if !(formmenber.isHidden?? && formmenber.isHidden())>    
    <#ibizinclude>
    ../@MACRO/CONTROL/FORM_MEMBER_LAYOUT.vue.ftl
    </#ibizinclude>
    </#if>
</#list>
</#assign>
<#if item.render??>
${item.render.code}
<#else>
<#if item.getPSLayout()?? &&  item.getPSLayout().getLayout() == "FLEX">
<#assign pageLayout = item.getPSLayout()>
<div style="height: 100%;display: flex;<#if pageLayout.getDir()!="">flex-direction: ${pageLayout.getDir()};</#if><#if pageLayout.getAlign()!="">justify-content: ${pageLayout.getAlign()};</#if><#if pageLayout.getVAlign()!="">align-items: ${pageLayout.getVAlign()};</#if>">
    ${content}
</div>
<#else>
    ${content}
</#if>
</#if>