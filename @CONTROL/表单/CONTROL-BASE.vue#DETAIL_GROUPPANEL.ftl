<#ibizinclude>
../@MACRO/CONTROL/LANGBASE.vue.ftl
</#ibizinclude>
<#if item.render??>
${item.render.code}
<#else>
<app-form-group 
    class='<#if item.getPSSysCss?? && item.getPSSysCss()??>${item.getPSSysCss().getCssName()}</#if>' 
    layoutType='<#if item.getPSLayoutPos()??>${item.getPSLayoutPos().getLayout()}</#if>' 
    titleStyle='<#if item.getLabelPSSysCss?? && item.getLabelPSSysCss()??>${item.getLabelPSSysCss().getCssName()}</#if>' 
    uiStyle="${item.getDetailStyle()}" 
    v-show="detailsModel.${item.name}.visible" 
    :uiActionGroup="detailsModel.${item.name}.uiActionGroup" 
    :caption="<#if langbase??>$t('${langbase}.details.${item.name}')<#else>'${item.getCaption()}'</#if>" 
    :isShowCaption="${item.isShowCaption()?c}" 
    :titleBarCloseMode="${item.getTitleBarCloseMode()}" 
    :isInfoGroupMode="${item.isInfoGroupMode()?c}" 
    <#if item.getPSSysImage()??><#assign img=item.getPSSysImage()><#if img.getCssClass()?? && (img.getCssClass()?length gt 0)>
    iconName="${img.getCssClass()}"
    </#if></#if>    
    @groupuiactionclick="groupUIActionClick($event)">
<#assign content>
    <#list item.getPSDEFormDetails() as formmenber>
    <#if !(formmenber.isHidden?? && formmenber.isHidden())>
    <#ibizinclude>
    ../@MACRO/CONTROL/FORM_MEMBER_LAYOUT.vue.ftl
    </#ibizinclude>
    </#if>
    </#list>
</#assign>
<#if item.getPSLayout()?? &&  item.getPSLayout().getLayout() == "FLEX">
<#assign pageLayout = item.getPSLayout()>
        <#if item.getPSSysImage()??><#assign img=item.getPSSysImage()><#if img.getCssClass()?? && (img.getCssClass()?length gt 0)><ion-icon class="${img.getCssClass()}" style="margin-right: 2px;"></ion-icon></#if></#if>    
        ${content}    
    </div>
<#else>
    ${content}    
</#if>
</app-form-group>
</#if>