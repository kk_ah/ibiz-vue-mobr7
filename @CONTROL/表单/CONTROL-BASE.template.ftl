<template>
<#ibizinclude>
../@MACRO/CONTROL/LANGBASE.vue.ftl
</#ibizinclude>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <div ref='${ctrl.name}' class="app-form <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#--    isNoTabHeader是否隐藏分页头部-->
    <#if ctrl.isNoTabHeader()>
        <#list ctrl.getPSDEFormPages() as formmenber>
            ${P.getPartCode(formmenber).code}
        </#list>
    <#else>
        <ion-tabs class="app-form-tabs" @ionTabsDidChange="detailsModel.${ctrl.name}.clickPage($event)">
            <#list ctrl.getPSDEFormPages() as formpage>
            <ion-tab class="app-form-tab" tab="${formpage.name}">
                <ion-row>
                    ${P.getPartCode(formpage).code}
                </ion-row>
            </ion-tab>
            </#list>
            <ion-tab-bar slot="top">
                <#list ctrl.getPSDEFormPages() as formpage>
                <ion-tab-button tab="${formpage.name}" :disabled="!detailsModel.${formpage.name}.visible">
                    <ion-label class="caption<#if formpage.getLabelPSSysCss?? && formpage.getLabelPSSysCss()??> ${formpage.getLabelPSSysCss().getCssName()}</#if>">
                        <#if formpage.getPSSysImage()??>
                        <#assign sysimage = formpage.getPSSysImage()/>
                        <#if sysimage.getImagePath() ==  "">
                            <ion-icon name="${sysimage.getCssClass()}" style="margin-right: 2px"></ion-icon>
                        <#else>
                            <img src="${sysimage.getImagePath()}" style="margin-right: 2px"/>
                        </#if>
                        </#if>
                        {{<#if langbase??>$t('${langbase}.details.${formpage.name}')<#else>'${formpage.getCaption()}'</#if>}}
                    </ion-label>
                </ion-tab-button>
                </#list> 
            </ion-tab-bar>
        </ion-tabs>
    </#if>
    </div>
</#if>
</template>