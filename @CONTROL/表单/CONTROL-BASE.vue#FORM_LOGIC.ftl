        <#list ctrl.getAllPSDEFormDetails() as formdetail>
        <#if formdetail.getPSDEFDGroupLogic('ITEMBLANK')??>
        <#assign ITEMBLANK = formdetail.getPSDEFDGroupLogic('ITEMBLANK')/>
        if (Object.is(name, '')<#if ITEMBLANK.getRelatedDetailNames()??><#list ITEMBLANK.getRelatedDetailNames() as detailName> || Object.is(name, '${detailName}')</#list></#if>) {
            let ret = true;
            <#if ITEMBLANK.getRelatedDetailNames()??>
            <#list ITEMBLANK.getRelatedDetailNames() as detailName>
            const _${detailName} = this.data.${detailName};
            </#list>
            </#if>
            if (${P.getPartCode(ITEMBLANK, 'DETAIL_LOGIC').code}) {
                ret = false;
            }
            this.rules.${formdetail.name}.some((rule: any) => {
                if (rule.hasOwnProperty('required')) {
                    rule.required = ret;
                }
                return false;
            });
        }
        </#if>
        <#if formdetail.getPSDEFDGroupLogic('ITEMENABLE')??>
        <#assign ITEMENABLE = formdetail.getPSDEFDGroupLogic('ITEMENABLE')/>
        if (Object.is(name, '')<#if ITEMENABLE.getRelatedDetailNames()??><#list ITEMENABLE.getRelatedDetailNames() as detailName> || Object.is(name, '${detailName}')</#list></#if>) {
            let ret = false;
            <#if ITEMENABLE.getRelatedDetailNames()??>
            <#list ITEMENABLE.getRelatedDetailNames() as detailName>
            const _${detailName} = this.data.${detailName};
            </#list>
            </#if>
            if (${P.getPartCode(ITEMENABLE, 'DETAIL_LOGIC').code}) {
                ret = true;
            }
            this.detailsModel.${formdetail.name}.setDisabled(!ret);
        }
        </#if>
        <#if formdetail.getPSDEFDGroupLogic('PANELVISIBLE')??>
        <#assign PANELVISIBLE = formdetail.getPSDEFDGroupLogic('PANELVISIBLE')/>
        if (Object.is(name, '')<#if PANELVISIBLE.getRelatedDetailNames()??><#list PANELVISIBLE.getRelatedDetailNames() as detailName> || Object.is(name, '${detailName}')</#list></#if>) {
            let ret = false;
            <#if PANELVISIBLE.getRelatedDetailNames()??>
            <#list PANELVISIBLE.getRelatedDetailNames() as detailName>
            const _${detailName} = this.data.${detailName};
            </#list>
            </#if>
            if (${P.getPartCode(PANELVISIBLE, 'DETAIL_LOGIC').code}) {
                ret = true;
            }
            this.detailsModel.${formdetail.name}.setVisible(ret);
        }
        </#if>
        </#list>