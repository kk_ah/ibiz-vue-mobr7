<div v-show="detailsModel.${item.name}.visible"<#if item.getContentWidth?? && item.getContentWidth()?? && item.getContentWidth()!=0>style="height:${item.getContentHeight()}px;width:${item.getContentWidth()}px"<#else>style="height:${item.getContentHeight()}px;width:100%"</#if>>
    <iframe src='${item.getIFrameUrl()}'></iframe>
</div>