<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>

.app-form {
<#if ctrl.getFormFuncMode?? && ctrl.getFormFuncMode()?? && ctrl.getFormFuncMode() == "WIZARDFORM">
    height: calc(100vh - 105px - 52px);
    overflow: auto;
<#else>
    height: 100%;
</#if>
    >.app-form-tabs {
        height: 100%;
        >.app-form-tab {
            height: 100%;
            overflow: auto;
        }
    }
}