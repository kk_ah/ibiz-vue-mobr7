        <#list ctrl.getAllPSPanelItems() as paneldetail>
        <#if paneldetail.getPSPanelItemGroupLogic('ITEMBLANK')??>
        <#assign ITEMBLANK = paneldetail.getPSPanelItemGroupLogic('ITEMBLANK')/>
        if (Object.is(name, '')<#if ITEMBLANK.getRelatedItemNames()??><#list ITEMBLANK.getRelatedItemNames() as detailName> || Object.is(name, '${detailName}')</#list></#if>) {
            let ret = true;
            <#if ITEMBLANK.getRelatedItemNames()??>
            <#list ITEMBLANK.getRelatedItemNames() as detailName>
            const _${detailName} = this.data.${detailName};
            </#list>
            </#if>
            if (${P.getPartCode(ITEMBLANK, 'DETAIL_LOGIC').code}) {
                ret = false;
            }
            this.rules.${paneldetail.name}.some((rule: any) => {
                if (rule.hasOwnProperty('required')) {
                    rule.required = ret;
                }
                return false;
            });
        }
        </#if>
        <#if paneldetail.getPSPanelItemGroupLogic('ITEMENABLE')??>
        <#assign ITEMENABLE = paneldetail.getPSPanelItemGroupLogic('ITEMENABLE')/>
        if (Object.is(name, '')<#if ITEMENABLE.getRelatedItemNames()??><#list ITEMENABLE.getRelatedItemNames() as detailName> || Object.is(name, '${detailName}')</#list></#if>) {
            let ret = false;
            <#if ITEMENABLE.getRelatedItemNames()??>
            <#list ITEMENABLE.getRelatedItemNames() as detailName>
            const _${detailName} = this.data.${detailName};
            </#list>
            </#if>
            if (${P.getPartCode(ITEMENABLE, 'DETAIL_LOGIC').code}) {
                ret = true;
            }
            this.detailsModel.${paneldetail.name}.setDisabled(!ret);
        }
        </#if>
        <#if paneldetail.getPSPanelItemGroupLogic('PANELVISIBLE')??>
        <#assign PANELVISIBLE = paneldetail.getPSPanelItemGroupLogic('PANELVISIBLE')/>
        if (Object.is(name, '')<#if PANELVISIBLE.getRelatedItemNames()??><#list PANELVISIBLE.getRelatedItemNames() as detailName> || Object.is(name, '${detailName}')</#list></#if>) {
            let ret = false;
            <#if PANELVISIBLE.getRelatedItemNames()??>
            <#list PANELVISIBLE.getRelatedItemNames() as detailName>
            const _${detailName} = this.data.${detailName};
            </#list>
            </#if>
            if (${P.getPartCode(PANELVISIBLE, 'DETAIL_LOGIC').code}) {
                ret = true;
            }
            this.detailsModel.${paneldetail.name}.setVisible(ret);
        }
        </#if>
        </#list>