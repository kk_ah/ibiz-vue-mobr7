<#assign layout = item.getPSLayout()/>
<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if layout.getLayout()=='FLEX'>
<div class="app-layoutpanel-container<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>" style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if>">
<#if item.render??>
${item.render.code}
<#else>
    <#--  <#if item.isShowCaption()>
    <#if item.getLabelPSSysCss()??>
    <div class="${item.getLabelPSSysCss().getCssName()}" style="height: 50px;${item.getLabelPSSysCss().getRawCssStyle()}">${item.getCaption()}</div>
    <#else>
    <div style="height: 50px;">${item.getCaption()}</div>
    </#if>
    </#if>  -->
    <div style="height:100%;display: flex;<#if layout.getDir()!="">flex-direction: ${layout.getDir()};</#if><#if layout.getAlign()!="">justify-content: ${layout.getAlign()};</#if><#if layout.getVAlign()!="">align-items: ${layout.getVAlign()};</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>">
<@ibizindent blank=8>
<#if item.getPSPanelItems()??>
<#list item.getPSPanelItems() as panelItem>
${P.getPartCode(panelItem).code}
</#list>
</#if>
</@ibizindent>
    </div>
</#if>
</div>
<#else>
<ion-col v-show="detailsModel.${item.name}.visible" ${tableAttr} style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if>" class="app-layoutpanel-container<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<#if item.render??>
${item.render.code}
<#else>
    <#--  <#if item.isShowCaption()>
    <#if item.getLabelPSSysCss()??>
    <div class="${item.getLabelPSSysCss().getCssName()}" style="height: 50px;${item.getLabelPSSysCss().getRawCssStyle()}">${item.getCaption()}</div>
    <#else>
    <div style="height: 50px;">${item.getCaption()}</div>
    </#if>
    </#if>  -->
    <ion-row style="height:100%;<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>">
<@ibizindent blank=8>
<#if item.getPSPanelItems()??>
<#list item.getPSPanelItems() as panelItem>
${P.getPartCode(panelItem).code}
</#list>
</#if>
</@ibizindent>
    </ion-row>
</#if>
</ion-col>
</#if>