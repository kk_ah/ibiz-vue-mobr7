<#assign selfContent>
${item.getHtmlContent()}
</#assign>

<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="${flexAttr}<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-rowitem<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</div>
<#else>
<ion-col v-show="detailsModel.${item.name}.visible" ${tableAttr} style="${flexAttr}<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-rowitem<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</ion-col>
</#if>


