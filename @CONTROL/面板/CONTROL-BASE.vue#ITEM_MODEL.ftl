<#assign detail>caption: '${item.getCaption()}', itemType: '${item.getItemType()}', name: '${item.getName()}', panel: this, visible: <#if item.getEditorType?? && item.getEditorType()?? && item.getEditorType() == 'HIDDEN'>false<#else>true</#if> </#assign>
<#if item.getItemType()  == 'CONTAINER'>
new PanelContainerModel({ ${detail} })
<#elseif item.getItemType()  == 'RAWITEM'>
new PanelRawitemModel({ ${detail} })
<#elseif item.getItemType()  == 'FIELD'>
new PanelFieldModel({ ${detail} })
<#elseif item.getItemType()  == 'CONTROL'>
new PanelControlModel({ ${detail} })
<#elseif item.getItemType()  == 'BUTTON'>
new PanelButtonModel({ ${detail} })
<#elseif item.getItemType()  == 'USERCONTROL'>
new PanelUserControlModel({ ${detail} })
</#if>