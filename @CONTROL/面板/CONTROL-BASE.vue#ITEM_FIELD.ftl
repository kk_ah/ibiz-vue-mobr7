<#assign selfContent>
<div class="item-field ">
    <#if item.getCaption() != ""><ion-label class="item-field-label <#if item.getLabelPSSysCss()??> ${item.getLabelPSSysCss().getCssName()}</#if>"<#if item.getLabelPSSysCss()??> style="${item.getLabelPSSysCss().getRawCssStyle()}"</#if>>${item.getCaption()}</ion-label></#if>
    ${P.getEditorCode(item, "PANELEDITOR.vue").code}
</div>
</#assign>

<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div v-show="detailsModel.${item.name}.visible" style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-field <#if item.getPSSysCss?? && item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</div>
<#else>
<ion-col v-show="detailsModel.${item.name}.visible" ${tableAttr} style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-field<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</ion-col>
</#if>