<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>

.app-layoutpanel {
    height: 100%;
    margin-bottom: 10px;
    > .app-layoutpanel-container {
        height: 100%;
        .app-layoutpanel-rowitem{
        }
    }
    .item-field{
        padding: 0 4px;
        display: flex;
        >.item-field-label{
            padding: 0 8px;
            overflow: initial;
        }
    }
}
