<#assign flexAttr>
<#if item.getPSLayoutPos()?? && item.getPSLayoutPos().getLayout() == "FLEX"><#assign layoutPos = item.getPSLayoutPos()/><#if layoutPos.getGrow() gt -1>flex-grow: ${layoutPos.getGrow()};</#if></#if></#assign>
<#assign tableAttr>
<#if item.getPSLayoutPos()?? && item.getPSLayoutPos().getLayout() != "FLEX"><#assign layoutPos = item.getPSLayoutPos()/><#if layoutPos.getColLG() gt -1> :lg="${layoutPos.getColLG()?c}"</#if><#if layoutPos.getColMD() gt -1> :size="${layoutPos.getColMD()?c}"</#if><#if layoutPos.getColSM() gt -1> :sm="${layoutPos.getColSM()?c}"</#if><#if layoutPos.getColMDOffset() gt -1> :offset="${layoutPos.getColMDOffset()?c}"</#if></#if></#assign>
<#assign content>
<#if item.render??>
${item.render.code}
<#elseif selfContent??>
${selfContent}
</#if>
</#assign>