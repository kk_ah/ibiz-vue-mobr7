<#assign selfContent>
${P.getCtrlCode(item.getPSControl(), 'CONTROL.html').code}
</#assign>

<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-ctrlpos<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</div>
<#else>
<ion-col v-show="detailsModel.${item.name}.visible" ${tableAttr} style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-ctrlpos<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</ion-col>
</#if>