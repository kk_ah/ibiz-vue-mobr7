<template>
    ${P.getPartCode(item,'PANEL').code}
</template>
<#assign import_block>
import { PanelUserControlModel, PanelButtonModel, PanelControlModel, PanelFieldModel, PanelRawitemModel, PanelContainerModel } from '@/model/panel-detail';
</#assign>
<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>

   /**
     * 生命周期
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */  
    created(){
        this.afterCreated();
    }
   /**
     * 执行created后的逻辑
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */  
    afterCreated(){
        if(this.item){
            this.data = this.item;
            this.panelLogic({ name: '', newVal: null, oldVal: null });
            return;
        }
       if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is('load', action)) {
                    this.load(data);
                }
            });
        } 
        this.panelLogic({ name: '', newVal: null, oldVal: null });
    }
   /**
     * 数据
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */  
    public data :any ={} ;

    /**
     * 部件行为--fetch
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected fetchAction!: string;

   /**
     * 显示处理提示
     *
     * @type {boolean}
     * @memberof Mob
     */
    @Prop({ default: true }) protected showBusyIndicator?: boolean;

    /**
     * 接口实现
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    getDatas(): any[] {
        if (!this.item) {
            return [];
        }
        return [this.item];
    }
    getData() {
        return this.item;
    }

    /**
     * 面板数据对象
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected item?: any;

    <#if ctrl.getAllPSPanelItems()??>
    <#list ctrl.getAllPSPanelItems() as panelItem>
    <#if panelItem.getItemType() == "BUTTON">
	/**
	 * 面板 ${panelItem.getCaption()} 事件
	 *
	 * @memberof @memberof ${srfclassname('${ctrl.codeName}')}
	 */
    protected ${panelItem.getName()}_click($event: any): void {
        <#if panelItem.getPSUIAction?? && panelItem.getPSUIAction()??>
        <#assign uiaction = panelItem.getPSUIAction()>
        this.${ctrl.name}_${panelItem.getName()}_click(null, null, $event);
        </#if>

    }
    </#if>
    </#list>
    </#if>

    /**
     * 数据加载
     *
     * @private
     * @param {*} [data={}]
     * @param {string} [type=""]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private async load(data: any = {}, type: string = ""): Promise<any> {
        const parentdata: any = {};
        this.$emit('beforeload', parentdata);
        Object.assign(data, parentdata);
        const response: any = await this.service.get(this.fetchAction, this.context, data, this.showBusyIndicator);
        if (!response || response.status !== 200) {
            this.$notify({ type: 'danger', message: response.info });
            return Promise.resolve(response);
        }
        this.$emit('load', (response.data && response.data.records) ? response.data.records : []);
        this.data = [];
        this.data = response.data.records;
        return Promise.resolve(response);
    }

    @Watch('item')
    public itemChange(){
         this.data = this.item;
    }


   /**
     * 表单逻辑
     *
     * @private
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
        <#if P.getPartCode(item,'PANEL_LOGIC').code?length gt 0>
        ${P.getPartCode(item,'PANEL_LOGIC').code}
        </#if>
    }

   /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected detailsModel: any = {
        <#list ctrl.getAllPSPanelItems() as paneletail>
        ${paneletail.name}: ${P.getPartCode(paneletail, 'ITEM_MODEL').code}, 
        </#list>
    };

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>

