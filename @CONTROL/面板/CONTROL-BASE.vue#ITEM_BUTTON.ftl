<#assign selfContent>
<ion-button  @click="${item.name}_click($event)" <#if item.getHeight() gt 0> style="height: ${item.getHeight()?c}px;"</#if>>
    <#if item.getPSSysImage()??>
    <#assign image = item.getPSSysImage()>
    <ion-icon name="${image.getCssClass()}"></ion-icon>
    </#if>
    <#if item.isShowCaption()>
    <#if item.getLabelPSSysCss()??> class="${item.getLabelPSSysCss().getCssName()}"</#if><#if item.getLabelPSSysCss()??> style="${item.getLabelPSSysCss().getRawCssStyle()}"</#if>${item.getCaption()}</#if>
</ion-button>
</#assign>
<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-button<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</div>
<#else>
<ion-col v-show="detailsModel.${item.name}.visible" ${tableAttr} style="${flexAttr}<#if item.getWidth() gt 0>width: ${item.getWidth()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-button<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    ${content}
</ion-col>
</#if>