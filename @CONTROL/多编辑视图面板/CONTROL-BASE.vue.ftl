<template>
<div class="app-mob-meditviewpanel <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
<#if ctrl.render??>
${ctrl.render.code}
<#else>
<div class="app-medit-view-panel">
    <div v-for="(item,index) in items" :key="index" class="app-medit-view-panel-card">
        <ion-card>
          <ion-card-header>
            <ion-card-subtitle>{{item.title}}</ion-card-subtitle>
            <ion-icon class="meditviewpanel_delete_icon" @click="deleteItem(item)" name="close-circle-outline"></ion-icon>
          </ion-card-header>
          <ion-card-content>
            <${srffilepath2(ctrl.getEmbeddedPSAppView().getCodeName())} 
              class="viewcontainer2"
              :_context="toString(item.viewdata)"
              :_viewparam="toString(item.viewparam)"
              :viewDefaultUsage="false"
              :panelState="panelState"
              :showTitle="false"
              @viewdataschange="viewDataChange"
              @viewload="viewload"
              @viewdirty="viewdirty(item,$event)"
            ></${srffilepath2(ctrl.getEmbeddedPSAppView().getCodeName())} >
          </ion-card-content>
        </ion-card>
    </div>
    <div class="meditviewpanel_ion-fab_space"></div>
    <ion-fab vertical="bottom" horizontal="end">
        <ion-fab-button @click="handleAdd">
        <ion-icon name="add"></ion-icon>
        </ion-fab-button>
    </ion-fab>
</div>
</#if>
</div>
</template>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>
    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getData(): any {
        return null;
    }

    /**
     * 显示处理提示
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({ default: true }) protected showBusyIndicator?: boolean;

    /**
     * 部件行为--update
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected updateAction!: string;
    
    /**
     * 部件行为--fetch
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected fetchAction!: string;
    
    /**
     * 部件行为--remove
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected removeAction!: string;
    
    /**
     * 部件行为--load
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected loadAction!: string;
    
    /**
     * 部件行为--loaddraft
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected loaddraftAction!: string;
    
    /**
     * 部件行为--create
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected createAction!: string;

    /**
     * 刷新数据
     *
     * @type {number}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected saveRefView?: number;

    /**
     * 刷新数据
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Watch('saveRefView')
    onSaveRefView(newVal: any, oldVal: any) {
        console.log('保存多项数据!');
        if (newVal > 0) {
            this.$emit('drdatasaved', false);
        }

    }
    
    /**
     * 对象转字符串
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected toString(item:any): string{
        return JSON.stringify(item);
    }

     /**
     * 面板状态订阅对象
     *
     * @private
     * @type {Subject<{action: string, data: any}>}
     * @memberof Meditviewpanel
     */
    protected panelState: Subject<ViewState> = new Subject();

    /**
     * 视图名称
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewname: string = '<#if ctrl.getEmbeddedPSAppView()??>${srffilepath2(ctrl.getEmbeddedPSAppView().getCodeName())}</#if>';

    /**
     * 获取数据对象
     *
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected items: any[] = [];

    /**
     * 计数器
     *
     * @type number
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected count: number = 0;

    <#--  BEGIN：参数处理  -->
<#list ctrl.getAllRelatedPSAppViews() as dataview>
<#if !dataview.isPSDEView()>

    /**
     * 关系实体参数对象
     *
     * @private
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private deResParameters: any[] = [];

    /**
     * 当前应用视图参数对象
     *
     * @private
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private parameters: any[] = [];
<#else>
<#--  <#assign dataview = ctrl.getAllRelatedPSAppViews()[0]>  -->
<#assign appDataEntity = dataview.getPSAppDataEntity()/>

    /**
     * 关系实体参数对象
     *
     * @private
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private deResParameters: any[] = [
        <#--  BEGIN：存在父关系路径  -->
        <#if dataview.getPSAppDERSPathCount() gt 0>
        <#list dataview.getPSAppDERSPath(dataview.getPSAppDERSPathCount() - 1) as deRSPath>
        <#assign majorPSAppDataEntity = deRSPath.getMajorPSAppDataEntity()/>
        { pathName: '${srfpluralize(majorPSAppDataEntity.codeName)?lower_case}', parameterName: '${majorPSAppDataEntity.getCodeName()?lower_case}' },
        </#list>
        </#if>
        <#--  END：存在父关系路径  -->
    ];

    /**
     * 当前应用视图参数对象
     *
     * @private
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private parameters: any[] = [
        { pathName: '${srfpluralize(appDataEntity.codeName)?lower_case}', parameterName: '${appDataEntity.getCodeName()?lower_case}' },
    ];
</#if>
</#list>
<#--  END：参数处理  -->

    /**
     * vue 声明周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */    
    protected afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is(action, 'load')) {
                    this.load(data);
                }
                if (Object.is(action, 'save')) {
                    this.saveData(data);
                }
            });
        }
    }   

     /**
      * 保存数据
      *
      * @memberof Meditviewpanel
      */
    protected saveData(data?: any) {
        this.count = 0;
        if(this.items.length >0){
            Object.assign(data,{showResultInfo:false,saveEmit:false});
            this.panelState.next({ tag: 'meditviewpanel', action: 'save', data: data });
        }else{
            this.$emit("drdatasaved",{action:'drdatasaved'});
        }
    }

    /**
     * 处理数据
     *
     * @private
     * @param {any[]} datas
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private doItems(datas: any[]): void {
        const [{ pathName, parameterName }] = this.parameters;
        datas.forEach((arg: any) => {
            let id: string = arg[parameterName] ? arg[parameterName] : this.$util.createUUID();
            let title :string = arg.${appde.getCodeName()?lower_case}name ?  arg.${appde.getCodeName()?lower_case}name :'新建';
            let item: any = { id: id,title:title, viewdata: {}, viewparam: {} };
            Object.assign(item.viewdata, this.$viewTool.getIndexViewParam());
            Object.assign(item.viewdata, this.context);

            // 关系应用实体参数
            this.deResParameters.forEach(({ pathName, parameterName }: { pathName: string, parameterName: string }) => {
                if (this.context[parameterName] && !Object.is(this.context[parameterName], '')) {
                    Object.assign(item.viewdata, { [parameterName]: this.context[parameterName] });
                } else if (arg[parameterName] && !Object.is(arg[parameterName], '')) {
                    Object.assign(item.viewdata, { [parameterName]: arg[parameterName] });
                }
            });

            // 当前视图参数（应用实体视图）
            this.parameters.forEach(({ pathName, parameterName }: { pathName: string, parameterName: string }) => {
                if (arg[parameterName] && !Object.is(arg[parameterName], '')) {
                    Object.assign(item.viewdata, { [parameterName]: arg[parameterName] });
                }
            });

            //合并视图参数
            Object.assign(item.viewparam, this.viewparams);
            this.items.push(item);
        });
    }
    /**
     * 数据加载
     *
     * @private
     * @param {*} data
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private load(data: any): void {
        if(!this.fetchAction){
            this.$notice.error(this.viewName+this.$t('app.view')+this.$t('app.ctrl.multieditviewpanel')+'loaddraftAction'+ this.$t('app.notConfig'));
            return;
        }
        let arg: any = {};
        Object.assign(arg, data,{viewparams:this.viewparams});
        this.items = [];
        const promice: Promise<any> = this.service.get(this.fetchAction,JSON.parse(JSON.stringify(this.context)),arg, this.showBusyIndicator);
        promice.then((response: any) => {
            if (!response.status || response.status !== 200) {
                if (response.errorMessage) {
                    this.$notify({ type: 'danger', message: response.errorMessage });
                }
                return;
            }
            const data: any = response.data;
           if (data.length > 0) {
                const items = JSON.parse(JSON.stringify(data));
                this.doItems(items);
            }
            this.$emit('load', this.items);
        }).catch((response: any) => {
            if (response && response.status === 401) {
                return;
            }
            this.$notify({ type: 'danger', message: response.errorMessage });
        });
    }

    /**
     * 增加数据
     * 
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected handleAdd(){
        if(!this.loaddraftAction){
            this.$notice.error(this.viewName+this.$t('app.view')+this.$t('app.ctrl.multieditviewpanel')+'loaddraftAction'+ this.$t('app.notConfig'));
            return;
        }
        const promice: Promise<any> = this.service.loadDraft(this.loaddraftAction,JSON.parse(JSON.stringify(this.context)),{viewparams:this.viewparams}, this.showBusyIndicator);
        promice.then((response: any) => {
            if (!response.status || response.status !== 200) {
                if (response.errorMessage) {
                    this.$notify({ type: 'danger', message: response.errorMessage });
                }
                return;
            }
            const data: any = response.data;
            this.doItems([data]);
        }).catch((response: any) => {
            if (response && response.status === 401) {
                return;
            }
            this.$notify({ type: 'danger', message: response.errorMessage });
        });
    }

    /**
     * 删除方法
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.name}')}
     */
    public deleteItem(item: any, index: number) {
        this.items.splice(index,1);
    }

    /**
     * 设置视图脏值变化
     *
     * @param {*} item
     * @param {boolean} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected setViewDirty(item: any, $event: boolean) {
        let index: number = this.items.findIndex((_item: any) => Object.is(_item.id, item.id));
        if (index === -1) {
            return;
        }
        Object.assign(this.items[index], { viewdirty: $event });
        let state: boolean = this.items.some((item: any) => {
            if (item.viewdirty) {
                return true;
            }
            return false;
        });
        this.$emit('viewdatadirty', state);
    }

     /**
     * 部件抛出事件
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewDataChange($event:any){
        if($event){
            try{
                $event = JSON.parse($event);
            }catch(error){
                return;
            }
            if(Object.is($event.action,'load')){
                console.log('加载----');
            }
            if(Object.is($event.action,'save')){
                this.count++;
                if (this.items.length === this.count) {
                    this.$emit('drdatasaved',{action:'save'});
                }
            }
            if(Object.is($event.action,'remove')){
                if ($event.data) {
                    let resultIndex = this.items.findIndex((value:any, index:any, arr:any) => {
                        return value['viewdata']['orderdetailtestid'] === $event.data['orderdetailtestid'];
                    });
                    if (resultIndex !== -1) {
                        this.items.splice(resultIndex, 1);
                    }
                }
            }            
        }
    }

    /**
     * 视图加载完成
     *
     * @returns
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewload($event:any){
        console.log('视图加载完成');
    }

    /**
     * editview9 视图数据变化
     *
     * @returns
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewdirty(item:any,$event:any){
        // editview9 视图数据变化;
        this.setViewDirty(item, $event);
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>
