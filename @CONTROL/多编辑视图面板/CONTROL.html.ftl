<#--  content  -->
<#assign content>
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}" 
    :saveRefView="saveRefView" 
    @viewdatadirty="onViewDataDirty" 
    @drdatasaved="onDRDataSaved" 
    updateAction="<#if ctrl.getUpdatePSControlAction()?? && ctrl.getUpdatePSControlAction().getPSAppDEMethod()??>${ctrl.getUpdatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    removeAction="<#if ctrl.getRemovePSControlAction()?? && ctrl.getRemovePSControlAction().getPSAppDEMethod()??>${ctrl.getRemovePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    <#--  loaddraftAction="<#if ctrl.getGetDraftPSControlAction()?? && ctrl.getGetDraftPSControlAction().getPSAppDEMethod()??>${ctrl.getGetDraftPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"  -->
    loaddraftAction="<#if ctrl.getPSAppDataEntity?? && ctrl.getPSAppDataEntity()??><#if ctrl.getPSAppDataEntity().isEnableTempData()>GetDraftTemp<#else>GetDraft</#if></#if>"
    loadAction="<#if ctrl.getGetPSControlAction()?? && ctrl.getGetPSControlAction().getPSAppDEMethod()??>${ctrl.getGetPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    createAction="<#if ctrl.getCreatePSControlAction()?? && ctrl.getCreatePSControlAction().getPSAppDEMethod()??>${ctrl.getCreatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    fetchAction="<#if ctrl.getFetchPSControlAction()?? && ctrl.getFetchPSControlAction().getPSAppDEMethod()??>${ctrl.getFetchPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>