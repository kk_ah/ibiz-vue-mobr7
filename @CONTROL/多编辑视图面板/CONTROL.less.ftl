<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
.app-mob-meditviewpanel{
    width: 100%;
    .meditviewpanel_ion-fab_space{
        height: 60px;
    }
    .meditviewpanel_delete_icon{
        position: absolute;
        right: 10px;
        top: 10px;
        font-size: 18px;
    }
}