<template>
    <div class='pickupviewpanel'>
        <component 
            v-if="inited && view.viewname && !Object.is(view.viewname, '')" 
            :is="view.viewname" 
            :_context="JSON.stringify(_context)"
            :_viewparams="JSON.stringify(_viewparams)"
            :viewDefaultUsage="false"
            :isSingleSelect="isSingleSelect"
            :isShowButtons="isShowButtons"
            @viewdataschange="onViewDatasChange"
            @viewdatasactivated="viewDatasActivated"
            @viewload="onViewLoad"
            ref="pickupview"
            :key="this.$util.createUUID()">
        </component>
    </div>
</template>