<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>
<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>

    /**
     * 面板导航参数
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public panelNavParam= <@getNavigateParams ctrl />;

    /**
     * 面板导航上下文
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public panelNavContext= <@getNavigateContext ctrl />;

    /**
     * 应用上下文
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public _context: any;

    /**
     * 视图参数
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public _viewparams: any;
    
    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getData(): any {
        return {};
    }

    /**
     * 视图名称
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected view: any = {
        viewname: '<#if ctrl.getPSAppDEView()??>${srffilepath2(ctrl.getPSAppDEView().getCodeName())}</#if>',
        data: {},
    }

    /**
     * 是否显示按钮
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({default: true}) protected isShowButtons!: boolean;

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected isSingleSelect?: boolean;

    /**
     * 初始化完成
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected inited: boolean = false;

    /**
     * 视图数据变化
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected onViewDatasChange($event: any): void {
        if ($event.length > 0) {
          $event.forEach((item:any,index:any) => {
              let srfmajortext = item['${ctrl.getPSAppDataEntity().getMajorPSAppDEField().getCodeName()?lower_case}'];
              if(srfmajortext){
                Object.assign($event[index],{srfmajortext: srfmajortext});
              }
          });
        }
        this.$emit('selectionchange', $event);
    }

    /**
     * 视图数据被激活
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewDatasActivated($event: any): void {
        this.$emit('activated', $event);
    }

    /**
     * 视图加载完成
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected onViewLoad($event: any): void {
        this.$emit('load', $event);
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */    
    protected afterCreated(){
        this.parsePanelParam();
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is('load', action)) {
                    Object.assign(this.viewparams, data);
                    this.inited = true;
                }
            });
        }
    }

    /**
     * 解析面板导航参数
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected parsePanelParam(){
        const { context: context, param: param } = this.$viewTool.formatNavigateParam( this.panelNavContext, this.panelNavParam, this.context, this.viewparams, {});
        this._context = { ...context };
        this._viewparams = { ...param };
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

    /**
     * 快速搜索
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public async quickSearch(query: string): Promise<any> {
        let pickupview:any = this.$refs.pickupview;
        if(pickupview){
            pickupview.quickSearch(query);
        }
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>