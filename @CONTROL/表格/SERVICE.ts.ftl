<#assign extendsClass>GridServiceBase</#assign>
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_HEADER.ts.ftl
</#ibizinclude>
<#assign import_block>
<#if ctrl.getAggMode() == "ALL" && ctrl.getAggPSAppDataEntity()??>
<#assign aggAppDataEntity = ctrl.getAggPSAppDataEntity() >
import ${srfclassname('${aggAppDataEntity.getCodeName()}')}Service from '@/service/${srffilepath2(aggAppDataEntity.getCodeName())}/${srffilepath2(aggAppDataEntity.getCodeName())}-service';
</#if>
</#assign>

    /**
     * 合并配置的默认值
     *
     * @protected
     * @param {*} [response={}]
     * @memberof ${srfclassname(ctrl.codeName)}Service
     */
    public mergeDefaults(response:any = {}): void { 
        if (response.data) {
    <#list ctrl.getPSDEGridEditItems() as edititem><#t>
      <#if edititem.getCreateDV?? && edititem.getCreateDV()??><#t>
        <#if !(edititem.getCreateDV() == '')><#t>
            Object.assign(response.data, { '${edititem.getCodeName()?lower_case}': '${edititem.getCreateDV()}' });
        </#if>
      </#if>
    </#list> 
        }
    }

    <#if ctrl.getAggMode() == "ALL" && ctrl.getAggPSAppDataEntity()??>
    <#assign aggAppDataEntity = ctrl.getAggPSAppDataEntity() >
    /**
     * 表格聚合加载数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<HttpResponse>}
     * @memberof ${srfclassname(ctrl.codeName)}Service
     */
    public async getAggData(action: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<HttpResponse> {
        await this.onBeforeAction(action, context, data, isLoading);
        data = this.handleRequestData(action, context, data);
        const service: any = await this.getService('${aggAppDataEntity.getCodeName()?lower_case}');
        let response: HttpResponse;
        if (Util.isFunction(service[action])) {
            response = await this.service[action](context, data);
        } else {
            response = null;
        }
        if (response.status === 200) {
            response = this.handleResponse(action, response);
        }
        return response;
    }
    </#if>
    
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_BOTTOM.ts.ftl
</#ibizinclude>