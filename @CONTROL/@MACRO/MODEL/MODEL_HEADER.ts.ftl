/**
 * ${srfclassname('${ctrl.codeName}')} 部件模型
 *
 * @export
 * @class ${srfclassname('${ctrl.codeName}')}Model
 */
export class ${srfclassname('${ctrl.codeName}')}Model {