<#ibizinclude>
./MODEL_HEADER.ts.ftl
</#ibizinclude>

    /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ${srfclassname('${ctrl.codeName}')}Model
    */
    public getDataItems(): any[] {
        return [
            // 工作流备注字段
            {
                name: 'srfwfmemo',
                prop: 'srfwfmemo',
                dataType: 'TEXT',
            },
<#--  表单项  -->
<#if ctrl.getPSDEFormItems()??>
    <#list ctrl.getPSDEFormItems() as dataitem>
            {
                name: '${dataitem.getName()}',
        <#if dataitem.getPSDEField()??>
            <#if ctrl.getControlType() == 'SEARCHFORM'>
                <#if dataitem.getPSDEFSearchMode?? && dataitem.getPSDEFSearchMode()??>
                prop: '${dataitem.getPSDEFSearchMode().getCodeName()?lower_case}', 
                <#else>
                prop: '${dataitem.getPSDEField().getCodeName()?lower_case}',
                </#if>
            <#else>
                prop: '${dataitem.getPSDEField().getCodeName()?lower_case}',
            </#if>
                dataType: '${dataitem.getPSDEField().getDataType()}',
        </#if>
            },
    </#list>
</#if>
<#--  关联主实体的主键  -->
<#if ctrl.getPSAppDataEntity()??>
    <#assign appDataEntity = ctrl.getPSAppDataEntity() />
    <#if appDataEntity.isMajor() == false && appDataEntity.getMinorPSAppDERSs()??>
        <#list appDataEntity.getMinorPSAppDERSs() as minorAppDERSs>
            <#if minorAppDERSs.getMajorPSAppDataEntity()??>
            <#assign majorAppDataEntity = minorAppDERSs.getMajorPSAppDataEntity() />
            {
                name: '${majorAppDataEntity.getCodeName()?lower_case}',
                prop: '${majorAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}',
                dataType: 'FONTKEY',
            },
            </#if>
        </#list>
    </#if>
</#if>
<#-- 界面主键标识 -->
<#if ctrl.getPSDEFormItems()??>
    <#list ctrl.getPSDEFormItems() as dataitem>
        <#if dataitem.getPSAppDEField()?? && dataitem.getPSAppDEField().isKeyField() == true>
            <#if !P.exists("importService1", dataitem.getPSAppDEField().getId(), "")>
            {
                name: '${ctrl.getPSAppDataEntity().getCodeName()?lower_case}',
                prop: '${dataitem.getPSDEField().getCodeName()?lower_case}',
                dataType: 'FONTKEY',
            },
            </#if>
        </#if>
    </#list>
</#if>
        ];
    }

<#ibizinclude>
./MODEL_BOTTOM.ts.ftl
</#ibizinclude>