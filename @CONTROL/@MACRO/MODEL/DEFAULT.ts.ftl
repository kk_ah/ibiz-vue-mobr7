<#ibizinclude>
./MODEL_HEADER.ts.ftl
</#ibizinclude>

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ${srfclassname('${ctrl.codeName}')}Model
    */
  public getDataItems(): any[] {
    return [
<#--  主实体所有属性  -->
<#if ctrl.getPSAppDataEntity()??>
  <#assign appDataEntity = ctrl.getPSAppDataEntity() />
  <#if appDataEntity.getAllPSAppDEFields?? && appDataEntity.getAllPSAppDEFields()??>
    <#list appDataEntity.getAllPSAppDEFields() as defield>
      {
      <#if defield.isKeyField()>
        name: '${appDataEntity.getCodeName()?lower_case}',
        prop: '${defield.getCodeName()?lower_case}',
      <#else>
        name: '${defield.getCodeName()?lower_case}',
      </#if>
      },
    </#list>
  </#if>
</#if>
<#--  关联主实体的主键  -->
<#if ctrl.getPSAppDataEntity()??>
  <#assign appDataEntity = ctrl.getPSAppDataEntity() />
  <#if appDataEntity.isMajor() == false && appDataEntity.getMinorPSAppDERSs()??>
    <#list appDataEntity.getMinorPSAppDERSs() as minorAppDERSs>
      <#if minorAppDERSs.getMajorPSAppDataEntity()??>
      <#assign majorAppDataEntity = minorAppDERSs.getMajorPSAppDataEntity() />
      {
        name: '${majorAppDataEntity.getCodeName()?lower_case}',
        prop: '${majorAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}',
        dataType: 'FONTKEY',
      },
      </#if>
    </#list>
  </#if>
</#if>
    ]
  }

<#ibizinclude>
./MODEL_BOTTOM.ts.ftl
</#ibizinclude>