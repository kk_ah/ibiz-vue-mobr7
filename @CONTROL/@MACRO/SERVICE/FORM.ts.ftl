<#ibizinclude>
./SERVICE_HEADER.ts.ftl
</#ibizinclude>

    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} [context]
     * @param {*} [data]
     * @param {boolean} [isLoading]
     * @returns {Promise<any[]>}
     * @memberof  ${srfclassname(ctrl.codeName)}Service
     */
    public async getItems(serviceName: string, interfaceName: string, context?: any, data?: any, isLoading?: boolean): Promise<any[]> {
    <#list ctrl.getPSDEFormItems() as deItem>
    <#if deItem.getPSEditor()?? && deItem.getPSEditor().getPSAppDataEntity?? && deItem.getPSEditor().getPSAppDataEntity()?? && deItem.getPSEditor().getPSAppDEDataSet?? && deItem.getPSEditor().getPSAppDEDataSet()??>
    <#assign _appde = deItem.getPSEditor().getPSAppDataEntity()/>
    <#assign deDataSet = deItem.getPSEditor().getPSAppDEDataSet()/>
    <#if !P.exists("importService4", _appde.getId() + deDataSet.getCodeName(), "")>
    <#if _appde.getId() == appde.getId()>
        if (Object.is(serviceName, '${srfclassname(_appde.getCodeName())}Service') && Object.is(interfaceName, '${deDataSet.getCodeName()}')) {
            await this.onBeforeAction(interfaceName, context, data, isLoading);
            const response: any = await this.service.${deDataSet.getCodeName()}(data);
            return this.doItems(response);
        }
    <#else>
        if (Object.is(serviceName, '${srfclassname(_appde.getCodeName())}Service') && Object.is(interfaceName, '${deDataSet.getCodeName()}')) {
            const service: any = await this.getService('${_appde.getCodeName()?lower_case}');
            await this.onBeforeAction(interfaceName, context, data, isLoading);
            const response: any = await service.${deDataSet.getCodeName()}(data);
            return this.doItems(response);
        }
    </#if>
    </#if>
    </#if>
    </#list>
        return [];
    }

    /**
     * 合并配置的默认值
     *
     * @protected
     * @param {*} [response={}]
     * @memberof ${srfclassname(ctrl.codeName)}Service
     */
    public mergeDefaults(response:any = {}): void {
        if (response.data) {
            <#list ctrl.getAllPSDEFormDetails() as formdetail><#t>
            <#if formdetail.getCreateDV?? && formdetail.getCreateDV()??><#t>
            <#if !(formdetail.getCreateDV() == '')><#t>
            Object.assign(response.data, { '${formdetail.getCodeName()?lower_case}': '${formdetail.getCreateDV()}' });
            </#if>
            </#if>
            </#list>
        }
    }

<#ibizinclude>
./SERVICE_BOTTOM.ts.ftl
</#ibizinclude>