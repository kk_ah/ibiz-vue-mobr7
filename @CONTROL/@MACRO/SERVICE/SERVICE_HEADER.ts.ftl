import { <#if extendsClass??>${extendsClass}</#if> } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { ${srfclassname(ctrl.codeName)}Model } from '@/app-core/ctrl-model/<#if ctrl.getPSAppDataEntity?? && ctrl.getPSAppDataEntity()??>${srffilepath2(ctrl.getPSAppDataEntity().getCodeName())}<#else>app</#if>/${srffilepath2(ctrl.getCodeName())}-${ctrl.getControlType()?lower_case}-model';
<#if import_block??>${import_block}</#if>

/**
 * ${srfclassname(ctrl.codeName)} 部件服务对象
 *
 * @export
 * @class ${srfclassname(ctrl.codeName)}Service
 * @extends {<#if extendsClass??>${extendsClass}</#if>}
 */
export class ${srfclassname(ctrl.codeName)}Service extends <#if extendsClass??>${extendsClass}</#if> {

    /**
     * 部件模型
     *
     * @protected
     * @type {${srfclassname(ctrl.codeName)}Model}
     * @memberof ControlServiceBase
     */
    protected model: ${srfclassname(ctrl.codeName)}Model = new ${srfclassname(ctrl.codeName)}Model();
<#if view.isPSDEView()?? && view.getTempMode?? && view.getTempMode()?? && view.getTempMode() == 2>

    /**
     * 是否为从数据临时模式
     *
     * @protected
     * @type {boolean}
     * @memberof ${srfclassname(ctrl.codeName)}Service
     */
    protected isTempMode: boolean = true;
</#if>
<#if appde??>

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ${srfclassname(ctrl.codeName)}Service
     */
    protected appDEName: string = '${appde.getCodeName()?lower_case}';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof ${srfclassname(ctrl.codeName)}Service
     */
    protected appDeKey: string = '${appde.getKeyPSAppDEField().getCodeName()?lower_case}';
</#if>