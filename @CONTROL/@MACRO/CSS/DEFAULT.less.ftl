// this is less
.srf_empty_class {
    width: 100%;
    height: 100%;
}

<#if ctrl.getPSSysCsses?? && ctrl.getPSSysCsses()??>
<#list ctrl.getPSSysCsses() as css>
<#if css.getRawCssStyle()?? && css.getRawCssStyle()?length gt 0>
.${css.getCssName()} {
    ${css.getRawCssStyle()}
}
</#if>
<#if css.getCssStyle()??>
${css.getCssStyle()}
</#if>
</#list>
</#if>