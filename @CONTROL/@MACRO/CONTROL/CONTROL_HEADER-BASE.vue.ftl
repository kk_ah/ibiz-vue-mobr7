<script lang='ts'>
import { Vue, Component, Prop, Provide, Emit, Watch, Model } from 'vue-property-decorator';
import { CreateElement } from 'vue';
import { Subject, Subscription } from 'rxjs';
import { ControlInterface } from '@/interface/control';
import GlobalUiService from '@/global-ui-service/global-ui-service';
<#if appde??>
import ${srfclassname('${appde.getCodeName()}')}Service from '@/app-core/service/${srffilepath2(appde.getCodeName())}/${srffilepath2(appde.getCodeName())}-service';
import ${srfclassname('${ctrl.codeName}')}Service from '@/app-core/ctrl-service/${srffilepath2(appde.getCodeName())}/${srffilepath2(ctrl.codeName)}-${ctrl.getControlType()?lower_case}-service';
<#else>
import ${srfclassname('${ctrl.codeName}')}Service from '@/app-core/ctrl-service/app/${srffilepath2(ctrl.codeName)}-${ctrl.getControlType()?lower_case}-service';
</#if>

<#if ctrl.getPSUIActions?? && ctrl.getPSUIActions()??>
<#list ctrl.getPSUIActions() as uiAction>
<#if uiAction.getPSAppDataEntity()??>
<#assign curAppEntity = uiAction.getPSAppDataEntity()/>
<#if !P.exists("importService", curAppEntity.getId(), "") >
import ${srfclassname('${curAppEntity.getCodeName()}')}UIService from '@/ui-service/${srffilepath2(curAppEntity.getCodeName())}/${srffilepath2(curAppEntity.getCodeName())}-ui-action';
<#if appde?? && (curAppEntity.getId() == appde.getId())><#assign hasAppDE = true /></#if>
</#if>
</#if>
</#list>
</#if>
<#if appde?? && !hasAppDE??>
import ${srfclassname('${appde.getCodeName()}')}UIService from '@/ui-service/${srffilepath2(appde.getCodeName())}/${srffilepath2(appde.getCodeName())}-ui-action';
</#if>

<#--  语言资源入口  -->
<#ibizinclude>
./LANGBASE.vue.ftl
</#ibizinclude>
<#if import_block??>${import_block}</#if>

@Component({
    components: {
    }
})
export default class ${srfclassname('${ctrl.codeName}')}Base extends Vue implements ControlInterface {

    /**
     * 名称
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected name?: string;

    /**
     * 视图名称
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected viewName!: string;


    /**
     * 视图通讯对象
     *
     * @type {Subject<ViewState>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected viewState!: Subject<ViewState>;

    /**
     * 应用上下文
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({ default: {} }) protected context?: any;

    /**
     * 视图参数
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({ default: {} }) protected viewparams?: any;

    /**
     * 视图状态事件
     *
     * @protected
     * @type {(Subscription | undefined)}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewStateEvent: Subscription | undefined;

    /**
     * 获取部件类型
     *
     * @returns {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected getControlType(): string {
        return '${ctrl.getControlType()}'
    }

    /**
     * 全局 ui 服务
     *
     * @type {GlobalUiService}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected globaluiservice: GlobalUiService = new GlobalUiService();


    <#if appde??>
    /**
     * 转化数据
     *
     * @param {any} args
     * @memberof  ${srfclassname('${ctrl.codeName}')}Base
     */
    public transformData(args: any) {
        let _this: any = this;
        if(_this.service && _this.service.handleRequestData instanceof Function && _this.service.handleRequestData('transform',_this.context,args)){
            return _this.service.handleRequestData('transform',_this.context,args)['data'];
        }
    }
    </#if>

    /**
     * 建构部件服务对象
     *
     * @type {${srfclassname('${ctrl.codeName}')}Service}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected service: ${srfclassname('${ctrl.codeName}')}Service = new ${srfclassname('${ctrl.codeName}')}Service({$store:this.$store});
<#if appde??>

    /**
     * 实体服务对象
     *
     * @type {${srfclassname('${appde.getCodeName()}')}Service}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected appEntityService: ${srfclassname('${appde.getCodeName()}')}Service = new ${srfclassname('${appde.getCodeName()}')}Service();

    /**
     * 界面UI服务对象
     *
     * @type {${srfclassname('${appde.getCodeName()}')}UIService}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */  
    public deUIService:${srfclassname('${appde.getCodeName()}')}UIService = new ${srfclassname('${appde.getCodeName()}')}UIService(this.$store);
</#if>
    <#if ctrl.getPSControls?? && ctrl.getPSControls()??>
    <#list ctrl.getPSControls() as childCtrl>
    <#if childCtrl.getControlType()??>
    <#if childCtrl.getHookEventNames()??>
    <#list childCtrl.getHookEventNames() as eventName>

    /**
     * ${childCtrl.name} 部件 ${eventName?lower_case} 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected ${childCtrl.name}_${eventName?lower_case}($event: any, $event2?: any) {
    <#if childCtrl.getPSControlLogics(eventName)??>
    <#list childCtrl.getPSControlLogics(eventName) as ctrlLogic>
    <#if ctrlLogic.getLogicType?? && ctrlLogic.getLogicType() == "APPVIEWENGINE" && ctrlLogic.getPSAppViewEngine()??>
        this.${ctrlLogic.getPSAppViewEngine().getName()}.onCtrlEvent('${childCtrl.name}', '${eventName?lower_case}', $event);
    <#else>
        <#if ctrlLogic.getEventArg()?? && ctrlLogic.getEventArg()?length gt 0>
        if (Object.is($event.tag, '${ctrlLogic.getEventArg()}')) {
            this.${ctrlLogic.name}($event, '<#if ctrlLogic.getLogicTag()?length gt 0>${ctrlLogic.getLogicTag()}</#if>', $event2);
        }
        <#else>
        this.${ctrlLogic.name}($event, '<#if ctrlLogic.getLogicTag()?length gt 0>${ctrlLogic.getLogicTag()}</#if>', $event2);
        </#if>
    </#if>
    </#list>
    </#if>
    }
    </#list>
    </#if>
    </#if>
    </#list>
    </#if>
    
<#if ctrl.getPSAppViewLogics?? && ctrl.getPSAppViewLogics()??>
<#list ctrl.getPSAppViewLogics() as logic>
<#if logic.getLogicTrigger() == "CUSTOM" || logic.getLogicTrigger() == "CTRLEVENT">

${P.getLogicCode(logic, "LOGIC.vue").code}
</#if>
</#list>
</#if>

    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected closeView(args: any[]): void {
        let _this: any = this;
        _this.$emit('closeview', args);
    }
