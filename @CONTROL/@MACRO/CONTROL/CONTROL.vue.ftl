<script lang='ts'>
import { Component } from 'vue-property-decorator';
import ${srfclassname('${ctrl.codeName}')}Base from './${srffilepath2(ctrl.getCodeName())}-${ctrl.getControlType()?lower_case}-base.vue';
<#if ctrl.getPSControls?? && ctrl.getPSControls()??>
<#list ctrl.getPSControls() as subctrl>
<#if subctrl.getControlType() != 'TOOLBAR' && subctrl.getControlType() != 'CONTEXTMENU' && !(subctrl.getControlType() == 'PORTLET' && subctrl.getPortletType() == 'CONTAINER')>
<#if subctrl.getPSAppDataEntity?? && subctrl.getPSAppDataEntity()??>
import view_${subctrl.getName()} from '@widgets/${srffilepath2(subctrl.getPSAppDataEntity().getCodeName())}/${srffilepath2(subctrl.getCodeName())}-${subctrl.getControlType()?lower_case}/${srffilepath2(subctrl.getCodeName())}-${subctrl.getControlType()?lower_case}.vue';
<#else>
import view_${subctrl.getName()} from '@widgets/app/${srffilepath2(subctrl.getCodeName())}-${subctrl.getControlType()?lower_case}/${srffilepath2(subctrl.getCodeName())}-${subctrl.getControlType()?lower_case}.vue';
</#if>
</#if>
</#list>
</#if>

<#if ctrl.getPSLayoutPanels?? && ctrl.getPSLayoutPanels()??>
<#list ctrl.getPSLayoutPanels() as panel>
import layout_${panel.getName()} from '@widgets/${srffilepath2(panel.getPSDataEntity().getCodeName())}/${srffilepath2(panel.getCodeName())}-${panel.getControlType()?lower_case}/${srffilepath2(panel.getCodeName())}-${panel.getControlType()?lower_case}.vue';
</#list>
</#if>
<#if import_block??>${import_block}</#if> 
@Component({
    components: {
        <#if ctrl.getPSControls?? && ctrl.getPSControls()??>
        <#list ctrl.getPSControls() as subctrl>
        <#if subctrl.getControlType() != 'TOOLBAR' &&  subctrl.getControlType() != 'CONTEXTMENU' && !(subctrl.getControlType() == 'PORTLET' && subctrl.getPortletType() == 'CONTAINER')>
        view_${subctrl.getName()}, 
        </#if>
        </#list>
        </#if>
        <#if ctrl.getPSLayoutPanels?? && ctrl.getPSLayoutPanels()??>
        <#list ctrl.getPSLayoutPanels() as panel>
        layout_${panel.getName()},
        </#list>
        </#if>
        <#if components??>${components}</#if> 
    }
})
export default class ${srfclassname('${ctrl.codeName}')} extends ${srfclassname('${ctrl.codeName}')}Base {

}
</script>