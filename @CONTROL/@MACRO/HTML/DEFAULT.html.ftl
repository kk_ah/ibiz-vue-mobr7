<#-- ctrl document  -->
<view_${ctrl.getName()}
    :viewState="viewState"
    viewName="${view.getName()}"  
    :viewparams="viewparams" 
    :context="context" 
    <#if content??>
    ${content}<#t>
    </#if> 
    name="${ctrl.name}"  
    ref='${ctrl.name}' 
    <#if ctrl.getHookEventNames()??>
    <#list ctrl.getHookEventNames() as eventName>
    @${eventName?lower_case}="${ctrl.name}_${eventName?lower_case}($event)"  
    </#list>
    </#if>
    @closeview="closeView($event)">
</view_${ctrl.getName()}>