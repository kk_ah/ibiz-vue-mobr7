<#-- ctrl document  -->
<view_${ctrl.getName()} 
    :viewState="viewState"  
    name='${ctrl.name}' 
    ref='${ctrl.name}'
    :viewparams="viewparams" 
    :context="context" 
    :formData="formData"
    <#if ctrl.getHookEventNames()??>
    <#list ctrl.getHookEventNames() as eventName>
    @${eventName?lower_case}='${ctrl.name}_${eventName?lower_case}($event)' 
    </#list>
    </#if>
    <#if ctrl.getPSAppDataEntity()??>parentName = "${ctrl.getPSAppDataEntity().getCodeName()}"</#if>
    <#if view.getViewType() == 'DEEDITVIEW4'>
    :isShowSlot="false"
    </#if>
    @closeview='closeView($event)'>
    <#if view.hasPSControl('form')>
    ${P.getCtrlCode('form', 'CONTROL.html').code}
    </#if>
</view_${ctrl.getName()}>