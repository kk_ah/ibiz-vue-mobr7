<#--  content  -->
<#assign content>
    :isSingleSelect="isSingleSelect" 
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}"
    <#if view.getViewType() == "DEGRIDVIEW" || view.getViewType() == "DEGRIDVIEW9">
    :isOpenEdit="${view.isRowEditDefault()?c}"
    @save="onSave"
    </#if>
    updateAction="<#if ctrl.getUpdatePSControlAction()?? && ctrl.getUpdatePSControlAction().getPSAppDEMethod()??>${ctrl.getUpdatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    removeAction="<#if ctrl.getRemovePSControlAction()?? && ctrl.getRemovePSControlAction().getPSAppDEMethod()??>${ctrl.getRemovePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    loaddraftAction="<#if ctrl.getGetDraftPSControlAction()?? && ctrl.getGetDraftPSControlAction().getPSAppDEMethod()??>${ctrl.getGetDraftPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    loadAction="<#if ctrl.getGetPSControlAction()?? && ctrl.getGetPSControlAction().getPSAppDEMethod()??>${ctrl.getGetPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    createAction="<#if ctrl.getCreatePSControlAction()?? && ctrl.getCreatePSControlAction().getPSAppDEMethod()??>${ctrl.getCreatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    fetchAction="<#if ctrl.getFetchPSControlAction()?? && ctrl.getFetchPSControlAction().getPSAppDEMethod()??>${ctrl.getFetchPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
</#assign>
<#ibizinclude>
./DEFAULT.html.ftl
</#ibizinclude>