<template>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <div class='app-tab-view-panel <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>' v-if='isActivied' >
        <#if ctrl.getEmbeddedPSAppDEView()??>
        <#assign embedddevedview = ctrl.getEmbeddedPSAppDEView()>
        <${srffilepath2(embedddevedview.getCodeName())} 
            :_context="JSON.stringify(_context)" 
            :_viewparams="JSON.stringify(_viewparams)" 
            :panelNavParam="panelNavParam"
            :panelNavContext="panelNavContext"
            :viewDefaultUsage="false" >
        </${srffilepath2(embedddevedview.getCodeName())}></#if>
    </div>
</#if>
</template>