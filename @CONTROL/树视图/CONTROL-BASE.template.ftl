<template>
    <div class="app-mob-treeview <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#if ctrl.render??>
    ${ctrl.render.code}
    <#else>
        <ion-searchbar></ion-searchbar>
        <div class="treeNav">
            <template v-for="(item,index) in treeNav">
            <ion-label  :key="index" class="sc-ion-label-ios-h sc-ion-label-ios-s ios hydrated" :class="index+1 < treeNav.length? 'treeNav-active':'' " @click="nav_click(item)">{{item.text}}</ion-label>
            <span class="tree-span" :key="index+'span'" v-if="index+1 < treeNav.length">></span>
            </template>
        </div>
        <div class="tree-partition" v-if="valueNodes.length > 0" ></div>
        <ion-list>
        <template v-for="item in rootNodes">
            <ion-item v-if="item.isNode"  :key="item.id" @click="click_node(item)">
                <ion-label>{{item.text}}</ion-label>
                <ion-icon class="tree-icon" slot="end" name="chevron-forward-outline"></ion-icon>
            </ion-item>
        </template>
       </ion-list>
        <div class="tree-partition tree-partition-bigger" v-if="rootNodes.length > 0"></div>
        <ion-list>
        <template v-for="item in valueNodes">
            <ion-item v-if="!item.isNode"  :key="item.id">
                <ion-label>{{item.text}}</ion-label>
            </ion-item>
        </template>
       </ion-list>
    </#if>
    </div>
</template>