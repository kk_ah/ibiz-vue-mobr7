<#assign import_block>
import i18n from '@/locale';
<#if ctrl.getPSDETreeNodes()??>
<#list ctrl.getPSDETreeNodes() as node>
<#if node.getTreeNodeType() == 'DE' && node.getPSAppDataEntity()??>
<#if appde?? && appde.getId() != node.getPSAppDataEntity().getId()>
<#if !P.exists("importService", node.getPSAppDataEntity().getId(), "")>
import ${srfclassname('${node.getPSAppDataEntity().codeName}')}Service from '@/app-core/service/${srffilepath2(node.getPSAppDataEntity().codeName)}/${srffilepath2(node.getPSAppDataEntity().codeName)}-service';
</#if>
</#if>
</#if>
</#list>
</#if>
</#assign>
<#assign extendsClass>TreeViewServiceBase</#assign>
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_HEADER.ts.ftl
</#ibizinclude>

<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>

<#-- 获取树节点关系导航上下文 -->
<#macro getNavContext nodeRs>
<#compress>
<#if nodeRs.getPSDETreeNodeRSNavContexts()??>
<#list nodeRs.getPSDETreeNodeRSNavContexts() as rsNavContext>
"${rsNavContext.getKey()}":{"isRawValue":${rsNavContext.isRawValue()?c},"value":"${rsNavContext.getValue()}"}<#if rsNavContext_has_next>,</#if>
</#list>
</#if>
</#compress>
</#macro>

<#-- 获取树节点关系导航参数 -->
<#macro getNavParams nodeRs>
<#compress>
<#if nodeRs.getPSDETreeNodeRSNavParams()??>
<#list nodeRs.getPSDETreeNodeRSNavParams() as rsNavParam>
"${rsNavParam.getKey()}":{"isRawValue":${rsNavParam.isRawValue()?c},"value":"${rsNavParam.getValue()}"}<#if rsNavParam_has_next>,</#if>
</#list>
</#if>
</#compress>
</#macro>

<#-- 获取树节点关系参数 -->
<#macro getParams nodeRs>
<#compress>
<#if nodeRs.getPSDETreeNodeRSParams()??>
<#list nodeRs.getPSDETreeNodeRSParams() as rsParam>
"${rsParam.getKey()}":{"value":"${rsParam.getValue()}"}<#if rsParam_has_next>,</#if>
</#list>
</#if>
</#compress>
</#macro>

<#if ctrl.getPSDETreeNodes()??>
<#list ctrl.getPSDETreeNodes() as node>
<#if node.getTreeNodeType() == 'DE' && node.getPSAppDataEntity()??>
<#if appde?? && appde.getId() != node.getPSAppDataEntity().getId()>
<#if !P.exists("importService2", node.getPSAppDataEntity().getId(), "")>

    /**
     * ${node.getPSAppDataEntity().getLogicName()}服务对象
     *
     * @type {${srfclassname('${node.getPSAppDataEntity().codeName}')}Service}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public ${node.getPSAppDataEntity().codeName?lower_case}Service: ${srfclassname('${node.getPSAppDataEntity().codeName}')}Service = new ${srfclassname('${node.getPSAppDataEntity().codeName}')}Service({ $store: this.getStore() });
</#if>
</#if>
</#if>
</#list>
</#if>

    /**
     * 节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public TREENODE_SEPARATOR: string = ';';
<#if ctrl.getPSDETreeNodes()??>
<#list ctrl.getPSDETreeNodes() as node>

    /**
     * ${node.getName()}节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
	public TREENODE_${node.getNodeType()?upper_case}: string = '${node.getNodeType()?j_string}';
</#list>
</#if>

    /**
     * 获取节点数据
     *
     * @param {string} action
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public async getNodes(context:any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let { srfparentkey, srfcat, srfnodeid, srfnodefilter, query }: { srfparentkey: string, srfcat: string, srfnodeid: string, srfnodefilter: string, query:string } = data;
        srfnodefilter = query ? query : srfnodefilter;
        let list: any[] = [];
        let filter: any = {};


    <#--  BEGIN：逻辑代码  -->
        if (!srfnodeid || Object.is(srfnodeid, '#')) {
            <#if ctrl.isRootVisible()>
            await this.fill${srfclassname(ctrl.getRootPSDETreeNode().getNodeType()?lower_case)}Nodes(context, filter, list);
            return Promise.resolve({ status: 200, data: list });
            <#else>
            srfnodeid = this.TREENODE_${ctrl.getRootPSDETreeNode().getNodeType()?upper_case};
            </#if>
        }

        let strTreeNodeId: string = srfnodeid;
        let strRealNodeId: string = '';
        let bRootSelect: boolean = false;
        let strNodeType: string | null = null;
        let strRootSelectNode: string = '';

        if (Object.is(strTreeNodeId, this.TREENODE_${ctrl.getRootPSDETreeNode().getNodeType()?upper_case})) {
            strNodeType = this.TREENODE_${ctrl.getRootPSDETreeNode().getNodeType()?upper_case};
            if (srfparentkey) {
                strRealNodeId = srfparentkey;
            }
        } else {
            let nPos = strTreeNodeId.indexOf(this.TREENODE_SEPARATOR);
            if (nPos === -1) {
                return Promise.reject({ status: 500, data: { title: '失败', message: `树节点${r'${strTreeNodeId}'}标识无效` } });
            }
            strNodeType = strTreeNodeId.substring(0, nPos);
			strRealNodeId = strTreeNodeId.substring(nPos + 1);
        }

        Object.assign(filter,
            {
                srfparentkey: srfparentkey,
                srfcat: srfcat,
                srfnodefilter: srfnodefilter,
                strRealNodeId: strRealNodeId,
                srfnodeid: srfnodeid,
                strNodeType: strNodeType,
                viewparams: JSON.parse(JSON.stringify(data)).viewparams
            }
        );

        // 分解节点标识
        let nodeid: string[] = strRealNodeId.split(this.TREENODE_SEPARATOR);
        for (let i = 0; i < nodeid.length; i++) {
            switch (i) {
                case 0:
                    Object.assign(filter, { nodeid: nodeid[0] });
                    break;
                case 1:
                    Object.assign(filter, { nodeid2: nodeid[1] });
                    break;
                case 2:
                    Object.assign(filter, { nodeid3: nodeid[2] });
                    break;
                case 3:
                    Object.assign(filter, { nodeid4: nodeid[3] });
                    break;
                default:
                    break;
            }
        }

        <#if ctrl.getPSDETreeNodes()??>
        <#list ctrl.getPSDETreeNodes() as node>
        if (Object.is(strNodeType, this.TREENODE_${node.getNodeType()?upper_case})) {
            await this.fill${srfclassname(node.getNodeType()?lower_case)}NodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        </#list>
        </#if>
    <#--  END：逻辑代码  -->
        return Promise.resolve({ status: 500, data: { title: '失败', message: `树节点${r'${strTreeNodeId}'}标识无效` } });
    }

<#--  BEGIN：填充代码  -->
<#if ctrl.getPSDETreeNodes()??>
<#list ctrl.getPSDETreeNodes() as node>
<#assign filtername="">
<#assign entityname="">
    /**
     * 填充 树视图节点[${node.getName()}]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public fill${srfclassname(node.getNodeType()?lower_case)}Nodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
    <#if node.getTreeNodeType() == 'STATIC'>
            let treeNode: any = {};
            Object.assign(treeNode, { text: '${node.getName()}'});
            Object.assign(treeNode, { isUseLangRes: true });
    <#if node.isEnableQuickSearch()>
            if(filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")){
                if((i18n.t(treeNode.text) as string).toUpperCase().indexOf(filter.getSrfnodefilter().toUpperCase())==-1)
                    return Promise.reject();
            }
    </#if>
    <#if (node.hasPSDETreeNodeRSs?? && node.hasPSDETreeNodeRSs()?? && node.hasPSDETreeNodeRSs()) ||  node.getTreeNodeType() != 'STATIC'>
            Object.assign(treeNode, { isNode: true });
    <#else>
            Object.assign(treeNode, { isNode: false });
    </#if>
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = '${node.getNodeType()?j_string}';

          <#if node.getNodeValue()?? && node.getNodeValue()?length gt 0>
            Object.assign(treeNode, { srfkey: '${node.getNodeValue()?j_string}' });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += '${node.getNodeValue()?j_string}';

            <#if node.isAppendPNodeId()>
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            </#if>
          <#else>
            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

          </#if>
            Object.assign(treeNode, { id: strNodeId });

            <#if node.getPSSysImage()??>
            <#if node.getPSSysImage().getCssClass()?? && node.getPSSysImage().getCssClass()?length gt 0>
            Object.assign(treeNode, { iconcls: '${node.getPSSysImage().getCssClass()}' });

            <#else>
            Object.assign(treeNode, { icon: '${node.getPSSysImage().getImagePath()}' });

            </#if>
            </#if>
            <#if node.isEnableCheck()>
            Object.assign(treeNode, { enablecheck: true });

            </#if>
            <#if node.isDisableSelect()>
            Object.assign(treeNode, { disabled: true });

            </#if>
            <#if node.isExpanded()>
            Object.assign(treeNode, { expanded: true });
            <#else>
            Object.assign(treeNode, { expanded: filter.isAutoexpand });
            </#if>
            <#if node.hasPSDETreeNodeRSs()>
            Object.assign(treeNode, { leaf: false });
            <#else>
            Object.assign(treeNode, { leaf: true });
            </#if>
            <#if node.isSelected()>
            Object.assign(treeNode, { selected: true });
            </#if>
            <#if node.getNavFilter()??>
            Object.assign(treeNode, { navfilter: "${node.getNavFilter()}" });
            </#if>
            <#if node.getPSNavigateContexts?? && node.getPSNavigateContexts()??>
            Object.assign(treeNode, {navigateContext: <@getNavigateContext node /> });
            </#if>
            <#if node.getPSNavigateParams?? && node.getPSNavigateParams()??>
            Object.assign(treeNode, {navigateParams: <@getNavigateParams node /> });
            </#if>
            <#-- 补充nodeid和nodeid2  -->
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
</#if>
<#if node.getTreeNodeType() == 'CODELIST' && node.getPSCodeList()??>
        <#if node.getPSCodeList().getCodeListType()?? && node.getPSCodeList().getCodeListType() == 'STATIC'>
            let codeListIems:Array<any> = [];
            if (this.getStore()) {
                codeListIems = (this.getStore() as any).getters.getCodeListItems('${node.getPSCodeList().codeName}');
            }
        <#elseif node.getPSCodeList().getCodeListType()?? && node.getPSCodeList().getCodeListType() == 'DYNAMIC'>
            this.codeListService.getItems('${node.getPSCodeList().codeName}',context).then((codeListIems:any)=>{
        </#if>
            if(codeListIems && codeListIems.length >0){
                const handleChildData = (context:any,item:any,) =>{
                    Object.assign(item,{srfappctx:context});
                    <#if node.getPSCodeList().getCodeListType() == 'STATIC'>
                    Object.assign(item, { text: i18n.t('codelist.${node.getPSCodeList().codeName}.'+item.value) });
                    Object.assign(item, { isUseLangRes: true });
                    <#else>
                    Object.assign(item, { text: item.text });
                    </#if>
                    Object.assign(item, { srfmajortext: item.text });
                    let strNodeId: string = "${srfjavastring(node.getNodeType())}";
                    Object.assign(item, { srfkey: item.value });
                    strNodeId += this.TREENODE_SEPARATOR;
                    strNodeId += item.value;
                    <#if node.isAppendPNodeId()>
                    strNodeId += TREENODE_SEPARATOR;
                    strNodeId += filter.getRealnodeid();
                    </#if>
                    Object.assign(item, { id: strNodeId });
                    <#if node.getPSSysImage()??>
                    <#if node.getPSSysImage().getCssClass()?? && node.getPSSysImage().getCssClass()?length gt 0>
                    Object.assign(item, { iconcls: '${node.getPSSysImage().getCssClass()}' });
                    <#else>
                    Object.assign(item, { icon: '${node.getPSSysImage().getImagePath()}' });
                    </#if>
                    </#if>
                    <#if node.isEnableCheck()>
                    Object.assign(item, { enablecheck: true });
                    </#if>
                    <#if node.isDisableSelect()>
                    Object.assign(item, { disabled: true });
                    </#if>
                    <#if node.isExpanded()>
                    <#if node.isExpandFirstOnly()>
                    Object.assign(item, { expanded: bFirst });
                    <#else>
                    Object.assign(item, { expanded: true });
                    </#if>
                    <#else>
                    Object.assign(item, { expanded: filter.isautoexpand });
                    </#if>
                    <#if node.isSelected()>
                    Object.assign(item, { selected: true });
                    </#if>
                    <#if node.getNavFilter()??>
                    Object.assign(item, { navfilter: "${node.getNavFilter()}" });
                    </#if>
                    <#if node.getPSNavigateContexts?? && node.getPSNavigateContexts()??>
                    Object.assign(item, {navigateContext: <@getNavigateContext node /> });
                    </#if>
                    <#if node.getPSNavigateParams?? && node.getPSNavigateParams()??>
                    Object.assign(item, {navigateParams: <@getNavigateParams node /> });
                    </#if>
                    <#-- 补充nodeid和nodeid2  -->
                    Object.assign(item, { nodeid: item.srfkey });
                    Object.assign(item, { nodeid2: item.pvalue });
                }
                codeListIems = this.handleDataSet(JSON.parse(JSON.stringify(codeListIems)),context,handleChildData);
                codeListIems.forEach((item:any) => {
                    let treeNode: any = {};
                    Object.assign(treeNode,{srfappctx:context});
                    <#if node.getPSCodeList().getCodeListType() == 'STATIC'>
                    Object.assign(treeNode, { text: i18n.t('codelist.${node.getPSCodeList().codeName}.'+item.value) });
                    Object.assign(treeNode, { isUseLangRes: true });
                    <#else>
                    Object.assign(treeNode, { text: item.text });
                    </#if>
                    <#if node.isEnableQuickSearch()>
                    if(filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")){
                        if((i18n.t(treeNode.text) as string).toUpperCase().indexOf(filter.getSrfnodefilter().toUpperCase())==-1)
                            return;
                    }
                    </#if>
                    Object.assign(treeNode, { srfmajortext: treeNode.text });
                    let strNodeId: string = "${srfjavastring(node.getNodeType())}";
                    Object.assign(treeNode, { srfkey: item.value });
                    strNodeId += this.TREENODE_SEPARATOR;
                    strNodeId += item.value;
                    <#if node.isAppendPNodeId()>
                    strNodeId += TREENODE_SEPARATOR;
                    strNodeId += filter.getRealnodeid();
                    </#if>
                    Object.assign(treeNode, { id: strNodeId });
                    <#if node.getPSSysImage()??>
                    <#if node.getPSSysImage().getCssClass()?? && node.getPSSysImage().getCssClass()?length gt 0>
                    Object.assign(treeNode, { iconcls: '${node.getPSSysImage().getCssClass()}' });
                    <#else>
                    Object.assign(treeNode, { icon: '${node.getPSSysImage().getImagePath()}' });
                    </#if>
                    </#if>
                    <#if node.isEnableCheck()>
                    Object.assign(treeNode, { enablecheck: true });
                    </#if>
                    <#if node.isDisableSelect()>
                    Object.assign(treeNode, { disabled: true });
                    </#if>
                    <#if node.isExpanded()>
                    <#if node.isExpandFirstOnly()>
                    Object.assign(treeNode, { expanded: bFirst });
                    <#else>
                    Object.assign(treeNode, { expanded: true });
                    </#if>
                    <#else>
                    Object.assign(treeNode, { expanded: filter.isautoexpand });
                    </#if>
                    <#if node.hasPSDETreeNodeRSs()>
                    Object.assign(treeNode, { leaf: false });
                    <#else>
                    Object.assign(treeNode, { leaf: true });
                    </#if>
                    <#if node.isSelected()>
                    Object.assign(treeNode, { selected: true });
                    </#if>
                    <#if node.getNavFilter()??>
                    Object.assign(treeNode, { navfilter: "${node.getNavFilter()}" });
                    </#if>
                    <#if node.getPSNavigateContexts?? && node.getPSNavigateContexts()??>
                    Object.assign(treeNode, {navigateContext: <@getNavigateContext node /> });
                    </#if>
                    <#if node.getPSNavigateParams?? && node.getPSNavigateParams()??>
                    Object.assign(treeNode, {navigateParams: <@getNavigateParams node /> });
                    </#if>
                    if(item.children && item.children.length >0){
                        Object.assign(treeNode, { children: item.children });
                    }
                    <#-- 补充nodeid和nodeid2  -->
                    Object.assign(treeNode, { nodeid: treeNode.srfkey });
                    Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
                    list.push(treeNode);
                    resolve(list);
                })
             }else{
                resolve(list);
             }
            <#if node.getPSCodeList().getCodeListType()?? && node.getPSCodeList().getCodeListType() == 'DYNAMIC'>
            });
            </#if>
        });
</#if>
<#if node.getTreeNodeType() == 'DE' && node.getPSAppDataEntity()??>
    <#assign filtername>${node.getPSAppDataEntity().codeName}SearchFilter</#assign>
    <#assign entityname>${node.getPSAppDataEntity().codeName}</#assign>
            let searchFilter: any = {};
    <#list ctrl.getPSDETreeNodeRSs() as noders>
    <#if noders.getChildPSDETreeNode().id == node.id>
    <#if noders.getParentFilter()?? && noders.getParentFilter()?length gt 0>
    <#assign pickupfield=noders.getParentFilter()>

            if (Object.is(filter.strNodeType, this.TREENODE_${noders.getParentPSDETreeNode().getNodeType()?upper_case})) {
                Object.assign(searchFilter, { n_${pickupfield?lower_case}_eq: filter.nodeid<#if noders.getParentValueLevel() gt 1>${noders.getParentValueLevel()?c}</#if> });
            }

    <#elseif noders.getParentPSDER1N()??>
    <#assign pickupfield=noders.getParentPSDER1N().getPSPickupDEField()>
            if (Object.is(filter.strNodeType, this.TREENODE_${noders.getParentPSDETreeNode().getNodeType()?upper_case})) {
                Object.assign(searchFilter, { n_${pickupfield.getName()?lower_case}_eq: filter.nodeid<#if noders.getParentValueLevel() gt 1>${noders.getParentValueLevel()?c}</#if> });
            }

    </#if>
    </#if>
    </#list>
            Object.assign(searchFilter, { total: false });
    <#if node.isEnableQuickSearch()>
            Object.assign(searchFilter, { query: filter.srfnodefilter });
    </#if>
            let bFirst: boolean = true;
            let records: any[] = [];
            try {
                this.search${srfclassname(node.getNodeType()?lower_case)}(context, searchFilter, filter).then((records:any) =>{
                    if(records && records.length >0){
                        records.forEach((entity: any) => {
                        let treeNode: any = {};
                        // 整理context
                        <#if node.getIdPSDEField()??>
                        let strId: string = entity.${node.getIdPSDEField().codeName?lower_case};
                        <#else>
                        let strId: string = entity.${node.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case};
                        </#if>
                        <#if node.getTextPSDEField()??>
                        let strText: string = entity.${node.getTextPSDEField().codeName?lower_case};
                        <#else>
                        let strText: string = entity.${node.getPSAppDataEntity().getMajorPSAppDEField().codeName?lower_case};
                        </#if>
                        Object.assign(treeNode,{srfparentdename:'${node.getPSAppDataEntity().getCodeName()}',srfparentkey:<#if node.getIdPSDEField()??>entity.${node.getIdPSDEField().codeName?lower_case}<#else>entity.${node.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case}</#if>});
                        let tempContext:any = JSON.parse(JSON.stringify(context));
                        Object.assign(tempContext,{srfparentdename:'${node.getPSAppDataEntity().getCodeName()}',srfparentkey:<#if node.getIdPSDEField()??>entity.${node.getIdPSDEField().codeName?lower_case}<#else>entity.${node.getPSAppDataEntity().getKeyPSAppDEField().codeName?lower_case}</#if>,${node.getPSAppDataEntity().getCodeName()?lower_case}:strId})
                        Object.assign(treeNode,{srfappctx:tempContext});
                        Object.assign(treeNode,{'${node.getPSAppDataEntity().getCodeName()?lower_case}':strId});
                        Object.assign(treeNode, { srfkey: strId });
                        Object.assign(treeNode, { text: strText, srfmajortext: strText });
                        let strNodeId: string = '${node.getNodeType()?j_string}';
                        strNodeId += this.TREENODE_SEPARATOR;
                        strNodeId += strId;
                        <#if node.isAppendPNodeId()>
                        strNodeId += this.TREENODE_SEPARATOR;
                        strNodeId += filter.realnodeid;
                        </#if>
                        Object.assign(treeNode, { id: strNodeId });
                        <#if node.getPSSysImage()??>
                        <#if node.getPSSysImage().getCssClass()?? && node.getPSSysImage().getCssClass()?length gt 0>
                        Object.assign(treeNode, { iconcls: '${node.getPSSysImage().getCssClass()}' });
                        <#else>
                        Object.assign(treeNode, { icon: '${node.getPSSysImage().getImagePath()}' });
                        </#if>
                        </#if>
                        <#if node.isEnableCheck()>
                        Object.assign(treeNode, { enablecheck: true });
                        </#if>
                        <#if node.isDisableSelect()>
                        Object.assign(treeNode, { disabled: true });
                        </#if>
                        <#if node.isExpanded()>
                        <#if node.isExpandFirstOnly()>
                        Object.assign(treeNode, { expanded: bFirst });
                        <#else>
                        Object.assign(treeNode, { expanded: true });
                        </#if>
                        <#else>
                        Object.assign(treeNode, { expanded: filter.isautoexpand });
                        </#if>
                        <#if node.hasPSDETreeNodeRSs()>
                        Object.assign(treeNode, { leaf: false });
                        <#else>
                        Object.assign(treeNode, { leaf: true });
                        </#if>
                        <#if node.getLeafFlagPSDEField()??>
                        let objLeafFlag = entity.${node.getLeafFlagPSDEField().codeName?lower_case};
                        if (objLeafFlag != null ) {
                            let strLeafFlag: string = objLeafFlag.toString().toLowerCase();
                            if (Object.is(strLeafFlag, '1') || Object.is(strLeafFlag, 'true')){
                                Object.assign(treeNode, { leaf: true });
                            }
                        }
                        </#if>
                        <#if node.getPSDETreeNodeDataItems()??>
                        <#list node.getPSDETreeNodeDataItems() as dataitem>
                        <#if dataitem.getPSDEField()??>
                        Object.assign(treeNode, { ${dataitem.name?lower_case}: entity.${dataitem.getPSDEField().codeName?lower_case} });
                        </#if>
                        </#list>
                        </#if>
                        <#if node.isSelected()>
                        Object.assign(treeNode, { selected: true });
                        </#if>
                        <#if node.getNavFilter()??>
                        Object.assign(treeNode, { navfilter: "${node.getNavFilter()}" });
                        </#if>
                        Object.assign(treeNode, { curData: entity });
                        <#if node.getPSNavigateContexts?? && node.getPSNavigateContexts()??>
                        Object.assign(treeNode, {navigateContext: <@getNavigateContext node /> });
                        </#if>
                        <#if node.getPSNavigateParams?? && node.getPSNavigateParams()??>
                        Object.assign(treeNode, {navigateParams: <@getNavigateParams node /> });
                        </#if>
                        <#-- 补充nodeid和nodeid2  -->
                        Object.assign(treeNode, { nodeid: treeNode.srfkey });
                        Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
                        list.push(treeNode);
                        resolve(list);
                        bFirst = false;
                    });
                    }else{
                        resolve(list);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        });

</#if>
	}
<#if (filtername?length gt 0) && (entityname?length gt 0)>

    /**
     * 获取查询集合
     *
     * @public
     * @param {any{}} context     
     * @param {*} searchFilter
     * @param {*} filter
     * @returns {any[]}
     * @memberof TestEnetityDatasService
     */
    public search${srfclassname(node.getNodeType()?lower_case)}(context:any={}, searchFilter: any, filter: any): Promise<any> {
        return new Promise((resolve:any,reject:any) =>{
            if(filter.viewparams){
                Object.assign(searchFilter,filter.viewparams);
            }
            if(!searchFilter.page){
                Object.assign(searchFilter,{page:0});
            }
            if(!searchFilter.size){
                Object.assign(searchFilter,{size:1000});
            }
            if(context && context.srfparentdename){
                Object.assign(searchFilter,{srfparentdename:JSON.parse(JSON.stringify(context)).srfparentdename});
            }
            if(context && context.srfparentkey){
                Object.assign(searchFilter,{srfparentkey:JSON.parse(JSON.stringify(context)).srfparentkey});
            }
            <#if node.getSortField?? && node.getSortField()?? && node.getSortField() != "" && node.getSortDir?? && node.getSortDir()?? && node.getSortDir() != "">
            Object.assign(searchFilter,{sort: '${node.getSortField()?lower_case},${node.getSortDir()?lower_case}'})
            </#if>
            <#if appde?? && appde.getId() != node.getPSAppDataEntity().getId()>
            const _appEntityService: any = this.${node.getPSAppDataEntity().codeName?lower_case}Service;
            <#else>
            const _appEntityService: any = this.service;
            </#if>
            let list: any[] = [];
            if (_appEntityService['${node.getPSAppDEDataSet().getCodeName()}'] && _appEntityService['${node.getPSAppDEDataSet().getCodeName()}'] instanceof Function) {
                const response: Promise<any> = _appEntityService['${node.getPSAppDEDataSet().getCodeName()}'](context, searchFilter, false);
                response.then((response: any) => {
                    if (!response.status || response.status !== 200) {
                        resolve([]);
                        console.log(JSON.stringify(context));
                        console.error('查询${node.getPSAppDEDataSet().getCodeName()}数据集异常!');
                    }
                    const data: any = response.data;
                    if (Object.keys(data).length > 0) {
                        list = JSON.parse(JSON.stringify(data));
                        resolve(list);
                    } else {
                        resolve([]);
                    }
                }).catch((response: any) => {
                        resolve([]);
                        console.log(JSON.stringify(context));
                        console.error('查询${node.getPSAppDEDataSet().getCodeName()}数据集异常!');
                });
            }
        })
    }
</#if>

    /**
     * 填充 树视图节点[${node.getName()}]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public async fill${srfclassname(node.getNodeType()?lower_case)}NodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
	<#if ctrl.getPSDETreeNodeRSs()??>
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		<#list ctrl.getPSDETreeNodeRSs() as noders>
		<#if noders.getParentPSDETreeNode().id == node.id>
		<#if noders.getSearchMode() == 1 || noders.getSearchMode() == 3>
			// 填充${noders.getChildPSDETreeNode().getName()}
            let ${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavContext:any = {<@getNavContext nodeRs=noders />};
            let ${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavParams:any = {<@getNavParams nodeRs=noders />};
            let ${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsParams:any = {<@getParams nodeRs=noders />};
			await this.fill${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}Nodes(context, filter, list ,${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavContext,${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavParams,${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsParams);
		</#if>
		</#if>
		</#list>
		} else {
		<#list ctrl.getPSDETreeNodeRSs() as noders>
		<#if noders.getParentPSDETreeNode().id == node.id>
		<#if noders.getSearchMode() == 2 || noders.getSearchMode() == 3>
			// 填充${noders.getChildPSDETreeNode().getName()}
            let ${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavContext:any = {<@getNavContext nodeRs=noders />};
            let ${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavParams:any = {<@getNavParams nodeRs=noders />};
            let ${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsParams:any = {<@getParams nodeRs=noders />};
			await this.fill${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}Nodes(context, filter, list ,${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavContext,${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsNavParams,${srfclassname(noders.getChildPSDETreeNode().getNodeType()?lower_case)}RsParams);
		</#if>
		</#if>
		</#list>
		}
	</#if>
	}

</#list>
</#if>

    /**
     * 处理代码表返回数据(树状结构)
     * 
     * @param result 返回数组
     * @param context 应用上下文
     * @param callBack 回调
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public handleDataSet(result:Array<any>,context:any,callBack:any){
        let list:Array<any> = [];
        if(result.length === 0){
            return list;
        }
        result.forEach((codeItem:any) =>{
            if(!codeItem.pvalue){
                let valueField:string = codeItem.value;
                this.setChildCodeItems(valueField,result,codeItem);
                list.push(codeItem);
            }
        })
        this.setNodeData(list,context,callBack);
        return list;
    }

    /**
     * 处理非根节点数据
     * 
     * @param result 返回数组
     * @param context 应用上下文
     * @param callBack 回调
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public setChildCodeItems(pValue:string,result:Array<any>,codeItem:any){
        result.forEach((item:any) =>{
            if(item.pvalue == pValue){
                let valueField:string = item.value;
                this.setChildCodeItems(valueField,result,item);
                if(!codeItem.children){
                    codeItem.children = [];
                }
                codeItem.children.push(item);
            }
        })
    }

    /**
     * 设置节点UI数据
     * 
     * @param result 返回数组
     * @param context 应用上下文
     * @param callBack 回调
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public setNodeData(result:Array<any>,context:any,callBack:any){
        result.forEach((item:any) =>{
            if(item.children){
                item.leaf = false;
                this.setNodeData(item.children,context,callBack);
            }else{
                item.leaf = true;
            }
            callBack(context,item);
        })
    }

    /**
     * 处理节点关系导航上下文
     *
     * @param context 应用上下文
     * @param filter 参数 
     * @param resNavContext 节点关系导航上下文
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public handleResNavContext(context:any,filter:any,resNavContext:any){
        if(resNavContext && Object.keys(resNavContext).length > 0){
            let tempContextData:any = JSON.parse(JSON.stringify(context));
            let tempViewParams:any = {};
            if(filter && filter.viewparams){
                tempViewParams = filter.viewparams;
            }
            Object.keys(resNavContext).forEach((item:any) =>{
                let curDataObj:any = resNavContext[item];
                this.handleCustomDataLogic(context,tempViewParams,curDataObj,tempContextData,item);
            })
            return tempContextData;
        }else{
            return context;
        }
    }

    /**
     * 处理关系导航参数
     *
     * @param context 应用上下文
     * @param filter 参数 
     * @param resNavParams 节点关系导航参数
     * @param resParams 节点关系参数
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
	public handleResNavParams(context:any,filter:any,resNavParams:any,resParams:any){
        if((resNavParams && Object.keys(resNavParams).length >0) || (resParams && Object.keys(resParams).length >0)){
            let tempViewParamData:any = {};
            let tempViewParams:any = {};
            if(filter && filter.viewparams){
                tempViewParams = filter.viewparams;
                tempViewParamData = JSON.parse(JSON.stringify(filter.viewparams));
            }
            if( Object.keys(resNavParams).length > 0){
                Object.keys(resNavParams).forEach((item:any) =>{
                    let curDataObj:any = resNavParams[item];
                    this.handleCustomDataLogic(context,tempViewParams,curDataObj,tempViewParamData,item);
                })
            }
            if( Object.keys(resParams).length > 0){
                Object.keys(resParams).forEach((item:any) =>{
                    let curDataObj:any = resParams[item];
                    tempViewParamData[item.toLowerCase()] = curDataObj.value;
                })
            }
            Object.assign(filter,{viewparams:tempViewParamData});
            return filter;
        }else{
            return filter;
        }
    }
    
    /**
     * 处理自定义节点关系导航数据
     * 
     * @param context 应用上下文
     * @param viewparams 参数 
     * @param curNavData 节点关系导航参数对象
     * @param tempData 返回数据
     * @param item 节点关系导航参数键值
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
	public handleCustomDataLogic(context:any,viewparams:any,curNavData:any,tempData:any,item:string){
		// 直接值直接赋值
		if(curNavData.isRawValue){
			if(Object.is(curNavData.value,"null") || Object.is(curNavData.value,"")){
                Object.defineProperty(tempData, item.toLowerCase(), {
                    value: null,
                    writable : true,
                    enumerable : true,
                    configurable : true
                });
            }else{
                Object.defineProperty(tempData, item.toLowerCase(), {
                    value: curNavData.value,
                    writable : true,
                    enumerable : true,
                    configurable : true
                });
            }
		}else{
			// 先从导航上下文取数，没有再从导航参数（URL）取数，如果导航上下文和导航参数都没有则为null
			if(context[(curNavData.value).toLowerCase()] != null){
				Object.defineProperty(tempData, item.toLowerCase(), {
					value: context[(curNavData.value).toLowerCase()],
					writable : true,
					enumerable : true,
					configurable : true
				});
			}else{
				if(viewparams[(curNavData.value).toLowerCase()] != null){
					Object.defineProperty(tempData, item.toLowerCase(), {
						value: viewparams[(curNavData.value).toLowerCase()],
						writable : true,
						enumerable : true,
						configurable : true
					});
				}else{
					Object.defineProperty(tempData, item.toLowerCase(), {
						value: null,
						writable : true,
						enumerable : true,
						configurable : true
					});
				}
			}
		}
	}


    constructor(opts: any = {}) {
        super(opts)
        this.getService(this.appDEName).then((res)=>{
            this.service = res;
        });
    }
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_BOTTOM.ts.ftl
</#ibizinclude>