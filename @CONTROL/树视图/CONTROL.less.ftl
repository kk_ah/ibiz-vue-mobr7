<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
.app-mob-treeview {
  .treeNav {
    padding-left: 14px;
    color: #a5a2a2;
    font-size: 14px;
    margin-bottom: 10px;
    .treeNav-active {
      color: #705697;
    }
    .tree-span {
      padding: 0 3px;
      color: #ccc;
    }
  }
  .tree-partition {
    height: 8px;
    background-color: #ededed;
  }
  .tree-icon {
    color: #ccc;
    font-size: 20px;
  }
  .tree-partition-bigger {
    height: 16px;
  }
  ion-list {
    ion-item {
      font-size: 15px;
      font-family: "微软雅黑";
    }
    ion-item:last-child {
      --inner-border-width: 0;
    }
  }
}

