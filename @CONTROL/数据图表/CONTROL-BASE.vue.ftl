<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#assign import_block>
import echarts from 'echarts';
</#assign>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getData(): any {
        return null;
    }

    /**
     * 显示处理提示
     *
     * @type {boolean}
     * @memberof MOBILEENTITY3Canlen
     */
    @Prop({ default: true }) protected showBusyIndicator!: boolean;

    /**
     * 部件行为--fetch
     *
     * @type {string}
     * @memberof Mob
     */
    @Prop() protected fetchAction!: string;  

    /**
    * Vue声明周期(组件初始化完毕)
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}
    */
    public created() {
         this.afterCreated();     
    }

    /**
    * 执行created后的逻辑
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}
    */
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is('load', action)) {
                    this.load(data);
                }
            });
        }  
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

    /**
     * 图表div绑定的id
     *
     * @type {}
     * @memberof ${srfclassname('${ctrl.name}')}
     */   
    public chartId:string = this.$util.createUUID();

    /**
     * echarts图表对象
     *
     * @type {}
     * @memberof ${srfclassname('${ctrl.name}')}
     */   
    public myChart:any;

    /**
     * 初始化图表所需参数
     *
     * @type {}
     * @memberof ${srfclassname('${ctrl.name}')}
     */   
    public chartOption:any = {};

    /**
     * 图表基础配置参数
     * 
     * @returns {*} 
     * @memberof ${srfclassname('${ctrl.name}')}Service
     */
    public defaultConfig: any = {
<#--  获取模板对象：BEGIN  -->
<#assign title=ctrl.getPSDEChartTitle()>
<#list ctrl.getPSDEChartSerieses() as series>
  <#--  暂只支持第一个序列  -->
  <#if series_index == 0>
    <#assign chartSeries = series/>
  </#if>
</#list>
<#--  获取模板对象：END  -->
<#--  公共配置：BEGIN  -->
        title: { 
          show: <#if title.isShowTitle()>true<#else>false</#if>,
<#if title.getTitle()??> 
          text: '${title.getTitle()}',
</#if>
<#if title.getSubTitle()??>
          subtext: '${title.getSubTitle()}'
</#if> 
        },
        legend: {top: 20},
        toolbox: { 
          show: true,  
          feature: { 
            dataView: { 
              show: false, 
              readOnly: true, 
              title: '数据视图' 
            }, 
            saveAsImage: { 
              show: false, 
              title: '保存为图片' 
            }
          }
        },
<#--  公共配置：END  -->
<#--  饼图配置：BEGIN  -->
<#if chartSeries.getSeriesType() == 'pie'>
        tooltip: { trigger: 'item' },
<#--  饼图配置：END  -->
<#--  折线图，柱状图配置：BEGIN  -->
<#elseif chartSeries.getSeriesType() == 'line' || chartSeries.getSeriesType() == 'bar'>
  <#assign xAxes = chartSeries.getXPSDEChartAxes() />
  <#assign yAxes = chartSeries.getYPSDEChartAxes() />
        tooltip: { trigger: 'axis' },
  <#if xAxes??>
        xAxis: {
    <#if xAxes.getAxesType?? && xAxes.getAxesType()??>
          type: '${xAxes.getAxesType()}', 
    </#if>
    <#if xAxes.getAxesPos?? && xAxes.getAxesPos()??>
          position: '${xAxes.getAxesPos()}', 
    </#if>
    <#if xAxes.getCaption?? && xAxes.getCaption()??>
          name: '${xAxes.getCaption()}', 
    </#if>
    <#if xAxes.getMaxValue?? && xAxes.getMaxValue()??>
          max: '${xAxes.getMaxValue()}', 
    </#if>
    <#if xAxes.getMinValue?? && xAxes.getMinValue()??>
          min: '${xAxes.getMinValue()}', 
    </#if>
        },
  </#if>
  <#if yAxes??>
        yAxis: {
    <#if yAxes.getAxesType?? && yAxes.getAxesType()??>
          type: '<#if yAxes.getAxesType() == 'numeric'>value<#else>${yAxes.getAxesType()}</#if>', 
    </#if>
    <#if yAxes.getAxesPos?? && yAxes.getAxesPos()??>
          position: '${yAxes.getAxesPos()}', 
    </#if>
    <#if yAxes.getCaption?? && yAxes.getCaption()??>
          name: '${yAxes.getCaption()}', 
    </#if>
    <#if yAxes.getMaxValue?? && yAxes.getMaxValue()??>
          max: '${yAxes.getMaxValue()}', 
    </#if>
    <#if yAxes.getMinValue?? && yAxes.getMinValue()??>
          min: '${yAxes.getMinValue()}', 
    </#if>
        },
  </#if>
</#if>
<#--  折线图，柱状图配置：BEGIN  -->
    }

    /**
     * 深度合并对象,把secondObj的内容合并给firstObj，返回合并后的firstObj
     * @param {} firstObj 被合并对象
     * @param {} secondObj 合并对象
     * @memberof ${srfclassname('${ctrl.name}')}
     */
    public deepObjectMerge(firstObj: any, secondObj: any) {
        for (let key in secondObj) {
            firstObj[key] =
                firstObj[key] && firstObj[key].toString() === "[object Object]"
                    ? this.deepObjectMerge(firstObj[key], secondObj[key])
                    : (firstObj[key] = secondObj[key]);
        }
        return firstObj;
    }

    /**
     * 刷新
     *
     * @param {*} [opt={}]
     * @memberof ${srfclassname('${ctrl.name}')}
     */
    protected refresh(opt: any = {}) {
        this.load(opt);
    }

    /**
     * 获取图表数据
     *
     * @param {*} [opt]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.name}')}
     */
    public async load(opt?: any): Promise<any> {
        let _this = this;
        const arg: any = { ...opt };
        const parentdata: any = {};
        this.$emit('beforeload', parentdata);
        Object.assign(arg, parentdata);
        Object.assign(arg, { viewparams: this.viewparams });
        const response = await this.service.search(this.fetchAction, { ...this.context }, arg, this.showBusyIndicator);
        if (response && response.status === 200) {
            _this.chartOption = _this.deepObjectMerge(_this.defaultConfig, response.data);
            _this.drawCharts();
        } else {
            console.error(response);
        }
    }

    /**
     * 绘制图表
     * 
     * @returns {*} 
     * @memberof ${srfclassname('${ctrl.name}')}
     */
    public drawCharts(){
        if(!this.myChart){
          let element:any =  document.getElementById(this.chartId);
          this.myChart = echarts.init(element);
        }
        this.myChart.setOption(this.chartOption);
        this.myChart.resize();
    }


<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>