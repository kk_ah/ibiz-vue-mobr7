<#ibizinclude>
../@MACRO/MODEL/MODEL_HEADER.ts.ftl
</#ibizinclude>

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ${srfclassname('${ctrl.getCodeName()}')}${srfclassname('${ctrl.name}')}Mode
	 */
	public getDataItems(): any[] {
		return [
			{
				name:'query',
				prop:'query'
			},
		]
	}

<#ibizinclude>
../@MACRO/MODEL/MODEL_BOTTOM.ts.ftl
</#ibizinclude>