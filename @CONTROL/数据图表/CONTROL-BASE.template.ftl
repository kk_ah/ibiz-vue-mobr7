<template>
    <div class="app-data-chart <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
<#if ctrl.render??><#t>
        ${ctrl.render.code}
<#else><#t>
        <div class="app-charts" :id="chartId" style="<#if ctrl.getWidth() gt 0>width: ${ctrl.getWidth()};</#if>height: <#if ctrl.getHeight() gt 0>${ctrl.getHeight()}px<#else>50vh</#if>;padding: 6px 0;"></div>
</#if>
  </div>
</template>
