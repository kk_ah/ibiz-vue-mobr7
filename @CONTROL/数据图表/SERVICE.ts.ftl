<#assign extendsClass>ChartServiceBase</#assign>
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_HEADER.ts.ftl
</#ibizinclude>

<#--  暂只支持第一个序列  -->
<#list ctrl.getPSDEChartSerieses() as series>
  <#if series_index == 0>
    <#assign chartSeries = series/>
  </#if>
</#list>
    /**
     * 生成图表数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public handleResponse(action: string, response: any): any {
        const chartOption: any = {};
<#--  获取x轴的分类属性字段  -->
        const catalogFields: any = [<#rt>
<#if chartSeries.getCatalogField?? && chartSeries.getCatalogField()??>
  <#assign catalogField = chartSeries.getCatalogField()>
  <#list catalogField?split(";") as field>
            "${field?lower_case}",<#t>
  </#list>
</#if>
        ];<#lt>
<#--  获取y轴值属性字段和中文名称  -->
        const valueFields: any = [<#rt>
<#if chartSeries.getValueField?? && chartSeries.getValueField()??>
  <#assign valueField = chartSeries.getValueField()>
    <#list valueField?split(";") as field>
            [ "${field?lower_case}", "${de.getPSDEField(field).getLogicName()}" ],<#t>
    </#list>
</#if>
        ];<#lt>
        // 数据按分类属性分组处理
        const xFields:any = [];
        const yFields:any = [];
        valueFields.forEach((field: any, index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                const num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
            }else{
                xFields.push(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
<#--  折线图和柱状图需要配置xAxis,饼图不需要  -->
<#if chartSeries.getSeriesType() == 'line' || chartSeries.getSeriesType() == 'bar'>
        chartOption.xAxis = { data: xFields };
</#if>
<#--  配置series  -->
        const series: any = [];
        valueFields.forEach((field: any,index: number) => {
            const yData: any = [];
            xFields.forEach((item:any, num: number) => {
<#if chartSeries.getSeriesType() == 'line' || chartSeries.getSeriesType() == 'bar'>
                yData.push(yFields[index][num]);
<#elseif chartSeries.getSeriesType() == 'pie'>
                yData.push({value: yFields[index][num], name: item});
</#if>
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
              name: field[1],
              type: "${chartSeries.getSeriesType()}",
              data: yData,
<#--  饼图额外配置  -->
<#if chartSeries.getSeriesType() == 'pie'>
              top:"40px",
              left: (100 / valueFields.length) * index + "%",
              right: (100 / valueFields.length) * (valueFields.length - index - 1) + "%",
              animationType: 'scale',
              animationEasing: 'elasticOut',
              animationDelay: function (idx: any) {
                  return Math.random() * 200;
              }
</#if>
            });
        });
        chartOption.series = series;
        return new HttpResponse(response.status, chartOption);
    }

<#ibizinclude>
../@MACRO/SERVICE/SERVICE_BOTTOM.ts.ftl
</#ibizinclude>