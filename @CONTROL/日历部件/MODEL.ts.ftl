<#ibizinclude>
../@MACRO/MODEL/MODEL_HEADER.ts.ftl
</#ibizinclude>

	/**
	 * 日历项类型
	 *
	 * @returns {any[]}
	 * @memberof ${srfclassname('${ctrl.getCodeName()}')}${srfclassname('${ctrl.name}')}Mode
	 */
	public itemType: string = "";

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ${srfclassname('${ctrl.getCodeName()}')}${srfclassname('${ctrl.name}')}Mode
	 */
	public getDataItems(): any[] {
        let dataItems: any = [
<#--  关联主实体的主键  -->
<#if ctrl.getPSAppDataEntity()??>
    <#assign appDataEntity = ctrl.getPSAppDataEntity() />
    <#if appDataEntity.isMajor() == false && appDataEntity.getMinorPSAppDERSs()??>
        <#list appDataEntity.getMinorPSAppDERSs() as minorAppDERSs>
            <#if minorAppDERSs.getMajorPSAppDataEntity()??>
            <#assign majorAppDataEntity = minorAppDERSs.getMajorPSAppDataEntity() />
            {
                name: '${majorAppDataEntity.getCodeName()?lower_case}',
                prop: '${majorAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}'
            },
            </#if>
        </#list>
    </#if>
</#if>
            {
                name: 'queryStart',
                prop: 'n_start_gtandeq'
            },
            {
                name: 'queryEnd',
                prop: 'n_end_ltandeq'
            },
            {
                name: 'color',
            },
            {
                name: 'textColor',
            },
            {
                name: 'itemType',
            },
        ];
        switch (this.itemType) {
    <#--  日历项实体映射  -->
<#if ctrl.getPSSysCalendarItems()??>
    <#list ctrl.getPSSysCalendarItems() as calendarItem>
        <#if calendarItem.getPSAppDataEntity()??>
            <#assign appDataEntity = calendarItem.getPSAppDataEntity() />
            case "${calendarItem.getItemType()?lower_case}":
                dataItems = 
                    [
                        ...dataItems,
                        {
                            name: '${appDataEntity.getCodeName()?lower_case}',
                            prop: '${appDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}'
                        },
                        {
                            name: 'title',
                            prop: '${appDataEntity.getMajorPSAppDEField().getCodeName()?lower_case}'
                        },
                        {
                            name:'start',
                            prop:'n_${calendarItem.getBeginTimePSDEField().getCodeName()?lower_case}_gtandeq'
                        },
                        {
                            name:'end',
                            prop:'n_${calendarItem.getEndTimePSDEField().getCodeName()?lower_case}_ltandeq'
                        },
                    ];
                break;
        </#if>
    </#list>
</#if>
        }
        return dataItems;
	}

<#ibizinclude>
../@MACRO/MODEL/MODEL_BOTTOM.ts.ftl
</#ibizinclude>