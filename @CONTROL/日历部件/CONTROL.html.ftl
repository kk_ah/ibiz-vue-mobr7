<#--  content  -->
<#assign content>
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}"  
    :showCheack="showCheack"
    @showCheackChange="showCheackChange"
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>