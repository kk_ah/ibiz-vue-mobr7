<#assign extendsClass>CalendarServiceBase</#assign>
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_HEADER.ts.ftl
</#ibizinclude>

    /**
     * 事件配置集合
     *
     * @protected
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected eventsConfig: any = {
<#if ctrl.getPSSysCalendarItems()??>
    <#list ctrl.getPSSysCalendarItems() as calendarItem>
        <#if calendarItem.getPSAppDataEntity()?? && calendarItem.getPSDEDataSet()??>
        '${calendarItem.getItemType()?lower_case}': {
            itemName : '${calendarItem.getName()}',
            itemType : '${calendarItem.getItemType()?lower_case}',
            color : '${calendarItem.getBKColor()}',
            textColor : '${calendarItem.getColor()}',
        },
        </#if>
    </#list>
</#if>
    };

    /**
     * 查询数据
     *
     * @param {string} itemType
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<HttpResponse>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public async search(itemType: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<HttpResponse> {
        let item: any = {};
        try {
<#if ctrl.getPSSysCalendarItems()??>
    <#list ctrl.getPSSysCalendarItems() as calendarItem>
        <#if calendarItem.getPSAppDataEntity()?? && calendarItem.getPSDEDataSet()??>

            <#assign _appde = calendarItem.getPSAppDataEntity() />
            <#assign deDataSet = calendarItem.getPSDEDataSet() />
            this.model.itemType = '${calendarItem.getItemType()?lower_case}';
            const _${calendarItem.getItemType()?lower_case}_data = this.handleRequestData('', context, data);
            await this.onBeforeAction('', context, _${calendarItem.getItemType()?lower_case}_data, isLoading);
                <#if _appde.getId() == appde.getId()>
            const _${calendarItem.getItemType()?lower_case} = await this.loadDEDataSet('Fetch${deDataSet.getCodeName()}', context, _${calendarItem.getItemType()?lower_case}_data, '${calendarItem.getItemType()?lower_case}');
                <#else>
            const _${calendarItem.getItemType()?lower_case} = await this.loadDEDataSet('Fetch${deDataSet.getCodeName()}', context, _${calendarItem.getItemType()?lower_case}_data, '${calendarItem.getItemType()?lower_case}', '${_appde.getCodeName()?lower_case}');
                </#if>
            Object.assign(item, { ${calendarItem.getItemType()?lower_case}: _${calendarItem.getItemType()?lower_case} });
        </#if>
    </#list>
</#if>
        } catch (response) {
            return new HttpResponse(response.status);
        }
        return new HttpResponse(200, item);
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public async update(itemType: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<any> {
        await this.onBeforeAction('', context, data, isLoading);
        data = this.handleRequestData('', context, data);
        let response: any;
        switch (itemType) {
<#if ctrl.getPSSysCalendarItems()??>
    <#list ctrl.getPSSysCalendarItems() as calendarItem>
        <#if calendarItem.getPSAppDataEntity()?? && calendarItem.getUpdatePSAppDEAction()??>
            <#assign _deAction = calendarItem.getUpdatePSAppDEAction() />
            <#assign _appde = calendarItem.getPSAppDataEntity() />
            <#if _appde.getId() == appde.getId()>
            case "${calendarItem.getItemType()}":
                response = await this.service.${_deAction.getCodeName()}(context, data);
                break;
            <#else>
            case "${calendarItem.getItemType()}":
                const _service: any = await this.getService('${_appde.getCodeName()?lower_case}');
                if (_service && _service['${_deAction.getCodeName()}'] && _service['${_deAction.getCodeName()}'] instanceof Function) {
                    response = await _service['${_deAction.getCodeName()}'](context, data);
                }
                break;
            </#if>
        </#if>
    </#list>
</#if>
            default:
                response = new HttpResponse(500, null, { code: 101, message: '未配置更新实体行为' });
        }
        if (!response.isError()) {
            response = this.handleResponse("", response);
        }
        return new HttpResponse(response.status, response.data, response.error);
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public async delete(itemType: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<any> {
        await this.onBeforeAction('', context, data, isLoading);
        data = this.handleRequestData('', context, data);
        let response: any;
        switch (itemType) {
<#if ctrl.getPSSysCalendarItems()??>
    <#list ctrl.getPSSysCalendarItems() as calendarItem>
        <#if calendarItem.getPSAppDataEntity()?? && calendarItem.getRemovePSAppDEAction()??>
            <#assign _deAction = calendarItem.getRemovePSAppDEAction() />
            <#assign _appde = calendarItem.getPSAppDataEntity() />
            <#if _appde.getId() == appde.getId()>
            case "${calendarItem.getItemType()}":
                response = await this.service.${_deAction.getCodeName()}(context, data);
                break;
            <#else>
            case "${calendarItem.getItemType()}":
                const _service: any = await this.getService('${_appde.getCodeName()?lower_case}');
                if (_service && _service['${_deAction.getCodeName()}'] && _service['${_deAction.getCodeName()}'] instanceof Function) {
                    response = await _service['${_deAction.getCodeName()}'](context, data);
                }
                break;
            </#if>
        </#if>
    </#list>
</#if>
            default:
                response = new HttpResponse(500, null, { code: 101, message: '未配置删除实体行为' });
        }
        if (!response.isError()) {
            response = this.handleResponse("", response);
        }
        return new HttpResponse(response.status, response.data, response.error);
    }

<#ibizinclude>
../@MACRO/SERVICE/SERVICE_BOTTOM.ts.ftl
</#ibizinclude>