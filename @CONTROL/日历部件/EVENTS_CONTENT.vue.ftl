                        <#if ctrl.getPSSysCalendarItems()??>
                        <#list ctrl.getPSSysCalendarItems() as calendarItem>
                        <div v-if="activeItem == '<#if calendarItem.getItemType()??>${calendarItem.getItemType()?lower_case}</#if>'" <#if ctrl.getCalendarStyle() == "TIMELINE">:key="item.id"</#if><#if calendarItem.getPSSysCalendarItemRVs()??> @click="onEventClick(item)"</#if>>
                        <#if calendarItem.getPSLayoutPanel?? && calendarItem.getPSLayoutPanel()??>
                            <#assign layoutpanel=calendarItem.getPSLayoutPanel()>
                            <layout_${layoutpanel.getName()} :context="{}" :viewparams="{}" :item="item"></layout_${layoutpanel.getName()}>
                        <#else>
                        <#if calendarItem.getTextPSAppDEField()??>
                            <#assign text=true>
                            <div class="evenname">{{item.${calendarItem.getTextPSAppDEField().getName()?lower_case}}}</div>
                        </#if>
                        <#if calendarItem.getContentPSAppDEField()??>
                            <#assign text=true>
                            <div class="evenname">{{item.${calendarItem.getContentPSDEField().getName()?lower_case}}}</div>
                        </#if>
                        <#if calendarItem.getIconPSAppDEField()??>
                            <#assign text=true>
                            <div class="evenname">{{item.${calendarItem.getIconPSAppDEField().getName()?lower_case}}}</div>
                        </#if>
                        <#if !text>
                            <div class="evenname">{{item.<#if appde.getMajorPSAppDEField()??>${appde.getMajorPSAppDEField().getCodeName()?lower_case}<#else>srfmajortext</#if>}}}</div>
                        </#if>
                        </#if>
                        </div>
                        </#list>
                        </#if>