## ibiz日历样式相关文档（移动端）

### 一、日历部件样式

#### 	1、月/月（复合时间轴）

##### 	(1)支持功能：年/月/日切换、切换日历样式、删除日程、日程展示标识、点击日期更新日程、长按事件多选、长按显示批操工具栏

##### 	(2)界面UI

<div style = "display:flex; overflow:hidden; ">
	<img src="images/README/mounth.png" ></img>
	<img src="images/README/mounth_top.png" ></img>
</div>

<div style = "display:flex; overflow:hidden; ">
	<img src="images/README/each.png" ></img>
</div>

#### 	2、周/周（复合时间轴）

##### 	(1)支持功能：年/月/日切换、删除日程、日程展示标识、时间轴日程、长按事件多选

##### 	(2)界面UI

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/week.png" ></img>
	<img src="images/README/week_touch.png" ></img>
</div>


#### 	3、天

##### 	(1)支持功能：展示日程

##### 	(2)界面UI

​	![day](images/README/day.png)

#### 4、复合时间轴

##### (1)支持功能：时间轴、展示日程、删除日程

##### (2)界面UI

​	![timezhou](images/README/timezhou.png)

### 二、应用场景

用于呈现与日期时间安排相关的信息，比如待办事项安排、日程安排、会议室安排。