<template>
    <div class="app-mob-calendar <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
        <div v-if="show" :class="['calender_box' , activeItem]">
    <#--  日历样式----月  -->
    <#if ctrl.getCalendarStyle() == "MONTH" || ctrl.getCalendarStyle() == "MONTH_TIMELINE">
            <app-calendar
                ref="calendar"
                @prev="prev"
                @next="next"
                :value="value"
                :markDate="markDate"
                :responsive="true"
                :isChangeStyle ="true"
                :illustration="illustration"
                @select="clickDay"
                @selectYear="selectYear"
                @selectMonth="selectMonth"
                :sign="sign"
                :events="eventsDate"
                :tileContent="tileContent"></app-calendar>
    <#--  日历样式----天  -->  
    <#elseif ctrl.getCalendarStyle() == "DAY">
            <div class="calendar-tools">
                <div class="calendar-prev" @click="prevDate"><ion-icon name="chevron-back-outline"></ion-icon></div>
                <div class="calendar-next" @click="nextDate"><ion-icon name="chevron-forward-outline"></ion-icon></div>
                <div class="calendar-info">
                {{year}}年{{month+1}}月{{day}}日
                </div>
            </div>
    <#--  日历样式----周  -->
    <#elseif ctrl.getCalendarStyle() == "WEEK" || ctrl.getCalendarStyle() == "WEEK_TIMELINE">
            <app-calendar
                ref="calendar"
                :weekSwitch="true"
                :value="value"
                @prev="prev"
                @next="next"
                :markDate="markDate"
                :illustration="illustration"
                :responsive="true"
                @select="clickDay"
                :sign="sign"
                @selectYear="selectYear"
                @selectMonth="selectMonth"
                :tileContent="tileContent"></app-calendar>
    <#--  日历样式----时间轴  -->
    <#elseif ctrl.getCalendarStyle() == "TIMELINE">
           
    <#--  日历样式----空  -->
    <#else>
    </#if>
    <#--  BEGIN：输出分页头部  -->
    <#if ctrl.getPSSysCalendarItems()??>
        <#assign state=false>
        <#list ctrl.getPSSysCalendarItems() as calendarItem><#if calendarItem_index gt 0><#assign state=true><#break></#if></#list>
        <#if state>
            <ion-segment :value="activeItem" @ionChange="ionChange">
            <#list ctrl.getPSSysCalendarItems() as calendarItem>
                <ion-segment-button value="${calendarItem.getItemType()?lower_case}">
                    <ion-label>${calendarItem.getName()}</ion-label>
                </ion-segment-button>
            </#list>
            </ion-segment>
        </#if>
    </#if>
    <#--  END：输出分页头部  -->
    <#if ctrl.getCalendarStyle() == "TIMELINE">
            <div class="calendar-events">
                <van-steps active-icon="passed" inactive-icon="passed" direction="vertical">
                    <van-step v-for="i in count" :key="i">
                        <p>{{year}}-{{month+1}}-{{i}}</p>
                        <template v-for="(it,index) in evendata[activeItem]">
                        <div v-if="it.time == year+'-'+month+1+'-'+i || it.time == year+'-'+'0'+(month+1)+'-'+i "  :key="index"  class="even-box">
                        <template v-for="item in it.evens">
<#-- BENGIN： 事件内容项 -->                    
<#ibizinclude>./EVENTS_CONTENT.vue.ftl</#ibizinclude>
<#--  END: 事件内容项 -->
                        </template>
                        <ion-icon @click="remove([item])" class="event-delete" name="close-outline"></ion-icon>
                        </div>
                        </template>
                    </van-step>
                </van-steps>
            </div>

    <#elseif ctrl.getCalendarStyle() == "MONTH_TIMELINE" || ctrl.getCalendarStyle() == "WEEK_TIMELINE">
            <div class="calendar-events">
                <van-steps active-icon="passed" inactive-icon="passed" direction="vertical">
                    <van-step v-for="(i,index) in evendata[activeItem]" :key="index">
                        <p>{{i.time}}</p>
                        <div class="touch"  v-for="(item,index) in i.evens" :key="index">
                            <ion-checkbox  v-show="showCheack" class="touch-checkbox" @click.stop="checkboxSelect(item)"></ion-checkbox>
                            <div  :key="item.id" class="even-box">
    <#-- BENGIN： 事件内容项 -->                    
    <#ibizinclude>./EVENTS_CONTENT.vue.ftl</#ibizinclude>
    <#--  END: 事件内容项 -->
                            <ion-icon v-show="!showCheack" @click="remove([item])" class="event-delete" name="close-outline"></ion-icon>
                            </div>
                        </div>
                    </van-step>
                </van-steps>
            </div>
    <#else>
            <div class="calendar-events">
                <ion-list>
                    <ion-item v-for="item in calendarItems[activeItem]"  :key="item.id" @click="onEventClick(item)">
                    <ion-label>{{item.title}}</ion-label>
                    <div slot="end">
                        <div >
                            <span class="events-label">执行人 </span>
                            <span class="events-content">{{ item.createman }}</span>
                        </div>
                    </div>
                    </ion-item>
                </ion-list>
            </div>
    </#if>
        </div>
  </#if>
    </div>
</template>