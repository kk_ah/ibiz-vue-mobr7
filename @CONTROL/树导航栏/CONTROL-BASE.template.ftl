<template>
    <div class="app-mob-treeexpbar <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#if ctrl.render??>
        ${ctrl.render.code}
    <#else>
        <div class="treeexpbar-container">
            <#if ctrl.getPSDETree()??>
            <#assign tree = ctrl.getPSDETree()/>
            ${P.getCtrlCode(tree, 'CONTROL.html').code}
            </#if>
             <component 
              v-if="selection.view && !Object.is(this.selection.view.viewname, '')" 
              :is="selection.view.viewname"
              class="viewcontainer2"
              :viewDefaultUsage="false"
              :viewdata="JSON.stringify(selection.data)"
              @viewdataschange="onViewDatasChange"
              @viewdatasactivated="viewDatasActivated"
              @viewload="onViewLoad">
            </component>
        </div>
    </#if>
    </div>
</template>