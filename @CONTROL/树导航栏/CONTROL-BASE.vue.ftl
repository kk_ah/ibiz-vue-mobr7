<#assign notneedCtrlService = true>
<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>
    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getDatas(): any[] {
      return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getData(): any {
      return {};
    }

    /**
     * 选中数据
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected selection: any = {};


    /**
     * 过滤值
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected srfnodefilter: string = '';

    /**
     * 是否加载默认关联视图
     *
     * @private
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    private istLoadDefaultRefView: boolean = false;

    /**
     * 分割宽度
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    <#--  protected split: string = '<#if ctrl.getWidth() gt 0>${ctrl.getWidth()?c}<#else>200</#if>px';  -->
    protected split: number = 0.2;

    /**
     * 获取关系项视图
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected getExpItemView(arg: any = {}): any {
        let expmode = arg.nodetype.toUpperCase();
        if (!expmode) {
            expmode = '';
        }
        <#list ctrl.getPSAppViewRefs() as item>
        <#if (item.getName()?index_of("EXPITEM:")==0)>
        <#assign refview = item.getRefPSAppView()>
        if (Object.is(expmode, '${item.getName()?substring(8)?upper_case}')) {
            return {  
                viewname: '${srffilepath2(refview.codeName)}', 
                parentdata: <#if item.getParentDataJO()??>${item.getParentDataJO()},<#else>{},</#if>
          <#if refview.getPSAppDataEntity?? && refview.getPSAppDataEntity()??>
                deKeyField:'${refview.getPSAppDataEntity().codeName?lower_case}'
          </#if>      
			};
        }
        </#if>
        </#list>
        return null;
    }

    /**
     * 树导航选中
     *
     * @param {any []} args
     * @param {string} [tag]
     * @param {*} [$event2]
     * @returns {void}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected ${ctrl.name}_selectionchange(args: any [], tag?: string, $event2?: any): void {
        if (args.length === 0) {
            return ;
        }
        const arg:any = args[0];
        if (!arg.id) {
            return;
        }
        const nodetype = arg.id.split(';')[0];
        const refview = this.getExpItemView({ nodetype: nodetype });
        if (!refview) {
            return;
        }
        const deKeyField = refview.deKeyField;
        const parentdata: any = { [deKeyField]: arg.srfkey };
        <#--  // 多选数据处理
        arg.id.split(';').forEach((value: string, index: number) => {
            if (index > 0) {
                const key = index + 1 === 1 ? 'srfparentkey' : 'srfparentkey' + (index + 1);
                Object.assign(parentdata, { [key]: value })
            }
        });  -->
        Object.assign(parentdata, refview.parentdata);
        this.selection = {};
        Object.assign(this.selection, { view: { viewname: refview.viewname }, data: parentdata });
    }

    /**
     * 树加载完成
     *
     * @param {any[]} args
     * @param {string} [tag]
     * @param {*} [$event2]
     * @returns {void}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected ${ctrl.name}_load(args: any[], tag?: string, $event2?: any): void {
    }

    /**
     * 执行搜索
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected onSearch(): void {
        <#if ctrl.getPSDETree()??>
        <#assign tree = ctrl.getPSDETree()/>
        this.istLoadDefaultRefView = false;
        this.viewState.next({ tag: '${tree.name}', action: 'filter', data: { srfnodefilter: this.srfnodefilter } });
        </#if>
    }

    /**
     * vue 声明周期
     *
     * @memberof @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */    
    protected afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                <#if ctrl.getPSDETree()??>
                <#assign tree = ctrl.getPSDETree()/>
                this.istLoadDefaultRefView = false;
                this.viewState.next({ tag: '${tree.name}', action: action, data: data });
                </#if>
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

    /**
     * 视图数据变化
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected onViewDatasChange($event: any): void {
        this.$emit('selectionchange', $event);
    }

    /**
     * 视图数据被激活
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected viewDatasActivated($event: any): void {
        this.$emit('activated', $event);
    }

    /**
     * 视图数据加载完成
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected onViewLoad($event: any): void {
        this.$emit('load', $event);
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>