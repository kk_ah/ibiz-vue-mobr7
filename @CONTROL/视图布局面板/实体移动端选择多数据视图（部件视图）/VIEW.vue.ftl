<div class="view-container app-mob-pickup-mdview ${srffilepath2(view.getCodeName())}<#if view.getPSSysCss()??><#assign singleCss=view.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <div class="view-content">
        <#if view.hasPSControl('mdctrl')>
        ${P.getCtrlCode('mdctrl', 'CONTROL.html').code}
        </#if>
    </div>
</div>