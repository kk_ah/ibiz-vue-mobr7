<#ibizinclude>
../@MACRO/DEFAULT.less.ftl
</#ibizinclude>

.${srffilepath2(view.getCodeName())} {
    .view-footer {
        padding: 0.5rem 1rem;
        text-align: center;
        border-top: 1px solid #ddd;
        .van-button {
            height: 2.5rem;
            line-height: 2.5rem;
            width: 40%;
        }
        .mobpickupview_button{
            ion-button{
                width: 48%;
                --border-radius: 0;
            }
        }
    }
}