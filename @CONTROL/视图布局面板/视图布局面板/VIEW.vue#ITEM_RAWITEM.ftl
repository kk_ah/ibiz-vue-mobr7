<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-rowitem<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<@ibizindent blank=4>
${item.getHtmlContent()}
</@ibizindent>
</div>
<#else>
<van-col span="24" ${tableAttr} style="<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-rowitem<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<@ibizindent blank=4>
${item.getHtmlContent()}
</@ibizindent>
</van-col>
</#if>


