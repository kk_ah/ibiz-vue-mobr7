<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-button<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    <van-button type="primary" long<#if item.getHeight() gt 0> style="height: ${item.getHeight()?c}px;"</#if>>
        <#if item.getPSSysImage()??>
        <#assign image = item.getPSSysImage()>
        <i class="${image.getCssClass()}"></i>
        </#if>
        <#if item.isShowCaption()>
        <span<#if item.getLabelPSSysCss()??> class="${item.getLabelPSSysCss().getCssName()}"</#if><#if item.getLabelPSSysCss()??> style="${item.getLabelPSSysCss().getRawCssStyle()}"</#if>>${item.getCaption()}</span>
        </#if>
    </van-button>
</div>
<#else>
<van-col span="24"${tableAttr} style="<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-button<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    <van-button type="primary" long<#if item.getHeight() gt 0> style="height: ${item.getHeight()?c}px;"</#if>>
        <#if item.getPSSysImage()??>
        <#assign image = item.getPSSysImage()>
        <i class="${image.getCssClass()}"></i>
        </#if>
        <#if item.isShowCaption()>
        <span<#if item.getLabelPSSysCss()??> class="${item.getLabelPSSysCss().getCssName()}"</#if><#if item.getLabelPSSysCss()??> style="${item.getLabelPSSysCss().getRawCssStyle()}"</#if>>${item.getCaption()}</span>
        </#if>
    </van-button>
</van-col>
</#if>