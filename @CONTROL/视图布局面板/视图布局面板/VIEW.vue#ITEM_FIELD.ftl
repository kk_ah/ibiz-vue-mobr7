<#if item.getParentLayoutMode()=='FLEX'>
<#else>
<van-col span="24"<#if item.getColLG() != -1> lg={${item.getColLG()?c}}</#if><#if item.getColMD() != -1> md={${item.getColMD()?c}}</#if><#if item.getColSM() != -1> sm={${item.getColSM()?c}}</#if><#if item.getColXS() != -1> xs={${item.getColXS()?c}}</#if> style="<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-field<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<div>${item.name}</div>
</van-col>
</#if>