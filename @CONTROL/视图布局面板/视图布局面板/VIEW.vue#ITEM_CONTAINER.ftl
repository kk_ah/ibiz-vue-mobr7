<#assign layout = item.getPSLayout()/>
<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if layout.getLayout()=='FLEX'>
<div class="app-layoutpanel-container<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>" style="${flexAttr}<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if>">
    <#if item.isShowCaption()>
    <#if item.getLabelPSSysCss()??>
    <div class="${item.getLabelPSSysCss().getCssName()}" style="height: 50px;${item.getLabelPSSysCss().getRawCssStyle()}">${item.getCaption()}</div>
    <#else>
    <div style="height: 50px;">${item.getCaption()}</div>
    </#if>
    </#if>
    <div style="height: <#if item.isShowCaption()>calc(100% - 50px)<#else>100%</#if>;display: flex;<#if layout.getDir()!="">flex-direction: ${layout.getDir()};</#if><#if layout.getAlign()!="">justify-content: ${layout.getAlign()};</#if><#if layout.getVAlign()!="">align-items: ${layout.getVAlign()};</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>">
<@ibizindent blank=8>
<#if item.getPSPanelItems()??>
<#list item.getPSPanelItems() as panelItem>
${P.getPartCode(panelItem).code}
</#list>
</#if>
</@ibizindent>
    </div>
</div>
<#else>
<van-col span="24"${tableAttr} style="${flexAttr}<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if>" class="app-layoutpanel-container<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
    <#if item.isShowCaption()>
    <#if item.getLabelPSSysCss()??>
    <div class="${item.getLabelPSSysCss().getCssName()}" style="height: 50px;${item.getLabelPSSysCss().getRawCssStyle()}">${item.getCaption()}</div>
    <#else>
    <div style="height: 50px;">${item.getCaption()}</div>
    </#if>
    </#if>
    <van-row style="height: <#if item.isShowCaption()>calc(100% - 50px)<#else>100%</#if>;<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>">
<@ibizindent blank=8>
<#if item.getPSPanelItems()??>
<#list item.getPSPanelItems() as panelItem>
${P.getPartCode(panelItem).code}
</#list>
</#if>
</@ibizindent>
    </van-row>
</van-col>
</#if>