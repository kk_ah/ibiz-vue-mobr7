<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-usercontrol<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<@ibizindent blank=4>
<#if item.getPSSysPFPlugin()??>
${item.getPSSysPFPlugin().getCode("CODE",pf.getId(),pfstyle.getId(),view,ctrl,item)}
</#if>
</@ibizindent>
</div>
<#else>
<van-col span="24" ${tableAttr} style="<#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-usercontrol<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<@ibizindent blank=4>
<#if item.getPSSysPFPlugin()??>
${item.getPSSysPFPlugin().getCode("CODE",pf.getId(),pfstyle.getId(),view,ctrl,item)}
</#if>
</@ibizindent>
</van-col>
</#if>