<#assign flexAttr>
<#if item.getPSLayoutPos()?? && item.getPSLayoutPos().getLayout() == "FLEX"><#assign layoutPos = item.getPSLayoutPos()/><#if layoutPos.getGrow() gt -1>flex-grow: ${layoutPos.getGrow()};</#if></#if></#assign>
<#assign tableAttr>
<#if item.getPSLayoutPos()?? && item.getPSLayoutPos().getLayout() != "FLEX"><#assign layoutPos = item.getPSLayoutPos()/><#if layoutPos.getColLG() gt -1> lg={${layoutPos.getColLG()?c}}</#if><#if layoutPos.getColMD() gt -1> md={${layoutPos.getColMD()?c}}</#if><#if layoutPos.getColSM() gt -1> sm={${layoutPos.getColSM()?c}}</#if><#if layoutPos.getColXS() gt -1> xs={${layoutPos.getColXS()?c}}</#if></#if></#assign>
