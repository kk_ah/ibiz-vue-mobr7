<#ibizinclude>
./@MACRO/PLAYOUT.vue.ftl
</#ibizinclude>
<#if item.getParentLayoutMode()=='FLEX'>
<div style="${flexAttr}<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-ctrlpos<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<@ibizindent blank=4>
${P.getCtrlCode('${item.name}', 'CONTROL.html').code}
</@ibizindent>
</div>
<#else>
<van-col span="24"${tableAttr} style="<#if item.getColWidth() != -1>width: ${item.getColWidth()?c}px;</#if><#if item.getHeight() gt 0>height: ${item.getHeight()?c}px;</#if><#if item.getPSSysCss()??>${item.getPSSysCss().getRawCssStyle()}</#if>" class="app-layoutpanel-ctrlpos<#if item.getPSSysCss()??> ${item.getPSSysCss().getCssName()}</#if>">
<@ibizindent blank=4>
${P.getCtrlCode('${item.name}', 'CONTROL.html').code}
</@ibizindent>
</van-col>
</#if>