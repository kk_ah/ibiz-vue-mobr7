<#if view.hasPSControl('calendar')>
<#assign calendar = view.getPSControl('calendar')>
<#assign view_content>
${P.getCtrlCode('calendar', 'CONTROL.html').code}
</#assign>
<#assign view_footer>
    <#if calendar.getBatchPSDEToolbar?? && calendar.getBatchPSDEToolbar()??>
        <#assign batchToolbar = calendar.getBatchPSDEToolbar()>
        <div v-show="showCheack" class="batch_btn">
                <@ibizindent blank=8>
                ${P.getCtrlCode(batchToolbar, 'CONTROL.html').code}
                </@ibizindent>
            <ion-button class="app-view-toolbar-button"  @click="cancelSelect" >
                <ion-icon name="arrow-undo-outline"></ion-icon>
                {{$t('app.button.cancel')}}
            </ion-button>
        </div>    
    </#if>
</#assign>
</#if>
<#ibizinclude>
../@MACRO/VIEW_LAYOUT_BASE.ftl
</#ibizinclude>