<#if view.hasPSControl('tabexppanel')>
<#assign tabexppanel = view.getPSControl('tabexppanel')>
<#assign view_header_botton>
        <ion-toolbar>
            <ion-segment :value="activiedTabViewPanel" @ionChange="tabExpPanelChange($event)">
            <#if tabexppanel.getPSControls()??>
                <#list tabexppanel.getPSControls() as tabviewpanel>
                <ion-segment-button value="${tabviewpanel.name}">${tabviewpanel.getCaption()}</ion-segment-button>
                </#list>
            </#if>
            </ion-segment>
        </ion-toolbar>

</#assign>
<#assign view_content>
${P.getCtrlCode('tabexppanel', 'CONTROL.html').code}
</#assign>
</#if>
<#ibizinclude>
../@MACRO/EMBED_VIEW_LAYOUT_BASE.ftl
</#ibizinclude>