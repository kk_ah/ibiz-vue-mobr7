<#if view.hasPSControl('form')>
<#assign view_content>
${P.getCtrlCode('form', 'CONTROL.html').code}
</#assign>
<#assign view_footer>
      <div class="wf_btn">
        <ion-button v-for="(linkItem,index) in linkModel" :key="index" @click="dynamic_toolbar_click(linkItem, $event)" class="wf_btn_item">
                {{linkItem.sequenceFlowName}}
        </ion-button>
      </div>
</#assign>
</#if>

<#ibizinclude>
../@MACRO/VIEW_LAYOUT_BASE.ftl
</#ibizinclude>