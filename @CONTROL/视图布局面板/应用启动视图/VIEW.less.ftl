<#ibizinclude>
../@MACRO/DEFAULT.less.ftl
</#ibizinclude>
.launch-container {
    background-color: #b8bece;
    height: 100vh;
    z-index: 999;
  .bg_img {
    width: 100%;
    height: 100%;
  }
  .app-layoutpanel {
    .skip_button {
      position: fixed !important;
      right: @startview-btn-offset-x;
      top: @startview-btn-offset-y;
    }
    .neverShow_button{
      position: fixed !important;
      left: @startview-btn-offset-x;
      top: @startview-btn-offset-y;
    }
    .launch-message {
      display: inline-block;
      width: @startview-launch-message-width;
      margin: 0px auto;
      line-height: @startview-launch-message-lineHeight;
      font-size: @startview-launch-message-fontSize;
      position: fixed;
      top: @startview-launch-message-offset-y;
      right: @startview-launch-message-offset-x;
      color: @startview-launch-message-color;
    }
  }
}