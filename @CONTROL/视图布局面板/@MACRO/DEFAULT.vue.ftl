<div class="view-container ${srffilepath2(view.getCodeName())}<#if view.getPSSysCss?? && view.getPSSysCss()??> ${view.getPSSysCss().getCssName()}</#if>">
    <card class='view-card<#if !view.isShowCaptionBar()> view-no-caption</#if>' :disHover="true" :padding="0" :bordered="false">
        <#if view.isShowCaptionBar()>
        <p slot="title">
            ${view.getCaption()}
        </p>
        </#if>
<#if view.hasPSControl('toolbar')>
<#assign toolbarCtrl = view.getPSControl('toolbar')>
<@ibizindent blank=8>
${P.getCtrlCode(toolbarCtrl, 'CONTROL.html').code}
</@ibizindent>
</#if>
        <div class="content-container">
<#if view.getAllPSControls()??>
<#list view.getAllPSControls() as ctrl>
<#if ctrl.getControlType() != "TOOLBAR">
<#if P.getCtrlCode(ctrl, 'CONTROL.html')??>
<@ibizindent blank=12>
${P.getCtrlCode(ctrl, 'CONTROL.html').code}
</@ibizindent>
</#if>
</#if>
</#list>
</#if>
        </div>
    </card>
</div>