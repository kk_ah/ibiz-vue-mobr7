<#ibizinclude>
../@MACRO/ISOUTPUTTOOLBAR.ftl
</#ibizinclude>
<#if view.getPSSysCss?? && view.getPSSysCss()??>
<#assign styleName = view.getPSSysCss().getCssName()/>
</#if>
<#if styleName?? && styleName == "TopFullScreen">
<#assign isStyleName = true/>
<#else>
<#assign isStyleName = false/>
</#if>
<ion-page :className="{ 'view-container': true<#if !isStyleName>, 'default-mode-view': true</#if>, '${view.getViewType()?lower_case}': true, '${srffilepath2(view.getCodeName())}': true<#if view.getPSSysCss?? && view.getPSSysCss()??>, '${view.getPSSysCss().getCssName()}': true</#if> }">
    
    <ion-header>
    <#if !view.isShowCaptionBar?? || view.isShowCaptionBar() == true>
        <ion-toolbar class="ionoc-view-header">
            <ion-buttons slot="start">
                <ion-button v-show="isShowBackButton" @click="closeView">
                    <ion-icon name="chevron-back"></ion-icon>
                    {{$t('app.button.back')}}
                </ion-button>
        <#--  输出左侧工具栏：BEGIN  -->
        <#list view.getAllPSControls() as ctrl>
        <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU'>
            <@ibizindent blank=16>
                ${P.getCtrlCode(ctrl,'CONTROL.html').code}
            </@ibizindent>
        </#if>
        </#list>
        <#--  输出左侧工具栏：END  -->
            </ion-buttons>
            <#if !view.isShowCaptionBar?? || view.isShowCaptionBar() == true><ion-title class="view-title"><label class="title-label"><ion-icon v-if="model.icon" :name="model.icon"></ion-icon> <img v-else-if="model.iconcls" :src="model.iconcls" alt=""> {{$t(model.srfCaption)}}</label></ion-title></#if>
        <#--  输出右侧工具栏：BEGIN  -->
        <#if isOutputToolBar('MOBNAVRIGHTMENU')>
            <ion-buttons slot="end">
            <#list view.getAllPSControls() as ctrl>
            <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU'>
            <@ibizindent blank=16>
                ${P.getCtrlCode(ctrl,'CONTROL.html').code}
            </@ibizindent>
            </#if>
            </#list>
            </ion-buttons>
        </#if>
        <#--  输出右侧工具栏左：END  -->
        </ion-toolbar>
        </#if>
        <#--  输出工具栏分组：BEGIN  -->
        <#list view.getAllPSControls() as ctrl>
        <#if ctrl.getControlType() == 'TOOLBAR'>
        <#if ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU' || ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU'>
        <#list ctrl.getPSDEToolbarItems() as item>
        <#if item.getItemType() == 'ITEMS'>
        <ion-backdrop tappable="false" style="height :100vh;z-index: 99;" v-show="${ctrl.name}Models.${item.name}.isshow" @ionBackdropTap="${ctrl.name}Models.${item.name}.isshow=false" visible="true"></ion-backdrop>
        <div class="toolbar_group_<#if ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU'>right<#else>left</#if>" v-show="${ctrl.name}Models.${item.name}.isshow" >
            <ion-list class="ionlist">
        <#list item.getPSDEToolbarItems() as toolbarItem>
                <ion-item  @click="${ctrl.name}_click({ tag: '${toolbarItem.name}' }, $event), ${ctrl.name}Models.${item.name}.isshow=false"><#if toolbarItem.isShowIcon()><#if toolbarItem.getPSSysImage()??>  <ion-icon name="<#if toolbarItem.getPSSysImage().getCssClass() != "">${toolbarItem.getPSSysImage().getCssClass()}<#else>reorder-four-outline</#if>" class="group_ion-icon"></ion-icon><#else><ion-icon class="group_ion-icon" name="reorder-four-outline"></ion-icon></#if></#if> {{<#if toolbarItem.isShowCaption()><#if langbase??>$t('${langbase}.${toolbarItem.name}.caption')<#else>'${toolbarItem.getCaption()}'</#if></#if>}} </ion-item>
        </#list>
                <ion-item  @click="${ctrl.name}Models.${item.name}.isshow=false"> <ion-icon name="close" class="group_ion-icon"></ion-icon> 关闭 </ion-item>
            </ion-list>
        </div>
        </#if>
        </#list>
        </#if>
        </#if>
        </#list>
        <#--  输出工具栏分组：END  -->
    <#--  输出快速搜索：BEGIN  -->
    <#if view.isEnableQuickSearch?? && view.isEnableQuickSearch() == true>
        <ion-toolbar>
            <ion-searchbar style="height: 36px; padding-bottom: 0px;" :placeholder="$t('app.fastsearch')" debounce="500" @ionChange="quickValueChange($event)" show-cancel-button="focus" :cancel-button-text="$t('app.button.cancel')"></ion-searchbar>
            <#if view.hasPSControl('searchform')>
            <ion-button class="filter-btn" size="small" slot="end"  @click="openSearchform"><ion-icon  slot="end" name="filter-outline"></ion-icon>过滤</ion-button>  
            </#if>
        </ion-toolbar>
    </#if>
    <#--  自定义头部快速搜索：BEGIN  -->    
        <#if view_enableQuickSearch??>
            ${view_enableQuickSearch}
        </#if>
    <#--  自定义头部快速搜索：END  -->
    <#--  输出快速搜索：END  -->
    <#--  自定义头部按钮：BEGIN  -->    
    <#if view_header_botton??>
        <@ibizindent blank=12>
        ${view_header_botton}
        </@ibizindent>
    </#if>
    <#--  自定义头部按钮：END  -->
    </ion-header>

    <#--  搜索表单：BEGIN   -->
    <#if view.hasPSControl('searchform')>
    <ion-menu side="start" content-id="searchform<#if view.getName()??>${view.getName()?lower_case}</#if>" ref='searchform<#if view.getName()??>${view.getName()?lower_case}</#if>'>
        <ion-header>
            <ion-toolbar translucent>
            <ion-title>条件搜索</ion-title>
            </ion-toolbar>
        </ion-header>
        <ion-content>
            ${P.getCtrlCode('searchform', 'CONTROL.html').code}
        </ion-content>
        <ion-footer>
        <div class="search-btn">
            <ion-button class="search-btn-item" shape="round" size="small" expand="full" color="light" @click="onReset">重置</ion-button>
            <ion-button class="search-btn-item" shape="round" size="small" expand="full" @click="onSearch">搜索</ion-button>
        </div>
        </ion-footer>
    </ion-menu>
    <div id="searchform<#if view.getName()??>${view.getName()?lower_case}</#if>"></div>
    </#if>
    <#--  搜索表单：END   -->

    <ion-content>
    <#--  视图是否支持下拉刷新：BEGIN  -->
    <#if view.isEnablePullDownRefresh?? && view.isEnablePullDownRefresh()>
        <ion-refresher 
            slot="fixed" 
            ref="loadmore" 
            pull-factor="0.5" 
            pull-min="50" 
            pull-max="100" 
            @ionRefresh="pullDownToRefresh($event)">
            <ion-refresher-content
                pulling-icon="arrow-down-outline"
                :pulling-text="$t('app.pulling_text')"
                refreshing-spinner="circles"
                refreshing-text="">
            </ion-refresher-content>
        </ion-refresher>
    </#if>
    <#--  视图是否支持下拉刷新：END  -->
    <@ibizindent blank=8>
    <#if view_content??>
        ${view_content}
    </#if>
    </@ibizindent>
    </ion-content>
<#--  输出底部工具栏或者底部内容：BEGIN  -->
<#if isOutputToolBar('MOBBOTTOMMENU') || view_footer??>
    <ion-footer class="view-footer" style="z-index:9;">
    <#list view.getAllPSControls() as ctrl>
    <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
    <@ibizindent blank=8>
        ${P.getCtrlCode(ctrl,'CONTROL.html').code}
    </@ibizindent>
    </#if>
    </#list>
    <#if view_footer??>
        ${view_footer}
    </#if>
    </ion-footer>
</#if>
<#--  输出底部工具栏或者底部内容：BEGIN  -->
<#--  输出底部工具栏分组：BEGIN  -->
    <#list view.getAllPSControls() as ctrl>
    <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
    <#list ctrl.getPSDEToolbarItems() as item>
    <#if item.getItemType() == 'ITEMS'>
    <ion-backdrop tappable="false" style="height :100vh;z-index: 99;" v-show="${ctrl.name}Models.${item.name}.isshow" @ionBackdropTap="${ctrl.name}Models.${item.name}.isshow=false" visible="true"></ion-backdrop>
    <div v-show="${ctrl.name}Models.${item.name}.isshow" class="footer_group">
      <ion-list class="ionlist">
    <#list item.getPSDEToolbarItems() as toolbarItem>
        <ion-item  @click="${ctrl.name}_click({ tag: '${toolbarItem.name}' }, $event), ${ctrl.name}Models.${item.name}.isshow=false"><#if toolbarItem.isShowIcon()><#if toolbarItem.getPSSysImage()??>  <ion-icon name="<#if toolbarItem.getPSSysImage().getCssClass() != "">${toolbarItem.getPSSysImage().getCssClass()}<#else>reorder-four-outline</#if>" class="group_ion-icon"></ion-icon><#else><ion-icon class="group_ion-icon" name="reorder-four-outline"></ion-icon></#if></#if> {{<#if toolbarItem.isShowCaption()><#if langbase??>$t('${langbase}.${toolbarItem.name}.caption')<#else>'${toolbarItem.getCaption()}'</#if></#if>}} </ion-item>
    </#list>
        <ion-item  @click="${ctrl.name}Models.${item.name}.isshow=false"> <ion-icon name="close" class="group_ion-icon"></ion-icon> 关闭 </ion-item>
      </ion-list>
    </div>
    </#if>
    </#list>
    </#if>
    </#list>
</ion-page>