<#--  判断是否输出工具栏：BEGIN  -->
<#--  方法名称：outputToolBar  -->
<#--  参数：styleType ： ('MOBNAVLEFTMENU' | 'MOBNAVRIGHTMENU' | 'MOBBOTTOMMENU') (左侧 | 右侧 | 底部)-->
<#function isOutputToolBar styleType>
<#assign state = false />
    <#list view.getAllPSControls() as ctrl>
    <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == styleType>
    <#assign state = true/>
    <#break>
    </#if>
    </#list>
<#return state>
</#function>
<#--  判断是否输出工具栏：END  -->