// this is less
.srf_empty_class {
    width: 100%;
    height: 100%;
}
.title-label{
    display: flex;
    align-items: center;
    justify-content: center;
    ion-icon {
      margin-right: 5px;
    }
    img{
      width: 22px;
      height: 22px;
      margin-right: 5px;
    }
}
<#list view.getAllPSControls() as ctrl>

<#if ctrl.getControlType() == 'TOOLBAR'>
${P.getCtrlCode(ctrl, 'CONTROL.less').code}
</#if>
</#list>

<#assign viewCssName = '' />
<#if view.getPSSysCss()??>
<#assign viewCssName = view.getPSSysCss().getCssName()>
</#if>
<#list view.getPSSysCsses() as syscss>
<#if syscss.getCssStyle()??>
<#if syscss.getCssName() != viewCssName>
<#if syscss.getRawCssStyle()?? && syscss.getRawCssStyle()?length gt 0>
.${syscss.getCssName() } {
    ${syscss.getRawCssStyle()}
}
</#if>
<#if syscss.getCssStyle()??>
${syscss.getCssStyle()}
</#if>
</#if>
</#if>
</#list>

<#if view.getPSSysCss()??>
<#assign css=view.getPSSysCss()/>
<#if css.getRawCssStyle()?? && css.getRawCssStyle()?length gt 0>
.${css.getCssName()} {
    ${css.getRawCssStyle()}
}
</#if>
<#if css.getCssStyle()??>
${css.getCssStyle()}
</#if>
</#if>
<#list view.getAllPSControls() as ctrl>
<#if ctrl.getControlType() == 'MOBMDCTRL' || ctrl.getControlType() == 'DATAVIEW'  &&  ctrl.isNoSort() == false>
//排序样式
.${srffilepath2(view.getCodeName())}-toolbar{
    z-index:10;
    .code-box{
      overflow-x: auto;
      .ibz-item{
            --inner-border-width:0;
            min-width: 50%;
        }
    }
    .view-tool {
        display: flex;
        padding: 0 @padding-sm;
        &-sorts {
            display: flex;
            flex-grow: 1;
            &-item {
                display: flex;
                padding-right: @margin-sm;
                height: 24px;
                .text {
                    display: flex;
                    flex-grow: 1;
                }
                .sort-icon {
                    display: flex;
                    flex-direction: column;
                    width: 20px;
                    padding-left: @padding-base;
                    justify-content: center;
                    ion-icon {
                        font-size: @font-size-xs - 2;
                    }
                    .sort-select{
                      color: #3880ff;;
                    }
                }
            }
        }
        &-searchform {
            display: flex;
            width: 60px;
            justify-content: flex-end;
            align-items: center;
        }
    }
}
</#if>
</#list>
<#if view.hasPSControl('searchform')>
//搜索表单按钮
.search-btn{
  display: flex;
  margin-bottom: 15px;
  .search-btn-item{
    width: 50%;
  }
}
</#if>
<#if view.isEnableQuickSearch?? && view.isEnableQuickSearch() == true>
.filter-btn{
  position: relative;
    top: 5px;
}
</#if>