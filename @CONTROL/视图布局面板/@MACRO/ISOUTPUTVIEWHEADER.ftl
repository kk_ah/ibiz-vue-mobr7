<#ibizinclude>
./ISOUTPUTTOOLBAR.ftl
</#ibizinclude>
<#function isOutputViewHeader>
<#assign state = false />

    <#--  是否显示视图标题  -->
    <#if !view.isShowCaptionBar?? || view.isShowCaptionBar()>
        <#assign state = true/>
    </#if>

    <#--  是否显示左侧导航菜单  -->
    <#if !state && isOutputToolBar('MOBNAVLEFTMENU')>
        <#assign state = true/>
    </#if>

    <#--  是否显示右侧导航菜单  -->
    <#if !state && isOutputToolBar('MOBNAVRIGHTMENU')>
        <#assign state = true/>
    </#if>

    <#--  是否存在快捷搜索  -->
    <#if !state && view.isEnableQuickSearch?? && view.isEnableQuickSearch() == true>
        <#assign state = true/>
    </#if>

    <#--  是否存在自定义头部按钮  -->
    <#if !state && view_header_botton??>
        <#assign state = true/>
    </#if>

<#return state>
</#function>