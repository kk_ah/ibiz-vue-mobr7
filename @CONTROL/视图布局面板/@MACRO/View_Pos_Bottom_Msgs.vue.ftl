<div class='view-bottom-messages'>
<#if view.getPSViewMsgGroup()??>
<#assign viewMsgGroup = view.getPSViewMsgGroup()/>
    <#list viewMsgGroup.getPSViewMsgGroupDetails() as detail>
    <#assign msg = detail.getPSViewMsg()>
    <#--  BEGIN：暂时支持静态  -->
    <#if msg.getCodeName() != '' && msg.getPosition() == 'BOTTOM' && msg.getDynamicMode() == 0>
    <#assign type = msg.getMessageType() />
    <alert type='<#if type == 'INFO'>info<#elseif type == 'WARN'>warning<#elseif type == 'ERROR'>error<#else>info</#if>' closable={${msg.isEnableRemove()?c}}>
        ${msg.getTitle()}
        <span slot='desc'>${msg.getMessage()}</span>
    </alert>
    </#if>
    <#--  END：暂时支持静态  -->
    </#list>
</#if>
</div>