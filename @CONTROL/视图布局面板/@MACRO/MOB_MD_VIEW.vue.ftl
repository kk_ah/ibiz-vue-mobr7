<#assign mdctrl = view.getPSControl('mdctrl')>
<#if view.getViewType?? && view.getViewType() ?? && view.getViewType()=='DEMOBWFDYNAEXPMDVIEW'>
    <#assign view_header_botton>
<ion-toolbar>
    <ion-segment value="0">
        <ion-segment-button v-for="(item,index) in wfStepModel" :key="index" @click="tab_click(item)" :value="index">
            <ion-label>{{item.userTaskName}}</ion-label>
        </ion-segment-button>
    </ion-segment>
</ion-toolbar>
    </#assign>
<#elseif !mdctrl.isNoSort()>
    <#assign state = false />
    <#list mdctrl.getPSListItems() as item>
        <#if item.isEnableSort()>
        <#assign state = true/>
        <#break>
        </#if>
    </#list>
    <#if state>
    <#assign view_header_botton>
<ion-toolbar class="${srffilepath2(view.getCodeName())}-toolbar default-sort">
    <div class="view-tool">
        <div class="view-tool-sorts">
        <#list mdctrl.getPSListItems() as item>
            <#if item.isEnableSort() && !item.getPSCodeList()??>
            <div class="view-tool-sorts-item">
                <span class="text" @click="onSort('<#if item.getName()??>${item.getName()}</#if>')"><#if item.getCaption()??>${item.getCaption()}</#if></span>
                <span class="sort-icon" @click="onSort('<#if item.getName()??>${item.getName()}</#if>')">
                    <ion-icon :class="{'ios' : true ,'hydrated': true ,'sort-select': sort.asc == '<#if item.getName()??>${item.getName()}</#if>'}" name="chevron-up-outline" ></ion-icon>
                    <ion-icon :class="{'ios' : true ,'hydrated': true ,'sort-select': sort.desc == '<#if item.getName()??>${item.getName()}</#if>'}" name="chevron-down-outline" ></ion-icon>
                </span>
            </div>
            </#if>
        </#list>
        </div>
    </div>
</ion-toolbar>
    <div style="display:flex;overflow: auto;">
    <#list mdctrl.getPSListItems() as item>
        <#if item.isEnableSort() && item.getPSCodeList?? && item.getPSCodeList()??>
        <#assign codelist = item.getPSCodeList()>
        <#if codelist.getCodeListType() == 'STATIC' && codelist.getAllPSCodeItems()??>
        <app-van-select  name="<#if item.getName()??>n_${item.getName()?lower_case}_eq</#if>" title="<#if item.getCaption()??>${item.getCaption()}</#if>" :items="[<#list codelist.getAllPSCodeItems() as codeitem>{value:'<#if codeitem.getValue()??>${codeitem.getValue()}</#if>',label:'<#if codeitem.getName()??>${codeitem.getName()}</#if>'},</#list>]" @onConfirm="onCategory"></app-van-select>
        </#if>
        </#if>
    </#list>   
    </div>
    </#assign>
    </#if>
</#if>
<#assign view_content>
${P.getCtrlCode('mdctrl', 'CONTROL.html').code}
</#assign>
<#assign view_footer>
    <#if mdctrl.getBatchPSDEToolbar?? && mdctrl.getBatchPSDEToolbar()??>
        <#assign batchToolbar = mdctrl.getBatchPSDEToolbar()> 
        <div v-show="showCheack" class="batch_btn">
                <@ibizindent blank=8>
                ${P.getCtrlCode(batchToolbar, 'CONTROL.html').code}
                </@ibizindent>
            <ion-button class="app-view-toolbar-button"  @click="cancelSelect" >
                <ion-icon name="arrow-undo-outline"></ion-icon>
                {{$t('app.button.cancel')}}
            </ion-button>
        </div>     
    </#if>
</#assign>