<#if view.hasPSControl('pickupviewpanel')>
<#assign view_enableQuickSearch>
    <van-search
    v-model="quickValue"
    :placeholder="$t('app.fastsearch')"
    @input="quickValueChange($event)"
  />
</#assign>
<#assign view_content>
${P.getCtrlCode('pickupviewpanel', 'CONTROL.html').code}
</#assign>
</#if>
<#assign view_footer>
<ion-toolbar style="text-align: center;" class="mobpickupview_button">
    <ion-button @click="onClickCancel" color="light">{{$t('app.button.cancel')}}</ion-button>
    <ion-button @click="onClickOk" :disabled="viewSelections.length === 0">{{$t('app.button.confirm')}}</ion-button>
</ion-toolbar>
</#assign>
<#ibizinclude>
./VIEW_LAYOUT_BASE.ftl
</#ibizinclude>