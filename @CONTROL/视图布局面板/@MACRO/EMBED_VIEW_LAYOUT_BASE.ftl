<#ibizinclude>
../@MACRO/ISOUTPUTTOOLBAR.ftl
</#ibizinclude>
<#ibizinclude>
../@MACRO/ISOUTPUTVIEWHEADER.ftl
</#ibizinclude>
<#if view.getPSSysCss?? && view.getPSSysCss()??>
<#assign styleName = view.getPSSysCss().getCssName()/>
</#if>
<#if styleName?? && styleName == "TopFullScreen">
<#assign isStyleName = true/>
<#else>
<#assign isStyleName = false/>
</#if>
<embed-view :className="{ 'view-container': true<#if !isStyleName>, 'default-mode-view': true</#if>, '${view.getViewType()?lower_case}': true, '${srffilepath2(view.getCodeName())}': true<#if view.getPSSysCss?? && view.getPSSysCss()??>, '${view.getPSSysCss().getCssName()}': true</#if> }">
<#if isOutputViewHeader()>
    <template slot="header">
    <#--  是否输出视图标题以及内容：BEGIN  -->
    <#if isOutputToolBar('MOBNAVLEFTMENU') || isOutputToolBar('MOBNAVRIGHTMENU') && (!view.isShowCaptionBar?? || view.isShowCaptionBar() == true)>
        <ion-toolbar>
        <#--  输出左侧工具栏：BEGIN  -->
        <#if isOutputToolBar('MOBNAVLEFTMENU')>
            <ion-buttons slot="start" class="ibiz-top-left-buttons ibiz-buttonGroup">
            <#list view.getAllPSControls() as ctrl>
            <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU'>
            <@ibizindent blank=16>
                ${P.getCtrlCode(ctrl,'CONTROL.html').code}
            </@ibizindent>
            </#if>
            </#list>
            </ion-buttons>
        </#if>
        <#--  输出左侧工具栏：END  -->
            <#if !view.isShowCaptionBar?? || view.isShowCaptionBar() == true><ion-title v-if="showTitle">{{$t(model.srfCaption)}}</ion-title></#if>
        <#--  输出右侧工具栏：BEGIN  -->
        <#if isOutputToolBar('MOBNAVRIGHTMENU')>
            <ion-buttons slot="end" class="ibiz-top-right-buttons ibiz-buttonGroup">
            <#list view.getAllPSControls() as ctrl>
            <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU'>
            <@ibizindent blank=16>
                ${P.getCtrlCode(ctrl,'CONTROL.html').code}
            </@ibizindent>
            </#if>
            </#list>
            </ion-buttons>
        </#if>
        <#--  输出右侧工具栏左：END  -->
        </ion-toolbar>
    </#if>
    <#--  输出工具栏分组：BEGIN  -->
        <#list view.getAllPSControls() as ctrl>
        <#if ctrl.getControlType() == 'TOOLBAR'>
        <#if ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU' || ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU'>
        <#list ctrl.getPSDEToolbarItems() as item>
        <#if item.getItemType() == 'ITEMS'>
        <ion-backdrop tappable="false" style="height :100vh;z-index: 99;" v-show="${ctrl.name}Models.${item.name}.isshow" @ionBackdropTap="${ctrl.name}Models.${item.name}.isshow=false" visible="true"></ion-backdrop>
        <div class="toolbar_group_<#if ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU'>right<#else>left</#if>" v-show="${ctrl.name}Models.${item.name}.isshow" >
            <ion-list class="ionlist">
        <#list item.getPSDEToolbarItems() as toolbarItem>
                <ion-item  @click="${ctrl.name}_click({ tag: '${toolbarItem.name}' }, $event), ${ctrl.name}Models.${item.name}.isshow=false"><#if toolbarItem.isShowIcon()><#if toolbarItem.getPSSysImage()??>  <ion-icon name="<#if toolbarItem.getPSSysImage().getCssClass() != "">${toolbarItem.getPSSysImage().getCssClass()}<#else>reorder-four-outline</#if>" class="group_ion-icon"></ion-icon><#else><ion-icon class="group_ion-icon" name="reorder-four-outline"></ion-icon></#if></#if> {{<#if toolbarItem.isShowCaption()><#if langbase??>$t('${langbase}.${toolbarItem.name}.caption')<#else>'${toolbarItem.getCaption()}'</#if></#if>}} </ion-item>
        </#list>
                <ion-item  @click="${ctrl.name}Models.${item.name}.isshow=false"> <ion-icon name="close" class="group_ion-icon"></ion-icon> 关闭 </ion-item>
            </ion-list>
        </div>
        </#if>
        </#list>
        </#if>
        </#if>
        </#list>
    <#--  输出工具栏分组：END  -->
    <#--  是否输出视图标题以及内容：END  -->
    <#--  输出快速搜索：BEGIN  -->
    <#--  部件布局隐藏快速搜索  -->
    <#-- 
    <#if view.isEnableQuickSearch?? && view.isEnableQuickSearch() == true>
        <ion-toolbar>
            <ion-searchbar style="height: 36px; padding-bottom: 0px;" :placeholder="$t('app.fastsearch')" debounce="500" @ionChange="quickValueChange($event)" show-cancel-button="focus" :cancel-button-text="$t('app.button.cancel')"></ion-searchbar>
        </ion-toolbar>
    </#if>
    -->
    <#--  输出快速搜索：END  -->
    <#--  自定义头部按钮：BEGIN  -->       
    <#if view_header_botton??>
        <@ibizindent blank=12>
            ${view_header_botton}
        </@ibizindent>
    </#if>
    <#--  自定义头部按钮：END  -->
    </template>
</#if>
    <template slot="content">
    <@ibizindent blank=8>
    <#if view_content??>
        ${view_content}
    </#if>
    </@ibizindent>
    </template>
<#--  输出底部工具栏或者底部内容：BEGIN  -->
<#if isOutputToolBar('MOBBOTTOMMENU') || view_footer??>
    <template slot="footer">
        <#list view.getAllPSControls() as ctrl>
        <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
        <@ibizindent blank=8>
        ${P.getCtrlCode(ctrl, 'CONTROL.html').code}
        </@ibizindent>
        </#if>
        </#list>
        <#if view_footer??>
        ${view_footer}
        </#if>
    </template>
</#if>
<#--  输出底部工具栏或者底部内容：BEGIN  -->
    <#list view.getAllPSControls() as ctrl>
    <#if ctrl.getControlType() == 'TOOLBAR' && ctrl.getToolbarStyle?? && ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
    <#list ctrl.getPSDEToolbarItems() as item>
    <#if item.getItemType() == 'ITEMS'>
    <ion-backdrop style="height :100vh;z-index: 99;" v-show="${ctrl.name}Models.${item.name}.isshow" @ionBackdropTap="${ctrl.name}Models.${item.name}.isshow=false" visible="true"></ion-backdrop>
    <div v-show="${ctrl.name}Models.${item.name}.isshow" class="footer_group">
      <ion-list class="ionlist">
    <#list item.getPSDEToolbarItems() as toolbarItem>
        <ion-item  @click="${ctrl.name}_click({ tag: '${toolbarItem.name}' }, $event), ${ctrl.name}Models.${item.name}.isshow=false"><#if toolbarItem.isShowIcon()><#if toolbarItem.getPSSysImage()??>  <ion-icon name="<#if toolbarItem.getPSSysImage().getCssClass() != "">${toolbarItem.getPSSysImage().getCssClass()}<#else>reorder-four-outline</#if>" class="group_ion-icon"></ion-icon><#else><ion-icon class="group_ion-icon" name="reorder-four-outline"></ion-icon></#if></#if> {{<#if toolbarItem.isShowCaption()><#if langbase??>$t('${langbase}.${toolbarItem.name}.caption')<#else>'${toolbarItem.getCaption()}'</#if></#if>}} </ion-item>
    </#list>
      </ion-list>
    </div>
    </#if>
    </#list>
    </#if>
    </#list>
</embed-view>