<ion-page class="view-container ${srffilepath2(view.getCodeName())}<#if view.getPSSysCss?? && view.getPSSysCss()??> ${view.getPSSysCss().getCssName()}</#if>">
        <#if view.isShowCaptionBar()>
    <ion-header>
        <ion-toolbar class="ionoc-view-header">
            <ion-buttons slot="start">
                <ion-button v-show="isShowBackButton" @click="closeView">
                    <ion-icon name="chevron-back"></ion-icon>
                    {{$t('app.button.back')}}
                </ion-button>
            </ion-buttons>
            <ion-title>{{$t(model.srfCaption)}}</ion-title>
        </ion-toolbar>
    </ion-header>
        </#if>
        <ion-content class="content-container">
<@ibizindent blank=12>
<#if view.hasPSControl('wizardpanel')>
${P.getCtrlCode('wizardpanel', 'CONTROL.html').code}
</#if>
</@ibizindent>
        </ion-content>
</ion-page>