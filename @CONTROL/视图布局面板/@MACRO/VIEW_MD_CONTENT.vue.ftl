<#if view.hasPSControl('propertypanel')>
<#assign propertypanel = view.getPSControl('propertypanel')>
<div class="<#if propertypanel.getUserTag()??>property-layout-${propertypanel.getUserTag()?lower_case}</#if>" style="flex-grow: 1;">
    <div>
        <@ibizindent blank=8>
        ${MDContent}
        </@ibizindent>
    </div>
    <div style="<#if propertypanel.getWidth() gt 0>width: '${propertypanel.getWidth()}px'</#if><#if propertypanel.getHeight() gt 0>;height: '${propertypanel.getHeight()}px'</#if> ">
        <@ibizindent blank=8>
        ${P.getCtrlCode('propertypanel', 'CONTROL.html').code}
        </@ibizindent>
    </div>
</div>
<#else>
${MDContent}
</#if>