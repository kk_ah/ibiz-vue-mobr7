<#if view.hasPSControl('wizardpanel')>
<#assign wizardpanel = view.getPSControl('wizardpanel')>
<#assign view_content>
${P.getCtrlCode('wizardpanel', 'CONTROL.html').code}
</#assign>
</#if>
<#ibizinclude>
../@MACRO/VIEW_LAYOUT_BASE.ftl
</#ibizinclude>