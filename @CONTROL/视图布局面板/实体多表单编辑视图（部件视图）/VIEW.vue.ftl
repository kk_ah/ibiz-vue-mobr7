<div  class="view-container  app-view-mobmeditview9  ${srffilepath2(view.getCodeName())}<#if view.getPSSysCss()??><#assign singleCss=view.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <div  class="view-content-ctrl">
<#if view.hasPSControl('meditviewpanel')>
<#assign mdctrl = view.getPSControl('meditviewpanel')>
        <div>
            <#if view.getAllPSControls()??>
            <#list view.getAllPSControls() as ctrl>
            <#if ctrl.getControlType() != "TOOLBAR">
            <#if P.getCtrlCode(ctrl, 'CONTROL.html')??>
            <@ibizindent blank=12>
            ${P.getCtrlCode(ctrl, 'CONTROL.html').code}
            </@ibizindent>
            </#if>
            </#if>
            </#list>
            </#if>
        </div>
</#if>
    </div>
</div>
