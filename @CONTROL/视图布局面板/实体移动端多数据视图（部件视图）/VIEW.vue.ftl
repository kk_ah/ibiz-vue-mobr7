<#if view.hasPSControl('mdctrl')>
<#assign mdctrl = view.getPSControl('mdctrl')>
<#assign view_content>
${P.getCtrlCode('mdctrl', 'CONTROL.html').code}
</#assign>
<#assign view_footer>
    <#if mdctrl.getBatchPSDEToolbar?? && mdctrl.getBatchPSDEToolbar()??>
        <#assign batchToolbar = mdctrl.getBatchPSDEToolbar()>
    <div v-show="showCheack" >
            <@ibizindent blank=8>
            ${P.getCtrlCode(batchToolbar, 'CONTROL.html').code}
            </@ibizindent>
    </div>     
    </#if>
</#assign>
</#if>
<#ibizinclude>
../@MACRO/EMBED_VIEW_LAYOUT_BASE.ftl
</#ibizinclude>