<#ibizinclude>
../@MACRO/DEFAULT.less.ftl
</#ibizinclude>

<#if view.hasPSControl('searchform')>
// 搜索表单内容
.searchform-content {
    display: flex;
    height: 100%;
    flex-direction: column;
    .form-content {
        flex-grow: 1;
        overflow: auto;
        height: 50%;
    }
    .form-action {
        padding: 0.5rem 1rem;
        text-align: center;
    }
}
</#if>