<#--  content  -->
<#assign content>
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}" 
    viewType="${view.getViewType()}"
    controlStyle="<#if ctrl.getControlStyle?? && ctrl.getControlStyle()??>${ctrl.getControlStyle()}</#if>"
    updateAction="<#if ctrl.getUpdatePSControlAction?? && ctrl.getUpdatePSControlAction()?? && ctrl.getUpdatePSControlAction().getPSAppDEMethod()??>${ctrl.getUpdatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    removeAction="<#if ctrl.getRemovePSControlAction?? && ctrl.getRemovePSControlAction()?? && ctrl.getRemovePSControlAction().getPSAppDEMethod()??>${ctrl.getRemovePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    loaddraftAction="<#if ctrl.getGetDraftPSControlAction?? && ctrl.getGetDraftPSControlAction()?? && ctrl.getGetDraftPSControlAction().getPSAppDEMethod()??>${ctrl.getGetDraftPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    loadAction="<#if ctrl.getGetPSControlAction?? && ctrl.getGetPSControlAction()?? && ctrl.getGetPSControlAction().getPSAppDEMethod()??>${ctrl.getGetPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    createAction="<#if ctrl.getCreatePSControlAction?? && ctrl.getCreatePSControlAction()?? && ctrl.getCreatePSControlAction().getPSAppDEMethod()??>${ctrl.getCreatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    fetchAction="<#if ctrl.getFetchPSControlAction?? && ctrl.getFetchPSControlAction()?? && ctrl.getFetchPSControlAction().getPSAppDEMethod()??>${ctrl.getFetchPSControlAction().getPSAppDEMethod().getCodeName()}</#if>" 
    :isMutli="!isSingleSelect"
    <#if view.getViewType?? && view.getViewType()?? && view.getViewType() == 'DEMOBMDVIEW' || view.getViewType() == 'DEMOBMDVIEW9'>
    :showCheack="showCheack"
    @showCheackChange="showCheackChange"
    </#if>
    :isTempMode="<#if ctrl.isTempMode?? && ctrl.isTempMode()??>${ctrl.isTempMode()?c}</#if>"
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>