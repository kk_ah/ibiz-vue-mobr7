 <#ibizinclude>./ITEM_LAYOUTPANEL.vue.ftl</#ibizinclude>
                    <ion-list  v-model="selectedArray"   v-if="isMutli">
                        <ion-item v-for="(item, index) of items" :key="index" class="app-mob-mdctrl-item" >
    <#if ctrl.getItemPSLayoutPanel()??>
<#--  BENGIN：输出项布局面板  -->
<@outPutItemLaoutPanel ctrl />
<#--  END：输出项布局面板  -->
    <#else>
                            <ion-checkbox color="secondary" :value="item.srfkey" @ionChange="checkboxChange"  slot="end"></ion-checkbox>
                            <ion-label>{{item.<#if appde.getMajorPSAppDEField()??>${appde.getMajorPSAppDEField().getCodeName()?lower_case}<#else>srfmajortext</#if>}}</ion-label>
    </#if>
                        </ion-item>
                    </ion-list>
    <#--                            单选-->
                    <ion-radio-group  :value="selectedValue" v-if="!isMutli">
                        <ion-item v-for="(item, index) of items" :key="index" class="app-mob-mdctrl-item"  @click="onSimpleSelChange(item)">
    <#if ctrl.getItemPSLayoutPanel()??>
<#--  BENGIN：输出项布局面板  -->
<@outPutItemLaoutPanel ctrl />
<#--  END：输出项布局面板  -->
    <#else>
                            <ion-label>{{item.<#if appde.getMajorPSAppDEField()??>${appde.getMajorPSAppDEField().getCodeName()?lower_case}<#else>srfmajortext</#if>}}</ion-label>
                            <ion-radio slot="end" :value="item.srfkey"></ion-radio>
    </#if>
                        </ion-item>
                    </ion-radio-group>