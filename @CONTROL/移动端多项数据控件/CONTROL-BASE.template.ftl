<#ibizinclude>./ACTION_GROUP.vue.ftl</#ibizinclude>
<#ibizinclude>./ITEM_LAYOUTPANEL.vue.ftl</#ibizinclude>
<template>
    <div  class="app-mob-mdctrl <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
        <div class="app-mob-mdctrl-${ctrl.name}">
          <van-pull-refresh class="app-mob-mdctrl-refresh" v-model="isLoading" success-text="刷新成功"  @refresh="refresh">
            <#if ctrl.getPSSysPFPlugin?? && ctrl.getPSSysPFPlugin()??>
                <#if ctrl.getPSSysPFPlugin().getPFPluginType?? && ctrl.getPSSysPFPlugin().getPFPluginType()?? && ctrl.getPSSysPFPlugin().getPFPluginType()=='LIST_RENDER'>
                    ${ctrl.render.code}
                </#if>
                <#if ctrl.getPSSysPFPlugin().getPFPluginType?? && ctrl.getPSSysPFPlugin().getPFPluginType()?? && ctrl.getPSSysPFPlugin().getPFPluginType()=='LIST_ITEMRENDER'>
                <ion-list class="items">
                <template v-if="(viewType == 'DEMOBMDVIEW9') && controlStyle != 'SWIPERVIEW' ">
                    ${ctrl.render.code}
                    <ion-button size="small" color="secondary" v-if="!isTempMode && !allLoaded" style ="position: relative;left: calc( 50% - 44px);"  @click="loadBottom">{{$t('app.button.loadmore')}}</ion-button>
                </template>
            </ion-list>
            <ion-list class="items">
            <#--多数据视图||多数据视图（部件视图）-->
                <template v-if="(viewType == 'DEMOBMDVIEW') && controlStyle != 'SWIPERVIEW' ">
                    ${ctrl.render.code}
                    <ion-button size="small" color="secondary" v-if="!isTempMode && !allLoaded" style ="position: relative;left: calc( 50% - 44px);"  @click="loadBottom">{{$t('app.button.loadmore')}}</ion-button>
                </template>
                <template v-else-if="(viewType == 'DEMOBMDVIEW9')">
                    ${ctrl.render.code}
                </template>
                <template v-else-if="(viewType == 'DEMOBMDVIEW' || viewType == 'DEMOBMDVIEW9') && controlStyle === 'SWIPERVIEW'">
                    ${ctrl.render.code}
                </template>
 <#-- BENGIN： 工作流多数据视图 -->
<#ibizinclude>./WORKFLOW_MDVIEW.template.ftl</#ibizinclude>
<#--  END: 工作流多数据视图 -->
                    <#--多数据选择视图（部件视图）-->
            <#--多选-->
                <template v-else>
 <#-- BENGIN： 多选视图 -->
<#ibizinclude>./PICKER_MDVIEW.template.ftl</#ibizinclude>
<#--  END: 多选视图 -->
                </template>
            </ion-list>
            <ion-infinite-scroll v-if="viewType == 'DEMOBMDVIEW'" :disabled="allLoaded" ref="loadmoreBottom" @ionInfinite="loadBottom" distince="1%">
                <ion-infinite-scroll-content
                    loadingSpinner="bubbles"
                    loadingText="正在加载数据">
                </ion-infinite-scroll-content>
            </ion-infinite-scroll>    
                </#if>

            <#else>
            <ion-list class="items">
                <template v-if="(viewType == 'DEMOBMDVIEW9') && controlStyle != 'SWIPERVIEW' ">
                    <div class="selectall">
                        <ion-checkbox :checked="selectAllIschecked"  v-show="showCheack"  @ionChange="checkboxAll"></ion-checkbox>
                        <ion-label class="selectal-label" v-show="showCheack">全选</ion-label>
                    </div>
                    <ion-item-sliding ref="sliding" v-for="(item, index) in items" @click="item_click(item)" :key="index" class="app-mob-mdctrl-item">
<#--  BENGIN：输出界面行为组  -->
<@outPutActionGroup ctrl />
<#--  END：输出界面行为组  -->
    <#if ctrl.getItemPSLayoutPanel()??>
<#--  BENGIN：输出项布局面板  -->
<@outPutItemLaoutPanel ctrl />
<#--  END：输出项布局面板  -->
    <#else>
                        <ion-item>
                            <ion-checkbox :checked="item.checked" v-show="showCheack" @click.stop="checkboxSelect(item)"></ion-checkbox>
        <#--实体列表项集合-->
            <#if  ctrl.getPSDEListItems()??>
                            <!-- 列表视图样式 -->
                            <app-list-default <#if ctrl.getPSDEListItems()??>:dataItemNames = "[<#list ctrl.getPSDEListItems() as item><#if item.getDataItemName()??>'${item.getDataItemName()}',</#if></#list>]"</#if> :item="item"  major="<#if appde.getMajorPSAppDEField()??>${appde.getMajorPSAppDEField().getCodeName()?lower_case}<#else>srfmajortext</#if>" v-if="controlStyle.substring(0,8) === 'LISTVIEW'"></app-list-default>
                                <!-- 图标视图样式 -->
                            <app-icon-list :item="item" v-if="controlStyle === 'ICONVIEW'"></app-icon-list>
            <#else>
                             <div>暂无数据</div>
            </#if>
                        </ion-item>
    </#if>
                    </ion-item-sliding>
                    <ion-button size="small" color="secondary" v-if="!isTempMode && !allLoaded" style ="position: relative;left: calc( 50% - 44px);"  @click="loadBottom">{{$t('app.button.loadmore')}}</ion-button>
                </template>
            </ion-list>
            <ion-list class="items">
            <#--多数据视图||多数据视图（部件视图）-->
                <template v-if="(viewType == 'DEMOBMDVIEW') && controlStyle != 'SWIPERVIEW' ">
                    <div class="selectall">
                        <ion-checkbox :checked="selectAllIschecked"  v-show="showCheack"  @ionChange="checkboxAll"></ion-checkbox>
                        <ion-label class="selectal-label" v-show="showCheack">全选</ion-label>
                    </div>
                    <ion-item-sliding  :ref="item.srfkey" v-for="(item, index) in items" @click="item_click(item)" :key="index" class="app-mob-mdctrl-item">
<#--  BENGIN：输出界面行为组  -->
<@outPutActionGroup ctrl />
<#--  END：输出界面行为组  -->
    <#if ctrl.getItemPSLayoutPanel()??>
<#--  BENGIN：输出项布局面板  -->
<@outPutItemLaoutPanel ctrl />
<#--  END：输出项布局面板  -->
    <#else>
                        <ion-item>
                            <ion-checkbox :checked="item.checked" v-show="showCheack" @click.stop="checkboxSelect(item)"></ion-checkbox>
    <#--实体列表项集合-->
        <#if  ctrl.getPSDEListItems()??>
                            <!-- 列表视图样式 -->
                            <app-list-default <#if ctrl.getPSDEListItems()??>:dataItemNames = "[<#list ctrl.getPSDEListItems() as item><#if item.getDataItemName()??>'${item.getDataItemName()}',</#if></#list>]"</#if> :item="item" major="<#if appde.getMajorPSAppDEField()??>${appde.getMajorPSAppDEField().getCodeName()?lower_case}<#else>srfmajortext</#if>" v-if="controlStyle.substring(0,8) === 'LISTVIEW'"></app-list-default>
                            <!-- 图标视图样式 -->
                            <app-icon-list :item="item" v-if="controlStyle === 'ICONVIEW'"></app-icon-list>
        <#else>
                            <div>暂无数据</div>
        </#if>
                        </ion-item>                      
    </#if>
                    </ion-item-sliding>
                    <ion-button size="small" color="secondary" v-if="!isTempMode && !allLoaded" style ="position: relative;left: calc( 50% - 44px);"  @click="loadBottom">{{$t('app.button.loadmore')}}</ion-button>
                </template>
                <template v-else-if="(viewType == 'DEMOBMDVIEW9')">
                </template>
                <template v-else-if="(viewType == 'DEMOBMDVIEW' || viewType == 'DEMOBMDVIEW9') && controlStyle === 'SWIPERVIEW'">
                    <app-list-swipe :items="items"></app-list-swipe>
                </template>
 <#-- BENGIN： 工作流多数据视图 -->
<#ibizinclude>./WORKFLOW_MDVIEW.template.ftl</#ibizinclude>
<#--  END: 工作流多数据视图 -->
                    <#--多数据选择视图（部件视图）-->
            <#--多选-->
                <template v-else>
 <#-- BENGIN： 多选视图 -->
<#ibizinclude>./PICKER_MDVIEW.template.ftl</#ibizinclude>
<#--  END: 多选视图 -->
                </template>
            </ion-list>
            <ion-infinite-scroll v-if="viewType == 'DEMOBMDVIEW'" :disabled="allLoaded" ref="loadmoreBottom" @ionInfinite="loadBottom" distince="1%">
                <ion-infinite-scroll-content
                    loadingSpinner="bubbles"
                    loadingText="正在加载数据">
                </ion-infinite-scroll-content>
            </ion-infinite-scroll>    
            </#if>
          </van-pull-refresh>
        </div>
    </div>
</template>