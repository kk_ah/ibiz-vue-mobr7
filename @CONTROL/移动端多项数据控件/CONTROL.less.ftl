<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
<#if ctrl.getItemPSLayoutPanel()??>
<#assign layoutpanel=ctrl.getItemPSLayoutPanel()>
    ${P.getCtrlCode(layoutpanel, 'CONTROL.less').code}
</#if>
.ibz-ionic-item{
    --inner-border-width:0;
}
.iconcheck{
    width: 70px;
}
.selectall{
    padding: 5px 20px;
    padding: 0 20px;
    display: flex;
}
.selectal-label{
    padding-left: 15px;
}
.app-mob-mdctrl{
    height:100%;
    .app-mob-mdctrl-${ctrl.name}{
        height:100%;
        .items{
            .ibz-ionic-item{
                --inner-padding-end: 10px;
                --padding-start: 10px;
            }
        }
        .app-mob-mdctrl-refresh{
            height:100%;
            overflow: auto;
            .van-pull-refresh__track{
                height:100%;
            }
        }
    }
}