<#macro outPutActionGroup  ctrl>
    <#if ctrl.getPSDEUIActionGroup()?? && ctrl.getPSDEUIActionGroup().getPSUIActionGroupDetails()??>
    <#assign details = ctrl.getPSDEUIActionGroup().getPSUIActionGroupDetails()>
                        <ion-item-options v-if="controlStyle != 'LISTVIEW3'" side="start">
            <#list details as detail>
                <#if  detail.getPSUIAction()??>
                    <#assign uiaction = detail.getPSUIAction()>
                            <ion-item-option v-show="item.${uiaction.getCodeName()}.visabled" :disabled="item.${uiaction.getCodeName()}.disabled"  color="<#if uiaction.getUIActionTag() == "Remove">danger<#else>primary</#if>" @click="mdctrl_click($event, '${detail.getName()}', item)">${uiaction.getCaption()}</ion-item-option>
                </#if>
            </#list>
                        </ion-item-options>
    </#if>
    <#if ctrl.getPSDEUIActionGroup2()?? && ctrl.getPSDEUIActionGroup2().getPSUIActionGroupDetails()??>
        <#assign details = ctrl.getPSDEUIActionGroup2().getPSUIActionGroupDetails()>
                        <ion-item-options v-if="controlStyle != 'LISTVIEW3'" side="end">
        <#list details as detail>
                <#if  detail.getPSUIAction()??>
                <#assign uiaction = detail.getPSUIAction()>
                            <ion-item-option v-show="item.${uiaction.getCodeName()}.visabled" :disabled="item.${uiaction.getCodeName()}.disabled" color="<#if uiaction.getUIActionTag() == "Remove">danger<#else>primary</#if>" @click="mdctrl_click($event, '${detail.getName()}', item)">${uiaction.getCaption()}</ion-item-option>
                </#if>
        </#list>
                        </ion-item-options>
    </#if>
</#macro>