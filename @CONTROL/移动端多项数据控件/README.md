### ibiz预置列表表现样式

#### 一、配置参数

##### 	（1）视图参数：通过配置数据部件的表现样式

​	配置：

​	![视图参数配置](./imgs/view-params-config.png)

​	代码：

​	![视图参数](./imgs/view-parmams.png)

##### 	（2）部件默认参数：通过配置移动端列表样式

配置：

​	![部件参数配置](./imgs/control-params-config.png)

代码：

​	![部件参数](./imgs/control-params.png)

#### 二、动态控制

![动态控制](./imgs/dynamic-control.png)

#### 三、效果展示

1、列表视图（默认）、（无刷新）、（无滑动）、长按多选、排序、批操工具栏

​	(1)、展示效果

![1589010711651](./imgs/each.png)	![默认列表样式](./imgs/default-list-view.png)

​		(2)、支持功能（刷新、滑动）  ps：无刷新不支持刷新、无滑动不支持滑动

   ![刷新行为](./imgs/refresh-action.png)	![左滑行为](./imgs/left-slide.png)

2、图标视图

​	(1)、展示效果

​	![icon列表样式](./imgs/icon-list-view.png)

​	(2)、支持功能（刷新、滑动）

3、图片滑动视图

​	(1)、展示效果

​	![图片滑动样式](./imgs/img-swiper-view.png)

​	(2)、支持功能（刷新）
