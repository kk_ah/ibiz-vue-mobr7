<template>
    <ion-grid class="app-mob-dashboard <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
        <#list ctrl.getAllPSPortlets() as portlet><#t>
        <#if portlet.getPortletType?? && portlet.getPortletType()?? && portlet.getPortletType() != 'CONTAINER'><#t>
            <ion-card>
            ${P.getCtrlCode(portlet, 'CONTROL.html').code}
            </ion-card>
        </#if>
        </#list>
    </ion-grid>
</template>