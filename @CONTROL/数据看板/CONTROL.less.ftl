<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>

.app-mob-dashboard {
    padding: @padding-base 0;
    .app-mob-portlet {
        margin: 0;
        width: 100%;
        &__header {
            padding-left: @padding-xs;
            font-size: @font-size-md;
        }
    }
}