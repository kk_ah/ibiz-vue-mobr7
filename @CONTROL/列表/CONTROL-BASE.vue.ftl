<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getData(): any {
        return null;
    }

    /**
     * 显示处理提示
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({ default: true }) protected showBusyIndicator?: boolean;
    
    /**
     * 部件行为--fetch
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() protected fetchAction!: string;

    /**
     * 列表数组
     *
     * @type {Array<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public items: Array<any> = [];

    /**
     * 列表类型
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({default:'LIST'}) protected listMode?: 'LISTEXPBAR' | 'LIST' | string;

    /**
     * 列表选中项的索引
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public listItem: number = 0;

    /**
     * 分页大小
     *
     * @type {number}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public pageSize: number = ${ ctrl.getPagingSize()?c};

    /**
     * 总页数
     *
     * @type {number}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public pageTotal: number = 0;

    /**
     * 当前页数
     *
     * @type {number}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public pageNumber: number = 0;

    /**
     * 部件样式
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({default: '<#if ctrl.getMobListStyle?? && ctrl.getMobListStyle()??>${ctrl.getMobListStyle()}</#if>'}) protected controlStyle!: string;

    /**
     * vue生命周期created
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public created() {
        this.handleCreated();
    }

    /**
     * 执行created后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public handleCreated() {
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                this.load(Object.assign(data, { page: this.pageNumber, size: this.pageSize }), "");
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

    /**
     * 列表切换回调
     * @param {number} listIndex
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public switchView(listIndex:number){
        this.items.findIndex((item,index)=>{
            if(index === listIndex){
                this.$emit('selectionchange', item);
            }
        })
    }

    /**
     * 数据加载
     *
     * @protected
     * @param {*} data
     * @param {*} type
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected async load(data: any, type: any): Promise<any> {
        const response: any = await this.service.search(this.fetchAction, this.context, data, this.showBusyIndicator);
        if (response && response.status === 200) {
            let datelist = response.data.records;
            this.formatdate(datelist);
            this.$emit("load",this.items);
        } else if (response && response.status !== 401) {
            const { data: _data } = response;
            this.$notice.error(_data.message)
        }
    }

    /**
     * 格式化数据
     *
     * @param {*} datelist
     * @memberof IBizChartViewController
     */
    public formatdate(datelist: any) {
        this.items = datelist;
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>