<#ibizinclude>
../@MACRO/MODEL/MODEL_HEADER.ts.ftl
</#ibizinclude>

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ${srfclassname('${ctrl.getCodeName()}')}${srfclassname('${ctrl.name}')}Mode
	 */
	public getDataItems(): any[] {
		return [
<#--  列表数据项  -->
<#if ctrl.getPSListDataItems()??>
  <#list ctrl.getPSListDataItems() as dataitem>
			{
				name: '${dataitem.getName()}',
    <#if dataitem.getPSDEField()??>
				prop: '${dataitem.getPSDEField().getCodeName()?lower_case}',
				dataType: '${dataitem.getPSDEField().getDataType()}',
    </#if>
			},
  </#list>
</#if>
<#--  关联主实体的主键  -->
<#if ctrl.getPSAppDataEntity()??>
  <#assign appDataEntity = ctrl.getPSAppDataEntity() />
  <#if appDataEntity.isMajor() == false && appDataEntity.getMinorPSAppDERSs()??>
    <#list appDataEntity.getMinorPSAppDERSs() as minorAppDERSs>
      <#if minorAppDERSs.getMajorPSAppDataEntity()??>
      <#assign majorAppDataEntity = minorAppDERSs.getMajorPSAppDataEntity() />
			{
				name: '${majorAppDataEntity.getCodeName()?lower_case}',
				prop: '${majorAppDataEntity.getKeyPSAppDEField().getCodeName()?lower_case}'
			},
      </#if>
    </#list>
  </#if>
  <#if ctrl.getPSListDataItems()??>
    <#list ctrl.getPSListDataItems() as dataitem>
      <#-- 界面主键标识 -->
      <#if dataitem.getPSDEField?? && dataitem.getPSDEField()??>
        <#if !P.exists("importService", dataitem.getPSDEField().getId(), "")>
          <#if dataitem.getPSDEField().isKeyDEField() == true>
			{
				name: '${appDataEntity.getCodeName()?lower_case}',
				prop: '${dataitem.getPSDEField().getCodeName()?lower_case}',
				dataType: 'FONTKEY',
			},
          </#if>
        </#if>
      </#if>
    </#list>
  </#if>
</#if>
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      }
		]
	}

<#ibizinclude>
../@MACRO/MODEL/MODEL_BOTTOM.ts.ftl
</#ibizinclude>