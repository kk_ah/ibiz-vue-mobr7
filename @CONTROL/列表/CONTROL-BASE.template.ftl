<template>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <div  class="app-mob-mdctrl <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
        <div v-if="listMode==='LISTEXPBAR'">
            <van-sidebar v-model="listItem" @change="switchView">
                <van-sidebar-item class="app-mob-list-item" v-for="item in items" :key="<#if appde??></#if>item.srfkey" :title='item.srfmajortext'/>
            </van-sidebar>
        </div>
        <div v-else class="app-mob-mdctrl-${ctrl.name}">
            <ion-list class="items">
                <template v-if="controlStyle != 'SWIPERVIEW' ">
                    <ion-item-sliding v-for="(item, index) in items" :key="index" class="app-mob-mdctrl-item">
                        <#if ctrl.getItemPSLayoutPanel()??>
                            <#assign layoutpanel=ctrl.getItemPSLayoutPanel()>
                        <div style="width:100%;">
                        <layout_${layoutpanel.getName()} :context="{}" :viewparams="{}" :item="item"></layout_${layoutpanel.getName()}>
                        </div>
                        <#else>
                        <#if ctrl.getPSDEUIActionGroup?? && ctrl.getPSDEUIActionGroup()?? && ctrl.getPSDEUIActionGroup().getPSUIActionGroupDetails()??>
                        <#assign details = ctrl.getPSDEUIActionGroup().getPSUIActionGroupDetails()>
                        <ion-item-options v-if="controlStyle != 'LISTVIEW3'" side="start">
                        <#list details as detail>
                        <#if  detail.getPSUIAction()??>
                        <#assign uiaction = detail.getPSUIAction()>
                            <ion-item-option color="<#if uiaction.getUIActionTag() == "Remove">danger<#else>primary</#if>" @click="mdctrl_click($event, '${detail.getName()}', item)">${uiaction.getCaption()}</ion-item-option>
                        </#if>
                        </#list>
                        </ion-item-options>
                        </#if>
                        <ion-item>
<#--                                    实体列表项集合-->
                            <#if  ctrl.getPSDEListItems()??>
                                <!-- 列表视图样式 -->
                                <app-list-default :item="item" v-if="controlStyle.substring(0,8) === 'LISTVIEW'"></app-list-default>
                                <!-- 图标视图样式 -->
                                <app-icon-list :item="item" v-if="controlStyle === 'ICONVIEW'"></app-icon-list>
                            <#else>
                                <div>暂无数据</div>
                            </#if>
                        </ion-item>
                        <#if ctrl.getPSDEUIActionGroup2?? &&  ctrl.getPSDEUIActionGroup2()?? && ctrl.getPSDEUIActionGroup2().getPSUIActionGroupDetails()??>
                        <#assign details = ctrl.getPSDEUIActionGroup2().getPSUIActionGroupDetails()>
                        <ion-item-options v-if="controlStyle != 'LISTVIEW3'" side="end">
                        <#list details as detail>
                        <#if  detail.getPSUIAction()??>
                        <#assign uiaction = detail.getPSUIAction()>
                            <ion-item-option color="<#if uiaction.getUIActionTag() == "Remove">danger<#else>primary</#if>" @click="mdctrl_click($event, '${detail.getName()}', item)">${uiaction.getCaption()}</ion-item-option>
                        </#if>
                        </#list>
                        </ion-item-options>
                        </#if>
                        </#if>
                    </ion-item-sliding>
                </template>
                <#--多数据视图||多数据视图（部件视图）-->
                <template v-else-if="controlStyle != 'LISTVIEW' ">
                        <ion-item-sliding v-for="(item, index) in items" :key="index" class="app-mob-mdctrl-item">
                            <#if ctrl.getItemPSLayoutPanel()??>
                                <#assign layoutpanel=ctrl.getItemPSLayoutPanel()>
                            <div style="width:100%;">
                            <layout_${layoutpanel.getName()} :context="{}" :viewparams="{}" :item="item"></layout_${layoutpanel.getName()}>
                            </div>
                            <#else>

                            <#if ctrl.getPSDEUIActionGroup?? &&  ctrl.getPSDEUIActionGroup()?? && ctrl.getPSDEUIActionGroup().getPSUIActionGroupDetails()??>
                            <#assign details = ctrl.getPSDEUIActionGroup().getPSUIActionGroupDetails()>
                            <ion-item-options v-if="controlStyle != 'LISTVIEW3'" side="start">
                            <#list details as detail>
                            <#if  detail.getPSUIAction()??>
                            <#assign uiaction = detail.getPSUIAction()>
                                <ion-item-option color="<#if uiaction.getUIActionTag() == "Remove">danger<#else>primary</#if>" @click="mdctrl_click($event, '${detail.getName()}', item)">${uiaction.getCaption()}</ion-item-option>
                            </#if>
                            </#list>
                            </ion-item-options>
                            </#if>
                            <ion-item>
<#--                                    实体列表项集合-->
                                    <#if  ctrl.getPSDEListItems()??>
                                    <!-- 列表视图样式 -->
                                    <app-list-default :item="item" v-if="controlStyle.substring(0,8) === 'LISTVIEW'"></app-list-default>
                                    <!-- 图标视图样式 -->
                                    <app-icon-list :item="item" v-if="controlStyle === 'ICONVIEW'"></app-icon-list>
                                <#else>
                                    <div>暂无数据</div>
                                </#if>
                            </ion-item>
                            <#if ctrl.getPSDEUIActionGroup2?? && ctrl.getPSDEUIActionGroup2()?? && ctrl.getPSDEUIActionGroup2().getPSUIActionGroupDetails()??>
                            <#assign details = ctrl.getPSDEUIActionGroup2().getPSUIActionGroupDetails()>
                            <ion-item-options v-if="controlStyle != 'LISTVIEW3'" side="end">
                            <#list details as detail>
                            <#if  detail.getPSUIAction()??>
                            <#assign uiaction = detail.getPSUIAction()>
                                <ion-item-option color="<#if uiaction.getUIActionTag() == "Remove">danger<#else>primary</#if>" @click="mdctrl_click($event, '${detail.getName()}', item)">${uiaction.getCaption()}</ion-item-option>
                            </#if>
                            </#list>
                            </ion-item-options>
                            </#if>

                            
                            </#if>
                        </ion-item-sliding>
                    </template>
                    <template v-else-if="controlStyle === 'SWIPERVIEW'">
                        <app-list-swipe :items="items"></app-list-swipe>
                    </template>
            </ion-list>
        </div>
    </div>
</#if>
</template>