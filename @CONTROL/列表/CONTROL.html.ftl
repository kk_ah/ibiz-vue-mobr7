<#--  content  -->
<#assign content>
     <#if view.getViewType?? && view.getViewType()??>
        listMode="<#if view.getViewType()=='DEMOBLISTEXPVIEW'>LISTEXPBAR<#else>LIST</#if>"
     </#if>
     updateAction="<#if ctrl.getUpdatePSControlAction?? && ctrl.getUpdatePSControlAction()?? && ctrl.getUpdatePSControlAction().getPSAppDEMethod()??>${ctrl.getUpdatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
     removeAction="<#if ctrl.getRemovePSControlAction?? && ctrl.getRemovePSControlAction()?? && ctrl.getRemovePSControlAction().getPSAppDEMethod()??>${ctrl.getRemovePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
     loaddraftAction="<#if ctrl.getGetDraftPSControlAction?? && ctrl.getGetDraftPSControlAction()?? && ctrl.getGetDraftPSControlAction().getPSAppDEMethod()??>${ctrl.getGetDraftPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
     loadAction="<#if ctrl.getGetPSControlAction?? && ctrl.getGetPSControlAction()?? && ctrl.getGetPSControlAction().getPSAppDEMethod()??>${ctrl.getGetPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
     createAction="<#if ctrl.getCreatePSControlAction?? && ctrl.getCreatePSControlAction()?? && ctrl.getCreatePSControlAction().getPSAppDEMethod()??>${ctrl.getCreatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
     :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}"  
     fetchAction="<#if ctrl.getFetchPSControlAction?? && ctrl.getFetchPSControlAction()?? && ctrl.getFetchPSControlAction().getPSAppDEMethod()??>${ctrl.getFetchPSControlAction().getPSAppDEMethod().getCodeName()}</#if>" 
    <#if ctrl.getControlStyle?? && ctrl.getControlStyle()??>controlStyle="${ctrl.getControlStyle()}"</#if>
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>
