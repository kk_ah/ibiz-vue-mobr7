<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
<#if ctrl.getItemPSLayoutPanel()??>
<#assign layoutpanel=ctrl.getItemPSLayoutPanel()>
    ${P.getCtrlCode(layoutpanel, 'CONTROL.less').code}
</#if>
.van-sidebar{
    width: 100%;
}
.van-sidebar-item--select::before{
    width: 4%;
    height: 100%;
}