## ibiz向导视图相关文档（移动端）

### 一、向导部件

##### 	(1)支持功能：操作向导(上一步、下一步)、提交向导表单

##### 	(2)界面UI

<div style = "display:flex; overflow:hidden; ">
	<img src="images/README/xd_1.png" ></img>
	<img src="images/README/xd_2.png" ></img>
</div>

<div style = "display:flex; overflow:hidden; ">
	<img src="images/README/xd_3.png" ></img>
</div>