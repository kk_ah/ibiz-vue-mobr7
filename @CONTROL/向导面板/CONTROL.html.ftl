<#assign content>
  :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}"
  <#if ctrl.getPSControlActions()??>
    <#list ctrl.getPSControlActions() as action>
      <#if action.getPSAppDEMethod?? && action.getPSAppDEMethod()??>
    ${action.name?lower_case}Action='${action.getPSAppDEMethod().getCodeName()}' 
      </#if>
    </#list>
  </#if>
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>