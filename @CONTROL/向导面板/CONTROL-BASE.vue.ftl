<template>
    <div class="app-wizard">
<#if ctrl.getPSDEWizard?? && ctrl.getPSDEWizard()?? && ctrl.getPSDEWizard().getPSDEWizardSteps?? && ctrl.getPSDEWizard().getPSDEWizardSteps()??>
    <van-steps class="wizard-steps"  :active="wizardForms.indexOf(activeForm)" finish-status="success">
    <#list ctrl.getPSDEWizard().getPSDEWizardSteps() as step>
      <van-step >${step.getTitle()}</van-step>
    </#list>
    </van-steps>
</#if>
        <template class="app-wizard-content">
<#if ctrl.getPSDEEditForms()??>
  <#list ctrl.getPSDEEditForms() as form>
    <#assign handler = form.getPSAjaxControlHandler() />
            <view_${form.getName()} 
              v-if="activeForm == '${form.getName()}'" 
              :key="'${form.name}'" 
              :viewState='wizardState'
              :context="context"
              :viewparams="viewparams"
              updateAction="<#if form.getUpdatePSControlAction()?? && form.getUpdatePSControlAction().getPSAppDEMethod()??>${form.getUpdatePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
              removeAction="<#if form.getRemovePSControlAction()?? && form.getRemovePSControlAction().getPSAppDEMethod()??>${form.getRemovePSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
              loadAction="<#if form.getGetPSControlAction()?? && form.getGetPSControlAction().getPSAppDEMethod()??>${form.getGetPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
      <#if form.getHookEventNames()??>
        <#list form.getHookEventNames() as eventName>
              @${eventName?lower_case}="${form.name}_${eventName?lower_case}" 
        </#list>
      </#if>
              name='${form.name}' 
              ref='${form.name}'>
            </view_${form.getName()}>
  </#list>
</#if>
        </template>
        <ion-footer class="app-wizard-footer">
            <ion-button :disabled="isDisabled('PREV')" @click="onClickPrev()" type="primary">上一步</ion-button>
            <ion-button :disabled="isDisabled('NEXT')" @click="onClickNext()" type="primary">下一步</ion-button>
            <ion-button :disabled="isDisabled('FINISH')" @click="onClickFinish()" type="primary">完成</ion-button>
        </ion-footer>
    </div>
</template>
<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>
    /**
     * 部件行为--init
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() public initAction!: string;
    
    /**
     * 部件行为--finish
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop() public finishAction!: string;

    /**
     * 显示处理提示
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    @Prop({ default: true }) public showBusyIndicator?: boolean;

    /**
      * 获取多项数据
      *
      * @returns {any[]}
      * @memberof ${srfclassname('${ctrl.codeName}')}
      */
    public getDatas(): any[] {
        return [this.formParam];
    }

    /**
      * 获取单项数据
      *
      * @returns {*}
      * @memberof ${srfclassname('${ctrl.codeName}')}
      */
    public getData(): any {
        return this.formParam;
    }

    /**
     * 视图状态订阅对象
     *
     * @public
     * @type {Subject<{action: string, data: any}>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public wizardState: Subject<ViewState> = new Subject();

    /**
     * 当前激活表单
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    <#if ctrl.getPSDEWizard()?? && ctrl.getPSDEWizard().getFirstPSDEWizardForm()??>
    <#assign firstForm = ctrl.getPSDEWizard().getFirstPSDEWizardForm()>
    public activeForm: string = '${ctrl.name}_form_${firstForm.getFormTag()}';
    <#else>
    public activeForm: string = '';
    </#if>

    /**
     * 向导表单参数
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public formParam: any = {};

    /**
     * 执行过的表单
     *
     * @public
     * @type {Array<string>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public historyForms: Array<string> = [];

    /**
     * 步骤行为集合
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public stepActions: any = {};

    /**
     * 向导表单集合
     *
     * @type {Array<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public wizardForms: Array<any> = [];

    /**
     * 当前状态
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public curState = '';

    /**
     * Vue声明周期(处理组件的输入属性)
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public created(): void {
        this.regFormActions();
        if(this.activeForm) {
            this.historyForms.push(this.activeForm);
        }
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (Object.is(tag, this.name)) {
                    if (Object.is('load', action)) {
                        this.doInit(data);
                    }
                }
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public destroyed() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

    /**
     * 注册表单步骤行为
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public regFormActions() {
      <#if ctrl.getPSDEWizard()?? && ctrl.getPSDEWizard().getPSDEWizardForms()??>
        <#list ctrl.getPSDEWizard().getPSDEWizardForms() as form>
        this.regFormAction('${ctrl.name}_form_${form.getFormTag()}', [<#if form.getStepActions()??><#list form.getStepActions() as action><#if action_index gt 0>,</#if>'${action}'</#list></#if>]);
        </#list>
      </#if>
    }

    /**
     * 注册表单步骤行为
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public regFormAction(name: string, actions: Array<string>) {
        this.stepActions[name] = actions;
        this.wizardForms.push(name);
    }

    /**
     * 初始化行为
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public doInit(opt: any = {}) {
        <#if ctrl.getPSDEWizard()?? && ctrl.getPSDEWizard().getInitPSDEAction()??>
        <#assign action = ctrl.getPSDEWizard().getInitPSDEAction()>
        const arg: any = { ...opt };
        Object.assign(arg,{viewparams:this.viewparams});
        const post: Promise<any> = this.service.init(this.initAction, JSON.parse(JSON.stringify(this.context)), arg, this.showBusyIndicator);
        post.then((response: any) => {
            if (response && response.status === 200) {
                this.formParam = response.data;
                if(response.data.${ctrl.getPSAppDataEntity().getCodeName()?lower_case}){
                    Object.assign(this.context,{${ctrl.getPSAppDataEntity().getCodeName()?lower_case}:response.data.${ctrl.getPSAppDataEntity().getCodeName()?lower_case}})
                }
                this.formLoad();
            }
        }).catch((response: any) => {
            if (response && response.status === 401) {
                return;
            }
            this.$notice.warning( response.info );
        });
        </#if>
    }

    /**
     * 表单加载
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public formLoad() {
        if(this.activeForm) {
            this.wizardState.next({ tag: this.activeForm, action: 'load', data: this.formParam });
        }
    }

    /**
     * 完成行为
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public doFinish() {
        <#if ctrl.getPSDEWizard()?? && ctrl.getPSDEWizard().getFinishPSDEAction()??>
        <#assign action = ctrl.getPSDEWizard().getFinishPSDEAction()>
        let arg: any = {};
        Object.assign(arg, this.formParam);
        Object.assign(arg,{viewparams:this.viewparams});
        const post: Promise<any> = this.service.finish(this.finishAction, JSON.parse(JSON.stringify(this.context)), arg, this.showBusyIndicator);
        post.then((response: any) => {
            if (response && response.status === 200) {
                const data = response.data;
                this.$emit("finish", data);
            }
        }).catch((response: any) => {
            if (response && response.status === 401) {
                return;
            }
            this.$notice.warning( response.info );
        });
        </#if>
    }

    /**
     * 向导表单加载完成
     *
     * @param {*} args
     * @param {string} name
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public ${ctrl.name}_formload(args: any, name: string, $event2?: any) {
        if(args) {
            Object.assign(this.formParam, args);
        }
    }

    /**
     * 向导表单保存完成
     *
     * @param {*} args
     * @param {string} name
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public ${ctrl.name}_formsave(args: any, name: string, $event2?: any) {
        Object.assign(this.formParam, args);
        if(Object.is(this.curState, 'NEXT')) {
            this.historyForms.push(name);
            if (this.getNextForm()) {
                this.activeForm = this.getNextForm();
                setTimeout(() => {
                    this.formLoad();
                }, 1);
            } else {
                this.doFinish();
            }
        }else if(Object.is(this.curState, 'FINISH')) {
            this.doFinish();
        }
    }

    /**
     * 获取下一步向导表单
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getNextForm() {
        let index = this.wizardForms.indexOf(this.activeForm);
        if(index >= 0) {
            if(this.wizardForms[index + 1]) {
                return this.wizardForms[index + 1];
            }
        }
        return undefined;
    }


    /**
     * 上一步
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public onClickPrev() {
        const length = this.historyForms.length;
        if(length > 1) {
            this.curState = 'PREV';
            this.activeForm = this.historyForms[length - 1];
            setTimeout(() => {
                this.formLoad();
            }, 1);
            this.historyForms.splice(length - 1, 1);
        }
    }

    /**
     * 下一步
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public onClickNext() {
        if(this.activeForm) {
            if(this.$refs && this.$refs[this.activeForm]){
                let form: any = this.$refs[this.activeForm];
                if(form.formValidateStatus()) {
                    this.curState = 'NEXT';
                    this.wizardState.next({ tag: this.activeForm, action: 'save', data: this.formParam });
                } else {
                    this.$notice.warning( '值规则校验异常' );
                }
            }
        }
    }

    /**
     * 完成
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public onClickFinish() {
        if(this.activeForm) {
            if(this.$refs && this.$refs[this.activeForm]){
                let form: any = this.$refs[this.activeForm];
                if(form.formValidateStatus()) {
                    this.curState = 'FINISH';
                    this.wizardState.next({ tag: this.activeForm, action: 'save', data: this.formParam });
                } else {
                     this.$notice.warning( '值规则校验异常' );
                }
            }
        }
    }

    /**
     * 是否禁用
     *
     * @param {string} type
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public isDisabled(type: string) {
        const actions: Array<string> = this.stepActions[this.activeForm]
        if(actions && actions.indexOf(type) < 0) {
            return true;
        }
        return false;
    }
    

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>