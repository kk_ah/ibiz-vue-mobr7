<#assign extendsClass>ChartServiceBase</#assign>
<#ibizinclude>
../@MACRO/SERVICE/SERVICE_HEADER.ts.ftl
</#ibizinclude>

<#--  暂只支持第一个序列  -->
<#list ctrl.getPSDEChartSerieses() as series>
  <#if series_index == 0>
    <#assign chartSeries = series/>
  </#if>
</#list>
    /**
     * 生成图表数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    public handleResponse(action: string, response: any): any {
        if (response.status !== 200) {
            console.log(response.error.message);
            return;
        }
        let chartOption:any = {};
<#--  获取x轴的分类属性字段  -->
<#if chartSeries.getCatalogField?? && chartSeries.getCatalogField()??>
  <#assign catalogField = chartSeries.getCatalogField()>
        let catalogFields: any = [<#rt>
  <#list catalogField?split(";") as field>
            "${field?lower_case}",<#t>
  </#list>
</#if>
        ];<#lt>
<#--  获取y轴值属性字段和中文名称  -->
<#if chartSeries.getValueField?? && chartSeries.getValueField()??>
  <#assign valueField = chartSeries.getValueField()>
        let valueFields: any = [<#rt>
    <#list valueField?split(";") as field>
            [ "${field?lower_case}", "${de.getPSDEField(field).getLogicName()}" ],<#t>
    </#list>
</#if>
        ];<#lt>
        let otherFields: any = ['planned_revenue'];
        // 数据按分类属性分组处理
        let xFields:any = [];
        let yFields:any = [];
        let oFields: any = [];
        valueFields.forEach((field: any,index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                let num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
                oFields[num] += item[otherFields[0]];
            }else{
                xFields.push(item[catalogFields[0]]);
                oFields.push(item[otherFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
        let series: any = [];
        valueFields.forEach((field: any,index: number) => {
            let yData: any = [];
            xFields.forEach((item:any, num: number) => {
                yData.push({value: (100 - (100 / xFields.length) * num), name: item, sum: yFields[index][num], total: oFields[num]});
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
                name:field[1],
                type:"funnel",
                data:yData,
                minSize: '60%',
                left: 10,
                right: 10,
                label: {
                    position: 'center',
                    formatter: (params: any) => {
                        return `<#noparse>${params.data.name}（${params.data.sum}个, 金额￥${params.data.total}）</#noparse>`
                    }
                },
            });
        });
        chartOption.series = series;
        return new HttpResponse(response.status, chartOption);
    }

<#ibizinclude>
../@MACRO/SERVICE/SERVICE_BOTTOM.ts.ftl
</#ibizinclude>
