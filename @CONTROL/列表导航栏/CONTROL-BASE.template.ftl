<template>
    <div class="app-mob-listexpbar <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#if ctrl.render??>
        ${ctrl.render.code}
    <#else>
        <div class="listexpbar-container">
            <div class="listexpbar_list">
            <#if ctrl.getPSDEList()??>
            <#assign list = ctrl.getPSDEList()/>
            ${P.getCtrlCode(list, 'CONTROL.html').code}
            </#if>
            </div>
            
             <component 
              v-if="selection.view && !Object.is(this.selection.view.viewname, '')" 
              :is="selection.view.viewname"
              class="viewcontainer2"
              :viewDefaultUsage="false"
              :_context="JSON.stringify(selection.context)"
              :_viewparam="JSON.stringify(selection.viewparam)"

              >
            </component>
        </div>
    </#if>
    </div>
</template>