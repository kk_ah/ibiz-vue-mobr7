<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getData(): any {
        return null;
    }

    /**
     * 搜素值
     * 
     * @public
     * @type {(string)}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public searchText:string = "";

<#if ctrl.getXDataPSControl()??>
<#assign xDataControl = ctrl.getXDataPSControl() />

    /**
     * 导航视图名称
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navViewName: string = "<#if xDataControl.getNavPSAppView()??><#assign navPSAppView = xDataControl.getNavPSAppView() />${srffilepath2(navPSAppView.getCodeName())}</#if>";

    /**
     * 导航视图参数
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navViewParam: string = '<#if xDataControl.getNavViewParamJO()??>${xDataControl.getNavViewParamJO()}</#if>';

    /**
     * 导航过滤项
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navFilter: string = "<#if xDataControl.getNavFilter()??>${xDataControl.getNavFilter()}</#if>";

    /**
     * 导航关系
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navPSDer: string = "<#if xDataControl.getNavPSDER()??>n_${xDataControl.getNavPSDER().getPSPickupDEField().getCodeName()?lower_case}_eq</#if>";
    
    /**
     * 导航上下文参数
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navigateContext:any = <#if xDataControl.getPSNavigateContexts?? && xDataControl.getPSNavigateContexts()??><@getNavigateContext xDataControl /><#else>null</#if>;

    /**
     * 导航视图参数
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navigateParams:any = <#if xDataControl.getPSNavigateParams?? && xDataControl.getPSNavigateParams()??><@getNavigateParams xDataControl /><#else>null</#if>;     
</#if>

    /**
     * 选中数据
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public selection: any = {};

    /**
     * 显示处理提示
     *
     * @type {boolean}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    @Prop({ default: true }) public showBusyIndicator!: boolean;

    <#if ctrl.getPSControls()??>
    <#assign controls = ctrl.getPSControls()/>
    <#list controls as singleControl>
    <#if singleControl.getControlType() == "TOOLBAR">

    ${P.getCtrlCode(singleControl, 'CONTROL.vue').code}
    </#if>
    </#list>
    </#if>

    /**
    * Vue声明周期(组件初始化完毕)
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}Base
    */
    public created() {
         this.afterCreated();     
    }

    /**
    * 执行created后的逻辑
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}Base
    */
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                <#if ctrl.getXDataPSControl?? && ctrl.getXDataPSControl()??>
                <#assign xDataControl = ctrl.getXDataPSControl() />
                this.viewState.next({ tag: '${xDataControl.getName()}', action: action, data: data });
                </#if>
            });
        }
    }

    /**
    * 执行mounted后的逻辑
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}Base
    */
    public afterMounted(){ 
        <#--  if(this.$store.getters.getViewSplit(this.viewUID)){
            this.split = this.$store.getters.getViewSplit(this.viewUID);
        }else{
            let containerWidth:number = (document.getElementById("${ctrl.getCodeName()?lower_case}") as any).offsetWidth;
            let containerHeight:number = (document.getElementById("${ctrl.getCodeName()?lower_case}") as any).offsetHeight;
            if(Object.is(this.showMode,'horizontal')){
                if(this.ctrlWidth){
                    this.split = this.ctrlWidth/containerWidth;
                }
            }else{
                if(this.ctrlHeight){
                    this.split = this.ctrlHeight/containerHeight;
                }
            }
            this.$store.commit("setViewSplit",{viewUID:this.viewUID,viewSplit:this.split}); 
        }    -->
    }

    /**
    * Vue声明周期(组件渲染完毕)
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}Base
    */
    public mounted() {
        this.afterMounted();     
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }
    
    /**
     * ${ctrl.name}的选中数据事件
     * 
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public ${ctrl.name}_selectionchange(arg: any, tag?: string, $event2?: any): void {
        let tempContext:any = {};
        let tempViewParam:any = {};
        if (!arg) {
            return ;
        }
        if(this.context){
            Object.assign(tempContext,JSON.parse(JSON.stringify(this.context)));
        }
        <#if ctrl.getXDataPSControl()??>
        <#assign xDataControl = ctrl.getXDataPSControl()/>
        <#if xDataControl.getPSAppDataEntity()??>
        <#assign curDataEntity = xDataControl.getPSAppDataEntity()/>
        Object.assign(tempContext,{'${curDataEntity.getCodeName()?lower_case}':arg['${curDataEntity.getCodeName()?lower_case}']});
        Object.assign(tempContext,{srfparentdename:'${curDataEntity.getCodeName()}',srfparentkey:arg['${curDataEntity.getCodeName()?lower_case}']});
        if(this.navFilter && !Object.is(this.navFilter,"")){
            Object.assign(tempViewParam,{[this.navFilter]:arg['${curDataEntity.getCodeName()?lower_case}']});
        }
        if(this.navPSDer && !Object.is(this.navPSDer,"")){
            Object.assign(tempViewParam,{[this.navPSDer]:arg['${curDataEntity.getCodeName()?lower_case}']});
        }
        </#if>
        </#if>
        const {context, param} = this.$viewTool.formatNavigateParam( this.navigateContext, this.navigateParams, tempContext, tempViewParam, arg );
        Object.assign(tempContext,context);
        Object.assign(tempViewParam,param);
        this.selection = {};
        Object.assign(this.selection, { view: { viewname: this.navViewName },context:tempContext,viewparam:tempViewParam});
        this.$forceUpdate();
    }

    /**
     * ${ctrl.name}的load完成事件
     * 
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public ${ctrl.name}_load(args:any, tag?: string, $event2?: any){
        if(!this.selection.view){
            this.listexpbar_list_selectionchange(args[0])
        }
        this.$emit('load',args);
    }

    /**
    * 执行搜索
    *
    * @memberof ${srfclassname('${ctrl.codeName}')}Base
    */
    public onSearch($event:any) {
<#if ctrl.getPSDEList()??>
  <#assign list = ctrl.getPSDEList()/>
        this.viewState.next({ tag: '${list.name}', action: "load", data: {query : this.searchText}});
</#if>
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>