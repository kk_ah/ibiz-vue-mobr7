<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
.listexpbar-container{
    display: flex;
    height: calc(100vh - 96px);
    flex-direction: row;
    justify-content: space-between;
    .listexpbar_list{
        width: 20%;
        height: 100%;
        overflow:scroll;
        overflow-x: hidden;
    }
    .viewcontainer2{
        width: 80%;
        height: 100%;
        margin-left: auto;
        overflow:scroll;
        overflow-x: hidden;
    }
}