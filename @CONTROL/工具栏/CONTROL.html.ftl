<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <#if ctrl.getToolbarStyle() == 'MOBWFACTIONMENU'>
<div class="app-toolbar-container <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#--  流程操作菜单：BEGIN  -->
    <div class="app-view-footer">
        <div class="app-quick-toolbar">
        <#list ctrl.getPSDEToolbarItems() as item>
            ${P.getPartCode(item).code}
        </#list>
        </div>
    </div>
</div>
    <#--  流程操作菜单：END  -->
    <#elseif ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
    <#--  底部菜单：BEGIN  -->
<div <#if view.hasPSControl('mdctrl') ||  view.hasPSControl('dataview') ||  view.hasPSControl('calendar')>v-show="!showCheack"</#if> class = "bottom_menu">
            <#list ctrl.getPSDEToolbarItems() as item>
                ${P.getPartCode(item).code}
                <#if custom_toolbar??>
                    ${custom_toolbar}
                </#if>
            </#list>
</div>
    <#--  底部菜单：END  -->
    <#elseif ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU'>
<div class="app-toolbar-container <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#--  导航左侧菜单：BEGIN  -->
    <div class="app-quick-toolbar">
    <#list ctrl.getPSDEToolbarItems() as item>
        ${P.getPartCode(item).code}
        <#if custom_toolbar??>
            ${custom_toolbar}
        </#if>
    </#list>
    </div>
</div>
    <#--  导航左侧菜单：END  -->
    <#elseif ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU'>
<div class="app-toolbar-container <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <#--  导航右侧菜单：BEGIN  -->
    <div class="app-quick-toolbar toolbar-right-bottons">
    <#list ctrl.getPSDEToolbarItems() as item>
        ${P.getPartCode(item).code}
        <#if custom_toolbar??>
            ${custom_toolbar}
        </#if>
    </#list>
    </div>
</div>
    <#--  导航右侧菜单：END  -->
    <#else>
<div class="app-toolbar-container <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
    <div class="app-quick-toolbar toolbar-left-bottons">
    <#list ctrl.getPSDEToolbarItems() as item>
        ${P.getPartCode(item).code}
        <#if custom_toolbar??>
            ${custom_toolbar}
        </#if>
    </#list>
    </div>
</div>
    </#if>
</#if>