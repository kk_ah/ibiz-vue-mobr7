## ibiz工具栏样式相关文档（移动端）

### 一、工具栏样式

#### 	1、移动端导航栏左侧菜单

##### 	(1)支持功能：icon、分组、显示模式

##### 	(2)界面UI

​	![lefttoolbar](./imgs/lefttoolbar.png)![lefttollbar_group](./imgs/lefttollbar_group.png)

​	

#### 	2、移动端导航栏右侧菜单

##### 	(1)支持功能：icon、分组、显示模式

##### 	(2)界面UI

​	![righttoolbar](./imgs/righttoolbar.png)![toptoolbar_group](./imgs/toptoolbar_group.png)

​	

#### 	3、移动端视图下方菜单

##### 	(1)支持功能：icon

##### 	(2)界面UI

​	![bottomtoolbar](./imgs/bottomtoolbar.png)