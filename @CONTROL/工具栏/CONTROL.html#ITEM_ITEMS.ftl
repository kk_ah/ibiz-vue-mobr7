<#ibizinclude>
../@MACRO/CONTROL/LANGBASE.vue.ftl
</#ibizinclude>
<#if ctrl.getToolbarStyle()?? >
<ion-button class="app-view-toolbar-button" @click="${ctrl.name}Models.${item.name}.isshow=!${ctrl.name}Models.${item.name}.isshow">
<#if item.isShowIcon()>
    <#if item.getPSSysImage()??>
    <ion-icon name="<#if item.getPSSysImage().getCssClass() != "">${item.getPSSysImage().getCssClass()}<#else>fa fa-ellipsis-h</#if>"  <#-- <#if item.getPSSysImage().getImagePath() != ""><img src="${item.getPSSysImage().getImagePath()}" /></#if> -->></ion-icon>
    </#if>
</#if>
<#if item.isShowCaption()>{{<#if langbase??>$t('${langbase}.${item.name}.caption')<#else>'${item.getCaption()}'</#if>}}</#if>
</ion-button>


<#if item.getPSDEToolbarItems()??>
<#--  右toolbar   begin -->
<#if ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU' || ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU' >
 <div class = "group-button" v-show="${ctrl.name}Models.${item.name}.isshow" @click="${ctrl.name}Models.${item.name}.isshow = false" z-index='9999'>

    <ion-list class="toolbar_group_<#if ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU'>right<#else>left</#if>">
<#list item.getPSDEToolbarItems() as toolbarItem>
        <ion-item @click="${ctrl.name}_click({ tag: '${toolbarItem.name}' }, $event)"><ion-button class="app-view-toolbar-button"><#if toolbarItem.isShowIcon()><#if toolbarItem.getPSSysImage()??>  <ion-icon name="<#if toolbarItem.getPSSysImage().getCssClass() != "">${toolbarItem.getPSSysImage().getCssClass()}<#else>reorder-four-outline</#if>"></ion-icon><#else><ion-icon name="reorder-four-outline"></ion-icon></#if></#if> {{<#if toolbarItem.isShowCaption()><#if langbase??>$t('${langbase}.${toolbarItem.name}.caption')<#else>'${toolbarItem.getCaption()}'</#if></#if>}}</ion-button> </ion-item>
</#list>
    </ion-list>
 </div>
</#if>

<#if ctrl.getToolbarStyle() == 'MOBBOTTOMMENU' >
<#list item.getPSDEToolbarItems() as toolbarItem>

</#list>
</#if>
</#if>
</#if>