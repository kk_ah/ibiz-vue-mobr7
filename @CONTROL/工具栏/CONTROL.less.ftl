<#ibizinclude>
../@MACRO/CSS/DEFAULT.less.ftl
</#ibizinclude>
<#if ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
.app-${ctrl.name}-bar-button {
    position: absolute;
    bottom: @toolbar-bottomtoolbar-offset-y;
    right: @toolbar-bottomtoolbar-offset-x;
    border-radius: @border-radius-round;
    height: @toolbar-bottomtoolbar-height;
    line-height: @toolbar-bottomtoolbar-height;
}
.bottom_menu{
    text-align: center;
}
.app-${ctrl.name}-bar-overlay {
    .wrapper {
        display: flex;
        align-items: flex-end;
        justify-content: flex-end;
        height: 100%;
        .block {
            width: 120px;
            height: 100%;
            display: flex;
            z-index: 2000;
            .app-toolbar-container {
                position: absolute;
                bottom: @toolbar-bottomtoolbar-group-offset-y;
                right: @toolbar-bottomtoolbar-group-offset-x;
                width: auto;
                height: auto;
                .righttoolbar-item {
                color: @toolbar-bottomtoolbar-item-color;
                margin-bottom: 10px;
                font-size: @toolbar-bottomtoolbar-item-fontSize;
                display: flex;
                .righttoolbar-item-btn {
                    margin-left: 5px;
                    border-radius: @border-radius-round;
                }
                }
            }
        }
    }
}
</#if>

.toolbar_group_right{
  position: absolute;
  right: 20px;
  z-index: 999;
  width: 40%;
}
.toolbar_group_left{
  position: absolute;
  left: 20px;
  z-index: 999;
  width: 40%;
}
ion-backdrop {
  opacity: 0.3;
}
.ionlist{
  border-radius: @toolbar-group-border-radius;
}
.group_ion-icon{
  margin-right: 10px;
}
.footer_group {
  position: absolute;
  z-index: 99;
  bottom: 67px;
  width: 40%;
  right: 20px;
}
.app-quick-toolbar{
  text-align: center;
}
<#assign havestyle = 0> 
<#if ctrl.getToolbarStyle() == 'MOBNAVLEFTMENU'|| ctrl.getToolbarStyle() == 'MOBNAVRIGHTMENU' && havestyle = 0>
<#assign havestyle = 1>

.group-button{
  display: flex;
  flex-flow: column;
  position: absolute;
}

.app-view-righttoolbar,
.app-view-lefttoolbar {
  .toolbar_group_right {
    border-radius: @toolbar-group-border-radius;
    position: absolute;
    right: @toolbar-toptoolbar-offset-x;
    top: @toolbar-toptoolbar-offset-y;
  }
  .toolbar_group_left {
    border-radius: @toolbar-group-border-radius;
    position: absolute;
    left:@toolbar-toptoolbar-offset-x;
    top: @toolbar-toptoolbar-offset-y;
  }
  .van-cell__title,
  .van-cell__value {
    text-align: left;
  }
  .van-cell {
    border-radius: @toolbar-group-border-radius;
  }
  .van-hairline--top-bottom::after, .van-hairline-unset--top-bottom::after {
    border-width: 0;
  }
  .van-button--default {
    background-color: transparent;
    border: none;
    padding: 0;
    color: #fff;
  }
}
</#if>
