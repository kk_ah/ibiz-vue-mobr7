${item.getName()?lower_case}: { isshow:false, name: '${item.getName()?lower_case}', caption: '${item.caption}', disabled: false, type: '${item.getItemType()}', visabled: true, dataaccaction: '', uiaction: { } }, 
<#if item.getPSDEToolbarItems()??>
<#list item.getPSDEToolbarItems() as toolbarItem>
 <@ibizindent blank=8>
  ${P.getPartCode(toolbarItem).code}
 </@ibizindent>
</#list>
</#if>