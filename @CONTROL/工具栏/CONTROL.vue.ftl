<#--  工具栏模型  -->

   /**
    * 工具栏 ${srfclassname('${view.name}')} 模型
    *
    * @type {*}
    * @memberof ${srfclassname('${view.name}')}
    */
    public ${ctrl.name?lower_case}Models: any = {
    <#if ctrl.getPSDEToolbarItems()??>
        <#list ctrl.getPSDEToolbarItems() as item>
        ${P.getPartCode(item).code}
        </#list>
    </#if>
    };
<#if ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>

    /**
     * 工具栏显示状态
     *
     * @type {boolean}
     * @memberof ${srfclassname('${view.name}')} 
     */
    public ${ctrl.name}ShowState: boolean = false;
</#if>
