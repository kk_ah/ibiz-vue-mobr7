<#ibizinclude>
../@MACRO/CONTROL/LANGBASE.vue.ftl
</#ibizinclude>
<#assign deuiaction=item.getPSUIAction()>
<#if ctrl.getToolbarStyle() == 'MOBBOTTOMMENU'>
    <ion-button @click="${ctrl.name}_click({ tag: '${item.name}' }, $event)" v-show="${(ctrl.getName())?lower_case}Models.${item.name}.visabled">
        <#if item.isShowIcon() && item.getPSSysImage()??>
        <ion-icon name="<#if item.getPSSysImage().getCssClass() != "">${item.getPSSysImage().getCssClass()}<#else>fa fa-ellipsis-h</#if>"></ion-icon>
        </#if>
        <#if item.isShowCaption()>{{<#if langbase??>$t('${langbase}.${item.name}.caption')<#else>'${item.getCaption()}'</#if>}}</#if>
    </ion-button>
<#elseif ctrl.getToolbarStyle() == 'MOBWFACTIONMENU'>
    <ion-button class="app-quick-toolbar-item" v-show="${(ctrl.getName()?lower_case)}Models.${item.name}.visabled" :disabled="${(ctrl.getName())?lower_case}Models.${item.name}.disabled" @click="${ctrl.name}_click({ tag: '${item.name}' }, $event)"  >
    <#if item.isShowIcon()>
        <#if item.getPSSysImage()??>
        <ion-icon  name="<#if item.getPSSysImage().getCssClass() != "">${item.getPSSysImage().getCssClass()}<#else>fa fa-ellipsis-h</#if>"> <#-- <#if item.getPSSysImage().getImagePath() != ""> <img src="${item.getPSSysImage().getImagePath()}" /></#if>--></ion-icon>
        </#if>
    </#if>
    <#if item.isShowCaption()>{{<#if langbase??>$t('${langbase}.${item.name}.caption')<#else>'${item.getCaption()}'</#if>}}</#if>
    </ion-button>
<#else>
    <ion-button class="app-view-toolbar-button" v-show="${(ctrl.getName())?lower_case}Models.${item.name}.visabled" :disabled="${(ctrl.getName())?lower_case}Models.${item.name}.disabled" @click="${ctrl.name}_click({ tag: '${item.name}' }, $event)" >
    <#if item.isShowIcon()>
        <#if item.getPSSysImage()??>
        <ion-icon class="ibiz-button-icon" name="<#if item.getPSSysImage().getCssClass() != "">${item.getPSSysImage().getCssClass()}<#else>fa fa-ellipsis-h</#if>"> <#--<#if item.getPSSysImage().getImagePath() != ""> <img class="ibiz-button-icon" src="${item.getPSSysImage().getImagePath()}" /></#if>--></ion-icon>
        </#if>
    </#if>
    <#if item.isShowCaption()>{{<#if langbase??>$t('${langbase}.${item.name}.caption')<#else>'${item.getCaption()}'</#if>}}</#if>
    </ion-button>
</#if>