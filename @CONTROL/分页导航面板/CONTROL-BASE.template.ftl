<template>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <span>
    <#list ctrl.getPSControls() as tabviewpanel>
        <span v-show="activiedTabViewPanel == '${tabviewpanel.name}'">
        <@ibizindent blank=12>
            ${P.getCtrlCode(tabviewpanel, 'CONTROL.html').code}
        </@ibizindent>
        </span>
    </#list>
    </span>
</#if>
</template>