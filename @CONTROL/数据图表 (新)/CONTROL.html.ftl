<#assign content>
    fetchAction="<#if ctrl.getFetchPSControlAction()?? && ctrl.getFetchPSControlAction().getPSAppDEMethod()??>${ctrl.getFetchPSControlAction().getPSAppDEMethod().getCodeName()}</#if>"
    :showBusyIndicator="${ctrl.isShowBusyIndicator()?c}" 
</#assign>
<#ibizinclude>
../@MACRO/HTML/DEFAULT.html.ftl
</#ibizinclude>