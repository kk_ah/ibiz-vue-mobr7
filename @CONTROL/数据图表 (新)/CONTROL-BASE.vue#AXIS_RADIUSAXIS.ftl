<#-- 绘图极坐标系的径向轴 -->
<#if item.render??>
${item.render.code}
<#else>
{
      min: 0,
      max: 10,
      interval: 2
}
</#if>