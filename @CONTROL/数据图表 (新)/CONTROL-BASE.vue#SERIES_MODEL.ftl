<#-- 序列模型start -->
<#if item.getEChartsType()??>
<#if item.getEChartsType() == "line">
new ChartLineSeries({
    name:'${item.name?lower_case}',
    <#if item.getIdField()??>seriesIdField:"${item.getIdField()?lower_case}",</#if>
    <#if item.getSeriesField?? && item.getSeriesField()??>seriesNameField:"${item.getSeriesField()?lower_case}",</#if>
    categorField:'<#if item.getCatalogField?? && item.getCatalogField()??>${item.getCatalogField()?lower_case}</#if>',
    <#if item.getCatalogPSCodeList()??><#assign categorCodeList = item.getCatalogPSCodeList() />categorCodeList:{type:'${categorCodeList.getCodeListType()}',tag:'${categorCodeList.getCodeName()}',emptycode:'empty',emptytext:'${categorCodeList.getEmptyText()}'},</#if>
    valueField:'${item.getValueField()?lower_case}',
    seriesValues:[],
    seriesIndex:${item.getIndex()},
    data:[],
    seriesMap:{},
    <#if item.getPSChartDataSet()??>
    <#assign chartDataSet = item.getPSChartDataSet()/>
    <#if chartDataSet.getPSChartDataSetFields()??>
    dataSetFields:[
    <#list chartDataSet.getPSChartDataSetFields() as singleField>
    {name:"${singleField.getName()?lower_case}",codelist:<#if singleField.getPSCodeList()??><#assign codelist = singleField.getPSCodeList()/>{type:"${codelist.getCodeListType()}",tag:"${codelist.getCodeName()}",emptycode:'empty',emptytext:'${categorCodeList.getEmptyText()}'}<#else>null</#if>,isGroupField:${singleField.isGroupField()?c},groupMode:"${singleField.getGroupMode()}"}<#if singleField_has_next>,</#if>
    </#list>
    ],
    </#if>
    </#if>
    ecxObject:{
    <#-- 序列label start -->
        <#if item.containsUserParam("ECX.label")>
        label:${item.getUserParam("ECX.label")},
        <#else>
        label:{
            show: true,
            position: 'inside'
        },
        </#if>
        <#-- 序列label end -->
        <#-- 序列labelLine start -->
        <#if item.containsUserParam("ECX.labelLine")>
        labelLine:${item.getUserParam("ECX.labelLine")},
        <#else>
        labelLine:{
            length: 10,
            lineStyle: {
                width: 1,
                type: 'solid'
            }
        },
        </#if>
        <#-- 序列labelLine end -->
        <#-- 序列itemStyle start -->
        <#if item.containsUserParam("ECX.itemStyle")>
        itemStyle:${item.getUserParam("ECX.itemStyle")},
        <#else>
        itemStyle:{
            borderColor: '#fff',
            borderWidth: 1
        },
        </#if>
        <#-- 序列itemStyle end -->
        <#-- 序列emphasis start -->
        <#if item.containsUserParam("ECX.emphasis")>
        emphasis:${item.getUserParam("ECX.emphasis")},
        <#else>
        emphasis:{
            label: {
                fontSize: 12
            }
        }
        </#if>
        <#-- 序列emphasis end -->
    },
    ecObject:{
        <#if item.getUserParamNames()??>
        <#list item.getUserParamNames() as userparam>
        <#if userparam?index_of("EC.")==0>
        ${userparam?remove_beginning("EC.")}:${item.getUserParam(userparam)},
        </#if>
        </#list>
        </#if>
    },
    seriesTemp:{
        type:'line',
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    seriesLayoutBy:"${item.getSeriesLayoutBy()}",
    baseOption:{
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    }
    })  
<#elseif item.getEChartsType() == "funnel">
new ChartFunnelSeries({
    name:'${item.name?lower_case}',
    <#if item.getIdField()??>seriesIdField:"${item.getIdField()?lower_case}",</#if>
    <#if item.getSeriesField?? && item.getSeriesField()??>seriesNameField:"${item.getSeriesField()?lower_case}",</#if>
    categorField:'<#if item.getCatalogField?? && item.getCatalogField()??>${item.getCatalogField()?lower_case}</#if>',
    <#if item.getCatalogPSCodeList()??><#assign categorCodeList = item.getCatalogPSCodeList() />categorCodeList:{type:'${categorCodeList.getCodeListType()}',tag:'${categorCodeList.getCodeName()}',emptycode:'empty',emptytext:'${categorCodeList.getEmptyText()}'},</#if>
    valueField:'${item.getValueField()?lower_case}',
    seriesValues:[],
    seriesIndex:${item.getIndex()},
    data:[],
    seriesMap:{},
    <#if item.getPSChartDataSet()??>
    <#assign chartDataSet = item.getPSChartDataSet()/>
    <#if chartDataSet.getPSChartDataSetFields()??>
    dataSetFields:[
    <#list chartDataSet.getPSChartDataSetFields() as singleField>
    {name:"${singleField.getName()?lower_case}",codelist:<#if singleField.getPSCodeList()??><#assign codelist = singleField.getPSCodeList()/>{type:"${codelist.getCodeListType()}",tag:"${codelist.getCodeName()}",emptycode:'empty',emptytext:'${codelist.getEmptyText()}'}<#else>null</#if>,isGroupField:${singleField.isGroupField()?c},groupMode:"${singleField.getGroupMode()}"}<#if singleField_has_next>,</#if>
    </#list>
    ],
    </#if>
    </#if>
    ecxObject:{
    <#-- 序列label start -->
        <#if item.containsUserParam("ECX.label")>
        label:${item.getUserParam("ECX.label")},
        <#else>
        label:{
            show: true,
            position: 'inside'
        },
        </#if>
        <#-- 序列label end -->
        <#-- 序列labelLine start -->
        <#if item.containsUserParam("ECX.labelLine")>
        labelLine:${item.getUserParam("ECX.labelLine")},
        <#else>
        labelLine:{
            length: 10,
            lineStyle: {
                width: 1,
                type: 'solid'
            }
        },
        </#if>
        <#-- 序列labelLine end -->
        <#-- 序列itemStyle start -->
        <#if item.containsUserParam("ECX.itemStyle")>
        itemStyle:${item.getUserParam("ECX.itemStyle")},
        <#else>
        itemStyle:{
            borderColor: '#fff',
            borderWidth: 1
        },
        </#if>
        <#-- 序列itemStyle end -->
        <#-- 序列emphasis start -->
        <#if item.containsUserParam("ECX.emphasis")>
        emphasis:${item.getUserParam("ECX.emphasis")},
        <#else>
        emphasis:{
            label: {
                fontSize: 12
            }
        }
        </#if>
        <#-- 序列emphasis end -->
    },
    ecObject:{
        <#if item.getUserParamNames()??>
        <#list item.getUserParamNames() as userparam>
        <#if userparam?index_of("EC.")==0>
        ${userparam?remove_beginning("EC.")}:${item.getUserParam(userparam)},
        </#if>
        </#list>
        </#if>
    },
    seriesTemp:{
        type:'funnel',
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    baseOption:{
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    seriesLayoutBy:"${item.getSeriesLayoutBy()}"
    })
<#elseif item.getEChartsType() == "pie">
new ChartPieSeries({
    name:'${item.name?lower_case}',
    <#if item.getIdField()??>seriesIdField:"${item.getIdField()?lower_case}",</#if>
    <#if item.getSeriesField?? && item.getSeriesField()??>seriesNameField:"${item.getSeriesField()?lower_case}",</#if>
    categorField:'<#if item.getCatalogField?? && item.getCatalogField()??>${item.getCatalogField()?lower_case}</#if>',
    <#if item.getCatalogPSCodeList()??><#assign categorCodeList = item.getCatalogPSCodeList() />categorCodeList:{type:'${categorCodeList.getCodeListType()}',tag:'${categorCodeList.getCodeName()}',emptycode:'empty',emptytext:'${categorCodeList.getEmptyText()}'},</#if>
    valueField:'${item.getValueField()?lower_case}',
    seriesValues:[],
    seriesIndex:${item.getIndex()},
    data:[],
    seriesMap:{},
    <#if item.getPSChartDataSet()??>
    <#assign chartDataSet = item.getPSChartDataSet()/>
    <#if chartDataSet.getPSChartDataSetFields()??>
    dataSetFields:[
    <#list chartDataSet.getPSChartDataSetFields() as singleField>
    {name:"${singleField.getName()?lower_case}",codelist:<#if singleField.getPSCodeList()??><#assign codelist = singleField.getPSCodeList()/>{type:"${codelist.getCodeListType()}",tag:"${codelist.getCodeName()}",emptycode:'empty',emptytext:'${codelist.getEmptyText()}'}<#else>null</#if>,isGroupField:${singleField.isGroupField()?c},groupMode:"${singleField.getGroupMode()}"}<#if singleField_has_next>,</#if>
    </#list>
    ],
    </#if>
    </#if>
    ecxObject:{
    <#-- 序列label start -->
        <#if item.containsUserParam("ECX.label")>
        label:${item.getUserParam("ECX.label")},
        <#else>
        label:{
            show: true,
            position: 'outside',
        },
        </#if>
        <#-- 序列label end -->
        <#-- 序列labelLine start -->
        <#if item.containsUserParam("ECX.labelLine")>
        labelLine:${item.getUserParam("ECX.labelLine")},
        <#else>
        labelLine:{
            show: true,
            length: 10,
            lineStyle: {
                width: 1,
                type: 'solid'
            }
        },
        </#if>
        <#-- 序列labelLine end -->
        <#-- 序列itemStyle start -->
        <#if item.containsUserParam("ECX.itemStyle")>
        itemStyle:${item.getUserParam("ECX.itemStyle")},
        <#else>
        itemStyle:{
            borderColor: '#fff',
            borderWidth: 1
        },
        </#if>
        <#-- 序列itemStyle end -->
        <#-- 序列emphasis start -->
        <#if item.containsUserParam("ECX.emphasis")>
        emphasis:${item.getUserParam("ECX.emphasis")},
        <#else>
        emphasis:{
            label: {
                fontSize: 12
            }
        }
        </#if>
        <#-- 序列emphasis end -->
    },
    ecObject:{
        <#if item.getUserParamNames()??>
        <#list item.getUserParamNames() as userparam>
        <#if userparam?index_of("EC.")==0>
        ${userparam?remove_beginning("EC.")}:${item.getUserParam(userparam)},
        </#if>
        </#list>
        </#if>
    },
    seriesTemp:{
        type:'pie',
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    baseOption:{
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    seriesLayoutBy:"${item.getSeriesLayoutBy()}"
    })
<#elseif item.getEChartsType() == "bar">
new ChartBarSeries({
    name:'${item.name?lower_case}',
    <#if item.getIdField()??>seriesIdField:"${item.getIdField()?lower_case}",</#if>
    <#if item.getSeriesField?? && item.getSeriesField()??>seriesNameField:"${item.getSeriesField()?lower_case}",</#if>
    categorField:'<#if item.getCatalogField?? && item.getCatalogField()??>${item.getCatalogField()?lower_case}</#if>',
    <#if item.getCatalogPSCodeList()??><#assign categorCodeList = item.getCatalogPSCodeList() />categorCodeList:{type:'${categorCodeList.getCodeListType()}',tag:'${categorCodeList.getCodeName()}',emptycode:'empty',emptytext:'${categorCodeList.getEmptyText()}'},</#if>
    valueField:'${item.getValueField()?lower_case}',
    seriesValues:[],
    seriesIndex:${item.getIndex()},
    data:[],
    seriesMap:{},
    <#if item.getPSChartDataSet()??>
    <#assign chartDataSet = item.getPSChartDataSet()/>
    <#if chartDataSet.getPSChartDataSetFields()??>
    dataSetFields:[
    <#list chartDataSet.getPSChartDataSetFields() as singleField>
    {name:"${singleField.getName()?lower_case}",codelist:<#if singleField.getPSCodeList()??><#assign codelist = singleField.getPSCodeList()/>{type:"${codelist.getCodeListType()}",tag:"${codelist.getCodeName()}",emptycode:'empty',emptytext:'${categorCodeList.getEmptyText()}'}<#else>null</#if>,isGroupField:${singleField.isGroupField()?c},groupMode:"${singleField.getGroupMode()}"}<#if singleField_has_next>,</#if>
    </#list>
    ],
    </#if>
    </#if>
    ecxObject:{
    <#-- 序列label start -->
        <#if item.containsUserParam("ECX.label")>
        label:${item.getUserParam("ECX.label")},
        <#else>
        label:{
            show: true,
            position: 'inside'
        },
        </#if>
        <#-- 序列label end -->
        <#-- 序列labelLine start -->
        <#if item.containsUserParam("ECX.labelLine")>
        labelLine:${item.getUserParam("ECX.labelLine")},
        <#else>
        labelLine:{
            length: 10,
            lineStyle: {
                width: 1,
                type: 'solid'
            }
        },
        </#if>
        <#-- 序列labelLine end -->
        <#-- 序列itemStyle start -->
        <#if item.containsUserParam("ECX.itemStyle")>
        itemStyle:${item.getUserParam("ECX.itemStyle")},
        <#else>
        itemStyle:{
            borderColor: '#fff',
            borderWidth: 1
        },
        </#if>
        <#-- 序列itemStyle end -->
        <#-- 序列emphasis start -->
        <#if item.containsUserParam("ECX.emphasis")>
        emphasis:${item.getUserParam("ECX.emphasis")},
        <#else>
        emphasis:{
            label: {
                fontSize: 13
            }
        }
        </#if>
        <#-- 序列emphasis end -->
    },
    ecObject:{
        <#if item.getUserParamNames()??>
        <#list item.getUserParamNames() as userparam>
        <#if userparam?index_of("EC.")==0>
        ${userparam?remove_beginning("EC.")}:${item.getUserParam(userparam)},
        </#if>
        </#list>
        </#if>
    },
    seriesTemp:{
        type:'bar',
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    baseOption:{
        <#if item.getBaseOptionJOString()??>
        ${item.getBaseOptionJOString()}
        </#if>
    },
    seriesLayoutBy:"${item.getSeriesLayoutBy()}"
    })
<#else>
new ChartLineSeries({name:'${item.name?lower_case}',categorField:'<#if item.getCatalogField?? && item.getCatalogField()??>${item.getCatalogField()?lower_case}</#if>',<#if item.getCatalogPSCodeList()??><#assign categorCodeList = item.getCatalogPSCodeList() />categorCodeList:{type:'${categorCodeList.getCodeListType()}',tag:'${categorCodeList.getCodeName()}'},</#if>valueField:'${item.getValueField()?lower_case}',data:[]}),
</#if>
</#if>
<#-- 序列模型end -->