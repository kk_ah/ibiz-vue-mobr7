<#-- 绘图极坐标系的角度轴 -->
<#if item.render??>
${item.render.code}
<#else>
{
      min: 0,
      max: 360,
      interval: 30,
      startAngle: 45
      
}
</#if>