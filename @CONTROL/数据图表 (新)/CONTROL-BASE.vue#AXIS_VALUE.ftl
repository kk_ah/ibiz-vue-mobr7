<#-- 绘图Y轴 -->
<#if item.render??>
${item.render.code}
<#else>
{
      <#if item.getIndex()??>gridIndex:${item.getIndex()},</#if>
      position:<#if item.getPosition()?? && item.getPosition() =='bottom'>"bottom"<#else>"top"</#if>,
      type:<#if item.getType()?? &&  item.getType() == 'numeric'>'value'<#else>'${item.getType()}'</#if>,
      name:<#if item.getCaption()??>'${item.getCaption()}'</#if>,
      <#if item.getMinValue()??>min:${item.getMinValue()},</#if>
      <#if item.getMaxValue()??>max:${item.getMaxValue()},</#if>
      <#if item.getBaseOptionJOString()??>
      ${item.getBaseOptionJOString()}
      </#if>
}
</#if>