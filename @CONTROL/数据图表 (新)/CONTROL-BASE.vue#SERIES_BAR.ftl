<#-- 柱状图序列start -->
<#if item.render??>
${item.render.code}
<#else>
{
    id:'<#if item.getName()??>${item.getName()?lower_case}</#if>',
    name:'<#if item.getCaption()??>${item.getCaption()}</#if>',
    type:'bar',
    xAxisIndex:${item.getIndex()},
    yAxisIndex:${item.getIndex()},
    datasetIndex:${item.getIndex()},
    encode: {
        <#if item.getPSChartSeriesEncode()??><#assign chartSeriesEncode = item.getPSChartSeriesEncode() /></#if>
        x: [<#if chartSeriesEncode.getX()??><#list chartSeriesEncode.getX() as xValue>'${xValue?lower_case}'<#if xValue_has_next>,</#if></#list></#if>],      
        y: [<#if chartSeriesEncode.getY()??><#list chartSeriesEncode.getY() as yValue>'${yValue?lower_case}'<#if yValue_has_next>,</#if></#list></#if>]
    }<#if item.getBaseOptionJOString()??>,
    ${item.getBaseOptionJOString()}
    </#if>
}
</#if>
<#-- 柱状图序列end -->