<#-- 折线图序列start -->
<#if item.render??>
${item.render.code}
<#else>
{
    id:'<#if item.getName()??>${item.getName()?lower_case}</#if>',
    name:'<#if item.getCaption()??>${item.getCaption()}</#if>',
    type:'pie',
    datasetIndex:${item.getIndex()},
    <#compress><#if item.getLeft()?? && item.getLeft() != "">left:"${item.getLeft()}",</#if>
    <#if item.getTop()?? && item.getTop() != 0>top:"${item.getTop()}",</#if>
    <#if item.getBottom()?? && item.getBottom() != 0>bottom:"${item.getBottom()}",</#if>
    <#if item.getRight()?? && item.getRight() != "">right:"${item.getRight()}",</#if>
    <#if item.getWidth()?? && item.getWidth() != "">width:"${item.getWidth()}",</#if>
    <#if item.getHeight()?? && item.getHeight() != "">height:"${item.getHeight()}",</#if></#compress>
    seriesLayoutBy:"${item.getSeriesLayoutBy()}",
    encode:{
        <#if item.getPSChartSeriesEncode()??><#assign chartSeriesEncode = item.getPSChartSeriesEncode() /></#if>
        itemName:"<#if chartSeriesEncode.getCategory()??>${chartSeriesEncode.getCategory()?lower_case}<#else>${item.getCatalogField()?lower_case}</#if>",
        value:"<#if chartSeriesEncode.getValue()??>${chartSeriesEncode.getValue()?lower_case}<#else>${item.getValueField()?lower_case}</#if>"
    }<#if item.getBaseOptionJOString()??>,
    ${item.getBaseOptionJOString()}
    </#if>
}
</#if>
<#-- 折线图序列end -->