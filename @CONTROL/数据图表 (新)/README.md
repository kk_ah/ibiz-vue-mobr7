## ibiz图表视图相关文档（移动端）

### 一、图表（序列）类型

#### 	1、漏斗图

##### (1) 支持功能

- ###### 自定义分组：需配置代码表

- ###### 图例数据自定义切换

##### 	(2) 平台配置

- 选择序列类型为漏斗图、坐标系为：无坐标系、分类属性为该实体的分类属性、值属性为该实体的值属性

<div style = "display:flex; overflow:hidden; ">
	<img src="images/README/pz_funnel_1.png" ></img>
</div>

- 

##### 	(3) 界面UI

<div style = "display:flex; overflow:hidden; ">
	<img src="images/README/funnel.png" ></img>
</div>



#### 	2、条形图

##### 	(1)支持功能：

- ###### 自定义分组：季度分组、年分组、月分组（示例为季度分组）

- ###### 图例数据自定义切换

##### (2) 平台配置

- 建立坐标轴（x、y)数据字段为该轴展示的数据（见二坐标轴配置）

- 选择序列类型为条形图、x、y坐标轴（为第一步建立的坐标轴）、分类属性为该实体的分类属性、值属性为该实体的值属性、分组模式

  <div style = "display : flex ; overflow: hidden; ">
  	<img src="images/README/pz_bar_2.png" ></img>
  </div>

##### 	(3)界面UI

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/bar.png" ></img>
</div>


#### 	3、饼图

##### 	(1)支持功能

- ###### 自定义分组：季度分组、年分组、月分组（示例为季度分组）、代码表

- ###### 图例数据自定义切换

##### (2) 平台配置

- 建立坐标轴（x、y)数据字段为该轴展示的数据（见二坐标轴配置）

- 选择序列类型为饼图、x、y坐标轴（为第一步建立的坐标轴）、分类属性为该实体的分类属性、值属性为该实体的值属性

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/pz_bing_1.png" ></img>
</div>

##### 	(3)界面UI

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/bing.png" ></img>
</div>

#### 4、折线图

##### (1)支持功能：

- ###### 自定义分组：需配置代码表

- ###### 图例数据自定义切换

##### (2)平台配置

- 建立坐标轴（x、y)数据字段为该轴展示的数据（见二坐标轴配置）

- 选择序列类型为折线图、x、y坐标轴（为第一步建立的坐标轴）、分类属性为该实体的分类属性、值属性为该实体的值属性

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/pz_line_1.png" ></img>
</div>

##### (3)界面UI

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/line.png" ></img>
</div>

### 二、坐标轴

新建坐标轴：标题为该坐标轴的标题、坐标轴类型（分类、数值、时间）、坐标轴位置（坐标轴展示数据的位置）

数据字段（该坐标轴的数据属性）

<div style = "display : flex ; overflow: hidden; ">
	<img src="images/README/pz_bar_1.png" ></img>
</div>