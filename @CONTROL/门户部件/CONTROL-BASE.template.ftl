<template>
    <ion-row>
        <ion-list class='app-mob-portlet <#if ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>'>
            <#if ctrl.isShowTitleBar()>
            <ion-list-header class='app-mob-portlet__header'>${ctrl.getTitle()} </ion-list-header>
            </#if><#t> 
            <#if ctrl.getPortletType?? && ctrl.getPortletType()??>
                <#if ctrl.getPortletType() == 'VIEW' && ctrl.getPortletPSAppView?? && ctrl.getPortletPSAppView()??><#t>
                <#--  视图  -->
                <#assign refview = ctrl.getPortletPSAppView()><#t>
                <${srffilepath2(refview.getCodeName())} :_context="JSON.stringify(context)" :_viewparams="JSON.stringify(viewparams)" :viewDefaultUsage="false" ></${srffilepath2(refview.getCodeName())}>
                <#elseif ctrl.getPortletType() == 'ACTIONBAR'>
                <#--  操作栏  -->
                <app-actionbar :items="actionBarModelData" @itemClick="handleItemClick"></app-actionbar>
                <#elseif  ctrl.getPortletType() == 'HTML'>
                <#--  网页  -->
                <iframe src="<#if ctrl.getPageUrl()??>${ctrl.getPageUrl()}</#if>" style="height: <#if ctrl.getHeight() gt 0>${ctrl.getHeight()?c}<#else>400</#if>px;width: 100%;border-width: 1px;"></iframe>
                <#elseif  ctrl.getPortletType() == 'CUSTOM'>
                <#--  自定义  -->
                    <#if ctrl.render??>
                    ${ctrl.render.code}
                    <#else>
                    <div>无扩展插件</div>
                    </#if>
                <#elseif ctrl.getContentPSControl()??>
                <#--  图表，列表，菜单  -->
                ${P.getCtrlCode(ctrl.getContentPSControl(), 'CONTROL.html').code}
                <#else>
                <div>${ctrl.name}类型${ctrl.getPortletType()}不提供内容输出</div>
                </#if>
            </#if>
        </ion-list>
    </ion-row>
</template>