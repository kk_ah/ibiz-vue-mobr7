<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#assign import_block>
<#--  计数器服务  -->
<#if ctrl.getPSAppCounterRefs?? && ctrl.getPSAppCounterRefs()??>
<#list ctrl.getPSAppCounterRefs() as singleCounterRef>
<#if singleCounterRef.getPSAppCounter()??>
<#assign appCounter = singleCounterRef.getPSAppCounter()/>
import  ${srfclassname('${appCounter.getCodeName()}')}CounterService  from '@/app-core/counter/${srffilepath2(appCounter.getCodeName())}/${srffilepath2(appCounter.getCodeName())}-counter';
</#if>
</#list>
</#if>
</#assign>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>

<#if ctrl.getPSAppCounterRefs?? && ctrl.getPSAppCounterRefs()??>
<#assign counterRefs = ''/>
<#list ctrl.getPSAppCounterRefs() as singleCounterRef>
<#if singleCounterRef.getPSAppCounter()??>
<#assign appCounter = singleCounterRef.getPSAppCounter()/>
    <#assign counterRefs>${counterRefs}this.${srfclassname('${appCounter.getCodeName()}')}counterservice<#if singleCounterRef_has_next>,</#if></#assign>
    
    /**
     * ${srfclassname('${appCounter.getCodeName()}')}CounterService计数器服务对象
     *
     * @type {${srfclassname('${appCounter.getCodeName()}')}CounterService}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected ${srfclassname('${appCounter.getCodeName()}')}counterservice: ${srfclassname('${appCounter.getCodeName()}')}CounterService = new ${srfclassname('${appCounter.getCodeName()}')}CounterService();
</#if>
</#list>

    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */    
    protected counterServiceArray:Array<any> = [${counterRefs}];
</#if>

    <#if ctrl.getPortletType() == 'ACTIONBAR'>

    /**
     * 操作栏模型数据
     *
     * @protected
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected actionBarModelData: any[] = [
    <#if  ctrl.getPSAppViewLogics?? && ctrl.getPSAppViewLogics()??>
        <#list ctrl.getPSAppViewLogics() as appViewLogic>
        {
            viewlogicname: "${appViewLogic.name}",
            <#if appViewLogic.getPSAppViewUIAction?? && appViewLogic.getPSAppViewUIAction()??>
            <#assign viewUIAction = appViewLogic.getPSAppViewUIAction()/>
            <#if viewUIAction.getPSUIAction?? && viewUIAction.getPSUIAction()??>
            <#assign uiaction = viewUIAction.getPSUIAction() />
            actionName: "${uiaction.getCaption()}",
            <#if uiaction.getPSAppCounter?? && uiaction.getPSAppCounter()??>
            <#assign counter = uiaction.getPSAppCounter() />
            counterService:this.${srfclassname('${counter.getCodeName()}')}counterservice,
            <#if uiaction.getCounterId()??>counterId: "${uiaction.getCounterId()}",</#if>
            </#if>
            </#if>
            </#if>
        }<#if appViewLogic_has_next>,</#if>
        </#list>
    </#if>
    ];

    /**
     * 触发界面行为
     *
     * @protected
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected handleItemClick($event: any) {
    <#if  ctrl.getPSAppViewLogics?? && ctrl.getPSAppViewLogics()??>
        <#list ctrl.getPSAppViewLogics() as appViewLogic>
        if (Object.is($event, '${appViewLogic.name}')) {
            this.${appViewLogic.name}(null);
        }
        </#list>
    </#if>
    }
    </#if>

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    public getData(): any {
        return {};
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}
     */    
    protected afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((name: string) => {
                    this.viewState.next({ tag: name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}
     */
    protected afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>