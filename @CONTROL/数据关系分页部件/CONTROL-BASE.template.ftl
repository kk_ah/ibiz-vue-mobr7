<template>
<#if ctrl.render??>
    ${ctrl.render.code}
<#else>
    <div class = "drtab<#if ctrl.getPSSysCss?? && ctrl.getPSSysCss()??><#assign singleCss = ctrl.getPSSysCss()> ${singleCss.getCssName()}</#if>">
        <ion-tabs :animated='false' class='app-dr-tab' name='${ctrl.getCodeName()?lower_case}' @ionTabsDidChange="tabPanelClick($event)">
        <ion-tab-bar slot="top">
  <#list ctrl.getRootItem().getAllItems() as dritem>
            <ion-tab-button   <#if dritem_index == 0>v-if="isShowSlot"</#if> :index='${dritem_index?c}' name='${dritem.getId()?lower_case}' tab='${dritem.getId()?lower_case}' class='' :disabled='items[${dritem_index?c}].disabled' >
                <ion-label>${dritem.text}</ion-label>
            </ion-tab-button>
  </#list>
        </ion-tab-bar>
  <#list ctrl.getRootItem().getAllItems() as dritem>     
    <#if dritem.getId()?lower_case == 'form'>
            <ion-tab tab="${dritem.getId()?lower_case}">
                        <div class='main-data'>
                            <slot></slot>
                        </div>
            </ion-tab>
    <#else>
            <ion-tab tab="${dritem.getId()?lower_case}">
                        <component
                        v-if="this.selection && Object.is(this.selection.id, '${dritem.getId()?lower_case}') && this.selection.view && !Object.is(this.selection.view.viewname, '')"
                        :is="selection.view.viewname"
                        class="viewcontainer2"
                        :_viewparams="JSON.stringify(selection.data)"
                        :_context="JSON.stringify(selection.param)"
                        :viewDefaultUsage="false"
                        :key="this.$util.createUUID()">
                        </component>
            </ion-tab>
    </#if>
  </#list>
        </ion-tabs>
    </div>   
</#if> 
</template>