<#ibizinclude>../../@NAVPARAMS/FUNC/PUBLIC.vue.ftl</#ibizinclude>

<#ibizinclude>
./CONTROL-BASE.template.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_HEADER-BASE.vue.ftl
</#ibizinclude>
    /**
     * 是否显示插槽
     *
     * @type {string}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    @Prop({default:true}) public isShowSlot?: boolean;

    /**
     *  应用实体参数名称
     *
     * @type string
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    @Prop() public parentName!: string;

    /**
     *  表单数据
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    @Prop({default:{}}) public formData?:any;
    
    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getDatas(): any[] {
        return this.items;
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getData(): any {
        return this.selection;
    }

    /**
     * 数据选中项
     *
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public selection: any = {};

    /**
     * 父数据
     *
     * @public
     * @type {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public parentData: any = {};

    /**
     * 关系栏数据项
     *
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public items: any[] = [
        <#list ctrl.getRootItem().getAllItems() as dritem>
        {
            index: ${dritem_index?c}, 
            id: '${dritem.getId()?lower_case}',
            name: '${dritem.getId()?lower_case}', 
            text: '${dritem.text}', 
            disabled: false, 
        },
        </#list>
    ];

    /**
     * 关系栏数据项导航参数集合
     *
     * @type {any[]}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public navParamsArray:Array<any> = [
    <#if ctrl.getPSDEDRCtrlItems?? && ctrl.getPSDEDRCtrlItems()??>
    <#list ctrl.getPSDEDRCtrlItems() as appdeDrCtrlItem>
        {
            id:'${appdeDrCtrlItem.getName()?lower_case}',
            localContext:<#if appdeDrCtrlItem.getPSNavigateContexts?? && appdeDrCtrlItem.getPSNavigateContexts()??><@getNavigateContext appdeDrCtrlItem /><#else>null</#if>,
            localViewParam:<#if appdeDrCtrlItem.getPSNavigateParams?? && appdeDrCtrlItem.getPSNavigateParams()??><@getNavigateParams appdeDrCtrlItem /><#else>null</#if>
        }<#if appdeDrCtrlItem_has_next>,</#if>
    </#list>
    </#if>
    ];

    /**
     * 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public created(): void {
        this.afterCreated();
    }

    /**
     * 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public mounted(){
        if(this.items.length>0){
            this.isShowSlot?this.tabPanelClick({detail:{tab:this.items[0].name}},true):this.items.length>1?this.tabPanelClick({detail:{tab:this.items[1].name}},true):()=>{};
        }
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof ${srfclassname('${ctrl.codeName}')}Base
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is('state', action)) {
                    const state = !this.context.${ctrl.getPSAppDataEntity().getCodeName()?lower_case} ? true : false;
                    this.setItemDisabled(state);
                }
            });
        }
        this.$nextTick(() => {
            this.$emit('selectionchange', [this.items[0]]);
        });
    }

    /**
     * vue 生命周期
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
        <#if destroyed_block??>
        ${destroyed_block}
        </#if>
    }

    /**
     * 获取关系项
     *
     * @public
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getDRTabItem(arg: any = {}): any {
        let expmode = arg.nodetype.toUpperCase();
        if (!expmode) {
            expmode = '';
        }
        <#list ctrl.getPSAppViewRefs() as item>
        <#if (item.getName()?index_of("DRITEM:")==0)>
        <#assign refview = item.getRefPSAppView()>
        if (Object.is(expmode, '${item.getName()?substring(7)}')) {
            return {  
                viewname: '${srffilepath2(refview.codeName)}', 
                parentdatajo: <#if item.getParentDataJO()??>${item.getParentDataJO()}<#else>{},</#if>
			};
        }
        </#if>
        </#list>
        return undefined;
    }

    /**
     * 设置关系项状态
     *
     * @param {boolean} state
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public setItemDisabled(state: boolean): void {
        this.items.forEach((item: any) => {
            if (Object.is(item.name, 'form')) {
                item.disabled = false;
            } else {
                item.disabled = state;
            }
        });
    }

    /**
     * 获取数据项
     *
     * @public
     * @param {any} item
     * @returns {*}
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public getItem(item: any): any {
        const arr: any[] = this.items.filter((_item: any) => Object.is(_item.id, item.detail.tab));
        if (arr) {
            return arr[0];
        }
        return null;
    }

    /**
     * 初始化导航参数
     *
     * @param {*} drItem
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public initNavParam(drItem:any){
        let returnNavParam:any = {};
        if(drItem && drItem.id){
            let curDRItem:any = this.navParamsArray.find((item:any) =>{
                return Object.is(item.id,drItem.id);
            })
            if(curDRItem){
                let localContext:any = curDRItem.localContext;
                let localViewParam:any = curDRItem.localViewParam;
                const {context, param} = this.$viewTool.formatNavigateParam( localContext, localViewParam, this.context, this.viewparams, this.formData );
                returnNavParam.localContext = context;
                returnNavParam.localViewParam = param;
                return returnNavParam;
            }else{
                return null;
            }
        }
    }

    /**
     * 选中节点
     *
     * @param {*} $event
     * @memberof ${srfclassname('${ctrl.codeName}')}Base
     */
    public tabPanelClick($event: any,isMounted :boolean= false): void {
        const item = this.getItem($event);
        if (Object.is(item.id, this.selection.id)) {
            return;
        }
        if(!isMounted){
            this.$emit('selectionchange', [item]);
        }
        let localNavParam:any = this.initNavParam(item);
        const refview = this.getDRTabItem({ nodetype: item.id });
        this.selection = {};
        const _context: any = { ...JSON.parse(JSON.stringify(this.context)) };
        if(localNavParam && localNavParam.localContext){
            Object.assign(_context,localNavParam.localContext);
        }
        Object.assign(_context,{srfparentdename:this.parentName,srfparentkey:_context[this.parentName.toLowerCase()]});
        const _params: any = {};
        if(localNavParam && localNavParam.localViewParam){
            Object.assign(_params,localNavParam.localViewParam);
        }
        if (refview && refview.parentdatajo) {
            Object.assign(_context, refview.parentdatajo);
            Object.assign(this.selection, { view: { viewname: refview.viewname }, data: _context, param: _params });
        }
        Object.assign(this.selection, item);
    }

<#ibizinclude>
../@MACRO/CONTROL/CONTROL_BOTTOM-BASE.vue.ftl
</#ibizinclude>

<#ibizinclude>
../@MACRO/CONTROL/CONTROL-BASE.style.ftl
</#ibizinclude>